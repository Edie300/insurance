import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { CasketStatus } from "@totum-insurance/common";

interface ChairAttrs {
  images: { }[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface ChairModel extends mongoose.Model<ChairDoc> {
  build(attrs: ChairAttrs): ChairDoc;
}

// An interface that describes the properties
// that a User Document has
export interface ChairDoc extends mongoose.Document {
  id: string;
  images: {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  version: number;
}

const chairSchema = new mongoose.Schema(
  {
    images: [],
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CasketStatus),
      default: CasketStatus.Unpublished,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

chairSchema.set("versionKey", "version");
chairSchema.plugin(updateIfCurrentPlugin);

chairSchema.statics.build = (attrs: ChairAttrs) => {
  return new chair(attrs);
};

const chair = mongoose.model<ChairDoc, ChairModel>("chair", chairSchema);

export { chair };

import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { CasketStatus } from "@totum-insurance/common";

interface HearseAttrs {
  images: { }[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface HearseModel extends mongoose.Model<HearseDoc> {
  build(attrs: HearseAttrs): HearseDoc;
}

// An interface that describes the properties
// that a User Document has
export interface HearseDoc extends mongoose.Document {
  id: string;
  images: {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  version: number;
}

const hearseSchema = new mongoose.Schema(
  {
    images: [],
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CasketStatus),
      default: CasketStatus.Unpublished,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

hearseSchema.set("versionKey", "version");
hearseSchema.plugin(updateIfCurrentPlugin);

hearseSchema.statics.build = (attrs: HearseAttrs) => {
  return new Hearse(attrs);
};

const Hearse = mongoose.model<HearseDoc, HearseModel>("Hearse", hearseSchema);

export { Hearse };

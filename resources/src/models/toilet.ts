import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { CasketStatus } from "@totum-insurance/common";

interface ToiletAttrs {
  images: { }[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface ToiletModel extends mongoose.Model<ToiletDoc> {
  build(attrs: ToiletAttrs): ToiletDoc;
}

// An interface that describes the properties
// that a User Document has
export interface ToiletDoc extends mongoose.Document {
  id: string;
  images: {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  version: number;
}

const toiletSchema = new mongoose.Schema(
  {
    images: [],
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CasketStatus),
      default: CasketStatus.Unpublished,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

toiletSchema.set("versionKey", "version");
toiletSchema.plugin(updateIfCurrentPlugin);

toiletSchema.statics.build = (attrs: ToiletAttrs) => {
  return new Toilet(attrs);
};

const Toilet = mongoose.model<ToiletDoc, ToiletModel>("Toilet", toiletSchema);

export { Toilet };

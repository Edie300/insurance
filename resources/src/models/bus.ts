import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { CasketStatus } from "@totum-insurance/common";

interface BusAttrs {
  images: { }[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface BusModel extends mongoose.Model<BusDoc> {
  build(attrs: BusAttrs): BusDoc;
}

// An interface that describes the properties
// that a User Document has
export interface BusDoc extends mongoose.Document {
  id: string;
  images: {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  version: number;
}

const busSchema = new mongoose.Schema(
  {
    images: [],
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CasketStatus),
      default: CasketStatus.Unpublished,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

busSchema.set("versionKey", "version");
busSchema.plugin(updateIfCurrentPlugin);

busSchema.statics.build = (attrs: BusAttrs) => {
  return new Bus(attrs);
};

const Bus = mongoose.model<BusDoc, BusModel>("Bus", busSchema);

export { Bus };

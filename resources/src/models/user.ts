import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { UserStatus } from "@totum-insurance/common";

import { CompanyDoc } from "./company";
import { BranchDoc } from "./branch";

// An interface that describes the properties
// that are requried to create a new User
interface UserAttrs {
  id: string;
  name: string;
  surname: string;
  employee_id: string;
  gender: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  date_of_birth: Date;
  phone: string;
  address: string;
  email: string;
  department: string;
  role: string;
  permissions_accounts: string[];
  permissions_packages: string[];
  permissions_claims: string[];
  permissions_users: string[];
  status: UserStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
  id: string;
  name: string;
  surname: string;
  employee_id: string;
  gender: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  date_of_birth: Date;
  phone: string;
  address: string;
  email: string;
  department: string;
  role: string;
  permissions_accounts: string[];
  permissions_packages: string[];
  permissions_claims: string[];
  permissions_users: string[];
  status: UserStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    employee_id: {
      type: String,
    },
    gender: {
      type: String,
      enum: ["male", "female"],
      required: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
    },
    title: {
      type: String,
    },
    national_id: {
      type: String,
    },

    passport: {
      type: String,
    },

    drivers_licence: {
      type: String,
    },

    phone: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    department: {
      type: String,
      required: true,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    role: {
      type: String,
    },
    permissions_accounts: [String],
    permissions_packages: [String],
    permissions_claims: [String],
    permissions_users: [String],

    status: {
      type: String,
      enum: Object.values(UserStatus),
      default: UserStatus.Created,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

userSchema.set("versionKey", "version");
userSchema.plugin(updateIfCurrentPlugin);

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User({
    _id: attrs.id,
    name: attrs.name,
    surname: attrs.surname,
    employee_id: attrs.employee_id,
    gender: attrs.gender,
    title: attrs.title,
    date_of_birth: attrs.date_of_birth,
    national_id: attrs.national_id,
    passport: attrs.passport,
    drivers_licence: attrs.drivers_licence,
    phone: attrs.phone,
    address: attrs.address,
    email: attrs.email,
    department: attrs.department,
    role: attrs.role,
    permissions_accounts: attrs.permissions_accounts,
    permissions_packages: attrs.permissions_packages,
    permissions_claims: attrs.permissions_claims,
    permissions_users: attrs.permissions_users,
    status: attrs.status,
    company: attrs.company,
    branch: attrs.branch,
  });
};

const User = mongoose.model<UserDoc, UserModel>("User", userSchema);

export { User };

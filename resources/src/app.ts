import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import cors from "cors";
import { errorHandler, NotFoundError } from "@totum-insurance/common";

import { createCasketRouter } from "./routes/create-casket";
import { deleteCasketRouter } from "./routes/delete-casket";
import { indexCasketRouter } from "./routes/index-casket";
import { publishCasketrRouter } from "./routes/publish-casket";
import { removeCasketImageRouter } from "./routes/remove-casket-image";
import { showCasketRouter } from "./routes/show-casket";
import { showCompanyCasketsRouter } from "./routes/show-company-caskets";
import { unpublishCasketRouter } from "./routes/unpublish-casket";
import { updateCasketRouter } from "./routes/update-casket";
import { uploadCasketImageRouter } from "./routes/upload-casket-image";



const app = express();
// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set("trust proxy", true);
app.use(json());
app.use(express.json({ limit: "5mb" }));
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);

app.use(createCasketRouter);
app.use(deleteCasketRouter);
app.use(indexCasketRouter);
app.use(publishCasketrRouter);
app.use(removeCasketImageRouter);
app.use(showCasketRouter);
app.use(showCompanyCasketsRouter);
app.use(unpublishCasketRouter);
app.use(updateCasketRouter);
app.use(uploadCasketImageRouter);


app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

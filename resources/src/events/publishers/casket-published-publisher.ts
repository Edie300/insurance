import { Subjects, Publisher, CasketPublishedEvent } from '@totum-insurance/common';

export class CasketPublishedPublisher extends Publisher<CasketPublishedEvent> {
  subject: Subjects.CasketPublished = Subjects.CasketPublished;
}

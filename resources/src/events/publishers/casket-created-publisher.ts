import { Subjects, Publisher, CasketCreatedEvent } from '@totum-insurance/common';

export class CasketCreatedPublisher extends Publisher<CasketCreatedEvent> {
  subject: Subjects.CasketCreated = Subjects.CasketCreated;
}

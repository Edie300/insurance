import { Subjects, Publisher, CasketUnpublishedEvent } from '@totum-insurance/common';

export class CasketUnPublishedPublisher extends Publisher<CasketUnpublishedEvent> {
  subject: Subjects.CasketUnpublished = Subjects.CasketUnpublished;
}

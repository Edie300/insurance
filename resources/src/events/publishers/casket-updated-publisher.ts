import { Subjects, Publisher, CasketUpdatedEvent } from '@totum-insurance/common';

export class CasketUpdatedPublisher extends Publisher<CasketUpdatedEvent> {
  subject: Subjects.CasketUpdated = Subjects.CasketUpdated;
}

import { Subjects, Publisher, CasketDeletedEvent } from '@totum-insurance/common';

export class CasketDeletedPublisher extends Publisher<CasketDeletedEvent> {
  subject: Subjects.CasketDeleted = Subjects.CasketDeleted;
}

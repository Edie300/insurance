import { Message } from 'node-nats-streaming';
import { Subjects, Listener, CompanyCreatedEvent } from '@totum-insurance/common';
import { Company } from '../../models/company';
import { queueGroupName } from './queue-group-name';

export class CompanyCreatedListener extends Listener<CompanyCreatedEvent> {
  subject: Subjects.CompanyCreated = Subjects.CompanyCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: CompanyCreatedEvent['data'], msg: Message) {

    console.log("company-created-listener::", data)
    const {  
      id,  
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      insurance,
      type,
      status
     } = data;

    const company = Company.build({
      id,  
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      insurance,
      type,
      status
    });
    await company.save();

    msg.ack();
  }
}

import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { Casket } from "../models/casket";

const router = express.Router();

router.get(
  "/api/caskets/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const casket = await Casket.findById(req.params.id)
      .populate("company")
      .populate("created_by");

    if (!casket) {
      throw new NotFoundError();
    }

    res.send(casket);
  }
);

export { router as showCasketRouter };

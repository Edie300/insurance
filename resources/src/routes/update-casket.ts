import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { Casket } from "../models/casket";
import { CasketUpdatedPublisher } from "../events/publishers/casket-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put(
  "/api/caskets/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const casket = await Casket.findById(req.params.id);

    if (!casket) {
      throw new NotFoundError();
    }

    casket.set({
      images: req.body.images,
      name: req.body.name,
      description: req.body.description,
      quantity: req.body.quantity,
    });

    await casket.save();

    new CasketUpdatedPublisher(natsWrapper.client).publish({
      id: casket.id,
      images: casket.images,
      name: casket.name,
      description: casket.description,
      quantity: casket.quantity,
      created_at: casket.created_at,
      created_by: casket.created_by,
      status: casket.status,
      company: casket.company,
      version: casket.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.send(casket);
  }
);

export { router as updateCasketRouter };

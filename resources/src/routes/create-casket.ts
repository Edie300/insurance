import mongoose, {Types, ObjectId} from "mongoose";
import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  CasketStatus,
} from "@totum-insurance/common";

import { Casket } from "../models/casket";
import { Company } from "../models/company";
import { User } from "../models/user";

import { CasketCreatedPublisher } from "../events/publishers/casket-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/caskets/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const { images, name, description, quantity } = req.body;

    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);
    if (!getCompany) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;

    const casket = Casket.build({
      images,
      name,
      description,
      status: CasketStatus.Unpublished,
      company,
      quantity,
      created_by,
      created_at,
    });

    await casket.save();

    new CasketCreatedPublisher(natsWrapper.client).publish({
      id: casket.id,
      images: casket.images,
      name: casket.name,
      description: casket.description,
      quantity: casket.quantity,
      status: casket.status,
      company: casket.company,
      created_by: casket.created_by,
      created_at: casket.created_at,
      version: casket.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(casket);
  }
);

export { router as createCasketRouter };

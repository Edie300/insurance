import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  CasketStatus
} from '@totum-insurance/common';

import { Casket } from '../models/casket';
import { CasketPublishedPublisher } from "../events/publishers/casket-published-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/caskets/publish/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const casket = await Casket.findById(id);

    if (!casket) {
      throw new NotFoundError();
    }

    casket.status = CasketStatus.Published;
    await casket.save();

    new CasketPublishedPublisher(natsWrapper.client).publish({
      id: casket.id,
      status: casket.status,
      version: casket.version,
            user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(204).send(casket);
  }
);

export { router as publishCasketrRouter };

import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { Casket } from "../models/casket";

const router = express.Router();

router.get(
  "/api/caskets/companies/:companyId",
  requireAuth,
  async (req: Request, res: Response) => {
    const caskets = await Casket.find({company: req.params.companyId})
      .populate("company")
      .populate("created_by");


    res.send(caskets);
  }
);

export { router as showCompanyCasketsRouter };

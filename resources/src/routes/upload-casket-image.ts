import express, { Request, Response } from "express";
import AWS from "aws-sdk";
import multer from "multer";
import { nanoid } from "nanoid";
import {
  BadRequestError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";

import { User } from "../models/user";

const router = express.Router();

const awsConfig = {
  accessKeyId: "AKIAU262SNIMGNH3M2OD",
  secretAccessKey: "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",
  region: "us-east-1",
  apiVersion: "2010-12-01",
};

const S3 = new AWS.S3(awsConfig);

const upload = multer();

router.post(
  "/api/caskets/upload_image",
  requireAuth,
  upload.single("image"),
  async (req: Request, res: Response) => {
    console.log("image====>", req.file);

    if (!req.file) {
      throw new BadRequestError("no file");
    }


    const type = req.file.mimetype.split("/")[1];
    console.log("file type====>", type);

      // image params
      const params = {
        Bucket: "totum-insurance",
        Key: `${nanoid()}.${type}`,
        Body: req.file.buffer,
        ACL: "public-read",
        ContentEncoding: "7bit",
        ContentType: `image/${type}`,
      };

      // upload to s3
      S3.upload(params, (err:any, data:any) => {
        if (err) {
          console.log(err);
          return res.sendStatus(400);
        }
        console.log(data);
        res.send(data);
      });
  }
);

export { router as uploadCasketImageRouter };

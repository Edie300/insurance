import mongoose from "mongoose";

import { app } from "./app";
import { natsWrapper } from "./nats-wrapper";
import { BranchCreatedListener } from "./events/listeners/branch-created-listener";
import { BranchUpdatedListener } from "./events/listeners/branch-updated-listener";
import { CompanyCreatedListener } from "./events/listeners/company-created-listener";
import { CompanyUpdatedListener } from "./events/listeners/company-updated-listener";
import { UserCreatedListener } from "./events/listeners/user-created-listener";

const start = async () => {
  // if (!process.env.JWT_KEY) {
  //   throw new Error('JWT_KEY must be defined');
  // }
  if (!process.env.NATS_URL) {
    throw new Error("NATS_URL must be defined.");
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error("NATS_CLUSTER_ID must be defined.");
  }
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error("NATS_CLIENT_ID must be defined.");
  }
  if (!process.env.MONGO_URI) {
    throw new Error("MONGO_URI must be defined");
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );

    // Following lines are required for gracefully shutting down client
    natsWrapper.client.on("close", () => {
      console.log("NATS connection closed");
      process.exit();
    });
    process.on("SIGINT", () => natsWrapper.client.close());
    process.on("SIGTERM", () => natsWrapper.client.close());

    new BranchCreatedListener(natsWrapper.client).listen();
    new BranchUpdatedListener(natsWrapper.client).listen();
    new CompanyCreatedListener(natsWrapper.client).listen();
    new CompanyUpdatedListener(natsWrapper.client).listen();
    new UserCreatedListener(natsWrapper.client).listen();

    await mongoose.connect(process.env.MONGO_URI);
    console.log("Connected to MongoDb");
  } catch (err) {
    console.error(err);
  }

  app.listen(7000, () => {
    console.log("Listening on port 7000!!!!!!!!");
  });
};

start();

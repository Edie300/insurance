import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { CasketStatus } from "@totum-insurance/common";

interface CasketAttrs {
  id: string;
  images:  {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface CasketModel extends mongoose.Model<CasketDoc> {
  build(attrs: CasketAttrs): CasketDoc;
  findByEvent(event: {
    id: string;
    version: number;
  }): Promise<CasketDoc | null>;
}

// An interface that describes the properties
// that a User Document has
export interface CasketDoc extends mongoose.Document {
  id: string;
  images:  {}[];
  name: string;
  description: string;
  quantity: number;
  status: CasketStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  version: number;
}

const casketSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CasketStatus),
      default: CasketStatus.Unpublished,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

casketSchema.set("versionKey", "version");
casketSchema.plugin(updateIfCurrentPlugin);

casketSchema.statics.findByEvent = (event: { id: string; version: number }) => {
  return Casket.findOne({
    _id: event.id,
    version: event.version - 1,
  });
};

casketSchema.statics.build = (attrs: CasketAttrs) => {
  return new Casket({
    _id: attrs.id,
    images: attrs.images,
    name: attrs.name,
    description: attrs.description,
    quantity: attrs.quantity,
    status: attrs.status,
    created_at: attrs.created_at,
    created_by: attrs.created_by,
    company: attrs.company,
  });
};

const Casket = mongoose.model<CasketDoc, CasketModel>("Casket", casketSchema);

export { Casket };

import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { AccountMedStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface AcountMedAttrs {
  id: string;
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountMedStatus;
  package_med: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
}

// An interface that describes the properties

interface AccountMedModel extends mongoose.Model<AccountMedDoc> {
  build(attrs: AcountMedAttrs): AccountMedDoc;
  findByEvent(event: {
    id: string;
    version: number;
  }): Promise<AccountMedDoc | null>;
}

interface AccountMedDoc extends mongoose.Document {
  id: string;
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountMedStatus;
  package_med: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
  version: number;
}

const accountMedSchema = new mongoose.Schema(
  {
    attachments: [],
    policy_id: {
      type: String,
      required: true,
    },

    balance: {
      type: Number,
      default: 0,
    },
    premium: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(AccountMedStatus),
      default: AccountMedStatus.Pending,
    },
    payment_status: {
      type: Number,
      default: 0,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    package_med: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageMed",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

accountMedSchema.set("versionKey", "version");
accountMedSchema.plugin(updateIfCurrentPlugin);

accountMedSchema.statics.findByEvent = (event: {
  id: string;
  version: number;
}) => {
  return AccountMed.findOne({
    _id: event.id,
    version: event.version - 1,
  });
};

accountMedSchema.statics.build = (attrs: AcountMedAttrs) => {
  return new AccountMed({
    _id: attrs.id,
    attachments: attrs.attachments,
    policy_id: attrs.policy_id,
    payment_status: attrs.payment_status,
    balance: attrs.balance,
    premium: attrs.premium,
    status: attrs.status,
    package_med: attrs.package_med,
    account_main: attrs.account_main,
    company: attrs.company,
    branch: attrs.branch,
    created_at: attrs.created_at,
    created_by: attrs.created_by,
  });
};

const AccountMed = mongoose.model<AccountMedDoc, AccountMedModel>(
  "AccountMed",
  accountMedSchema
);

export { AccountMed };

import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { PackageMedStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface PackageMedAttrs {
  id: string;
  attachments: {}[];
  name: string;
  description: string;
  annual_global_limit: number;
  general_practitioners: number;
  number_of_initial_visits: number;
  medical_specialist: number;
  hospitalization_limit: number;
  waiting_time: number;
  claim_processing_time: number;
  private_hospitalization: number;
  private_hospitalization_CV19: number;
  public_hospitalization: number;
  ambulance_services: boolean;
  air_evacuation: boolean;
  number_of_dependents: number;
  age_limit_max: number;
  age_limit_min: number;
  premium_add: number;
  drugs_3ben: number;
  drugs_abv_3ben: number;
  chronic_drug_limit: number;
  drugs_cv19: number;
  drugs_acute: number;
  dental: number;
  optical: number;
  psychiatric: number;
  physiotherapy: number;
  prosthetics: number;
  pathology: number;
  specialized_radiology: number;
  blood_transfusion: number;
  maternity: number;
  family_planning: number;
  infertility: number;
  hearing_aids: number;
  hearing_scan: number;
  funeral_cashback: number;
  premium_waver: number;
  wellness: number;
  tandCs: string;
  policy_maturity: number;
  currency: string;
  payment_type: string;
  service_area: string;
  child_max_age: number;
  child_max_age_school: number;
  waiting_periods: { period: number; benefits: string[] }[];
  family_rate: number;
  dependent_rate: number;
  individual_rates: { min_age: number; max_age: number; rate: number }[];
  child_rate: number;
  unbilled_dependents: string[];
  created_at: Date;
  created_by: Types.ObjectId;
  status: PackageMedStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface PackageMedModel extends mongoose.Model<PackageMedDoc> {
  build(attrs: PackageMedAttrs): PackageMedDoc;
  findByEvent(event: {
    id: string;
    version: number;
  }): Promise<PackageMedDoc | null>;
}

// An interface that describes the properties
// that a User Document has
interface PackageMedDoc extends mongoose.Document {
  attachments: {}[];
  name: string;
  description: string;
  annual_global_limit: number;
  general_practitioners: number;
  number_of_initial_visits: number;
  medical_specialist: number;
  hospitalization_limit: number;
  waiting_time: number;
  claim_processing_time: number;
  private_hospitalization: number;
  private_hospitalization_CV19: number;
  public_hospitalization: number;
  ambulance_services: boolean;
  air_evacuation: boolean;
  number_of_dependents: number;
  age_limit_max: number;
  age_limit_min: number;
  premium_add: number;
  drugs_3ben: number;
  drugs_abv_3ben: number;
  chronic_drug_limit: number;
  drugs_cv19: number;
  drugs_acute: number;
  dental: number;
  optical: number;
  psychiatric: number;
  physiotherapy: number;
  prosthetics: number;
  pathology: number;
  specialized_radiology: number;
  blood_transfusion: number;
  maternity: number;
  family_planning: number;
  infertility: number;
  hearing_aids: number;
  hearing_scan: number;
  funeral_cashback: number;
  premium_waver: number;
  wellness: number;
  tandCs: string;
  policy_maturity: number;
  currency: string;
  payment_type: string;
  service_area: string;
  child_max_age: number;
  child_max_age_school: number;
  waiting_periods: { period: number; benefits: string[] }[];
  family_rate: number;
  dependent_rate: number;
  individual_rates: { min_age: number; max_age: number; rate: number }[];
  child_rate: number;
  unbilled_dependents: string[];
  created_at: Date;
  created_by: Types.ObjectId;
  status: PackageMedStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const packageMedSchema = new mongoose.Schema(
  {
    attachments: [],

    name: {
      type: String,
      required: [true, "Please add a name"],
      trim: true,
      maxlength: [50, "Name can not be more than 50 characters"],
    },
    description: {
      type: String,
      required: [true, "Please add a description"],
      maxlength: [700, "Description can not be more than 700 characters"],
    },
    annual_global_limit: { type: Number },
    general_practitioners: { type: Number },
    number_of_initial_visits: { type: Number },
    medical_specialist: { type: Number },
    hospitalization_limit: { type: Number },
    waiting_time: { type: Number },
    claim_processing_time: { type: Number },
    private_hospitalization: { type: Number },
    private_hospitalization_CV19: { type: Number },
    public_hospitalization: { type: Number },
    ambulance_services: { type: Boolean, default: false },
    air_evacuation: { type: Boolean, default: false },
    number_of_dependents: { type: Number },
    age_limit_max: { type: Number },
    age_limit_min: { type: Number },
    premium_add: { type: Number },
    drugs_3ben: { type: Number },
    drugs_abv_3ben: { type: Number },
    chronic_drug_limit: { type: Number },
    drugs_cv19: { type: Number },
    drugs_acute: { type: Number },
    dental: { type: Number },
    optical: { type: Number },
    psychiatric: { type: Number },
    physiotherapy: { type: Number },
    prosthetics: { type: Number },
    pathology: { type: Number },
    specialized_radiology: { type: Number },
    blood_transfusion: { type: Number },
    maternity: { type: Number },
    family_planning: { type: Number },
    infertility: { type: Number },
    hearing_aids: { type: Number },
    hearing_scan: { type: Number },
    funeral_cashback: { type: Number },
    premium_waver: { type: Number },
    wellness: { type: Number },
    tandCs: { type: String },
    policy_maturity: { type: Number },
    service_area: { type: String },
    child_max_age: { type: Number },
    child_max_age_school: { type: Number },
    waiting_periods: [
      {
        period: { type: Number },
        benefits: [],
      },
    ],
    family_rate: { type: Number },
    dependent_rate: { type: Number },
    individual_rates: [
      {
        min_age: { type: Number },
        max_age: { type: Number },
        rate: { type: Number },
      },
    ],
    child_rate: { type: Number },
    unbilled_dependents: [],
    currency: {
      type: String,
      enum: ["ZW", "US", "ZAR"],
      required: [true, "Please choose currency"],
    },
    payment_type: {
      type: String,
      enum: ["weekly", "yearly", "monthly", "quarterly"],
      default: "monthly",
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(PackageMedStatus),
      default: PackageMedStatus.Unpublished,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

packageMedSchema.set("versionKey", "version");
packageMedSchema.plugin(updateIfCurrentPlugin);

packageMedSchema.statics.findByEvent = (event: { id: string; version: number }) => {
  return PackageMed.findOne({
    _id: event.id,
    version: event.version - 1,
  });
};

packageMedSchema.statics.build = (attrs: PackageMedAttrs) => {
  return new PackageMed({
    _id: attrs.id,
    attachments: attrs.attachments,
    name: attrs.name,
    description: attrs.description,
    annual_global_limit: attrs.annual_global_limit,
    general_practitioners: attrs.general_practitioners,
    number_of_initial_visits: attrs.number_of_initial_visits,
    medical_specialist: attrs.medical_specialist,
    hospitalization_limit: attrs.hospitalization_limit,
    waiting_time: attrs.waiting_time,
    claim_processing_time: attrs.claim_processing_time,
    private_hospitalization: attrs.private_hospitalization,
    private_hospitalization_CV19: attrs.private_hospitalization_CV19,
    public_hospitalization: attrs.public_hospitalization,
    ambulance_services: attrs.ambulance_services,
    air_evacuation: attrs.air_evacuation,
    number_of_dependents: attrs.number_of_dependents,
    age_limit_max: attrs.age_limit_max,
    age_limit_min: attrs.age_limit_min,
    premium_add: attrs.premium_add,
    drugs_3ben: attrs.drugs_3ben,
    drugs_abv_3ben: attrs.drugs_abv_3ben,
    chronic_drug_limit: attrs.chronic_drug_limit,
    drugs_cv19: attrs.drugs_cv19,
    drugs_acute: attrs.drugs_acute,
    dental: attrs.dental,
    optical: attrs.optical,
    psychiatric: attrs.psychiatric,
    physiotherapy: attrs.physiotherapy,
    prosthetics: attrs.prosthetics,
    pathology: attrs.pathology,
    specialized_radiology: attrs.specialized_radiology,
    blood_transfusion: attrs.blood_transfusion,
    maternity: attrs.maternity,
    family_planning: attrs.family_planning,
    infertility: attrs.infertility,
    hearing_aids: attrs.hearing_aids,
    hearing_scan: attrs.hearing_scan,
    funeral_cashback: attrs.funeral_cashback,
    premium_waver: attrs.premium_waver,
    wellness: attrs.wellness,
    tandCs: attrs.tandCs,
    policy_maturity: attrs.policy_maturity,
    service_area: attrs.service_area,
    child_max_age: attrs.child_max_age,
    child_max_age_school: attrs.child_max_age_school,
    waiting_periods: attrs.waiting_periods,
    family_rate:attrs.family_rate,
    dependent_rate:attrs.dependent_rate,
    individual_rates: attrs.individual_rates,
    child_rate: attrs.child_rate,
    unbilled_dependents: attrs.unbilled_dependents,
    currency: attrs.currency,
    payment_type: attrs.payment_type,
    status: attrs.status,
    created_by: attrs.created_by,
    created_at: attrs.created_at,
    company: attrs.company,
    branch: attrs.branch,
  });
};

const PackageMed = mongoose.model<PackageMedDoc, PackageMedModel>(
  "PackageMed",
  packageMedSchema
);

export { PackageMed };

import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import cors from "cors";
import { errorHandler, NotFoundError } from '@totum-insurance/common';

import { approveFunClaimRouter } from './routes/approve-fun-claim';
import { deleteFunClaimRouter} from './routes/delete-fun-claim';
import { indexFunClaimRouter } from './routes/index-fun-claim';
import { createFunClaimRouter } from './routes/create-fun-claim';
import { rejectFunClaimRouter } from './routes/reject-fun-claim';
import { showAccountFunClaimsRouter } from './routes/show-account-fun-claims';
import { showCompanyFunClaimsRouter } from './routes/show-company-fun-claims';
import { showFunClaimRouter } from './routes/show-fun-claim';
import { updateFunProductRouter } from './routes/update-fun-claim';
import { approveMedClaimRouter } from './routes/approve-med-claim';
import { deleteMedClaimRouter} from './routes/delete-med-claim';
import { indexMedClaimRouter } from './routes/index-med-claim';
import { createMedClaimRouter } from './routes/create-med-claim';
import { rejectMedClaimRouter } from './routes/reject-med-claim';
import { showMedClaimRouter } from './routes/show-med-claim';
import { updateMedProductRouter } from './routes/update-med-claim';

const app = express();

// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
);

app.use(approveFunClaimRouter);
app.use(deleteFunClaimRouter);
app.use(indexFunClaimRouter);
app.use(createFunClaimRouter);
app.use(rejectFunClaimRouter);
app.use(showAccountFunClaimsRouter);
app.use(showCompanyFunClaimsRouter);
app.use(showFunClaimRouter);
app.use(updateFunProductRouter);
app.use(approveMedClaimRouter);
app.use(deleteMedClaimRouter);
app.use(indexMedClaimRouter);
app.use(createMedClaimRouter);
app.use(rejectMedClaimRouter);
app.use(showMedClaimRouter);
app.use(updateMedProductRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

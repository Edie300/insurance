import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus,
} from "@totum-insurance/common";

import { ClaimMed } from "../models/claim-medical";
import { ClaimMedRejectedPublisher } from "../events/publishers/claim-med-rejected-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch(
  "/api/claims_medical/reject/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const claimMed = await ClaimMed.findById(id);

    if (!claimMed) {
      throw new NotFoundError();
    }

    claimMed.status = ClaimFunStatus.Rejected;
    claimMed.rejected_by = req.currentUser!.id;

    await claimMed.save();

    new ClaimMedRejectedPublisher(natsWrapper.client).publish({
      id: claimMed.id,
      status: claimMed.status,
      rejected_by: claimMed.rejected_by,
      version: claimMed.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.status(204).send(claimMed);
  }
);

export { router as rejectMedClaimRouter };

import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { ClaimFun } from "../models/claim-funeral";

const router = express.Router();

router.get(
  "/api/claims_funeral/account_fun/:accountId",
  requireAuth,
  async (req: Request, res: Response) => {
    const claimsFun = await ClaimFun.find({ account_fun: req.params.accountId })
    .populate("account_main")
      .populate("account_fun")
      .populate("package_fun")
      .populate("company")
      .populate("branch")
      .populate("created_by");

    res.send(claimsFun);
  }
);

export { router as showAccountFunClaimsRouter };

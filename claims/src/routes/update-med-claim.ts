import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { ClaimMed } from "../models/claim-medical";

import { ClaimMedUpdatedPublisher  } from "../events/publishers/claim-med-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/claims_medical/:id",  requireAuth, async (req: Request, res: Response) => {
  const claimMed = await ClaimMed.findById(req.params.id);

  if (!claimMed) {
    throw new NotFoundError();
  }

  claimMed.set({
    attachments: req.body.attachments,
    policy_id: req.body.policy_id,
    claim_id: req.body.claim_id,
    patient_type: req.body.patient_type,
    patient_name: req.body.patient_name,
    patient_surname: req.body.patient_surname,
    patient_national_id: req.body.patient_national_id,
    patient_passport: req.body.patient_passport,
    patient_gender: req.body.patient_gender,
    patient_date_of_birth: req.body.patient_date_of_birth, 
    patient_address: req.body.patient_address,
    patient_phone: req.body.patient_phone,
    patient_email: req.body.patient_email,
    patient_relationship: req.body.patient_relationship,
    patient_suffix: req.body.patient_suffix,
    patient_staff: req.body.patient_staff,
    provider_namas_peyee_no: req.body.provider_namas_peyee_no,
    provider_date_claim_closed: req.body.provider_date_claim_closed,
    provider_referring_practitioner: req.body.provider_referring_practitioner,
    provider_anaesthetist: req.body.provider_anaesthetist,
    provider_surgical_assistant: req.body.provider_surgical_assistant,
    claiment_name: req.body.claiment_name,
    claiment_surname: req.body.claiment_surname,
    claiment_title: req.body.claiment_title,
    claiment_dob: req.body.claiment_dob,
    claiment_email: req.body.claiment_email,
    claiment_address: req.body.claiment_address,
    claiment_cell: req.body.claiment_cell,
    claiment_national_id: req.body.claiment_national_id,
    claiment_passport: req.body.claiment_passport,
    claiment_relationship: req.body.claiment_relationship,
    bank_account_holder: req.body.bank_account_holder,
    bank_account_number: req.body.bank_account_number,
    bank_name: req.body.bank_name,
    bank_branch_code: req.body.bank_branch_code,
    bank_account_type: req.body.bank_account_type,
    created_by: req.body.created_by,
    approved_by: req.body.approved_by,
    rejected_by: req.body.rejected_by,
    status: req.body.status,
    account: req.body.account,
    package_med: req.body.package_med,
    company: req.body.company,
    branch: req.body.branch,
  });

  await claimMed.save();

  // new ClaimMedUpdatedPublisher(natsWrapper.client).publish({
  //   id: claimMed.id,
  //   attachments: claimMed.attachments,
  //   claim_id: claimMed.claim_id,
  //   claim_type: claimMed.claim_type,
  //   patient_type: claimMed.patient_type,
  //   patient_name: claimMed.patient_name,
  //   patient_surname: claimMed.patient_surname,
  //   patient_national_id: claimMed.patient_national_id,
  //   patient_passport: claimMed.patient_passport,
  //   patient_gender: claimMed.patient_gender,
  //   patient_date_of_birth: claimMed.patient_date_of_birth,
  //   patient_address: claimMed.patient_address,
  //   patient_phone: claimMed.patient_phone,
  //   patient_email: claimMed.patient_email,
  //   patient_relationship: claimMed.patient_relationship,
  //   patient_suffix: claimMed.patient_suffix,
  //   patient_staff: claimMed.patient_staff,
  //   provider_namas_peyee_no: claimMed.provider_namas_peyee_no,
  //   provider_date_claim_closed: claimMed.provider_date_claim_closed,
  //   provider_referring_practitioner: claimMed.provider_referring_practitioner,
  //   provider_anaesthetist: claimMed.provider_anaesthetist,
  //   provider_surgical_assistant: claimMed.provider_surgical_assistant,
  //   claiment_name: claimMed.claiment_name,
  //   claiment_surname: claimMed.claiment_surname,
  //   claiment_title: claimMed.claiment_title,
  //   claiment_dob: claimMed.claiment_dob,
  //   claiment_email: claimMed.claiment_email,
  //   claiment_address: claimMed.claiment_address,
  //   claiment_cell: claimMed.claiment_cell,
  //   claiment_national_id: claimMed.claiment_national_id,
  //   claiment_passport: claimMed.claiment_passport,
  //   claiment_relationship: claimMed.claiment_relationship,
  //   bank_account_holder: claimMed.bank_account_holder,
  //   bank_account_number: claimMed.bank_account_number,
  //   bank_name: claimMed.bank_name,
  //   bank_branch_code: claimMed.bank_branch_code,
  //   bank_account_type: claimMed.bank_account_type,
  //   created_at: claimMed.created_at,
  //   created_by: claimMed.created_by,
  //   approved_by: claimMed.approved_by,
  //   rejected_by: claimMed.rejected_by,
  //   status: claimMed.status,
  //   version: claimMed.version,
  //   account: claimMed.account,
  //   package_med: claimMed.package_med,
  //   company: claimMed.company,
  //   branch: claimMed.branch,
  //   user: {
  //     id: req.currentUser!.id,
  //     date: new Date(),
  //   },
  // });

  res.send(claimMed);
});

export { router as updateMedProductRouter };

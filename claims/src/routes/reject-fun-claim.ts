import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus,
} from "@totum-insurance/common";

import { ClaimFun } from "../models/claim-funeral";
import { ClaimFunRejectedPublisher } from "../events/publishers/claim-fun-rejected-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch(
  "/api/claims_funeral/reject/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const claimFun = await ClaimFun.findById(id);

    if (!claimFun) {
      throw new NotFoundError();
    }

    claimFun.status = ClaimFunStatus.Rejected;
    claimFun.rejected_by = req.currentUser!.id;
    await claimFun.save();

    new ClaimFunRejectedPublisher(natsWrapper.client).publish({
      id: claimFun.id,
      status: claimFun.status,
      rejected_by: claimFun.rejected_by,
      version: claimFun.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.status(204).send(claimFun);
  }
);

export { router as rejectFunClaimRouter };

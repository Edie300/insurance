import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimMedStatus,
} from "@totum-insurance/common";

import { ClaimMed } from "../models/claim-medical";
import { User } from "../models/user";

import { ClaimMedCreatedPublisher } from "../events/publishers/claim-med-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/claims_medical/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      policy_id,
      claim_id,
      claim_type,
      patient_type,
      patient_name,
      patient_surname,
      patient_national_id,
      patient_passport,
      patient_gender,
      patient_date_of_birth,
      patient_address,
      patient_phone,
      patient_email,
      patient_relationship,
      patient_suffix,
      patient_staff,
      provider_namas_peyee_no,
      provider_date_claim_closed,
      provider_referring_practitioner,
      provider_anaesthetist,
      provider_surgical_assistant,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_dob,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      approved_by,
      rejected_by,
      account,
      package_med,
      company,
      branch,
    } = req.body;


    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }


    const created_at = new Date();
    const created_by = user._id;

    const claimMed = ClaimMed.build({
      attachments,
      policy_id,
      claim_id,
      claim_type,
      patient_type,
      patient_name,
      patient_surname,
      patient_national_id,
      patient_passport,
      patient_gender,
      patient_date_of_birth,
      patient_address,
      patient_phone,
      patient_email,
      patient_relationship,
      patient_suffix,
      patient_staff,
      provider_namas_peyee_no,
      provider_date_claim_closed,
      provider_referring_practitioner,
      provider_anaesthetist,
      provider_surgical_assistant,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_dob,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      created_at,
      created_by,
      approved_by,
      rejected_by,
      account,
      package_med,
      company,
      branch,
      status: ClaimMedStatus.Pending,
    });

    await claimMed.save();
    new ClaimMedCreatedPublisher(natsWrapper.client).publish({
      id: claimMed.id,
      attachments: claimMed.attachments,
      claim_id: claimMed.claim_id,
      claim_type: claimMed.claim_type,
      patient_type: claimMed.patient_type,
      patient_name: claimMed.patient_name,
      patient_surname: claimMed.patient_surname,
      patient_national_id: claimMed.patient_national_id,
      patient_passport: claimMed.patient_passport,
      patient_gender: claimMed.patient_gender,
      patient_date_of_birth: claimMed.patient_date_of_birth,
      patient_address: claimMed.patient_address,
      patient_phone: claimMed.patient_phone,
      patient_email: claimMed.patient_email,
      patient_relationship: claimMed.patient_relationship,
      patient_suffix: claimMed.patient_suffix,
      patient_staff: claimMed.patient_staff,
      provider_namas_peyee_no: claimMed.provider_namas_peyee_no,
      provider_date_claim_closed: claimMed.provider_date_claim_closed,
      provider_referring_practitioner: claimMed.provider_referring_practitioner,
      provider_anaesthetist: claimMed.provider_anaesthetist,
      provider_surgical_assistant: claimMed.provider_surgical_assistant,
      claiment_name: claimMed.claiment_name,
      claiment_surname: claimMed.claiment_surname,
      claiment_title: claimMed.claiment_title,
      claiment_dob: claimMed.claiment_dob,
      claiment_email: claimMed.claiment_email,
      claiment_address: claimMed.claiment_address,
      claiment_cell: claimMed.claiment_cell,
      claiment_national_id: claimMed.claiment_national_id,
      claiment_passport: claimMed.claiment_passport,
      claiment_relationship: claimMed.claiment_relationship,
      bank_account_holder: claimMed.bank_account_holder,
      bank_account_number: claimMed.bank_account_number,
      bank_name: claimMed.bank_name,
      bank_branch_code: claimMed.bank_branch_code,
      bank_account_type: claimMed.bank_account_type,
      created_at: claimMed.created_at,
      created_by: claimMed.created_by,
      approved_by: claimMed.approved_by,
      rejected_by: claimMed.rejected_by,
      status: ClaimMedStatus.Pending,
      version: claimMed.version,
      account: claimMed.account,
      package_med: claimMed.package_med,
      company: claimMed.company,
      branch: claimMed.branch,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(claimMed);
  }
);

export { router as createMedClaimRouter };

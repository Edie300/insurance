import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";
import { ClaimFun } from "../models/claim-funeral";

const router = express.Router();

router.get(
  "/api/claims_funeral",

  async (req: Request, res: Response) => {
    // Copy req.query
    const reqQuery = { ...req.query };

    // Fields to exclude
    const removeFields = ["select", "sort", "page", "limit"];

    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach((param) => delete reqQuery[param]);

    // Create query string
    let queryStr = JSON.stringify(reqQuery);

    // Create operators ($gt, $gte, etc)
    queryStr = queryStr.replace(
      /\b(gt|gte|lt|lte|in)\b/g,
      (match) => `$${match}`
    );

    let query = JSON.parse(queryStr);
    
    const claimsFun = await ClaimFun.find(query)
      .populate("account_fun")
      .populate("package_fun")
      .populate("company")
      .populate("branch");
    res.send(claimsFun);
  }
);

export { router as indexFunClaimRouter };

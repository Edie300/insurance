import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus
} from '@totum-insurance/common';

import { ClaimFun } from '../models/claim-funeral';
import {  ClaimFunApprovedPublisher } from "../events/publishers/claim-fun-approved-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/claims_funeral/approve/:id',requireAuth, async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const claimFun = await ClaimFun.findById(id);

    if (!claimFun) {
      throw new NotFoundError();
    }

    claimFun.status = ClaimFunStatus.Approved;
    await claimFun.save();

    new ClaimFunApprovedPublisher(natsWrapper.client).publish({
      id: claimFun.id,
      status: claimFun.status,
      approved_by: req.currentUser!.id,
      version: claimFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(204).send(claimFun);
  }
);

export { router as approveFunClaimRouter };

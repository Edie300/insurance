import express, { Request, Response } from 'express';
import { NotFoundError, requireAuth } from '@totum-insurance/common';
import { ClaimFun } from '../models/claim-funeral';


const router = express.Router();

router.get(
  '/api/claims_funeral/companies/:companyId',
  requireAuth,
  async (req: Request, res: Response) => {

    const claimsFun = await ClaimFun.find({company: req.params.companyId})
    .populate("account_fun")
    .populate("package_fun")
    .populate("company")
    .populate("branch")
    .populate("created_by");
  
    res.send(claimsFun);
  });

export { router as showCompanyFunClaimsRouter };

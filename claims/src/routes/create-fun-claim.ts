import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus,
} from "@totum-insurance/common";

import { AccountFun } from "../models/account-funeral";
import { ClaimFun } from "../models/claim-funeral";
import { PackageFun } from "../models/package-funeral";
import { User } from "../models/user";

import { ClaimFunCreatedPublisher } from "../events/publishers/claim-fun-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/claims_funeral/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      claim_type,
      deceased_type,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      entity_id,
      account_fun,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      console.log("user not found")
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // find account funeral
    const accountFun = await AccountFun.findById(account_fun);
    if (!accountFun) {
      console.log("account funeral not found")
      throw new NotFoundError();
    }
    const { account_main, package_fun } = accountFun;

    const packageFun = await PackageFun.findById(package_fun);
    if (!packageFun) {
      console.log("package funeral not found")
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;
    const claim_id = (Math.random() * 100000).toFixed();
    const sum_assured = packageFun.sum_assured;

    const claimFun = ClaimFun.build({
      attachments,
      claim_id,
      claim_type,
      deceased_type,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      status: ClaimFunStatus.Pending,
      sum_assured,
      entity_id,
      created_at,
      created_by,
      account_main,
      package_fun,
      account_fun,
      company,
      branch,
    });

    await claimFun.save();
    new ClaimFunCreatedPublisher(natsWrapper.client).publish({
      id: claimFun.id,
      attachments: claimFun.attachments,
      claim_id: claimFun.claim_id,
      claim_type: claimFun.claim_type,
      deceased_type: claimFun.deceased_type,
      date_of_death: claimFun.date_of_death,
      cause_of_death: claimFun.cause_of_death,
      date_of_funeral: claimFun.date_of_funeral,
      place_of_burial: claimFun.place_of_burial,
      claiment_name: claimFun.claiment_name,
      claiment_surname: claimFun.claiment_surname,
      claiment_title: claimFun.claiment_title,
      claiment_date_of_birth: claimFun.claiment_date_of_birth,
      claiment_email: claimFun.claiment_email,
      claiment_address: claimFun.claiment_address,
      claiment_cell: claimFun.claiment_cell,
      claiment_national_id: claimFun.claiment_national_id,
      claiment_passport: claimFun.claiment_passport,
      claiment_relationship: claimFun.claiment_relationship,
      bank_account_holder: claimFun.bank_account_holder,
      bank_account_number: claimFun.bank_account_number,
      bank_name: claimFun.bank_name,
      bank_branch_code: claimFun.bank_branch_code,
      bank_account_type: claimFun.bank_account_type,
      status: claimFun.status,
      sum_assured: claimFun.sum_assured,
      entity_id: claimFun.entity_id,
      account_main: claimFun.account_main,
      created_at: claimFun.created_at,
      created_by: claimFun.created_by,
      package_fun: claimFun.package_fun,
      account_fun: claimFun.account_fun,
      company: claimFun.company,
      branch: claimFun.branch,
      version: claimFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(claimFun);
  }
);

export { router as createFunClaimRouter };

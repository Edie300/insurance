import { Publisher, Subjects, ClaimMedApprovedEvent } from "@totum-insurance/common";

export class ClaimMedApprovedPublisher extends Publisher<ClaimMedApprovedEvent> {
  subject: Subjects.ClaimMedApproved = Subjects.ClaimMedApproved;
}

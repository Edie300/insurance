import { Publisher, Subjects, ClaimMedCreatedEvent } from "@totum-insurance/common";

export class ClaimMedCreatedPublisher extends Publisher<ClaimMedCreatedEvent> {
  subject: Subjects.ClaimMedCreated = Subjects.ClaimMedCreated;
}

import { Publisher, Subjects, ClaimFunApprovedEvent } from "@totum-insurance/common";

export class ClaimFunApprovedPublisher extends Publisher<ClaimFunApprovedEvent> {
  subject: Subjects.ClaimFunApproved = Subjects.ClaimFunApproved;
}

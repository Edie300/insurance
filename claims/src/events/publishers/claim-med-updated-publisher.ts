import { Publisher, Subjects, ClaimMedUpdatedEvent } from "@totum-insurance/common";

export class ClaimMedUpdatedPublisher extends Publisher<ClaimMedUpdatedEvent> {
  subject: Subjects.ClaimMedUpdated = Subjects.ClaimMedUpdated;
}


import { Publisher, Subjects, ClaimFunUpdatedEvent } from "@totum-insurance/common";

export class ClaimFunUpdatedPublisher extends Publisher<ClaimFunUpdatedEvent> {
  subject: Subjects.ClaimFunUpdated = Subjects.ClaimFunUpdated;
}


import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMainUpdatedEvent,
} from "@totum-insurance/common";
import { AccountMain } from "../../models/account-main";
import { queueGroupName } from "./queue-group-name";

export class AccountMainUpdatedListener extends Listener<AccountMainUpdatedEvent> {
  subject: Subjects.AccountMainUpdated = Subjects.AccountMainUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMainUpdatedEvent["data"], msg: Message) {
    console.log("account-main-updated-listener::", data);
    
    const {
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
    } = data;

    const accountMain = await AccountMain.findById(id);

    if (!accountMain) {
      throw new Error("account main not found");
    }

    accountMain.set({
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
    });

    await accountMain.save();

    msg.ack();
  }
}

import mongoose, {Types, ObjectId} from "mongoose";
import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  CasketCreatedEvent,
} from "@totum-insurance/common";
import { Casket } from "../../models/casket";
import { queueGroupName } from "./queue-group-name";

export class CasketCreatedListener extends Listener<CasketCreatedEvent> {
  subject: Subjects.CasketCreated = Subjects.CasketCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: CasketCreatedEvent["data"], msg: Message) {
    console.log("casket-created-listener::", data);
    const {
      id,
      images,
      name,
      description,
      quantity,
      status,
      created_at,
      created_by,
      company,
      user,
    } = data;

    const casket = Casket.build({
      id,
      images,
      name,
      description,
      quantity,
      status,
      created_at,
      created_by,
      company,
    });
    await casket.save();

    msg.ack();
  }
}

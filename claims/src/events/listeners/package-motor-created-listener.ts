import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  PackageMotorCreatedEvent,
} from "@totum-insurance/common";
import { PackageMotor } from "../../models/package-motor";
import { queueGroupName } from "./queue-group-name";

export class PackageMotorCreatedListener extends Listener<PackageMotorCreatedEvent> {
  subject: Subjects.PackageMotorCreated = Subjects.PackageMotorCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageMotorCreatedEvent["data"], msg: Message) {
    console.log("package-motor-created-listener::", data);
    const { 
    id,
    attachments,
    name,
    description,
    type,
    act_of_god,
    bodily_damage,
    cash_in_lieu,
    property_damage,
    waiting_time,
    claim_processing_time,
    cash_back,
    cash_back_period,
    road_side_assistance,
    towing_services,
    premium_main,
    breakdown_fuel,
    airtime,
    personal_injury_protection,
    period_of_cover,
    full_vehicle_cover,
    policy_maturity,
    premium,
    currency,
    payment_type,
    status,
    company
    ,user
     } = data;

    const packageMotor = PackageMotor.build({
      id,
    attachments,
    name,
    description,
    type,
    act_of_god,
    bodily_damage,
    cash_in_lieu,
    property_damage,
    waiting_time,
    claim_processing_time,
    cash_back,
    cash_back_period,
    road_side_assistance,
    towing_services,
    premium_main,
    breakdown_fuel,
    airtime,
    personal_injury_protection,
    period_of_cover,
    full_vehicle_cover,
    policy_maturity,
    premium,
    currency,
    payment_type,
    status,
    company,

     });
    await packageMotor.save();


    msg.ack();
  }
}

import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMedUpdatedEvent,
} from "@totum-insurance/common";
import { AccountMed } from "../../models/account-medical";
import { queueGroupName } from "./queue-group-name";

export class AccountMedUpdatedListener extends Listener<AccountMedUpdatedEvent> {
  subject: Subjects.AccountMedUpdated = Subjects.AccountMedUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMedUpdatedEvent["data"], msg: Message) {
    console.log("account-med-updated-listener::", data);
    const accountMed = await AccountMed.findByEvent(data);

    if (!accountMed) {
      throw new Error("Account medical not found");
    }
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance,
      premium,
      account_main,
      created_at,
      created_by,
    } = data;

    accountMed.set({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance,
      premium,
      account_main,
      created_at,
      created_by,
    });
    await accountMed.save();

    msg.ack();
  }
}

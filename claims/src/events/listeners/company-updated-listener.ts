import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  CompanyUpdatedEvent,
} from "@totum-insurance/common";
import { Company } from "../../models/company";
import { queueGroupName } from "./queue-group-name";

export class CompanyUpdatedListener extends Listener<CompanyUpdatedEvent> {
  subject: Subjects.CompanyUpdated = Subjects.CompanyUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: CompanyUpdatedEvent["data"], msg: Message) {
    console.log("company-updated-listener::", data)
    const company = await Company.findByEvent(data);


    if (!company) {
      throw new Error("company not found");
    }
    const {
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      status,
      type,
      insurance,
    } = data;

    company.set({
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      status,
      type,
      insurance,
    });
    await company.save();

    msg.ack();
  }
}

import { Message } from "node-nats-streaming";
import { Subjects, Listener, UserCreatedEvent } from "@totum-insurance/common";
import { User } from "../../models/user";
import { queueGroupName } from "./queue-group-name";

export class UserCreatedListener extends Listener<UserCreatedEvent> {
  subject: Subjects.UserCreated = Subjects.UserCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: UserCreatedEvent["data"], msg: Message) {
    console.log("user-created-listener::", data);
    const {
      id,
      name,
      surname,
      employee_id,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      date_of_birth,
      phone,
      address,
      email,
      department,
      role,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
      status,
      company,
      branch,
      version,
    } = data;

    const user = User.build({
      id,
      name,
      surname,
      employee_id,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      date_of_birth,
      phone,
      address,
      email,
      department,
      role,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
      status,
      company,
      branch,
      version,
    });
    await user.save();

    msg.ack();
  }
}

import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  VehicleMotorCreatedEvent,
} from "@totum-insurance/common";
import { Vehicle } from "../../models/vehicle";
import { queueGroupName } from "./queue-group-name";

export class VehicleMotorCreatedListener extends Listener<VehicleMotorCreatedEvent> {
  subject: Subjects.VehicleMotorCreated = Subjects.VehicleMotorCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: VehicleMotorCreatedEvent["data"], msg: Message) {
    console.log("vehicle-motor-created-listener::", data);
    const {
      id,
      attachments,
      status,
      package_motor,
      company,
      make,
      model,
      year,
      reg_number,
      color,
      engine_number,
      chassis_number,
      tax_class,
      body_type,
      principal_driver,
      account_motor,
      account_main,
      created_at,
      created_by,
      user,
    } = data;

    const vehicle = Vehicle.build({
      id,
      attachments,
      make,
      model,
      year,
      reg_number,
      color,
      engine_number,
      chassis_number,
      tax_class,
      body_type,
      principal_driver,
      account_motor,
      status,
      package_motor,
      company,
      account_main,
      created_at,
      created_by,
    });
    await vehicle.save();


    msg.ack();
  }
}

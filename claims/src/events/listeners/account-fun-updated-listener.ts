import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountFunUpdatedEvent,
} from "@totum-insurance/common";
import { AccountFun } from "../../models/account-funeral";
import { queueGroupName } from "./queue-group-name";

export class AccountFunUpdatedListener extends Listener<AccountFunUpdatedEvent> {
  subject: Subjects.AccountFunUpdated = Subjects.AccountFunUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunUpdatedEvent["data"], msg: Message) {
    console.log("account-fun-updated-listener::", data);
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
    } = data;
    const accountFun = await AccountFun.findById(id);

    if (!accountFun) {
      throw new Error("Account Funeral not found");
    }

    accountFun.set({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
    });
    await accountFun.save();

    msg.ack();
  }
}

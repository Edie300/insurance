import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMainCreatedEvent,
} from "@totum-insurance/common";
import { AccountMain } from "../../models/account-main";
import { queueGroupName } from "./queue-group-name";

export class AccountMainCreatedListener extends Listener<AccountMainCreatedEvent> {
  subject: Subjects.AccountMainCreated = Subjects.AccountMainCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMainCreatedEvent["data"], msg: Message) {
    console.log("account-main-created-listener::", data);
    const {
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
    } = data;

    const accountMain = AccountMain.build({
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
    });
    await accountMain.save();

    msg.ack();
  }
}

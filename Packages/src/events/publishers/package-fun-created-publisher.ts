import { Subjects, Publisher, PackageFunCreatedEvent } from '../../../node_modules/@totum-insurance/common/build';

export class PackageFunCreatedPublisher extends Publisher<PackageFunCreatedEvent> {
  subject: Subjects.PackageFunCreated = Subjects.PackageFunCreated;
}

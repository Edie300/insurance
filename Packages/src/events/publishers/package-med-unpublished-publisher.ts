import { Subjects, Publisher, PackageMedUnPublishedEvent } from '@totum-insurance/common';

export class PackageMedUnPublishedPublisher extends Publisher<PackageMedUnPublishedEvent> {
  subject: Subjects.PackageMedUnPublished = Subjects.PackageMedUnPublished;
}

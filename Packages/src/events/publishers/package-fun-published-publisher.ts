import { Subjects, Publisher, PackageFunPublishedEvent } from '../../../node_modules/@totum-insurance/common/build';

export class PackageFunPublishedPublisher extends Publisher<PackageFunPublishedEvent> {
  subject: Subjects.PackageFunPublished = Subjects.PackageFunPublished;
}

import { Subjects, Publisher, PackageMotorDeletedEvent } from '@totum-insurance/common';

export class PackageMotorDeletedPublisher extends Publisher<PackageMotorDeletedEvent> {
  subject: Subjects.PackageMotorDeleted = Subjects.PackageMotorDeleted;
}

import { Subjects, Publisher, PackageFunDeletedEvent } from '../../../node_modules/@totum-insurance/common/build';

export class PackageFunDeletedPublisher extends Publisher<PackageFunDeletedEvent> {
  subject: Subjects.PackageFunDeleted = Subjects.PackageFunDeleted;
}

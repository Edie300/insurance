import { Subjects, Publisher, PackageMotorCreatedEvent } from '@totum-insurance/common';

export class PackageMotorCreatedPublisher extends Publisher<PackageMotorCreatedEvent> {
  subject: Subjects.PackageMotorCreated = Subjects.PackageMotorCreated;
}

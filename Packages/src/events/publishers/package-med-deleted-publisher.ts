import { Subjects, Publisher, PackageMedDeletedEvent } from '@totum-insurance/common';

export class PackageMedDeletedPublisher extends Publisher<PackageMedDeletedEvent> {
  subject: Subjects.PackageMedDeleted = Subjects.PackageMedDeleted;
}

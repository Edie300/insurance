import { Subjects, Publisher, PackageMedUpdatedEvent } from '@totum-insurance/common';

export class PackageMedUpdatedPublisher extends Publisher<PackageMedUpdatedEvent> {
  subject: Subjects.PackageMedUpdated = Subjects.PackageMedUpdated;
}

import { Subjects, Publisher, PackageFunUnPublishedEvent } from '../../../node_modules/@totum-insurance/common/build';

export class PackageFunUnPublishedPublisher extends Publisher<PackageFunUnPublishedEvent> {
  subject: Subjects.PackageFunUnPublished = Subjects.PackageFunUnPublished;
}

import { Subjects, Publisher, PackageFunUpdatedEvent } from '../../../node_modules/@totum-insurance/common/build';

export class PackageFunUpdatedPublisher extends Publisher<PackageFunUpdatedEvent> {
  subject: Subjects.PackageFunUpdated = Subjects.PackageFunUpdated;
}

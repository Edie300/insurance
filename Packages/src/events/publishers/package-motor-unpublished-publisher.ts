import { Subjects, Publisher, PackageMotorUnpublishedEvent } from '@totum-insurance/common';

export class PackageMotorUnPublishedPublisher extends Publisher<PackageMotorUnpublishedEvent> {
  subject: Subjects.PackageMotorUnPublished = Subjects.PackageMotorUnPublished;
}

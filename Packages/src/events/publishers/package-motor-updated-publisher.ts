import { Subjects, Publisher, PackageMotorUpdatedEvent } from '@totum-insurance/common';

export class PackageMotorUpdatedPublisher extends Publisher<PackageMotorUpdatedEvent> {
  subject: Subjects.PackageMotorUpdated = Subjects.PackageMotorUpdated;
}

import { Subjects, Publisher, PackageMotorPublishedEvent } from '@totum-insurance/common';

export class PackageMotorPublishedPublisher extends Publisher<PackageMotorPublishedEvent> {
  subject: Subjects.PackageMotorPublished = Subjects.PackageMotorPublished;
}

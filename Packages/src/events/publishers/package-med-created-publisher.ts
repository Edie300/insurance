import { Subjects, Publisher, PackageMedCreatedEvent } from '@totum-insurance/common';

export class PackageMedCreatedPublisher extends Publisher<PackageMedCreatedEvent> {
  subject: Subjects.PackageMedCreated = Subjects.PackageMedCreated;
}

import { Subjects, Publisher, PackageMedPublishedEvent } from '@totum-insurance/common';

export class PackageMedPublishedPublisher extends Publisher<PackageMedPublishedEvent> {
  subject: Subjects.PackageMedPublished = Subjects.PackageMedPublished;
}

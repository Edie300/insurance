import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  PackageFunUnPublishedEvent,
} from "@totum-insurance/common";
import { PackageFunUpdatedPublisher } from "../publishers/package-fun-updated-publisher";
import { PackageFun } from "../../models/package-funeral";
import { queueGroupName } from "./queue-group-name";

export class PackageFunUnPublishedListener extends Listener<PackageFunUnPublishedEvent> {
  subject: Subjects.PackageFunUnPublished = Subjects.PackageFunUnPublished;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunUnPublishedEvent["data"], msg: Message) {
    console.log("package-fun-unpublished-listener::", data);
    const {
      id,
      status,
      version,
      user

    } = data;
    const packageFun = await PackageFun.findById(id);

    if (!packageFun) {
      throw new Error("package funeral not found");
    }


    new PackageFunUpdatedPublisher(this.client).publish({
      id: packageFun.id,
      attachments: packageFun.attachments,
      name: packageFun.name,
      description: packageFun.description,
      policy_lapse_period: packageFun.policy_lapse_period,
      policy_archive_period: packageFun.policy_archive_period,
      payment_grace_period: packageFun.payment_grace_period,
      waiting_period: packageFun.waiting_period,
      principal_member_min_age: packageFun.principal_member_min_age,
      principal_member_max_age: packageFun.principal_member_max_age,
      dependent_max_age: packageFun.dependent_max_age,
      dependents_max_count: packageFun.dependents_max_count,
      policy_maturity: packageFun.policy_maturity,
      child_max_age: packageFun.child_max_age,
      child_max_age_school: packageFun.child_max_age_school,
      sum_assured: packageFun.sum_assured,
      hearse: packageFun.hearse,
      bus: packageFun.bus,
      groceries: packageFun.groceries,
      cash_in_lieu: packageFun.cash_in_lieu,
      accidental_death: packageFun.accidental_death,
      mortuary_services: packageFun.mortuary_services,
      events_management: packageFun.events_management,
      burial_services: packageFun.burial_services,
      service_area: packageFun.service_area,
      family_rate: packageFun.family_rate,
      dependent_rate: packageFun.dependent_rate,
      individual_rates: packageFun.individual_rates,
      child_rate: packageFun.child_rate,
      unbilled_dependents: packageFun.unbilled_dependents,
      currency: packageFun.currency,
      payment_type: packageFun.payment_type,
      casket: packageFun.casket,
      company: packageFun.company,
      status: packageFun.status,
      created_at: packageFun.created_at,
      created_by: packageFun.created_by,
      version: packageFun.version,
      user
    });

    msg.ack();
  }
}

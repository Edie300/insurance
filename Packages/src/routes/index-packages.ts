import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  requireAuth,
  validateRequest,
  NotFoundError,
  BadRequestError,
} from "@totum-insurance/common";
import { PackageFun } from "../models/package-funeral";
import { PackageMed } from "../models/package-medical";
import { PackageMotor } from "../models/package-motor";

const router = express.Router();

router.get(
  "/api/packages/companies/:companyId",
  requireAuth,
  async (req: Request, res: Response) => {
    let packages = [];

    const { companyId } = req.params;

    const packageFun = await PackageFun.find({ company: companyId })
      .populate("company")
      .populate("created_by");

    if (packageFun) {
      packages.push(...packageFun);
    }

    const packageMed = await PackageMed.find({ company: companyId })
      .populate("company")
      .populate("created_by");

    if (packageMed) {
      packages.push(...packageMed);
    }

    const packageMotor = await PackageMotor.find({ company: companyId })
      .populate("company")
      .populate("created_by");

    if (packageMotor) {
      packages.push(...packageMotor);
    }

    const sortedPackages = packages.sort((a, b) => {
      return a.created_at < b.created_at
        ? -1
        : a.created_at > b.created_at
        ? 1
        : 0;
    });

    res.send(sortedPackages);
  }
);

export { router as indexPackagesRouter };

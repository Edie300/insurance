import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";
import { PackageMotor } from "../models/package-motor";

const router = express.Router();

router.get("/api/packages_motor", async (req: Request, res: Response) => {
  const packages = await PackageMotor.find().populate('company').populate('created_by');
  res.send(packages);
});

export { router as indexPackageMotorRouter };

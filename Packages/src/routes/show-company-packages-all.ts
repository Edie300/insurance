import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { PackageFun } from "../models/package-funeral";
import { Company } from "../models/company";

const router = express.Router();

router.get(
  "/api/packages/companies/:companyId",
  requireAuth,

  async (req: Request, res: Response) => {
    
    // Check if company exists
    const getCompany = await Company.findById(req.params.companyId);
    if (!getCompany) {
      throw new NotFoundError();
    }

    const packagesFun = await PackageFun.find({ company: req.params.companyId })
      .populate("company")
      .populate('casket')
      .populate("created_by");

    res.send(packagesFun);
  }
);

export { router as showCompanyPackagesAllRouter };

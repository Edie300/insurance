import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  PackageMotorStatus,
} from "@totum-insurance/common";

import { PackageMotor } from "../models/package-motor";
import { PackageMotorDeletedPublisher } from "../events/publishers/package-motor-deleted-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch(
  "/api/packages_motor/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const packageMotor = await PackageMotor.findById(id);

    if (!packageMotor) {
      throw new NotFoundError();
    }

    packageMotor.status = PackageMotorStatus.Deleted;
    await packageMotor.save();

    new PackageMotorDeletedPublisher(natsWrapper.client).publish({
      id: packageMotor.id,
      status: packageMotor.status,
      version: packageMotor.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.status(204).send(packageMotor);
  }
);

export { router as deletePackageMotorRouter };

import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { PackageMotor } from "../models/package-motor";
import { Company } from "../models/company";

const router = express.Router();

router.get(
  "/api/packages_motor/companies/:companyId", 
  requireAuth,

  async (req: Request, res: Response) => {
    // Check if company exists
    const getCompany = await Company.findById(req.params.companyId);
    if (!getCompany) {
      throw new NotFoundError();
    }

    const packagesMotor = await PackageMotor.find({
      company: req.params.companyId,
    })
      .populate("company")
      .populate("created_by");

    res.send(packagesMotor);
  }
);

export { router as showCompanyPackagesMotorRouter };

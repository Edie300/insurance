import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { PackageMed } from "../models/package-medical";
import { Company } from "../models/company";

const router = express.Router();

router.get(
  "/api/packages_medical/companies/:companiesId",
  requireAuth,

  async (req: Request, res: Response) => {
    // Check if company exists
    const getCompany = await Company.findById(req.params.companyId);
    if (!getCompany) {
      throw new NotFoundError();
    }

    const packagesMed = await PackageMed.find({ company: req.params.companyId })
      .populate("company")
      .populate("created_by");

    res.send(packagesMed);
  }
);

export { router as showCompanyPackagesMedRouter };

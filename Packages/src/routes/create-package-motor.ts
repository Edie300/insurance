import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  PackageMotorStatus,
} from "@totum-insurance/common";

import { PackageMotor } from "../models/package-motor";
import { Company } from "../models/company";
import { User } from "../models/user";

import { PackageMotorCreatedPublisher } from "../events/publishers/package-motor-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/packages_motor/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      name,
      description,
      type,
      act_of_god,
      bodily_damage,
      cash_in_lieu,
      property_damage,
      waiting_time,
      claim_processing_time,
      cash_back,
      cash_back_period,
      road_side_assistance,
      towing_services,
      premium_main,
      breakdown_fuel,
      airtime,
      personal_injury_protection,
      period_of_cover,
      full_vehicle_cover,
      policy_maturity,
      premium,
      currency,
      payment_type,
    } = req.body;

    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company } = user;
    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;

    const packageMotor = PackageMotor.build({
      attachments,
      name,
      description,
      type,
      act_of_god,
      bodily_damage,
      cash_in_lieu,
      property_damage,
      waiting_time,
      claim_processing_time,
      cash_back,
      cash_back_period,
      road_side_assistance,
      towing_services,
      premium_main,
      breakdown_fuel,
      airtime,
      personal_injury_protection,
      period_of_cover,
      full_vehicle_cover,
      policy_maturity,
      premium,
      currency,
      payment_type,
      created_at,
      created_by,
      status: PackageMotorStatus.Unpublished,
      company,
    });

    await packageMotor.save();

    new PackageMotorCreatedPublisher(natsWrapper.client).publish({
      id: packageMotor.id,
      attachments: packageMotor.attachments,
      name: packageMotor.name,
      description: packageMotor.description,
      type: packageMotor.type,
      act_of_god: packageMotor.act_of_god,
      bodily_damage: packageMotor.bodily_damage,
      cash_in_lieu: packageMotor.cash_in_lieu,
      property_damage: packageMotor.property_damage,
      waiting_time: packageMotor.waiting_time,
      claim_processing_time: packageMotor.claim_processing_time,
      cash_back: packageMotor.cash_back,
      cash_back_period: packageMotor.cash_back_period,
      road_side_assistance: packageMotor.road_side_assistance,
      towing_services: packageMotor.towing_services,
      premium_main: packageMotor.premium_main,
      breakdown_fuel: packageMotor.breakdown_fuel,
      airtime: packageMotor.airtime,
      personal_injury_protection: packageMotor.personal_injury_protection,
      period_of_cover: packageMotor.period_of_cover,
      full_vehicle_cover: packageMotor.full_vehicle_cover,
      policy_maturity: packageMotor.policy_maturity,
      premium: packageMotor.premium,
      currency: packageMotor.currency,
      payment_type: packageMotor.payment_type,
      status: packageMotor.status,
      created_at: packageMotor.created_at,
      created_by: packageMotor.created_by,
      company: packageMotor.company,
      version: packageMotor.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.status(201).send(packageMotor);
  }
);

export { router as createPackageMotorRouter };

import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { PackageFun } from "../models/package-funeral";

const router = express.Router();

router.get(
  "/api/packages_funeral/:id",

  async (req: Request, res: Response) => {
    const packageFun = await PackageFun.findById(req.params.id)
      .populate("company")
      .populate("casket")
      .populate("created_by");

    if (!packageFun) {
      throw new NotFoundError();
    }

    res.send(packageFun);
  }
);

export { router as showPackageFunRouter };

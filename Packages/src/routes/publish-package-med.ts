import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  PackageMedStatus
} from '@totum-insurance/common';

import { PackageMed } from '../models/package-medical';
import { PackageMedPublishedPublisher } from "../events/publishers/package-med-published-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/packages_medical/publish/:id',requireAuth, async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const packageMed = await PackageMed.findById(id);

    if (!packageMed) {
      throw new NotFoundError();
    }

    packageMed.status = PackageMedStatus.Published;
    await packageMed.save();

    new PackageMedPublishedPublisher(natsWrapper.client).publish({
      id: packageMed.id,
      status: packageMed.status,
      version: packageMed.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.status(204).send(packageMed);
  }
);

export { router as publishPackageMedRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { PackageMotor } from "../models/package-motor";
import { PackageMotorUpdatedPublisher } from "../events/publishers/package-motor-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put(
  "/api/packages_motor/:id",
  requireAuth,
  async (req: Request, res: Response) => {
    const packageMotor = await PackageMotor.findById(req.params.id);

    if (!packageMotor) {
      throw new NotFoundError();
    }

    packageMotor.set({
      attachments: req.body.attachments,
      name: req.body.name,
      description: req.body.description,
      policy_lapse_period: req.body.policy_lapse_period,
      policy_archive_period: req.body.policy_archive_period,
      payment_grace_period: req.body.payment_grace_period,
      waiting_period: req.body.waiting_period,
      principal_member_min_age: req.body.principal_member_min_age,
      principal_member_max_age: req.body.principal_member_max_age,
      dependent_max_age: req.body.dependent_max_age,
      dependents_max_count: req.body.dependents_max_count,
      policy_maturity: req.body.policy_maturity,
      child_max_age: req.body.child_max_age,
      child_max_age_school: req.body.child_max_age_school,
      sum_assured: req.body.sum_assured,
      hearse: req.body.hearse,
      bus: req.body.bus,
      groceries: req.body.groceries,
      cash_in_lieu: req.body.cash_in_lieu,
      accidental_death: req.body.accidental_death,
      mortuary_services: req.body.mortuary_services,
      events_management: req.body.events_management,
      burial_services: req.body.burial_services,
      premium_family: req.body.premium_family,
      premium_dependent: req.body.premium_dependent,
      currency: req.body.currency,
      payment_type: req.body.payment_type,
      created_at: req.body.created_at,
      created_by: req.body.created_by,
      company: req.body.company,
      status: req.body.status,
    });

    await packageMotor.save();

    new PackageMotorUpdatedPublisher(natsWrapper.client).publish({
      id: packageMotor.id,
      attachments: packageMotor.attachments,
      name: packageMotor.name,
      description: packageMotor.description,
      type: packageMotor.type,
      act_of_god: packageMotor.act_of_god,
      bodily_damage: packageMotor.bodily_damage,
      cash_in_lieu: packageMotor.cash_in_lieu,
      property_damage: packageMotor.property_damage,
      waiting_time: packageMotor.waiting_time,
      claim_processing_time: packageMotor.claim_processing_time,
      cash_back: packageMotor.cash_back,
      cash_back_period: packageMotor.cash_back_period,
      road_side_assistance: packageMotor.road_side_assistance,
      towing_services: packageMotor.towing_services,
      premium_main: packageMotor.premium_main,
      breakdown_fuel: packageMotor.breakdown_fuel,
      airtime: packageMotor.airtime,
      personal_injury_protection: packageMotor.personal_injury_protection,
      period_of_cover: packageMotor.period_of_cover,
      full_vehicle_cover: packageMotor.full_vehicle_cover,
      policy_maturity: packageMotor.policy_maturity,
      premium: packageMotor.premium,
      currency: packageMotor.currency,
      payment_type: packageMotor.payment_type,
      created_at: packageMotor.created_at,
      created_by: packageMotor.created_by,
      status: packageMotor.status,
      company: packageMotor.company,
      version: packageMotor.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    res.send(packageMotor);
  }
);

export { router as updatePackageMotorRouter };

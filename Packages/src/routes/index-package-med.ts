import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";
import { PackageMed } from "../models/package-medical";

const router = express.Router();

router.get("/api/packages_medical", async (req: Request, res: Response) => {
  const packages = await PackageMed.find().populate('company').populate('created_by');
  res.send(packages);
});

export { router as indexPackageMedRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { PackageFun } from "../models/package-funeral";
import { PackageFunUpdatedPublisher } from "../events/publishers/package-fun-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/packages_funeral/:id", async (req: Request, res: Response) => {
  const packageFun = await PackageFun.findById(req.params.id);

  if (!packageFun) {
    throw new NotFoundError();
  }

  packageFun.set({
    attachments: req.body.attachments,
    name: req.body.name,
    description: req.body.description,
    policy_lapse_period: req.body.policy_lapse_period,
    policy_archive_period: req.body.policy_archive_period,
    payment_grace_period: req.body.payment_grace_period,
    waiting_period: req.body.waiting_period,
    principal_member_min_age: req.body.principal_member_min_age,
    principal_member_max_age: req.body.principal_member_max_age,
    dependent_max_age: req.body.dependent_max_age,
    dependents_max_count: req.body.dependents_max_count,
    policy_maturity: req.body.policy_maturity,
    child_max_age: req.body.child_max_age,
    child_max_age_school: req.body.child_max_age_school,
    sum_assured: req.body.sum_assured,
    hearse: req.body.hearse,
    bus: req.body.bus,
    groceries: req.body.groceries,
    cash_in_lieu: req.body.cash_in_lieu,
    accidental_death: req.body.accidental_death,
    mortuary_services: req.body.mortuary_services,
    events_management: req.body.events_management,
    burial_services: req.body.burial_services,
    service_area: req.body.service_area,
    family_rate: req.body.family_rate,
    dependent_rate: req.body.dependent_rate,
    individual_rates: req.body.individual_rates,
    child_rate: req.body.child_rate,
    unbilled_dependents: req.body.unbilled_dependents,
    currency: req.body.currency,
    payment_type: req.body.payment_type,
    casket: req.body.casket,
  });

  await packageFun.save();

  new PackageFunUpdatedPublisher(natsWrapper.client).publish({
    id: packageFun.id,
    attachments: packageFun.attachments,
    name: packageFun.name,
    description: packageFun.description,
    policy_lapse_period: packageFun.policy_lapse_period,
    policy_archive_period: packageFun.policy_archive_period,
    payment_grace_period: packageFun.payment_grace_period,
    waiting_period: packageFun.waiting_period,
    principal_member_min_age: packageFun.principal_member_min_age,
    principal_member_max_age: packageFun.principal_member_max_age,
    dependent_max_age: packageFun.dependent_max_age,
    dependents_max_count: packageFun.dependents_max_count,
    policy_maturity: packageFun.policy_maturity,
    child_max_age: packageFun.child_max_age,
    child_max_age_school: packageFun.child_max_age_school,
    sum_assured: packageFun.sum_assured,
    hearse: packageFun.hearse,
    bus: packageFun.bus,
    groceries: packageFun.groceries,
    cash_in_lieu: packageFun.cash_in_lieu,
    accidental_death: packageFun.accidental_death,
    mortuary_services: packageFun.mortuary_services,
    events_management: packageFun.events_management,
    burial_services: packageFun.burial_services,
    service_area: packageFun.service_area,
    family_rate: packageFun.family_rate,
    dependent_rate: packageFun.dependent_rate,
    individual_rates: packageFun.individual_rates,
    child_rate: packageFun.child_rate,
    unbilled_dependents: packageFun.unbilled_dependents,
    currency: packageFun.currency,
    payment_type: packageFun.payment_type,
    casket: packageFun.casket,
    company: packageFun.company,
    status: packageFun.status,
    created_at: packageFun.created_at,
    created_by: packageFun.created_by,
    version: packageFun.version,
    user: {
      id:  req.currentUser!.id, 
      date: new Date(),
    },
  });

  res.send(packageFun);
});

export { router as updatePackageFunRouter };

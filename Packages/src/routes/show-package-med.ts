import express, { Request, Response } from 'express';
import { NotFoundError } from '@totum-insurance/common';
import { PackageMed } from '../models/package-medical';


const router = express.Router();

router.get(
  '/api/packages_medical/:id',

  async (req: Request, res: Response) => {

    const packageMed = await PackageMed.findById(req.params.id).populate('company').populate('created_by');

    if (!packageMed) {
      throw new NotFoundError();
    }
  
    res.send(packageMed);
  });

export { router as showPackageMedRouter };

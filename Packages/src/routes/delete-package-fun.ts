import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  PackageFunStatus
} from '@totum-insurance/common';

import { PackageFun } from '../models/package-funeral';
import { PackageFunDeletedPublisher } from "../events/publishers/package-fun-deleted-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/packages_funeral/:id',  requireAuth, async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const packageFun = await PackageFun.findById(id);

    if (!packageFun) {
      throw new NotFoundError();
    }

    packageFun.status = PackageFunStatus.Deleted;
    await packageFun.save();

    new PackageFunDeletedPublisher(natsWrapper.client).publish({
      id: packageFun.id,
      status: packageFun.status,
      version: packageFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(204).send(packageFun);
  }
);

export { router as deletePackageFunRouter };

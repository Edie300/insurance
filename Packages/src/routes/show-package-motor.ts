import express, { Request, Response } from 'express';
import { NotFoundError } from '@totum-insurance/common';
import { PackageMotor } from '../models/package-motor';


const router = express.Router();

router.get(
  '/api/packages_motor/:id',

  async (req: Request, res: Response) => {

    const packageMotor = await PackageMotor.findById(req.params.id).populate('company').populate('created_by');

    if (!packageMotor) {
      throw new NotFoundError();
    }
  
    res.send(packageMotor);
  });

export { router as showPackageMotorRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { PackageMed } from "../models/package-medical";
import { PackageMedUpdatedPublisher } from "../events/publishers/package-med-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/packages_medical/:id",requireAuth, async (req: Request, res: Response) => {
  const packageMed = await PackageMed.findById(req.params.id);

  if (!packageMed) {
    throw new NotFoundError();
  }

  packageMed.set({
    attachments: req.body.attachments,
    name: req.body.name,
    description: req.body.description,
    annual_global_limit: req.body.annual_global_limit,
    general_practitioners: req.body.general_practitioners,
    number_of_initial_visits: req.body.number_of_initial_visits,
    medical_specialist: req.body.medical_specialist,
    hospitalization_limit: req.body.hospitalization_limit,
    waiting_time: req.body.waiting_time,
    claim_processing_time: req.body.claim_processing_time,
    private_hospitalization: req.body.private_hospitalization,
    private_hospitalization_CV19: req.body.private_hospitalization_CV19,
    public_hospitalization: req.body.public_hospitalization,
    ambulance_services: req.body.ambulance_services,
    air_evacuation: req.body.air_evacuation,
    number_of_dependents: req.body.number_of_dependents,
    age_limit_max: req.body.age_limit_max,
    age_limit_min: req.body.age_limit_min,
    premium_add: req.body.premium_add,
    drugs_3ben: req.body.drugs_3ben,
    drugs_abv_3ben: req.body.drugs_abv_3ben,
    chronic_drug_limit: req.body.chronic_drug_limit,
    drugs_cv19: req.body.drugs_cv19,
    drugs_acute: req.body.drugs_acute,
    dental: req.body.dental,
    optical: req.body.optical,
    psychiatric: req.body.psychiatric,
    physiotherapy: req.body.physiotherapy,
    prosthetics: req.body.prosthetics,
    pathology: req.body.pathology,
    specialized_radiology: req.body.specialized_radiology,
    blood_transfusion: req.body.blood_transfusion,
    maternity: req.body.maternity,
    family_planning: req.body.family_planning,
    infertility: req.body.infertility,
    hearing_aids: req.body.hearing_aids,
    hearing_scan: req.body.hearing_scan,
    funeral_cashback: req.body.funeral_cashback,
    premium_waver: req.body.premium_waver,
    wellness: req.body.wellness,
    tandCs: req.body.tandCs,
    policy_maturity: req.body.policy_maturity,
    premium_main: req.body.premium_main,
    premium_adult: req.body.premium_adult,
    premium_child: req.body.premium_child,
    waiting_period: req.body.waiting_period,
    currency: req.body.currency,
    payment_type: req.body.payment_type,
  });

  await packageMed.save();

  new PackageMedUpdatedPublisher(natsWrapper.client).publish({
    id: packageMed.id,
    attachments: packageMed.attachments,
    name: packageMed.name,
    description: packageMed.name,
    annual_global_limit: packageMed.annual_global_limit,
    general_practitioners: packageMed.general_practitioners,
    number_of_initial_visits: packageMed.number_of_initial_visits,
    medical_specialist: packageMed.medical_specialist,
    hospitalization_limit: packageMed.hospitalization_limit,
    waiting_time: packageMed.waiting_time,
    claim_processing_time: packageMed.claim_processing_time,
    private_hospitalization: packageMed.private_hospitalization,
    private_hospitalization_CV19: packageMed.private_hospitalization_CV19,
    public_hospitalization: packageMed.public_hospitalization,
    ambulance_services: packageMed.ambulance_services,
    air_evacuation: packageMed.air_evacuation,
    number_of_dependents: packageMed.number_of_dependents,
    age_limit_max: packageMed.age_limit_max,
    age_limit_min: packageMed.age_limit_min,
    premium_add: packageMed.premium_add,
    drugs_3ben: packageMed.drugs_3ben,
    drugs_abv_3ben: packageMed.drugs_abv_3ben,
    chronic_drug_limit: packageMed.chronic_drug_limit,
    drugs_cv19: packageMed.drugs_cv19,
    drugs_acute: packageMed.drugs_acute,
    dental: packageMed.dental,
    optical: packageMed.optical,
    psychiatric: packageMed.psychiatric,
    physiotherapy: packageMed.physiotherapy,
    prosthetics: packageMed.prosthetics,
    pathology: packageMed.pathology,
    specialized_radiology: packageMed.specialized_radiology,
    blood_transfusion: packageMed.blood_transfusion,
    maternity: packageMed.maternity,
    family_planning: packageMed.family_planning,
    infertility: packageMed.infertility,
    hearing_aids: packageMed.hearing_aids,
    hearing_scan: packageMed.hearing_scan,
    funeral_cashback: packageMed.funeral_cashback,
    premium_waver: packageMed.premium_waver,
    wellness: packageMed.wellness,
    tandCs: packageMed.tandCs,
    policy_maturity: packageMed.policy_maturity,
    service_area: packageMed.service_area,
    child_max_age: packageMed.child_max_age,
    child_max_age_school: packageMed.child_max_age_school,
    waiting_periods: packageMed.waiting_periods,
    family_rate: packageMed.family_rate,
    dependent_rate: packageMed.dependent_rate,
    individual_rates: packageMed.individual_rates,
    child_rate: packageMed.child_rate,
    unbilled_dependents: packageMed.unbilled_dependents,
    currency: packageMed.currency,
    payment_type: packageMed.payment_type,
    company: packageMed.company,
    branch: packageMed.branch,
    created_at: packageMed.created_at,
    created_by: packageMed.created_by,
    status: packageMed.status,
    version: packageMed.version,
    user: {
      id:  req.currentUser!.id, 
      date: new Date(),
    },
  });

  res.send(packageMed);
});

export { router as updatePackageMedRouter };

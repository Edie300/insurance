import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  requireAuth,
  validateRequest,
  NotFoundError,
  BadRequestError,
} from "@totum-insurance/common";
import { PackageFun } from "../models/package-funeral";
import { User } from "../models/user";

const router = express.Router();

router.get(
  "/api/packages_funeral",
  requireAuth,
  async (req: Request, res: Response) => {
    // Copy req.query
    const reqQuery = { ...req.query };

    // Fields to exclude
    const removeFields = ["select", "sort", "page", "limit"];

    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach((param) => delete reqQuery[param]);

    // Create query string
    let queryStr = JSON.stringify(reqQuery);

    // Create operators ($gt, $gte, etc)
    queryStr = queryStr.replace(
      /\b(gt|gte|lt|lte|in)\b/g,
      (match) => `$${match}`
    );

    let query = JSON.parse(queryStr);

    const packages = await PackageFun.find(query)
      .populate("company")
      .populate("casket")
      .populate("created_by");
    res.send(packages);
  }
);

export { router as indexPackageFunRouter };

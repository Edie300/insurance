import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  PackageFunStatus,
} from "@totum-insurance/common";

import { PackageFun } from "../models/package-funeral";
import { Company } from "../models/company";
import { Casket } from "../models/casket";
import { User } from "../models/user";

import { PackageFunCreatedPublisher } from "../events/publishers/package-fun-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/packages_funeral/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      casket,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const {company } = user
    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if casket exists
    const getCasket = await Casket.findById(casket);

    if (!getCasket) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;

    const packageFun = PackageFun.build({
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      created_at,
      created_by,
      status: PackageFunStatus.Unpublished,
      casket,
      company,
    });

    await packageFun.save();

    new PackageFunCreatedPublisher(natsWrapper.client).publish({
      id: packageFun.id,
      attachments: packageFun.attachments,
      name: packageFun.name,
      description: packageFun.description,
      policy_lapse_period: packageFun.policy_lapse_period,
      policy_archive_period: packageFun.policy_archive_period,
      payment_grace_period: packageFun.payment_grace_period,
      waiting_period: packageFun.waiting_period,
      principal_member_min_age: packageFun.principal_member_min_age,
      principal_member_max_age: packageFun.principal_member_max_age,
      dependent_max_age: packageFun.dependent_max_age,
      dependents_max_count: packageFun.dependents_max_count,
      policy_maturity: packageFun.policy_maturity,
      child_max_age: packageFun.child_max_age,
      child_max_age_school: packageFun.child_max_age_school,
      sum_assured: packageFun.sum_assured,
      hearse: packageFun.hearse,
      bus: packageFun.bus,
      groceries: packageFun.groceries,
      cash_in_lieu: packageFun.cash_in_lieu,
      accidental_death: packageFun.accidental_death,
      mortuary_services: packageFun.mortuary_services,
      events_management: packageFun.events_management,
      burial_services: packageFun.burial_services,
      service_area: packageFun.service_area,
      family_rate: packageFun.family_rate,
      dependent_rate: packageFun.dependent_rate,
      individual_rates: packageFun.individual_rates,
      child_rate: packageFun.child_rate,
      unbilled_dependents: packageFun.unbilled_dependents,
      currency: packageFun.currency,
      payment_type: packageFun.payment_type,
      created_at: packageFun.created_at,
      created_by: packageFun.created_by,
      status: packageFun.status,
      casket: packageFun.casket,
      company: packageFun.company,
      version: packageFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(packageFun);
  }
);

export { router as createPackageFunRouter };

import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  PackageFunStatus,
} from "@totum-insurance/common";

import { PackageFun } from "../models/package-funeral";
import { PackageFunPublishedPublisher } from "../events/publishers/package-fun-published-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch(
  "/api/packages_funeral/publish/:id",
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const packageFun = await PackageFun.findById(id);

    if (!packageFun) {
      throw new NotFoundError();
    }

    packageFun.status = PackageFunStatus.Published;
    await packageFun.save();

    new PackageFunPublishedPublisher(natsWrapper.client).publish({
      id: packageFun.id,
      status: packageFun.status,
      version: packageFun.version,
      user: {
        id: "628867578740348297d9e12b",
        date: new Date(),
      },
    });

    res.status(204).send(packageFun);
  }
);

export { router as publishPackageFunRouter };

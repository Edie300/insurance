import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  PackageMotorStatus
} from '@totum-insurance/common';

import { PackageMotor } from '../models/package-motor';
import { PackageMotorUnPublishedPublisher } from "../events/publishers/package-motor-unpublished-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/packages-motor/unpublish/:id',requireAuth, async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const packageMotor = await PackageMotor.findById(id);

    if (!packageMotor) {
      throw new NotFoundError();
    }

    packageMotor.status =  PackageMotorStatus.Unpublished;
    await packageMotor.save();

    new PackageMotorUnPublishedPublisher(natsWrapper.client).publish({
      id: packageMotor.id,
      status: packageMotor.status,
      version: packageMotor.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(204).send(packageMotor);
  }
);

export { router as unpublishPackageMotorRouter };

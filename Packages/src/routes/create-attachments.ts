import express, { Request, Response } from "express";
import AWS from "aws-sdk";
import multer from "multer";
import { nanoid } from "nanoid";
import {
  BadRequestError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";

import { User } from "../models/user";

interface Img {
  ETag: string;
  Location: string;
  key: string;
  Key: string;
  Bucket: string;
}

const router = express.Router();

const awsConfig = {
  accessKeyId: "AKIAU262SNIMGNH3M2OD",
  secretAccessKey: "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",
  region: "us-east-1",
  apiVersion: "2010-12-01",
};

const S3 = new AWS.S3(awsConfig);

const upload = multer();

// declare const Buffer;

router.post(
  "/api/packages/upload_attachments",
  requireAuth,
  // upload.array("attachments"),
  async (req: Request, res: Response) => {
    let arrayImages: Array <Img> = [];
    console.log("attachments====>", req.body);

    if (!req.body) {
      throw new BadRequestError("no files");
    }

    req.body.forEach(async (attachment: { data_url: string }) => {
      // prepare the image
      const base64Data = Buffer.from(
        attachment.data_url.replace(/^data:image\/\w+;base64,/, ""),
        "base64"
      );

      const type = attachment.data_url.split(";")[0].split("/")[1];

      // image params
      const params = {
        Bucket: "totum-insurance",
        Key: `${nanoid()}.${type}`,
        Body: base64Data,
        ACL: "public-read",
        ContentEncoding: "base64",
        ContentType: `image/${type}`,
      };

      // upload to s3
      await S3.upload(params, (err: any, data: any) => {
        if (err) {
          console.log(err);
        }
        console.log(data);
        arrayImages.push(data);
      });
    });

    console.log("image arrays", arrayImages);

    res.send(arrayImages);

    // const type = req.file.mimetype.split("/")[1];
    // console.log("file type====>", type);

    //   // image params
    //   const params = {
    //     Bucket: "totum-insurance",
    //     Key: `${nanoid()}.${type}`,
    //     Body: req.file.buffer,
    //     ACL: "public-read",
    //     ContentEncoding: "7bit",
    //     ContentType: `image/${type}`,
    //   };

    //   // upload to s3
    //   S3.upload(params, (err:any, data:any) => {
    //     if (err) {
    //       console.log(err);
    //       return res.sendStatus(400);
    //     }
    //     console.log(data);
    //     res.send(data);
    //   });
  }
);

export { router as createAttachmentsRouter };

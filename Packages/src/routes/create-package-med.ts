import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  PackageMedStatus,
} from "@totum-insurance/common";

import { PackageMed } from "../models/package-medical";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { User } from "../models/user";

import { PackageMedCreatedPublisher } from "../events/publishers/package-med-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/packages_medical/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      name,
      description,
      annual_global_limit,
      general_practitioners,
      number_of_initial_visits,
      medical_specialist,
      hospitalization_limit,
      waiting_time,
      claim_processing_time,
      private_hospitalization,
      private_hospitalization_CV19,
      public_hospitalization,
      ambulance_services,
      air_evacuation,
      number_of_dependents,
      age_limit_max,
      age_limit_min,
      premium_add,
      drugs_3ben,
      drugs_abv_3ben,
      chronic_drug_limit,
      drugs_cv19,
      drugs_acute,
      dental,
      optical,
      psychiatric,
      physiotherapy,
      prosthetics,
      pathology,
      specialized_radiology,
      blood_transfusion,
      maternity,
      family_planning,
      infertility,
      hearing_aids,
      hearing_scan,
      funeral_cashback,
      premium_waver,
      wellness,
      tandCs,
      policy_maturity,
      currency,
      payment_type,
      service_area,
      child_max_age,
      child_max_age_school,
      waiting_periods,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
    } = req.body;

    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company, branch } = user;
    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;

    const packageMed = PackageMed.build({
      attachments,
      name,
      description,
      annual_global_limit,
      general_practitioners,
      number_of_initial_visits,
      medical_specialist,
      hospitalization_limit,
      waiting_time,
      claim_processing_time,
      private_hospitalization,
      private_hospitalization_CV19,
      public_hospitalization,
      ambulance_services,
      air_evacuation,
      number_of_dependents,
      age_limit_max,
      age_limit_min,
      premium_add,
      drugs_3ben,
      drugs_abv_3ben,
      chronic_drug_limit,
      drugs_cv19,
      drugs_acute,
      dental,
      optical,
      psychiatric,
      physiotherapy,
      prosthetics,
      pathology,
      specialized_radiology,
      blood_transfusion,
      maternity,
      family_planning,
      infertility,
      hearing_aids,
      hearing_scan,
      funeral_cashback,
      premium_waver,
      wellness,
      tandCs,
      policy_maturity,
      currency,
      payment_type,
      service_area,
      child_max_age,
      child_max_age_school,
      waiting_periods,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      created_at,
      created_by,
      company,
      branch,
      status: PackageMedStatus.Unpublished,
    });

    await packageMed.save();

    new PackageMedCreatedPublisher(natsWrapper.client).publish({
      id: packageMed.id,
      attachments: packageMed.attachments,
      name: packageMed.name,
      description: packageMed.name,
      annual_global_limit: packageMed.annual_global_limit,
      general_practitioners: packageMed.general_practitioners,
      number_of_initial_visits: packageMed.number_of_initial_visits,
      medical_specialist: packageMed.medical_specialist,
      hospitalization_limit: packageMed.hospitalization_limit,
      waiting_time: packageMed.waiting_time,
      claim_processing_time: packageMed.claim_processing_time,
      private_hospitalization: packageMed.private_hospitalization,
      private_hospitalization_CV19: packageMed.private_hospitalization_CV19,
      public_hospitalization: packageMed.public_hospitalization,
      ambulance_services: packageMed.ambulance_services,
      air_evacuation: packageMed.air_evacuation,
      number_of_dependents: packageMed.number_of_dependents,
      age_limit_max: packageMed.age_limit_max,
      age_limit_min: packageMed.age_limit_min,
      premium_add: packageMed.premium_add,
      drugs_3ben: packageMed.drugs_3ben,
      drugs_abv_3ben: packageMed.drugs_abv_3ben,
      chronic_drug_limit: packageMed.chronic_drug_limit,
      drugs_cv19: packageMed.drugs_cv19,
      drugs_acute: packageMed.drugs_acute,
      dental: packageMed.dental,
      optical: packageMed.optical,
      psychiatric: packageMed.psychiatric,
      physiotherapy: packageMed.physiotherapy,
      prosthetics: packageMed.prosthetics,
      pathology: packageMed.pathology,
      specialized_radiology: packageMed.specialized_radiology,
      blood_transfusion: packageMed.blood_transfusion,
      maternity: packageMed.maternity,
      family_planning: packageMed.family_planning,
      infertility: packageMed.infertility,
      hearing_aids: packageMed.hearing_aids,
      hearing_scan: packageMed.hearing_scan,
      funeral_cashback: packageMed.funeral_cashback,
      premium_waver: packageMed.premium_waver,
      wellness: packageMed.wellness,
      tandCs: packageMed.tandCs,
      policy_maturity: packageMed.policy_maturity,
      service_area: packageMed.service_area,
      child_max_age: packageMed.child_max_age,
      child_max_age_school: packageMed.child_max_age_school,
      waiting_periods: packageMed.waiting_periods,
      family_rate: packageMed.family_rate,
      dependent_rate: packageMed.dependent_rate,
      individual_rates: packageMed.individual_rates,
      child_rate: packageMed.child_rate,
      unbilled_dependents: packageMed.unbilled_dependents,
      currency: packageMed.currency,
      payment_type: packageMed.payment_type,
      company: packageMed.company,
      branch: packageMed.branch,
      created_at: packageMed.created_at,
      created_by: packageMed.created_by,
      status: PackageMedStatus.Unpublished,
      version: packageMed.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(packageMed);
  }
);

export { router as createPackageMedRouter };

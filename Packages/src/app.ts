import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import cors from "cors";
import { errorHandler, NotFoundError } from "@totum-insurance/common";

// import { createAttachmentsRouter } from "./routes/create-attachments";
import { deletePackageFunRouter } from "./routes/delete-package-fun";
import { indexPackageFunRouter } from "./routes/index-package-fun";
import { createPackageFunRouter } from "./routes/create-package-fun";
import { showPackageFunRouter } from "./routes/show-package-fun";
import { updatePackageFunRouter } from "./routes/update-package-fun";
import { publishPackageFunRouter } from "./routes/publish-package-fun";
import { unpublishPackageFunRouter } from "./routes/unpublish-package-fun";
import { deletePackageMedRouter } from "./routes/delete-package-med";
import { indexPackageMedRouter } from "./routes/index-package-med";
import { createPackageMedRouter } from "./routes/create-package-med";
import { showCompanyPackagesAllRouter } from "./routes/show-company-packages-all";
import { showCompanyPackagesFunRouter } from "./routes/show-company-packages-fun";
import { showCompanyPackagesMedRouter } from "./routes/show-company-packages-med";
import { showCompanyPackagesMotorRouter } from "./routes/show-company-packages-motor";
import { showPackageMedRouter } from "./routes/show-package-med";
import { updatePackageMedRouter } from "./routes/update-package-med";
import { publishPackageMedRouter } from "./routes/publish-package-med";
import { unpublishPackageMedRouter } from "./routes/unpublish-package-med";
import { deletePackageMotorRouter } from "./routes/delete-package-motor";
import { indexPackageMotorRouter } from "./routes/index-package-motor";
import { indexPackagesRouter } from "./routes/index-packages";
import { createPackageMotorRouter } from "./routes/create-package-motor";
import { showPackageMotorRouter } from "./routes/show-package-motor";
import { updatePackageMotorRouter } from "./routes/update-package-motor";
import { publishPackageMotorRouter } from "./routes/publish-package-motor";
import { unpublishPackageMotorRouter } from "./routes/unpublish-package-motor";

const app = express();
// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set("trust proxy", true);
app.use(json());
app.use(express.json({ limit: "5mb" }));
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);

// app.use(createAttachmentsRouter);
app.use(deletePackageFunRouter);
app.use(indexPackageFunRouter);
app.use(createPackageFunRouter);
app.use(showPackageFunRouter);
app.use(updatePackageFunRouter);
app.use(publishPackageFunRouter);
app.use(unpublishPackageFunRouter);
app.use(deletePackageMedRouter);
app.use(indexPackageMedRouter);
app.use(indexPackagesRouter);
app.use(createPackageMedRouter);
app.use(showCompanyPackagesAllRouter);
app.use(showCompanyPackagesFunRouter);
app.use(showCompanyPackagesMedRouter);
app.use(showCompanyPackagesMotorRouter);
app.use(showPackageMedRouter);
app.use(updatePackageMedRouter);
app.use(publishPackageMedRouter);
app.use(unpublishPackageMedRouter);
app.use(deletePackageMotorRouter);
app.use(indexPackageMotorRouter);
app.use(createPackageMotorRouter);
app.use(showPackageMotorRouter);
app.use(updatePackageMotorRouter);
app.use(publishPackageMotorRouter);
app.use(unpublishPackageMotorRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { PackageMotorStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface PackageMotorAttrs {
  attachments: {}[];
  name: string;
  description: string;
  type: string;
  act_of_god: boolean;
  bodily_damage: boolean;
  cash_in_lieu: number;
  property_damage: boolean;
  waiting_time: number;
  claim_processing_time: number;
  cash_back: boolean;
  cash_back_period: number;
  road_side_assistance: boolean;
  towing_services: boolean;
  premium_main: number;
  breakdown_fuel: number;
  airtime: number;
  personal_injury_protection: number;
  period_of_cover: number;
  full_vehicle_cover: boolean;
  policy_maturity: number;
  premium: number;
  currency: string;
  payment_type: string;
  created_at: Date;
  created_by: Types.ObjectId;
  status: PackageMotorStatus;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface PackageMotorModel extends mongoose.Model<PackageMotorDoc> {
  build(attrs: PackageMotorAttrs): PackageMotorDoc;
}

// An interface that describes the properties
// that a User Document has
interface PackageMotorDoc extends mongoose.Document {
  attachments: {}[];
  name: string;
  description: string;
  type: string;
  act_of_god: boolean;
  bodily_damage: boolean;
  cash_in_lieu: number;
  property_damage: boolean;
  waiting_time: number;
  claim_processing_time: number;
  cash_back: boolean;
  cash_back_period: number;
  road_side_assistance: boolean;
  towing_services: boolean;
  premium_main: number;
  breakdown_fuel: number;
  airtime: number;
  personal_injury_protection: number;
  period_of_cover: number;
  full_vehicle_cover: boolean;
  policy_maturity: number;
  premium: number;
  currency: string;
  payment_type: string;
  created_at: Date;
  created_by: Types.ObjectId;
  status: PackageMotorStatus;
  company: Types.ObjectId;
  version: number;
}

const packageMotorSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: [true, "Please add a name"],
      trim: true,
      maxlength: [50, "Name can not be more than 50 characters"],
    },

    description: {
      type: String,
      required: [true, "Please add a description"],
      maxlength: [700, "Description can not be more than 700 characters"],
    },

    type: {
      type: String,
      enum: ["thirdParty", "fullCover", "fireAndTheft"],
    },

    act_of_god: {
      type: Boolean,
      default: false,
    },
    bodily_damage: {
      type: Boolean,
      default: false,
    },
    cash_in_lieu: {
      type: Number,
    },
    property_damage: {
      type: Boolean,
      default: false,
    },
    waiting_time: {
      type: Number,
    },
    claim_processing_time: {
      type: Number,
      default: 1,
    },
    cash_back: {
      type: Boolean,
      default: false,
    },
    cash_back_period: {
      type: Number,
      default: 60,
    },
    road_side_assistance: {
      type: Boolean,
      default: false,
    },
    towing_services: {
      type: Boolean,
      default: false,
    },
    premium_main: {
      type: Number,
    },
    breakdown_fuel: {
      type: Number,
    },
    airtime: {
      type: Number,
    },
    personal_injury_protection: {
      type: Number,
    },
    period_of_cover: {
      type: Number,
    },
    full_vehicle_cover: {
      type: Boolean,
      default: false,
    },
    policy_maturity: {
      type: Number,
    },

    premium: {
      type: Number,
      required: [true, "Please add a premium amount"],
    },
    currency: {
      type: String,
      enum: ["ZW", "US", "ZAR"],
    },
    payment_type: {
      type: String,
      enum: ["weekly", "yearly", "monthly"],
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },

    status: {
      type: String,
      enum: Object.values(PackageMotorStatus),
      default: PackageMotorStatus.Unpublished,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        ret.type = 2;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

packageMotorSchema.set("versionKey", "version");
packageMotorSchema.plugin(updateIfCurrentPlugin);

packageMotorSchema.statics.build = (attrs: PackageMotorAttrs) => {
  return new PackageMotor(attrs);
};

const PackageMotor = mongoose.model<PackageMotorDoc, PackageMotorModel>(
  "PackageMotor",
  packageMotorSchema
);

export { PackageMotor };

import { Message } from 'node-nats-streaming';
import { Subjects, Listener, PackageFunUpdatedEvent } from '@totum-insurance/common';
import { PackageFun } from '../../models/package-funeral';
import { queueGroupName } from './queue-group-name';

export class PackageFunUpdatedListener extends Listener<PackageFunUpdatedEvent> {
  subject: Subjects.PackageFunUpdated = Subjects.PackageFunUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunUpdatedEvent['data'], msg: Message) {

    console.log("package-fun-created-listener::", data)
    const { 
      id, 
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      created_at,
      created_by,
      status,
      casket,
      company,
     } = data;

     const packageFun = await PackageFun.findById(id);

     if (!packageFun) {
      // if company not found acknowlegde nats event bus
      // msg.ack();
      throw new Error("company not found");
    }

     packageFun.set({  
      id, 
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      created_at,
      created_by,
      status,
      company,
      casket
    });
    await packageFun.save();

    msg.ack();
  }
}

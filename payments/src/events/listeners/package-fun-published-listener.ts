import { Message } from 'node-nats-streaming';
import { Subjects, Listener, PackageFunPublishedEvent } from '@totum-insurance/common';
import { PackageFun } from '../../models/package-funeral';
import { queueGroupName } from './queue-group-name';

export class PackageFunPublishedListener extends Listener<PackageFunPublishedEvent> {
  subject: Subjects.PackageFunPublished = Subjects.PackageFunPublished;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunPublishedEvent['data'], msg: Message) {

    console.log("package-fun-published-listener::", data)
    const { 
      id, 
      status,
     } = data;

     const packageFun = await PackageFun.findById(id);

     if (!packageFun) {
      // if company not found acknowlegde nats event bus
      // msg.ack();
      throw new Error("package funeral not found");
    }

     packageFun.set({  
      status
    });
    await packageFun.save();

    msg.ack();
  }
}

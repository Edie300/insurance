import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  BranchUpdatedEvent,
} from "@totum-insurance/common";
import { Branch } from "../../models/branch";
import { queueGroupName } from "./queue-group-name";

export class BranchUpdatedListener extends Listener<BranchUpdatedEvent> {
  subject: Subjects.BranchUpdated = Subjects.BranchUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: BranchUpdatedEvent["data"], msg: Message) {
    console.log("branch-updated-listener::", data);
    const { id, name, phones, landlines, address, status, company } = data;
    const branch = await Branch.findById(id);

    if (!branch) {
      throw new Error("branch not found");
    }

    branch.set({
      id,
      name,
      phones,
      landlines,
      address,
      status,
      company,
    });
    await branch.save();

    msg.ack();
  }
}

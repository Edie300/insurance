import { Message } from 'node-nats-streaming';
import { Subjects, Listener, PackageFunDeletedEvent } from '@totum-insurance/common';
import { PackageFun } from '../../models/package-funeral';
import { queueGroupName } from './queue-group-name';

export class PackageFunDeletedListener extends Listener<PackageFunDeletedEvent> {
  subject: Subjects.PackageFunDeleted = Subjects.PackageFunDeleted;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunDeletedEvent['data'], msg: Message) {

    console.log("package-fun-published-listener::", data)
    const { 
      id, 
      status,
     } = data;

     const packageFun = await PackageFun.findById(id);

     if (!packageFun) {
      // if company not found acknowlegde nats event bus
      // msg.ack();
      throw new Error("package funeral not found");
    }

     packageFun.set({  
      status
    });
    await packageFun.save();

    msg.ack();
  }
}

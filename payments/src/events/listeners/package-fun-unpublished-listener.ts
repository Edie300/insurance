import { Message } from 'node-nats-streaming';
import { Subjects, Listener, PackageFunUnPublishedEvent } from '@totum-insurance/common';
import { PackageFun } from '../../models/package-funeral';
import { queueGroupName } from './queue-group-name';

export class PackageFunUnpublishedListener extends Listener<PackageFunUnPublishedEvent> {
  subject: Subjects.PackageFunUnPublished = Subjects.PackageFunUnPublished;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunUnPublishedEvent['data'], msg: Message) {

    console.log("package-fun-upublished-listener::", data)
    const { 
      id, 
      status,
     } = data;

     const packageFun = await PackageFun.findById(id);

     if (!packageFun) {
      // if company not found acknowlegde nats event bus
      // msg.ack();
      throw new Error("package funeral not found");
    }

     packageFun.set({  
      status
    });
    await packageFun.save();

    msg.ack();
  }
}

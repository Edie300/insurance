import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  CasketUpdatedEvent,
} from "@totum-insurance/common";
import { Casket } from "../../models/casket";
import { queueGroupName } from "./queue-group-name";

export class CasketUpdatedListener extends Listener<CasketUpdatedEvent> {
  subject: Subjects.CasketUpdated = Subjects.CasketUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: CasketUpdatedEvent["data"], msg: Message) {
    console.log("casket-updated-listener::", data);
    const {
      id,
      images,
      name,
      description,
      quantity,
      status,
      created_at,
      created_by,
      company,
    } = data;
    const casket = await Casket.findById(id);

    if (!casket) {
      throw new Error("casket not found");
    }

    casket.set({
      id,
      images,
      name,
      description,
      quantity,
      status,
      created_at,
      created_by,
      company,
    });
    await casket.save();

    msg.ack();
  }
}

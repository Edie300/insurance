import { Publisher, Subjects, PaymentCreatedEvent } from "@totum-insurance/common";

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
}

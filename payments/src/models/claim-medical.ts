import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { ClaimMedStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface ClaimMedAttrs {
  attachments: {}[];
  policy_id: string;
  claim_id: string;
  claim_type: number;
  patient_type: number;
  patient_name: string;
  patient_surname: string;
  patient_national_id: string;
  patient_passport: string;
  patient_gender: string;
  patient_date_of_birth: Date;
  patient_address: string;
  patient_phone: string;
  patient_email: string;
  patient_relationship: string;
  patient_suffix: string;
  patient_staff: string;
  provider_namas_peyee_no: string;
  provider_date_claim_closed: string;
  provider_referring_practitioner: string;
  provider_anaesthetist: string;
  provider_surgical_assistant: string;
  claiment_name: string;
  claiment_surname: string;
  claiment_title: string;
  claiment_dob: string;
  claiment_email: string;
  claiment_address: string;
  claiment_cell: string;
  claiment_national_id: string;
  claiment_passport: string;
  claiment_relationship: string;
  bank_account_holder: string;
  bank_account_number: string;
  bank_name: string;
  bank_branch_code: string;
  bank_account_type: string;
  status: number;
  created_at: Date;
  created_by: Types.ObjectId;
  approved_by: Types.ObjectId;
  rejected_by: Types.ObjectId;
  account: Types.ObjectId;
  package_med: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface ClaimMedModel extends mongoose.Model<ClaimMedDoc> {
  build(attrs: ClaimMedAttrs): ClaimMedDoc;
}

// An interface that describes the properties
// that a User Document has
interface ClaimMedDoc extends mongoose.Document {
  attachments: {}[];
  policy_id: string;
  claim_id: string;
  claim_type: number;
  patient_type: number;
  patient_name: string;
  patient_surname: string;
  patient_national_id: string;
  patient_passport: string;
  patient_gender: string;
  patient_date_of_birth: Date;
  patient_address: string;
  patient_phone: string;
  patient_email: string;
  patient_relationship: string;
  patient_suffix: string;
  patient_staff: string;
  provider_namas_peyee_no: string;
  provider_date_claim_closed: string;
  provider_referring_practitioner: string;
  provider_anaesthetist: string;
  provider_surgical_assistant: string;
  claiment_name: string;
  claiment_surname: string;
  claiment_title: string;
  claiment_dob: string;
  claiment_email: string;
  claiment_address: string;
  claiment_cell: string;
  claiment_national_id: string;
  claiment_passport: string;
  claiment_relationship: string;
  bank_account_holder: string;
  bank_account_number: string;
  bank_name: string;
  bank_branch_code: string;
  bank_account_type: string;
  status: number;
  created_at: Date;
  created_by: Types.ObjectId;
  approved_by: Types.ObjectId;
  rejected_by: Types.ObjectId;
  account: Types.ObjectId;
  package_med: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const claimMedSchema = new mongoose.Schema(
  {
    attachments: [],
    claim_type: { type: Number },
    policy_id: { type: String, required: true },
    claim_id: { type: String, required: true },
    deceased_type: { type: Number },
    deceased_name: { type: String },
    deceased_surname: { type: String },
    deceased_title: { type: String },
    deceased_date_of_birth: { type: Date },
    deceased_email: { type: String },
    deceased_address: { type: String },
    deceased_cell: { type: String },
    deceased_national_id: { type: String },
    deceased_passport: { type: String },
    deceased_relationship: { type: String },
    date_of_death: { type: String },
    cause_of_death: { type: String },
    date_of_funeral: { type: String },
    place_of_burial: { type: String },
    claiment_name: { type: String },
    claiment_surname: { type: String },
    claiment_title: { type: String },
    claiment_date_of_birth: { type: Date },
    claiment_email: { type: String },
    claiment_address: { type: String },
    claiment_cell: { type: String },
    claiment_national_id: { type: String },
    claiment_passport: { type: String },
    claiment_relationship: { type: String },
    bank_account_holder: { type: String },
    bank_account_number: { type: String },
    bank_name: { type: String },
    bank_branch_code: { type: String },

    // 0: savings, 1: cheque, 2: current, 3: transmission
    bank_account_type: {
      type: Number,
      enum: [0, 1, 2, 3],
    },

    status: {
      type: Number,
      enum: Object.values(ClaimMedStatus),
      default: ClaimMedStatus.Pending,
    },

    sum_assured: {
      type: Number,
    },

    // processing time is in hours
    processing_time: {
      type: Number,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    account: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Account",
      required: true,
    },
    package_med: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageMed",
      required: true,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

claimMedSchema.set("versionKey", "version");
claimMedSchema.plugin(updateIfCurrentPlugin);

claimMedSchema.statics.build = (attrs: ClaimMedAttrs) => {
  return new ClaimMed(attrs);
};

const ClaimMed = mongoose.model<ClaimMedDoc, ClaimMedModel>(
  "ClaimMed",
  claimMedSchema
);

export { ClaimMed };

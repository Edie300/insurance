import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";

import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/payments/:id", async (req: Request, res: Response) => {


  res.send("update payment");
});

export { router as updatePaymentRouter };

import express, { Request, Response } from 'express';
import { NotFoundError, requireAuth } from '@totum-insurance/common';
import { ClaimFun } from '../models/claim-funeral';


const router = express.Router();

router.get(
  '/api/payments',
  requireAuth,
  async (req: Request, res: Response) => {

  
    res.send("get all payments");
  });

export { router as indexPaymentRouter };

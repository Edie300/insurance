import express, { Request, Response } from 'express';
import { NotFoundError, requireAuth } from '@totum-insurance/common';
import { ClaimFun } from '../models/claim-funeral';


const router = express.Router();

router.get(
  '/api/payments/:id',
  requireAuth,
  async (req: Request, res: Response) => {


  
    res.send("show payment");
  });

export { router as showPaymentRouter };

import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus,
  PaymentStatus
} from "@totum-insurance/common";

import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { AccountFun } from "../models/account-funeral";
import { ClaimFun } from "../models/claim-funeral";
import { User } from "../models/user";
import { Payment } from "../models/payment";

import { PaymentCreatedPublisher } from "../events/publishers/payment-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/payments/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      type,
      method,
      amount,
      payer_name,
      payer_surname,
      payer_phone,
      currency,
      premium,
      account_main,
      account,
      account_package,
    } = req.body;

    // find user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      throw new NotFoundError();
    }

    // Check if account funeral exists
    const accountFun = await AccountFun.findById(account);

    if (!accountFun) {
      throw new NotFoundError();
    }

    const balance_before = accountFun.balance;
    const balance_after = accountFun.balance + amount;
    const created_at = new Date();
    const created_by = user._id;
    const payment_id = (Math.random() * 100000).toFixed();

    const payment = Payment.build({
      payment_id,
      type,
      method,
      amount,
      payer_name,
      payer_surname,
      payer_phone,
      currency,
      premium,
      balance_before,
      balance_after,
      created_at,
      created_by,
      status: PaymentStatus.Success,
      account_main,
      account,
      account_package,
      company,
      branch,
    });

    await payment.save();

    new PaymentCreatedPublisher(natsWrapper.client).publish({
      id: payment.id,
      payment_id: payment.payment_id,
      type: payment.type,
      method: payment.method,
      amount: payment.amount,
      payer_name: payment.payer_name,
      payer_surname: payment.payer_surname,
      payer_phone: payment.payer_phone,
      currency: payment.currency,
      premium: payment.premium,
      balance_before: payment.balance_before,
      balance_after: payment.balance_after,
      created_at: payment.created_at,
      created_by: payment.created_by,
      status: payment.status,
      account_main: payment.account_main,
      account: payment.account,
      account_package: payment.account_package,
      company: payment.company,
      branch: payment.branch,
      version: payment.version,
    });

    res.status(201).send(payment);
  }
);

export { router as createPaymentRouter };

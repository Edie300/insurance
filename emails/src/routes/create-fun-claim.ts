import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus,
} from "@totum-insurance/common";

import { AccountFun } from "../models/account-funeral";
import { ClaimFun } from "../models/claim-funeral";
import { User } from "../models/user";

import { ClaimFunCreatedPublisher } from "../events/publishers/claim-fun-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/claims_funeral/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      claim_type,
      deceased_type,
      deceased_name,
      deceased_surname,
      deceased_title,
      deceased_date_of_birth,
      deceased_email,
      deceased_address,
      deceased_cell,
      deceased_national_id,
      deceased_passport,
      deceased_relationship,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      sum_assured,
      processing_time,
      account_fun,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      console.log("user not found")
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // find account funeral
    const accountFun = await AccountFun.findById(account_fun);
    if (!accountFun) {
      console.log("account funeral not found")
      throw new NotFoundError();
    }
    const { account_main, package_fun } = accountFun;

    const created_at = new Date();
    const created_by = user._id;
    const claim_id = (Math.random() * 100000).toFixed();

    const claimFun = ClaimFun.build({
      attachments,
      claim_id,
      claim_type,
      deceased_type,
      deceased_name,
      deceased_surname,
      deceased_title,
      deceased_date_of_birth,
      deceased_email,
      deceased_address,
      deceased_cell,
      deceased_national_id,
      deceased_passport,
      deceased_relationship,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      status: ClaimFunStatus.Pending,
      sum_assured,
      processing_time,
      created_at,
      created_by,
      account_main,
      package_fun,
      account_fun,
      company,
      branch,
    });

    await claimFun.save();
    new ClaimFunCreatedPublisher(natsWrapper.client).publish({
      id: claimFun.id,
      attachments: claimFun.attachments,
      claim_id: claimFun.claim_id,
      claim_type: claimFun.claim_type,
      deceased_type: claimFun.deceased_type,
      deceased_name: claimFun.deceased_name,
      deceased_surname: claimFun.deceased_surname,
      deceased_title: claimFun.deceased_title,
      deceased_date_of_birth: claimFun.deceased_date_of_birth,
      deceased_email: claimFun.deceased_email,
      deceased_address: claimFun.deceased_address,
      deceased_cell: claimFun.deceased_cell,
      deceased_national_id: claimFun.deceased_national_id,
      deceased_passport: claimFun.deceased_passport,
      deceased_relationship: claimFun.deceased_relationship,
      date_of_death: claimFun.date_of_death,
      cause_of_death: claimFun.cause_of_death,
      date_of_funeral: claimFun.date_of_funeral,
      place_of_burial: claimFun.place_of_burial,
      claiment_name: claimFun.claiment_name,
      claiment_surname: claimFun.claiment_surname,
      claiment_title: claimFun.claiment_title,
      claiment_date_of_birth: claimFun.claiment_date_of_birth,
      claiment_email: claimFun.claiment_email,
      claiment_address: claimFun.claiment_address,
      claiment_cell: claimFun.claiment_cell,
      claiment_national_id: claimFun.claiment_national_id,
      claiment_passport: claimFun.claiment_passport,
      claiment_relationship: claimFun.claiment_relationship,
      bank_account_holder: claimFun.bank_account_holder,
      bank_account_number: claimFun.bank_account_number,
      bank_name: claimFun.bank_name,
      bank_branch_code: claimFun.bank_branch_code,
      bank_account_type: claimFun.bank_account_type,
      status: claimFun.status,
      sum_assured: claimFun.sum_assured,
      processing_time: claimFun.processing_time,
      version: claimFun.version,
      account_main: claimFun.account_main,
      created_at: claimFun.created_at,
      created_by: claimFun.created_by,
      package_fun: claimFun.package_fun,
      account_fun: claimFun.account_fun,
      company: claimFun.company,
      branch: claimFun.branch,
    });

    res.status(201).send(claimFun);
  }
);

export { router as createFunClaimRouter };

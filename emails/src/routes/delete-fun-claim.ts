import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus
} from '@totum-insurance/common';

import { ClaimFun } from '../models/claim-funeral';

const router = express.Router();

router.delete('/api/claims_funeral/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const claimFun = await ClaimFun.findById(id);

    if (!claimFun) {
      throw new NotFoundError();
    }

    claimFun.status = ClaimFunStatus.Rejected;
    await claimFun.save();

    res.status(204).send(claimFun);
  }
);

export { router as deleteFunClaimRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";
import { ClaimFun } from "../models/claim-funeral";

const router = express.Router();

router.get(
  "/api/claims_funeral",

  async (req: Request, res: Response) => {
    const claimsFun = await ClaimFun.find()
      .populate("account_fun")
      .populate("package_fun")
      .populate("company")
      .populate("branch");
    res.send(claimsFun);
  }
);

export { router as indexFunClaimRouter };

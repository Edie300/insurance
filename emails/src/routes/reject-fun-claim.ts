import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus
} from '@totum-insurance/common';

import { ClaimFun } from '../models/claim-funeral';
import {  ClaimFunRejectedPublisher  } from "../events/publishers/claim-fun-rejected-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/claims_funeral/reject/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const claimFun = await ClaimFun.findById(id);

    if (!claimFun) {
      throw new NotFoundError();
    }

    claimFun.status = ClaimFunStatus.Rejected;
    await claimFun.save();

    new ClaimFunRejectedPublisher(natsWrapper.client).publish({
      id: claimFun.id,
      status: claimFun.status,
      rejected_by: claimFun.id,
      version: claimFun.version,
    });

    res.status(204).send(claimFun);
  }
);

export { router as rejectFunClaimRouter };

import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus
} from '@totum-insurance/common';

import { ClaimMed } from '../models/claim-medical';

const router = express.Router();

router.delete('/api/claims_medical/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const claimMed = await ClaimMed.findById(id);

    if (!claimMed) {
      throw new NotFoundError();
    }

    claimMed.status = ClaimFunStatus.Rejected;
    await claimMed.save();

    res.status(204).send(claimMed);
  }
);

export { router as deleteMedClaimRouter };

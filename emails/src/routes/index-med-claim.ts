import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";
import { ClaimMed } from "../models/claim-medical";

const router = express.Router();

router.get(
  "/api/claims_medical",

  async (req: Request, res: Response) => {
    const claimsMed = await ClaimMed.find()
      .populate("AccountMed")
      .populate("ProductMed")
      .populate("Company")
      .populate("Branch");
    res.send(claimsMed);
  }
);

export { router as indexMedClaimRouter };

import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  ClaimFunStatus
} from '@totum-insurance/common';

import { ClaimMed } from '../models/claim-medical';
import {  ClaimMedApprovedPublisher } from "../events/publishers/claim-med-approved-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/claims_medical/approve/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const claimMed = await ClaimMed.findById(id);

    if (!claimMed) {
      throw new NotFoundError();
    }

    claimMed.status = ClaimFunStatus.Approved;
    await claimMed.save();

    new ClaimMedApprovedPublisher(natsWrapper.client).publish({
      id: claimMed.id,
      status: claimMed.status,
      approved_by: claimMed.id,
      version: claimMed.version,
    });

    res.status(204).send(claimMed);
  }
);

export { router as approveMedClaimRouter };

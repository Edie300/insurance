import express, { Request, Response } from 'express';
import { NotFoundError, requireAuth } from '@totum-insurance/common';
import { ClaimFun } from '../models/claim-funeral';


const router = express.Router();

router.get(
  '/api/claims_funeral/:id',
  requireAuth,
  async (req: Request, res: Response) => {

    const claimFun = await ClaimFun.findById(req.params.id)
    .populate("account_fun")
    .populate("package_fun")
    .populate("company")
    .populate("branch")
    .populate("created_by");

    if (!claimFun) {
      throw new NotFoundError();
    }
  
    res.send(claimFun);
  });

export { router as showFunClaimRouter };

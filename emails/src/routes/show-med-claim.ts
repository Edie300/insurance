import express, { Request, Response } from 'express';
import { NotFoundError } from '@totum-insurance/common';
import { ClaimMed } from '../models/claim-medical';


const router = express.Router();

router.get(
  '/api/claims_medical/:id',

  async (req: Request, res: Response) => {

    const claimMed = await ClaimMed.findById(req.params.id)
    .populate("AccountMed")
    .populate("ProductMed")
    .populate("Company")
    .populate("Branch");

    if (!claimMed) {
      throw new NotFoundError();
    }
  
    res.send(claimMed);
  });

export { router as showMedClaimRouter };

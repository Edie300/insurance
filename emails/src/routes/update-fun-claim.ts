import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { ClaimFun } from "../models/claim-funeral";

import { ClaimFunUpdatedPublisher  } from "../events/publishers/claim-fun-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/claims_funeral/:id", async (req: Request, res: Response) => {
  const claimFun = await ClaimFun.findById(req.params.id);

  if (!claimFun) {
    throw new NotFoundError();
  }

  claimFun.set({
    attachments: req.body.attachments,
    claim_id: req.body.claim_id,
    claim_type: req.body.claim_type,
    deceased_type: req.body.deceased_type,
    deceased_name: req.body.deceased_name,
    deceased_surname: req.body.deceased_surname,
    deceased_title: req.body.deceased_title,
    deceased_date_of_birth: req.body.deceased_date_of_birth,
    deceased_email: req.body.deceased_email,
    deceased_address: req.body.deceased_address,
    deceased_cell: req.body.deceased_cell,
    deceased_national_id: req.body.deceased_national_id,
    deceased_passport: req.body.deceased_passport,
    deceased_relationship: req.body.deceased_relationship,
    date_of_death: req.body.date_of_death,
    cause_of_death: req.body.cause_of_death,
    date_of_funeral: req.body.date_of_funeral,
    place_of_burial: req.body.place_of_burial,
    claiment_name: req.body.claiment_name,
    claiment_surname: req.body.claiment_surname,
    claiment_title: req.body.claiment_title,
    claiment_date_of_birth: req.body.claiment_date_of_birth,
    claiment_email: req.body.claiment_email,
    claiment_address: req.body.claiment_address,
    claiment_cell: req.body.claiment_cell,
    claiment_national_id: req.body.claiment_national_id,
    claiment_passport: req.body.claiment_passport,
    claiment_relationship: req.body.claiment_relationship,
    bank_account_holder: req.body.bank_account_holder,
    bank_account_number: req.body.bank_account_number,
    bank_name: req.body.bank_name,
    bank_branch_code: req.body.bank_branch_code,
    bank_account_type: req.body.bank_account_type,
    status: req.body.status,
    sum_assured: req.body.sum_assured,
    processing_time: req.body.processing_time,
    version: req.body.version,
    created_at: req.body.created_at,
    created_by: req.body.created_by,
    package_fun: req.body.package_fun,
    account_fun: req.body.account_fun,
    company: req.body.company,
    branch: req.body.branch,
  });

  await claimFun.save();

  new ClaimFunUpdatedPublisher(natsWrapper.client).publish({
    id: claimFun.id,
    attachments: claimFun.attachments,
    claim_id: claimFun.claim_id,
    claim_type: claimFun.claim_type,
    deceased_type: claimFun.deceased_type,
    deceased_name: claimFun.deceased_name,
    deceased_surname: claimFun.deceased_surname,
    deceased_title: claimFun.deceased_title,
    deceased_date_of_birth: claimFun.deceased_date_of_birth,
    deceased_email: claimFun.deceased_email,
    deceased_address: claimFun.deceased_address,
    deceased_cell: claimFun.deceased_cell,
    deceased_national_id: claimFun.deceased_national_id,
    deceased_passport: claimFun.deceased_passport,
    deceased_relationship: claimFun.deceased_relationship,
    date_of_death: claimFun.date_of_death,
    cause_of_death: claimFun.cause_of_death,
    date_of_funeral: claimFun.date_of_funeral,
    place_of_burial: claimFun.place_of_burial,
    claiment_name: claimFun.claiment_name,
    claiment_surname: claimFun.claiment_surname,
    claiment_title: claimFun.claiment_title,
    claiment_date_of_birth: claimFun.claiment_date_of_birth,
    claiment_email: claimFun.claiment_email,
    claiment_address: claimFun.claiment_address,
    claiment_cell: claimFun.claiment_cell,
    claiment_national_id: claimFun.claiment_national_id,
    claiment_passport: claimFun.claiment_passport,
    claiment_relationship: claimFun.claiment_relationship,
    bank_account_holder: claimFun.bank_account_holder,
    bank_account_number: claimFun.bank_account_number,
    bank_name: claimFun.bank_name,
    bank_branch_code: claimFun.bank_branch_code,
    bank_account_type: claimFun.bank_account_type,
    status: claimFun.status,
    sum_assured: claimFun.sum_assured,
    processing_time: claimFun.processing_time,
    version: claimFun.version,
    account_main: claimFun.account_main,
    created_at: claimFun.created_at,
    created_by: claimFun.created_by,
    package_fun: claimFun.package_fun,
    account_fun: claimFun.account_fun,
    company: claimFun.company,
    branch: claimFun.branch,
  });

  res.send(claimFun);
});

export { router as updateFunProductRouter };

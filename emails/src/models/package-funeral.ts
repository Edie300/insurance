import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { PackageFunStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface PackageFunAttrs {
  id: string;
  attachments: {}[];
  name: string;
  description: string;
  policy_lapse_period: number;
  policy_archive_period: number;
  payment_grace_period: number;
  waiting_period: number;
  principal_member_min_age: number;
  principal_member_max_age: number;
  dependent_max_age: number;
  dependents_max_count: number;
  policy_maturity: number;
  child_max_age: number;
  child_max_age_school: number;
  sum_assured: number;
  hearse: boolean;
  bus: boolean;
  groceries: boolean;
  cash_in_lieu: number;
  accidental_death: boolean;
  mortuary_services: boolean;
  events_management: boolean;
  burial_services: boolean;
  premium_family: number;
  premium_dependent: number;
  currency: string;
  payment_type: number;
  status: PackageFunStatus;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface PackageFunModel extends mongoose.Model<PackageFunDoc> {
  build(attrs: PackageFunAttrs): PackageFunDoc;
}

// An interface that describes the properties
// that a User Document has
interface PackageFunDoc extends mongoose.Document {
  id: string;
  attachments: {}[];
  name: string;
  description: string;
  policy_lapse_period: number;
  policy_archive_period: number;
  payment_grace_period: number;
  waiting_period: number;
  principal_member_min_age: number;
  principal_member_max_age: number;
  dependent_max_age: number;
  dependents_max_count: number;
  policy_maturity: number;
  child_max_age: number;
  child_max_age_school: number;
  sum_assured: number;
  hearse: boolean;
  bus: boolean;
  groceries: boolean;
  cash_in_lieu: number;
  accidental_death: boolean;
  mortuary_services: boolean;
  events_management: boolean;
  burial_services: boolean;
  premium_family: number;
  premium_dependent: number;
  currency: string;
  payment_type: number;
  status: PackageFunStatus;
  company: Types.ObjectId;
  version: number;
}

const packageFunSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: true,
      trim: true,
    },

    description: {
      type: String,
      required: true,
    },

    // number is months for lapse and archive periods
    policy_lapse_period: { type: Number, default: 3 },
    policy_archive_period: { type: Number, default: 6 },
    payment_grace_period: { type: Number, default: 1 },
    waiting_period: { type: Number, default: 6 },

    // number is in years for ages
    principal_member_min_age: { type: Number, default: 18 },
    principal_member_max_age: { type: Number, default: null },
    dependent_max_age: { type: Number, default: null },
    dependents_max_count: { type: Number, default: null },
    policy_maturity: { type: Number, default: null },
    child_max_age: { type: Number, default: 18 },
    child_max_age_school: { type: Number, default: 21 },

    sum_assured: { type: Number },
    hearse: { type: Boolean, default: false },
    bus: { type: Boolean, default: false },
    groceries: { type: Number },
    cash_in_lieu: { type: Number },
    accidental_death: { type: Boolean, default: false },
    mortuary_services: { type: Boolean, default: false },
    events_management: { type: Boolean, default: false },
    burial_services: { type: Boolean, default: false },

    premium_family: { type: Number },
    premium_dependent: { type: Number },

    currency: {
      type: String,
      enum: ["ZW", "US", "ZAR"],
    },

    // 0: weekly , 1: monthly, 2: qouterly, 3: yearly
    payment_type: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(PackageFunStatus),
      default: PackageFunStatus.Unpublished,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

packageFunSchema.set("versionKey", "version");
packageFunSchema.plugin(updateIfCurrentPlugin);

packageFunSchema.statics.build = (attrs: PackageFunAttrs) => {
  return new PackageFun({
    _id: attrs.id,
    attachments: attrs.attachments,
    name: attrs.name,
    description: attrs.description,
    policy_lapse_period: attrs.policy_lapse_period,
    policy_archive_period: attrs.policy_archive_period,
    payment_grace_period: attrs.payment_grace_period,
    waiting_period: attrs.waiting_period,
    principal_member_min_age: attrs.principal_member_min_age,
    principal_member_max_age: attrs.principal_member_max_age,
    dependent_max_age: attrs.dependent_max_age,
    dependents_max_count: attrs.dependents_max_count,
    policy_maturity: attrs.policy_maturity,
    child_max_age: attrs.child_max_age,
    child_max_age_school: attrs.child_max_age_school,
    sum_assured: attrs.sum_assured,
    hearse: attrs.hearse,
    bus: attrs.bus,
    groceries: attrs.groceries,
    cash_in_lieu: attrs.cash_in_lieu,
    accidental_death: attrs.accidental_death,
    mortuary_services: attrs.mortuary_services,
    events_management: attrs.events_management,
    burial_services: attrs.burial_services,
    premium_family: attrs.premium_family,
    premium_dependent: attrs.premium_dependent,
    currency: attrs.currency,
    payment_type: attrs.payment_type,
    status: attrs.status,
    company: attrs.company,
  });
};

const PackageFun = mongoose.model<PackageFunDoc, PackageFunModel>(
  "PackageFun",
  packageFunSchema
);

export { PackageFun };

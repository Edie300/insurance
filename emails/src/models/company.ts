import mongoose from "mongoose";
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';
import { CompanyStatus} from "@totum-insurance/common"



// An interface that describes the properties
// that are requried to create a new User
interface CompanyAttrs {
  id: string;
  logo: {};
  name: string;
  tax_number: string;
  phones: string[];
  landlines: string[];
  email: string;
  address: string;
  insurance: number[];
  // status: CompanyStatus;
}

// An interface that describes the properties
// that a User Model has
interface CompanyModel extends mongoose.Model<CompanyDoc> {
  build(attrs: CompanyAttrs): CompanyDoc;
}

// An interface that describes the properties
// that a User Document has
export interface CompanyDoc extends mongoose.Document {
  logo: {};
  name: string;
  tax_number: string;
  phones: string[];
  landlines: string[];
  email: string;
  address: string;
  insurance: number[];
  version: number;
  // status: CompanyStatus;
}

const companySchema = new mongoose.Schema(
  {
    logo: {},
    name: {
      type: String,
      required: true,
    },
    tax_number: String,
    phones: [String],
    landlines:  [String],
    email: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    insurance:[Number],
    // status: {
    //   type: String,
    //   enum: Object.values(CompanyStatus),
    //   default: CompanyStatus.Created,
    // },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

companySchema.set('versionKey', 'version');
companySchema.plugin(updateIfCurrentPlugin);

companySchema.statics.build = (attrs: CompanyAttrs) => {
  return new Company({
    _id: attrs.id,
    logo: attrs.logo,
    name: attrs.name,
    tax_number: attrs.tax_number,
    phones: attrs.phones,
    landlines: attrs.landlines,
    email: attrs.email,
    address: attrs.address,
    insurance: attrs.insurance
  });
};

const Company = mongoose.model<CompanyDoc, CompanyModel>(
  "Company",
  companySchema
);

export { Company };

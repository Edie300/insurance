import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  PackageMedUpdatedEvent,
} from "@totum-insurance/common";
import { PackageMed } from "../../models/package-medical";
import { queueGroupName } from "./queue-group-name";

export class PackageMedUpdatedListener extends Listener<PackageMedUpdatedEvent> {
  subject: Subjects.PackageMedUpdated = Subjects.PackageMedUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageMedUpdatedEvent["data"], msg: Message) {
    console.log("package-med-updated-listener::", data);
    const {      
      id,
      attachments,
      name,
      description,
      annual_global_limit,
      general_practitioners,
      number_of_initial_visits,
      medical_specialist,
      hospitalization_limit,
      waiting_time,
      claim_processing_time,
      private_hospitalization,
      private_hospitalization_CV19,
      public_hospitalization,
      ambulance_services,
      air_evacuation,
      number_of_dependents,
      age_limit_max,
      age_limit_min,
      premium_add,
      drugs_3ben,
      drugs_abv_3ben,
      chronic_drug_limit,
      drugs_cv19,
      drugs_acute,
      dental,
      optical,
      psychiatric,
      physiotherapy,
      prosthetics,
      pathology,
      specialized_radiology,
      blood_transfusion,
      maternity,
      family_planning,
      infertility,
      hearing_aids,
      hearing_scan,
      funeral_cashback,
      premium_waver,
      wellness,
      tandCs,
      policy_maturity,
      premium_main,
      premium_adult,
      premium_child,
      waiting_period,
      currency,
      payment_type,
      status,
      company,
      branch } = data;
    const packageMed = await PackageMed.findById(id);

    if (!packageMed) {
      throw new Error("package medical not found");
    }

    packageMed.set({
      id,
      attachments,
      name,
      description,
      annual_global_limit,
      general_practitioners,
      number_of_initial_visits,
      medical_specialist,
      hospitalization_limit,
      waiting_time,
      claim_processing_time,
      private_hospitalization,
      private_hospitalization_CV19,
      public_hospitalization,
      ambulance_services,
      air_evacuation,
      number_of_dependents,
      age_limit_max,
      age_limit_min,
      premium_add,
      drugs_3ben,
      drugs_abv_3ben,
      chronic_drug_limit,
      drugs_cv19,
      drugs_acute,
      dental,
      optical,
      psychiatric,
      physiotherapy,
      prosthetics,
      pathology,
      specialized_radiology,
      blood_transfusion,
      maternity,
      family_planning,
      infertility,
      hearing_aids,
      hearing_scan,
      funeral_cashback,
      premium_waver,
      wellness,
      tandCs,
      policy_maturity,
      premium_main,
      premium_adult,
      premium_child,
      waiting_period,
      currency,
      payment_type,
      status,
      company,
      branch
    });
    await packageMed.save();

    msg.ack();
  }
}

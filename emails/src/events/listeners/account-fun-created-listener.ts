import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountFunCreatedEvent,
} from "@totum-insurance/common";
import { AccountFun } from "../../models/account-funeral";
import { queueGroupName } from "./queue-group-name";

export class AccountFunCreatedListener extends Listener<AccountFunCreatedEvent> {
  subject: Subjects.AccountFunCreated = Subjects.AccountFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunCreatedEvent["data"], msg: Message) {
    console.log("account-fun-created-listener::", data);
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
    } = data;

    const accountFun = AccountFun.build({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by
    });
    await accountFun.save();

    msg.ack();
  }
}

import { Publisher, Subjects, ClaimFunRejectedEvent } from "@totum-insurance/common";

export class ClaimFunRejectedPublisher extends Publisher<ClaimFunRejectedEvent> {
  subject: Subjects.ClaimFunRejected = Subjects.ClaimFunRejected;
}

import { Publisher, Subjects, ClaimMedRejectedEvent } from "@totum-insurance/common";

export class ClaimMedRejectedPublisher extends Publisher<ClaimMedRejectedEvent> {
  subject: Subjects.ClaimMedRejected = Subjects.ClaimMedRejected;
}

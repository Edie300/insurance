import mongoose from "mongoose";
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';
import { CompanyStatus} from "@totum-insurance/common"



// An interface that describes the properties
// that are requried to create a new User
interface CompanyAttrs {
  logo: {};
  name: string;
  tax_number: string;
  phones: string[];
  landlines: string[];
  email: string;
  address: string;
  insurance: number[];
  type: number;
  status: CompanyStatus;
}

// An interface that describes the properties
// that a User Model has
interface CompanyModel extends mongoose.Model<CompanyDoc> {
  build(attrs: CompanyAttrs): CompanyDoc;
}

// An interface that describes the properties
// that a User Document has
export interface CompanyDoc extends mongoose.Document {
  logo: {};
  name: string;
  tax_number: string;
  phones: string[];
  landlines: string[];
  email: string;
  address: string;
  insurance: number[];
  type: number;
  status: CompanyStatus;
  version: number;
}

const companySchema = new mongoose.Schema(
  {
    logo: {},
    name: {
      type: String,
      required: true,
    },
    tax_number: String,
    phones: [String],
    landlines:  [String],
    email: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    insurance:[Number],
    type: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(CompanyStatus),
      default: CompanyStatus.Created,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

companySchema.set('versionKey', 'version');
companySchema.plugin(updateIfCurrentPlugin);

companySchema.statics.build = (attrs: CompanyAttrs) => {
  return new Company(attrs);
};

const Company = mongoose.model<CompanyDoc, CompanyModel>(
  "Company",
  companySchema
);

export { Company };

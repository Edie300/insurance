import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  BranchStatus,
} from "@totum-insurance/common";

import { Branch } from "../models/branch";
import { Company } from "../models/company";

import { BranchCreatedPublisher } from "../events/publishers/branch-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post("/api/branches/create", async (req: Request, res: Response) => {
  const { name, phones, landlines, address, company } = req.body;

  // check if company exist

  const getCompany = await Company.findById(company);

  if (!getCompany) {
    throw new NotFoundError();
  }

  const branch = Branch.build({
    name,
    phones,
    landlines,
    address,
    status: BranchStatus.Created,
    company,
  });

  await branch.save();

  new BranchCreatedPublisher(natsWrapper.client).publish({
    id: branch.id,
    name: branch.name,
    phones: branch.phones,
    landlines: branch.landlines,
    address: branch.address,
    status: branch.status,
    company: branch.company,
    version: branch.version
  });


  res.status(201).send(branch);
});

export { router as createBranchRouter };

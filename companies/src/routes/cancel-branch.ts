import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
} from '@totum-insurance/common';

import { Company } from '../models/company';

const router = express.Router();

router.delete('/api/companies/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const company = await Company.findById(id);

    if (!company) {
      throw new NotFoundError();
    }

    company.status = 3;
    await company.save();

    // company account suspended
    console.log(company)

    res.status(204).send(company);
  }
);

export { router as cancelBranchRouter };

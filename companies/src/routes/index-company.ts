import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";

import { Company } from "../models/company";

const router = express.Router();

router.get(
  "/api/companies",

  async (req: Request, res: Response) => {
    const companies = await Company.find();
    res.send(companies);
  }
);

export { router as indexCompanyRouter };

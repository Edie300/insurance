import express, { Request, Response } from 'express';
import { NotFoundError } from '@totum-insurance/common';
import { Branch } from '../models/branch';


const router = express.Router();

router.get(
  '/api/branches/:id',

  async (req: Request, res: Response) => {

    const branch = await Branch.findById(req.params.id).populate('company');

    if (!branch) {
      throw new NotFoundError();
    }
  
    res.send(branch);
  });

export { router as showBranchRouter };

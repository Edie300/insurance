import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  BadRequestError,
  requireAuth,
} from "@totum-insurance/common";
import { Branch } from "../models/branch";

const router = express.Router();

router.get(
  "/api/branches",
  requireAuth,

  async (req: Request, res: Response) => {
    const branches = await Branch.find().populate("company");
    res.send(branches);
  }
);

export { router as indexBranchRouter };

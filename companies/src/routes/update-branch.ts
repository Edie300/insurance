import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { Branch } from "../models/branch";

import { BranchUpdatedPublisher } from '../events/publishers/branch-updated-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.put("/api/branches/:id", async (req: Request, res: Response) => {
  const branch = await Branch.findById(req.params.id);

  if (!branch) {
    throw new NotFoundError();
  }

  branch.set({
    name: req.body.name,
    phones: req.body.phones,
    landlines: req.body.landlines,
    address: req.body.address,
    status: req.body.status,
    company: req.body.company
  });

  await branch.save();

  new BranchUpdatedPublisher(natsWrapper.client).publish({
    id: branch.id,
    name: branch.name,
    phones: branch.phones,
    landlines: branch.landlines,
    address: branch.address,
    status: branch.status,
    company: branch.company,
    version: branch.version,
  });

  res.send(branch);
});

export { router as updateBranchRouter };

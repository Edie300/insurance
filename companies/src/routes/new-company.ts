import express, { Request, Response } from "express";
import { CompanyStatus } from "@totum-insurance/common";

import { CompanyCreatedPublisher } from "../events/publishers/company-created-publisher";
import { natsWrapper } from "../nats-wrapper";

import { Company } from "../models/company";

const router = express.Router();

router.post("/api/companies/create", async (req: Request, res: Response) => {
  const {
    logo,
    name,
    tax_number,
    phones,
    landlines,
    email,
    address,
    insurance,
    type,
  } = req.body;

  const company = Company.build({
    logo,
    name,
    tax_number,
    phones,
    landlines,
    email,
    address,
    insurance,
    type,
    status: CompanyStatus.Created,
  });

  await company.save();

  new CompanyCreatedPublisher(natsWrapper.client).publish({
    id: company.id,
    logo: company.logo,
    name: company.name,
    tax_number: company.tax_number,
    phones: company.phones,
    landlines: company.landlines,
    email: company.email,
    address: company.address,
    insurance: company.insurance,
    type: company.type,
    status: company.status, 
    version: company.version
  });

  res.status(201).send(company);
});

export { router as createCompanyRouter };

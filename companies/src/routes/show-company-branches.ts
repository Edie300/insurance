import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { Branch } from "../models/branch";

const router = express.Router();

router.get(
  "/api/branches/companies/:CompanyId",
  requireAuth,

  async (req: Request, res: Response) => {
    const branches = await Branch.find({ company: req.params.CompanyId }).populate(
      "company"
    );

    res.send(branches);
  }
);

export { router as showCompanyBranchesRouter };

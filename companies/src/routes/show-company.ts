import express, { Request, Response } from 'express';
import { NotFoundError } from '@totum-insurance/common';
import { Company } from '../models/company';


const router = express.Router();

router.get(
  '/api/companies/:id',

  async (req: Request, res: Response) => {

    const company = await Company.findById(req.params.id);

    if (!company) {
      throw new NotFoundError();
    }
  
    res.send(company);
  });

export { router as showCompanyRouter };

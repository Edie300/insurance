import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { CompanyUpdatedPublisher } from '../events/publishers/company-updated-publisher';
import { natsWrapper } from '../nats-wrapper';

import { Company } from "../models/company";

const router = express.Router();

router.put("/api/companies/:id", async (req: Request, res: Response) => {
  const company = await Company.findById(req.params.id);

  if (!company) {
    throw new NotFoundError();
  }

  company.set({
    logo: req.body.logo,
    name: req.body.name,
    tax_number: req.body.tax_number,
    phones: req.body.phones,
    landlines: req.body.landlines,
    email: req.body.email,
    address: req.body.address,
    insurance: req.body.insurance,
    type: req.body.type,
    status: req.body.status,
  });

  await company.save();
  new CompanyUpdatedPublisher(natsWrapper.client).publish({
    id: company.id,
    logo: company.logo,
    name: company.name,
    tax_number: company.tax_number,
    phones: company.phones,
    landlines: company.landlines,
    email: company.email,
    address: company.address,
    insurance: company.insurance,
    type: company.type,
    status: company.status,
    version: company.version,
  });

  res.send(company);
});

export { router as updateCompanyRouter };

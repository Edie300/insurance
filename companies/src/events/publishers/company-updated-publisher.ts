import { Publisher, Subjects, CompanyUpdatedEvent } from "@totum-insurance/common";

export class CompanyUpdatedPublisher extends Publisher<CompanyUpdatedEvent> {
  subject: Subjects.CompanyUpdated = Subjects.CompanyUpdated;
}


import { Publisher, Subjects, BranchCreatedEvent } from "@totum-insurance/common";

export class BranchCreatedPublisher extends Publisher<BranchCreatedEvent> {
  subject: Subjects.BranchCreated = Subjects.BranchCreated;
}

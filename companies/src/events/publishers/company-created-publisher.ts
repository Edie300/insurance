import { Publisher, Subjects, CompanyCreatedEvent } from "@totum-insurance/common";

export class CompanyCreatedPublisher extends Publisher<CompanyCreatedEvent> {
  subject: Subjects.CompanyCreated = Subjects.CompanyCreated;
}

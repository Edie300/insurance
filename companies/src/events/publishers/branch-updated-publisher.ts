import { Publisher, Subjects, BranchUpdatedEvent } from "@totum-insurance/common";

export class BranchUpdatedPublisher extends Publisher<BranchUpdatedEvent> {
  subject: Subjects.BranchUpdated = Subjects.BranchUpdated;
}


import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import cors from "cors";
import { errorHandler, NotFoundError } from '@totum-insurance/common';

import { cancelCompanyRouter} from './routes/cancel-company';
import { indexCompanyRouter } from './routes/index-company';
import { createBranchRouter } from './routes/new-branch';
import { showCompanyRouter } from './routes/show-company';
import { updateCompanyRouter } from './routes/update-company';
import { removeCompanyImageRouter } from './routes/remove-company-logo';
import { uploadCompanyImageRouter } from './routes/upload-company-logo';
import {cancelBranchRouter} from './routes/cancel-branch';
import { indexBranchRouter } from './routes/index-branch';
import { createCompanyRouter} from './routes/new-company';
import { showBranchRouter } from './routes/show-branch';
import { showCompanyBranchesRouter } from './routes/show-company-branches';
import { updateBranchRouter } from './routes/update-branch';

const app = express();

// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
);

app.use(cancelCompanyRouter);
app.use(indexCompanyRouter);
app.use(createCompanyRouter);
app.use(showCompanyRouter);
app.use(updateCompanyRouter);
app.use(removeCompanyImageRouter);
app.use(uploadCompanyImageRouter);
app.use(cancelBranchRouter);
app.use(indexBranchRouter);
app.use(createBranchRouter);
app.use(showBranchRouter);
app.use(showCompanyBranchesRouter);
app.use(updateBranchRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

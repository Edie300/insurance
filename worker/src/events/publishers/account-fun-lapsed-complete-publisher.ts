import { Publisher, Subjects, AccountMainElapsedEvent } from "@totum-insurance/common";

export class FunAccountElapsedComplete extends Publisher<AccountMainElapsedEvent> {
  subject: Subjects.AccountMainElapsed = Subjects.AccountMainElapsed;
}

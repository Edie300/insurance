import { Publisher, Subjects, AccountFunWaitingCompleteEvent } from "@totum-insurance/common";

export class FunAccountWaitingComplete extends Publisher<AccountFunWaitingCompleteEvent> {
  subject: Subjects.AccountFunWaitingComplete = Subjects.AccountFunWaitingComplete;
}

import { Publisher, Subjects, AccountFunArchivedEvent } from "@totum-insurance/common";

export class FunAccountArchivedComplete extends Publisher<AccountFunArchivedEvent> {
  subject: Subjects.AccountFunArchivedComplete = Subjects.AccountFunArchivedComplete;
}

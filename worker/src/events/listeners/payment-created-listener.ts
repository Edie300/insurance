import { Message } from "node-nats-streaming";
import { Subjects, Listener, PaymentCreatedEvent } from "@totum-insurance/common";
import { queueGroupName } from "./queue-group-name";
import { funAccountWaitingQueue } from '../../queues/fun-account-waiting-queue';

export class PaymentCreatedListener extends Listener<PaymentCreatedEvent> {
  subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PaymentCreatedEvent["data"], msg: Message) {
    console.log("payment-created-listener::", data);
    const delay = 3600;
    console.log('Waiting this many milliseconds to process the job:', delay);

    await funAccountWaitingQueue.add(
      {
        id: data.id,
        version: data.version
      },
      {
        delay,
      }
    );

    msg.ack();
  }
}

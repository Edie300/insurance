import { Message } from "node-nats-streaming";
// import moment from "moment";
import {
  Subjects,
  Listener,
  AccountFunCreatedEvent,
} from "@totum-insurance/common";
import { PackageFun} from "../../models/package-funeral"
import { queueGroupName } from "./queue-group-name";
import { funAccountWaitingQueue } from '../../queues/fun-account-waiting-queue';

export class AccountFunCreatedListener extends Listener<AccountFunCreatedEvent> {
  subject: Subjects.AccountFunCreated = Subjects.AccountFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunCreatedEvent["data"], msg: Message) {
    console.log("account-fun-created-listener::", data);

    const {created_at, package_fun} = data

    const packageFun = await PackageFun.findById(package_fun);

    if (!packageFun) {
      throw new Error("package funeral not found");
    }

    const delay = packageFun.waiting_period * 60000;
    console.log('delay for waiting period', delay);

    await funAccountWaitingQueue.add(
      {
        id: data.id,
        version: data.version
      },
      {
        delay,
      }
    );
    msg.ack();
  }
}

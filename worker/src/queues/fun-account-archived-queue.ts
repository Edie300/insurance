import Queue from 'bull';
import { FunAccountWaitingComplete } from '../events/publishers/account-fun-waiting-complete-publisher';
import { natsWrapper } from '../nats-wrapper';

interface Payload {
  id: string;
  version: number;
}

const funAccountWaitingQueue = new Queue<Payload>('fun-account:waiting-period', {
  redis: {
    host: process.env.REDIS_HOST,
  },
});

funAccountWaitingQueue.process(async (job) => {
  new FunAccountWaitingComplete(natsWrapper.client).publish({
    id: job.data.id,
    version: job.data.version
  });
});

export { funAccountWaitingQueue };

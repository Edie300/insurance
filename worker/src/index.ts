import mongoose from "mongoose";
import { natsWrapper } from './nats-wrapper';
import { AccountFunCreatedListener } from "./events/listeners/account-fun-created-listener";
import { PaymentCreatedListener } from "./events/listeners/payment-created-listener";
import { BranchCreatedListener } from "./events/listeners/branch-created-listener";
import { BranchUpdatedListener } from "./events/listeners/branch-updated-listener";
import { CasketCreatedListener } from "./events/listeners/casket-created-listener";
import { CasketUpdatedListener } from "./events/listeners/casket-updated-listener";
import { CompanyCreatedListener } from "./events/listeners/company-created-listener";
import { CompanyUpdatedListener } from "./events/listeners/company-updated-listener";
import { PackageFunCreatedListener } from "./events/listeners/package-fun-created-listener";
import { PackageFunUpdatedListener } from "./events/listeners/package-fun-updated-listener";
import { PackageMedCreatedListener } from "./events/listeners/package-med-created-listener";
import { PackageMedUpdatedListener } from "./events/listeners/package-med-updated-listener";
import { PackageMotorCreatedListener } from "./events/listeners/package-motor-created-listener";
import { PackageMotorUpdatedListener } from "./events/listeners/package-motor-updated-listener";
import { UserCreatedListener } from "./events/listeners/user-created-listener";

const start = async () => {

  if (!process.env.NATS_URL) {
    throw new Error("NATS_URL must be defined.");
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error("NATS_CLUSTER_ID must be defined.");
  }
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error("NATS_CLIENT_ID must be defined.");
  }
  if (!process.env.MONGO_URI) {
    throw new Error("MONGO_URI must be defined");
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );

    // Following lines are required for gracefully shutting down client
    natsWrapper.client.on("close", () => {
      console.log("NATS connection closed");
      process.exit();
    });
    process.on("SIGINT", () => natsWrapper.client.close());
    process.on("SIGTERM", () => natsWrapper.client.close());

    new AccountFunCreatedListener(natsWrapper.client).listen();
    new PaymentCreatedListener(natsWrapper.client).listen();
    new BranchCreatedListener(natsWrapper.client).listen();
    new BranchUpdatedListener(natsWrapper.client).listen();
    new CasketCreatedListener(natsWrapper.client).listen();
    new CasketUpdatedListener(natsWrapper.client).listen();
    new CompanyCreatedListener(natsWrapper.client).listen();
    new CompanyUpdatedListener(natsWrapper.client).listen();
    new PackageFunCreatedListener(natsWrapper.client).listen();
    new PackageFunUpdatedListener(natsWrapper.client).listen();
    new PackageMedCreatedListener(natsWrapper.client).listen();
    new PackageMedUpdatedListener(natsWrapper.client).listen();
    new PackageMotorCreatedListener(natsWrapper.client).listen();
    new PackageMotorUpdatedListener(natsWrapper.client).listen();
    new UserCreatedListener(natsWrapper.client).listen();

    await mongoose.connect(process.env.MONGO_URI);
    console.log("Connected to MongoDb");

  } catch (err) {
    console.error(err);
  }


};

start();

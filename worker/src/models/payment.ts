import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { PaymentStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface PaymentAttrs {
  id: string;
  payment_id: string;
  type: number;
  method: string;
  amount: number;
  payer_name: string;
  payer_surname: string;
  payer_phone: string;
  currency: string;
  premium: number;
  balance_before: number;
  balance_after: number;
  created_at: Date;
  created_by: Types.ObjectId;
  status: PaymentStatus;
  account_main: Types.ObjectId;
  account: Types.ObjectId;
  account_package: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;

}

// An interface that describes the properties
// that a User Model has
interface PaymentModel extends mongoose.Model<PaymentDoc> {
  build(attrs: PaymentAttrs): PaymentDoc;
  findByEvent(event: {
    id: string;
    version: number;
  }): Promise<PaymentDoc | null>;
}

// An interface that describes the properties
// that a User Document has
interface PaymentDoc extends mongoose.Document {
  id: string;
  payment_id: string;
  type: number;
  method: string;
  amount: number;
  payer_name: string;
  payer_surname: string;
  payer_phone: string;
  currency: string;
  premium: number;
  balance_before: number;
  balance_after: number;
  created_at: Date;
  created_by: Types.ObjectId;
  status: PaymentStatus;
  account_main: Types.ObjectId;
  account: Types.ObjectId;
  account_package: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const paymentSchema = new mongoose.Schema(
  {
    payment_id: {
      type: String,
      required: true,
    },
    type: {
      type: Number,
      required: true,
    },
    method: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    payer_name: {
      type: String,
      required: true,
    },
    payer_surname: {
      type: String,
    },
    payer_phone: {
      type: String,
    },
    currency: {
      type: String,
      required: true,
    },
    premium: {
      type: Number,
      required: true,
    },
    balance_before: {
      type: Number,
      required: true,
    },
    balance_after: {
      type: Number,
      required: true,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    account: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    account_package: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(PaymentStatus),
      default: PaymentStatus.Success,
    }
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

paymentSchema.set("versionKey", "version");
paymentSchema.plugin(updateIfCurrentPlugin);

paymentSchema.statics.findByEvent = (event: { id: string; version: number }) => {
  return Payment.findOne({
    _id: event.id,
    version: event.version - 1,
  });
};

paymentSchema.statics.build = (attrs: PaymentAttrs) => {
  return new Payment({
    id: attrs.id,
    payment_id: attrs.payment_id,
    type: attrs.type,
    method: attrs.method,
    amount: attrs.amount,
    payer_name: attrs.payer_name,
    payer_surname: attrs.payer_surname,
    payer_phone: attrs.payer_phone,
    currency: attrs.currency,
    premium: attrs.premium,
    balance_before: attrs.balance_before,
    balance_after: attrs.balance_after,
    created_at: attrs.created_at,
    created_by: attrs.created_by,
    status: attrs.status,
    account_main: attrs.account_main,
    account: attrs.account,
    account_package: attrs.account_package,
    company: attrs.company,
    branch: attrs.branch, 
  });
};

const Payment = mongoose.model<PaymentDoc, PaymentModel>(
  "Payment",
  paymentSchema
);

export { Payment };

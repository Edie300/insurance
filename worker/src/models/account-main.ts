import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";

// An interface that describes the properties
// that are requried to create a new User
interface AcountMainAttrs {
  attachments: {}[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  email: string;
  phone: string;
  address: string;
  account_id: string;
  payment_status: number;
  status: number;
  created_at: Date;
  created_by: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
}

// An interface that describes the properties

interface AccountMainModel extends mongoose.Model<AccountMainDoc> {
  build(attrs: AcountMainAttrs): AccountMainDoc;
}

interface AccountMainDoc extends mongoose.Document {
  id: string;
  attachments: {}[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  email: string;
  phone: string;
  address: string;
  account_id: string;
  payment_status: number;
  created_at: Date;
  created_by: Types.ObjectId;
  status: number;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const accountMainSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    gender: {
      type: String,
      enum: ["male", "female"],
      required: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
    },
    marital_status: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    national_id: {
      type: String,
      required: true,
    },
    passport: {
      type: String,
    },
    drivers_licence: {
      type: String,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    account_id: {
      type: String,
      required: true,
    },
    status: {
      type: Number,
      default: 0,
    },
    payment_status: {
      type: Number,
      default: 0,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
  },
  {
    toJSON: {
      virtuals: true,
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
    toObject: { getters: true, virtuals: true },
  }
);

accountMainSchema.set("versionKey", "version");
accountMainSchema.plugin(updateIfCurrentPlugin);

accountMainSchema.statics.build = (attrs: AcountMainAttrs) => {
  return new AccountMain(attrs);
};

const AccountMain = mongoose.model<AccountMainDoc, AccountMainModel>(
  "AccountMain",
  accountMainSchema
);

// Reverse populate with virtuals
accountMainSchema.virtual("funeral_accounts", {
  ref: "AccountFun",
  localField: "_id",
  foreignField: "account_main",
  justOne: false,
});

export { AccountMain };

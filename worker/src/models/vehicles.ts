import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";

// An interface that describes the properties
// that are requried to create a new User
interface VehicleAttrs {
  attachments: {}[];
  make: string;
  model: string;
  year: Date;
  reg_number: string;
  color: string;
  engine_number: string;
  chassis_number: string;
  tax_class: string;
  body_type: string;
  principal_driver: string;
  other_drivers: [
    {
      driver_name: string;
      driver_surname: string;
      drivers_licence: string;
      driver_id: string;
      driver_cell: string;
      driver_email: string;
      driver_address: string;
      driver_occupation: string;
      relationship: string;
      status: number;
    }
  ];

  status: number;
  account: Types.ObjectId;
  company: Types.ObjectId;
  package_motor: Types.ObjectId;
}

// An interface that describes the properties

interface VehicleModel extends mongoose.Model<VehicleDoc> {
  build(attrs: VehicleAttrs): VehicleDoc;
}

interface VehicleDoc extends mongoose.Document {
  attachments: {}[];
  make: string;
  model: string;
  year: Date;
  reg_number: string;
  color: string;
  engine_number: string;
  chassis_number: string;
  tax_class: string;
  body_type: string;
  principal_driver: string;
  other_drivers: [
    {
      driver_name: string;
      driver_surname: string;
      drivers_licence: string;
      driver_id: string;
      driver_cell: string;
      driver_email: string;
      driver_address: string;
      driver_occupation: string;
      relationship: string;
      status: number;
    }
  ];

  status: number;
  account: Types.ObjectId;
  company: Types.ObjectId;
  package_motor: Types.ObjectId;
  version: number;
}

const vehicleSchema = new mongoose.Schema(
  {
    attachments: [],
    make: {
      type: String,
      required: true,
    },
    model: {
      type: String,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
    reg_number: {
      type: String,
      required: true,
    },
    color: {
      type: String,
      required: true,
    },
    engine_number: {
      type: String,
      required: true,
    },
    chassis_number: {
      type: String,
      required: true,
    },
    tax_class: {
      type: Number,
      required: true,
    },
    body_type: {
      type: String,
      required: true,
    },
    principal_driver: {
      type: String,
      required: true,
    },
    other_drivers: [
      {
        driver_name: {
          type: String,
        },
        driver_surname: {
          type: String,
        },
        driver_id: {
          type: String,
        },
        driver_cell: {
          type: String,
        },
        driver_email: {
          type: String,
        },
        driver_address: {
          type: String,
        },
        driver_occupation: {
          type: String,
        },
        relationship: {
          type: String,
        },
      },
    ],
    status: {
      type: Number,
      default: 0,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    account: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMotor",
      required: true,
    },
    package_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageMotor",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

vehicleSchema.set("versionKey", "version");
vehicleSchema.plugin(updateIfCurrentPlugin);

vehicleSchema.statics.build = (attrs: VehicleAttrs) => {
  return new Vehicle(attrs);
};

const Vehicle = mongoose.model<VehicleDoc, VehicleModel>(
  "Vehicle",
  vehicleSchema
);

export { Vehicle };

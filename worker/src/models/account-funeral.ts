import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { AccountFunStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface AcountFunAttrs {
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountFunStatus;
  package_fun: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
}

// An interface that describes the properties

interface AccountFunModel extends mongoose.Model<AccountFunDoc> {
  build(attrs: AcountFunAttrs): AccountFunDoc;
}

export interface AccountFunDoc extends mongoose.Document {
  id: string;
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountFunStatus;
  package_fun: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
  version: number;
}

const accountFunSchema = new mongoose.Schema(
  {
    attachments: [],
    policy_id: {
      type: String,
      required: true,
    },

    balance: {
      type: Number,
      default: 0,
    },
    premium: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(AccountFunStatus),
      default: AccountFunStatus.Pending,
    },
    payment_status: {
      type: Number,
      default: 0,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    package_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageFun",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        ret.type = 0;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

accountFunSchema.set("versionKey", "version");
accountFunSchema.plugin(updateIfCurrentPlugin);

accountFunSchema.statics.build = (attrs: AcountFunAttrs) => {
  return new AccountFun(attrs);
};

const AccountFun = mongoose.model<AccountFunDoc, AccountFunModel>(
  "AccountFun",
  accountFunSchema
);

export { AccountFun };

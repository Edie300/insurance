import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  CompanyUpdatedEvent,
} from "@totum-insurance/common";
import { Company } from "../../models/company";
import { queueGroupName } from "./queue-group-name";

export class CompanyUpdatedListener extends Listener<CompanyUpdatedEvent> {
  subject: Subjects.CompanyUpdated = Subjects.CompanyUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: CompanyUpdatedEvent["data"], msg: Message) {
    console.log("company-updated-listener::", data)
    const {
      id,
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      status,
      type,
      insurance,
    } = data;
    const company = await Company.findById(id);

    if (!company) {
      msg.ack();
      throw new Error("company not found");
    }

    company.set({
      logo,
      name,
      tax_number,
      phones,
      landlines,
      email,
      address,
      status,
      type,
      insurance,
    });
    await company.save();

    msg.ack();
  }
}

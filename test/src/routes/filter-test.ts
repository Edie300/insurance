import express, { Request, Response } from "express";

const router = express.Router();

interface Query {
  limit?: string
}

router.get(
  "/api/test/create",

  async (req: Request, res: Response) => {

    let query;

    // Copy req.query
    const reqQuery = { ...req.query };
  
    // Fields to exclude
    const removeFields = ["select", "sort", "page", "limit"];
  
    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach((param) => delete reqQuery[param]);
  
    // Create query string
    let queryStr = JSON.stringify(reqQuery);
  
    // Create operators ($gt, $gte, etc)
    queryStr = queryStr.replace(
      /\b(gt|gte|lt|lte|in)\b/g,
      (match) => `$${match}`
    );
  
    queryStr = JSON.parse(queryStr);
  

    res.status(201).send(queryStr);
  }
);

export { router as testRouter };

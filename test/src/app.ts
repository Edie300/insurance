import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import cors from "cors";
import { errorHandler, NotFoundError } from "@totum-insurance/common";

// import { currentUserRouter } from "./routes/current-user";
import { testRouter } from "./routes/filter-test";


const app = express();

// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    signed: false,
    secure: false,
  })
);


app.use(testRouter);


app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

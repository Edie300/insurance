import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { UserStatus } from "@totum-insurance/common";

import { Password } from "../services/password";
import { CompanyDoc } from "./company";
import { BranchDoc } from "./branch";

// An interface that describes the properties
// that are requried to create a new User
interface UserAttrs {
  name: string;
  surname: string;
  employee_id: string;
  gender: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  date_of_birth: Date;
  phone: string;
  address: string;
  email: string;
  department: string;
  password: string;
  role: string;
  permissions_accounts: string[];
  permissions_packages: string[];
  permissions_claims: string[];
  permissions_users: string[];
  status: UserStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
  id: string;
  name: string;
  surname: string;
  employee_id: string;
  gender: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  date_of_birth: Date;
  phone: string;
  address: string;
  email: string;
  department: string;
  password: string;
  role: string;
  permissions_accounts: string[];
  permissions_packages: string[];
  permissions_claims: string[];
  permissions_users: string[];
  status: UserStatus;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    employee_id: {
      type: String,
    },
    gender: {
      type: String,
      enum: ["male", "female"],
      required: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
    },
    title: {
      type: String,
    },
    national_id: {
      type: String,
    },

    passport: {
      type: String,
    },

    drivers_licence: {
      type: String,
    },

    phone: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    department: {
      type: String,
      required: true,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    role: {
      type: String,
    },
    permissions_accounts: [String],
    permissions_packages: [String],
    permissions_claims: [String],
    permissions_users: [String],
    password: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(UserStatus),
      default: UserStatus.Created,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
        delete ret.__v;
      },
    },
  }
);

userSchema.set("versionKey", "version");
userSchema.plugin(updateIfCurrentPlugin);

userSchema.pre("save", async function (done) {
  if (this.isModified("password")) {
    const hashed = await Password.toHash(this.get("password"));
    this.set("password", hashed);
  }
  done();
});

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User(attrs);
};

const User = mongoose.model<UserDoc, UserModel>("User", userSchema);

export { User };

import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import cors from "cors";
import { errorHandler, NotFoundError } from "@totum-insurance/common";


import { approveMainAccountRouter } from "./routes/approve-main-account";
import { deleteFunAccountRouter } from "./routes/delete-fun-account";
import { deleteMainAccountRouter } from "./routes/delete-main-account";
import { deleteMedAccountRouter } from "./routes/delete-med-account";
import { indexSubAccountRouter } from "./routes/index-sub-account";
import { indexFunAccountRouter } from "./routes/index-fun-account";
import { indexMainAccountRouter } from "./routes/index-main-account";
import { indexMedAccountRouter } from "./routes/index-med-account";
import { createFunDependentRouter } from "./routes/new-fun-dependent";
import { createFunAccountRouter } from "./routes/new-fun-account";
import { createMainAccountRouter } from "./routes/new-main-account";
import { createMedAccountRouter } from "./routes/new-med-account";
import { createMedDependentRouter } from "./routes/new-med-dependent";
import { createMotorAccountRouter } from "./routes/new-motor-account";
import { createMotorVehicleRouter } from "./routes/new-motor-vehicle";
import { showAccountFunDependentsRouter } from "./routes/show-account-fun-dependents";
import { showFunDependentRouter } from "./routes/show-fun-dependent";
import { showFunAccountRouter } from "./routes/show-fun-account";
import { showMainAccountRouter } from "./routes/show-main-account";
import { showMedAccountRouter } from "./routes/show-med-account";
import { showMedDependentRouter } from "./routes/show-med-dependent";
import { updateFunAccountRouter } from "./routes/update-fun-account";
import { updateMainAccountRouter } from "./routes/update-main-account";
import { updateMedAccountRouter } from "./routes/update-med-account";

const app = express();

// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== "test",
  })
);

app.use(approveMainAccountRouter);
app.use(deleteFunAccountRouter);
app.use(deleteMainAccountRouter);
app.use(deleteMedAccountRouter);
app.use(indexSubAccountRouter);
app.use(indexFunAccountRouter);
app.use(indexMainAccountRouter);
app.use(indexMedAccountRouter);
app.use(createFunDependentRouter);
app.use(createFunAccountRouter);
app.use(createMainAccountRouter);
app.use(createMedAccountRouter);
app.use(createMedDependentRouter);
app.use(createMotorAccountRouter);
app.use(createMotorVehicleRouter);
app.use(showAccountFunDependentsRouter);
app.use(showFunDependentRouter);
app.use(showFunAccountRouter);
app.use(showMainAccountRouter);
app.use(showMedAccountRouter);
app.use(showMedDependentRouter);
app.use(updateFunAccountRouter);
app.use(updateMainAccountRouter);
app.use(updateMedAccountRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";

import { AccountFun } from "../models/account-funeral";

const router = express.Router();

router.get(
  "/api/accounts_funeral",

  async (req: Request, res: Response) => {
    const accountsFun = await AccountFun.find()
    .populate("account_main")
    .populate("package_fun")
    .populate("company")
    .populate("branch")
    .populate("created_by");
    res.send(accountsFun);
  }
);

export { router as indexFunAccountRouter };

import express, { Request, Response } from "express";
import { NotFoundError, requireAuth } from "@totum-insurance/common";
import { DependentFun } from "../models/dependent-funeral";
import { AccountFun } from "../models/account-funeral";

const router = express.Router();

router.get(
  "/api/accounts_funeral/dependents/:id",
  requireAuth,

  async (req: Request, res: Response) => {
    const dependentsFun = await DependentFun.find({account_fun: req.params.id})
      .populate("account_main")
      .populate("account_fun")
      .populate("package_fun")
      .populate("company")
      .populate("created_by");

    if (!dependentsFun) {
      throw new NotFoundError();
    }

    res.send(dependentsFun);
  }
);

export { router as showAccountFunDependentsRouter };

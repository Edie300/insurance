import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { DependentMed } from "../models/dependent-medical";
import { AccountFun } from "../models/account-funeral";

const router = express.Router();

router.get(
  "/api/dependents_medical/:id",

  async (req: Request, res: Response) => {
    const dependentMed = await DependentMed.findById(req.params.id)
      .populate("account_main")
      .populate("account_med")
      .populate("package_med")
      .populate("company")
      .populate("created_by");

    if (!dependentMed) {
      throw new NotFoundError();
    }

    res.send(dependentMed);
  }
);

export { router as showMedDependentRouter };

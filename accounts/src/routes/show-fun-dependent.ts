import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { DependentFun } from "../models/dependent-funeral";
import { AccountFun } from "../models/account-funeral";

const router = express.Router();

router.get(
  "/api/dependents_funeral/:id",

  async (req: Request, res: Response) => {
    const dependentFun = await DependentFun.findById(req.params.id)
      .populate("account_main")
      .populate("account_fun")
      .populate("package_fun")
      .populate("company")
      .populate("created_by");

    if (!dependentFun) {
      throw new NotFoundError();
    }

    res.send(dependentFun);
  }
);

export { router as showFunDependentRouter };

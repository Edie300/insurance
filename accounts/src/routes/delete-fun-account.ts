import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
} from '@totum-insurance/common';

import { AccountFun } from '../models/account-funeral';

const router = express.Router();

router.delete('/api/accounts_funeral/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const accountFun = await AccountFun.findById(id);

    if (!accountFun) {
      throw new NotFoundError();
    }

    accountFun.status = 3;
    await accountFun.save();

    res.status(204).send(accountFun);
  }
);

export { router as deleteFunAccountRouter };

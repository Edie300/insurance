import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  AccountMedStatus,
  DependentMedStatus
} from "@totum-insurance/common";

import { AccountMed } from "../models/account-medical";
import { DependentMed } from "../models/dependent-medical";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageMed } from "../models/package-medical";
import { User } from "../models/user";

import { AccountMedCreatedPublisher } from "../events/publishers/account-med-created-publisher";
import { DependentMedCreatedPublisher } from "../events/publishers/dependent-med-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/accounts_medical/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const { attachments, package_med, account_main, dependents } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      throw new NotFoundError();
    }

    // Check if package exists
    const getPackageMed = await PackageMed.findById(package_med);

    if (!getPackageMed) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;
    const policy_id = (Math.random() * 100000).toFixed();

    const accountMed = AccountMed.build({
      attachments,
      policy_id,
      payment_status: "0",
      balance: 0,
      premium: 0,
      status: AccountMedStatus.Pending,
      company,
      branch,
      package_med,
      account_main,
      created_at,
      created_by,
    });

    await accountMed.save();

    new AccountMedCreatedPublisher(natsWrapper.client).publish({
      id: accountMed.id,
      attachments: accountMed.attachments,
      policy_id: accountMed.policy_id,
      payment_status: accountMed.payment_status,
      balance: accountMed.balance,
      premium: accountMed.premium,
      status: accountMed.status,
      company: accountMed.company,
      branch: accountMed.branch,
      package_med: accountMed.package_med,
      account_main: accountMed.account_main,
      created_at: accountMed.created_at,
      created_by: accountMed.created_by,
      version: accountMed.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    // Add dependents in array to account

    dependents.forEach(
      async (dep: {
        name: string;
        surname: string;
        gender: string;
        date_of_birth: Date;
        marital_status: string;
        title: string;
        national_id: string;
        passport?: string;
        drivers_licence?: string;
        email?: string;
        phone: string;
        address: string;
        relationship: string;
        occupation?: string;
      }) => {
        const {
          name,
          surname,
          gender,
          date_of_birth,
          marital_status,
          title,
          national_id,
          passport,
          drivers_licence,
          email,
          phone,
          address,
          relationship,
          occupation,
        } = dep;

        const dependentMed = DependentMed.build({
          name,
          surname,
          gender,
          date_of_birth,
          marital_status,
          title,
          national_id,
          passport,
          drivers_licence,
          email,
          phone,
          address,
          relationship,
          occupation,
          account_med: accountMed._id,
          status: DependentMedStatus.Pending,
          account_main,
          created_at,
          created_by,
          package_med,
          company,
        });

        await dependentMed.save();

        new DependentMedCreatedPublisher(natsWrapper.client).publish({
          id: dependentMed.id,
          attachments: dependentMed.attachments,
          name: dependentMed.name,
          surname: dependentMed.surname,
          gender: dependentMed.gender,
          date_of_birth: dependentMed.date_of_birth,
          marital_status: dependentMed.marital_status,
          title: dependentMed.title,
          national_id: dependentMed.national_id,
          passport: dependentMed.passport,
          drivers_licence: dependentMed.drivers_licence,
          email: dependentMed.email,
          phone: dependentMed.phone,
          address: dependentMed.address,
          relationship: dependentMed.relationship,
          occupation: dependentMed.occupation,
          account_med: dependentMed.account_med,
          status: dependentMed.status,
          account_main: dependentMed.account_main,
          created_at: dependentMed.created_at,
          created_by: dependentMed.created_by,
          package_med: dependentMed.package_med,
          company: dependentMed.company,
          version: dependentMed.version,
          user: {
            id:  req.currentUser!.id, 
            date: new Date(),
          },
        });
      }
    );

    res.status(201).send(accountMed);
  }
);

export { router as createMedAccountRouter };

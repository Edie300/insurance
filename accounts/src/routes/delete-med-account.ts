import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
} from "@totum-insurance/common";

import { AccountMed } from "../models/account-medical";

const router = express.Router();

router.delete(
  "/api/accounts_medical/:id",
  async (req: Request, res: Response) => {
    const { id } = req.params;

    const accountMed = await AccountMed.findById(id);

    if (!accountMed) {
      throw new NotFoundError();
    }

    accountMed.status = 3;
    await accountMed.save();

    res.status(204).send(accountMed);
  }
);

export { router as deleteMedAccountRouter };

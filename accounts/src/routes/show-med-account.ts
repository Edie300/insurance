import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { AccountMed } from "../models/account-medical";

const router = express.Router();

router.get(
  "/api/accounts_medical/:id",

  async (req: Request, res: Response) => {
    const accountMed = await AccountMed.findById(req.params.id)
    .populate("account_main")
    .populate("package_med")
    .populate("company")
    .populate("branch")
    .populate("created_by");

    if (!accountMed) {
      throw new NotFoundError();
    }

    res.send(accountMed);
  }
);

export { router as showMedAccountRouter };

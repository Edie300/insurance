import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  requireAuth,
  NotFoundError,
  BadRequestError,
} from "@totum-insurance/common";

import { AccountMain } from "../models/account-main";
import { AccountFun } from "../models/account-funeral";
import { AccountMed } from "../models/account-medical";
import { AccountMotor } from "../models/account-motor";

const router = express.Router();

router.post(
  "/api/accounts_sub_accounts",
  requireAuth,

  async (req: Request, res: Response) => {
    let subAccounts = [];

    const { account_main } = req.body;

    // Check if company exists
    const accountMain = await AccountMain.findById(account_main);
    console.log("account main", accountMain);

    if (!accountMain) {
      throw new NotFoundError();
    }

    const accountsFun = await AccountFun.find({ account_main })
      .populate("account_main")
      .populate("package_fun")
      .populate("company")
      .populate("branch")
      .populate("created_by");
    if (accountsFun) {
      subAccounts.push(...accountsFun);
    }

    const accountsMed = await AccountMed.find({ account_main })
      .populate("account_main")
      .populate("package_med")
      .populate("company")
      .populate("branch")
      .populate("created_by");
    if (accountsMed) {
      accountsMed.map((obj) => ({ ...obj, type: 1 }));
      subAccounts.push(...accountsMed);
    }

    const accountsMotor = await AccountMotor.find({ account_main });
    if (accountsMotor) {
      subAccounts.push(...accountsMotor);
    }

    const sortedSubAccounts = subAccounts.sort((a, b) => {
      return a.created_at < b.created_at
        ? -1
        : a.created_at > b.created_at
        ? 1
        : 0;
    });

    res.send(sortedSubAccounts);
  }
);

export { router as indexSubAccountRouter };

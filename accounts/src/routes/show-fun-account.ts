import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { AccountFun } from "../models/account-funeral";

const router = express.Router();

router.get(
  "/api/accounts_funeral/:id",

  async (req: Request, res: Response) => {
    const accountFun = await AccountFun.findById(req.params.id)
    .populate("account_main")
    .populate("package_fun")
    .populate("company")
    .populate("branch")
    .populate("created_by");

    if (!accountFun) {
      throw new NotFoundError();
    }

    res.send(accountFun);
  }
);

export { router as showFunAccountRouter };







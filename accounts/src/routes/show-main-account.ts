import express, { Request, Response } from "express";
import { NotFoundError } from "@totum-insurance/common";
import { AccountMain } from "../models/account-main";

const router = express.Router();

router.get(
  "/api/accounts_main/:id",

  async (req: Request, res: Response) => {
    const accountMain = await AccountMain.findById(req.params.id)
    .populate("funeral_accounts") 
    .populate("company")
    .populate("branch")
    .populate("created_by")

    

    if (!accountMain) {
      throw new NotFoundError();
    }

    res.send(accountMain);
  }
);

export { router as showMainAccountRouter };







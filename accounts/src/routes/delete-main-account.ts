import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
} from '@totum-insurance/common';

import { AccountMain } from '../models/account-main';

const router = express.Router();

router.delete('/api/accounts_main/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const accountMain = await AccountMain.findById(id);

    if (!accountMain) {
      throw new NotFoundError();
    }

    accountMain.status = 3;
    await accountMain.save();

    res.status(204).send(accountMain);
  }
);

export { router as deleteMainAccountRouter };

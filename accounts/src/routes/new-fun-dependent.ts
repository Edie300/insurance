import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  DependentFunStatus,
} from "@totum-insurance/common";

import { AccountFun } from "../models/account-funeral";
import { DependentFun} from "../models/dependent-funeral";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageFun } from "../models/package-funeral";
import { User } from "../models/user";

import { DependentFunCreatedPublisher } from "../events/publishers/dependent-fun-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/dependents_funeral/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      account_fun,
      package_fun,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company} = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if account Funeral exists
    const getAccount = await AccountFun.findById(account_fun);

    if (!getAccount) {
      throw new NotFoundError();
    }

    // Check if package funeral exists
    const getPackageFun = await PackageFun.findById(package_fun);

    if (!getPackageFun) {
      throw new NotFoundError();
    }

    const { account_main } = getAccount

    const created_at = new Date();
    const created_by = user._id;

    const dependentFun = DependentFun.build({
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      account_fun,
      status: DependentFunStatus.Pending,
      account_main,
      created_at,
      created_by,
      package_fun,
      company
    });

    await dependentFun.save();

    new DependentFunCreatedPublisher(natsWrapper.client).publish({
      id: dependentFun.id,
      attachments: dependentFun.attachments,
      name: dependentFun.name,
      surname: dependentFun.surname,
      gender: dependentFun.gender,
      date_of_birth: dependentFun.date_of_birth,
      marital_status: dependentFun.marital_status,
      title: dependentFun.title,
      national_id: dependentFun.national_id,
      passport: dependentFun.passport,
      drivers_licence: dependentFun.drivers_licence,
      email: dependentFun.email,
      phone: dependentFun.phone,
      address: dependentFun.address,
      relationship: dependentFun.relationship,
      occupation: dependentFun.occupation,
      account_fun: dependentFun.account_fun,
      status: dependentFun.status,
      account_main: dependentFun.account_main,
      created_at: dependentFun.created_at,
      created_by: dependentFun.created_by,
      package_fun: dependentFun.package_fun,
      company: dependentFun.company,
      version: dependentFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });



    res.status(201).send(dependentFun);
  }
);

export { router as createFunDependentRouter };

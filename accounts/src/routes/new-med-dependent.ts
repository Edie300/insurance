import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  DependentMedStatus,
} from "@totum-insurance/common";

import { AccountMed } from "../models/account-medical";
import { DependentMed} from "../models/dependent-medical";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageMed } from "../models/package-medical";
import { User } from "../models/user";

import { DependentMedCreatedPublisher } from "../events/publishers/dependent-med-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/dependents_medical/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      account_med,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company} = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      console.log("company not found")
      throw new NotFoundError();
    }

    // Check if account Funeral exists
    const getAccount = await AccountMed.findById(account_med);

    if (!getAccount) {
      console.log("account not found")
      throw new NotFoundError();
    }


    const { account_main, package_med } = getAccount

    const created_at = new Date();
    const created_by = user._id;

    const dependentMed = DependentMed.build({
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      account_med,
      status: DependentMedStatus.Pending,
      account_main,
      created_at,
      created_by,
      package_med,
      company
    });

    await dependentMed.save();

    new DependentMedCreatedPublisher(natsWrapper.client).publish({
      id: dependentMed.id,
      attachments: dependentMed.attachments,
      name: dependentMed.name,
      surname: dependentMed.surname,
      gender: dependentMed.gender,
      date_of_birth: dependentMed.date_of_birth,
      marital_status: dependentMed.marital_status,
      title: dependentMed.title,
      national_id: dependentMed.national_id,
      passport: dependentMed.passport,
      drivers_licence: dependentMed.drivers_licence,
      email: dependentMed.email,
      phone: dependentMed.phone,
      address: dependentMed.address,
      relationship: dependentMed.relationship,
      occupation: dependentMed.occupation,
      account_med: dependentMed.account_med,
      status: dependentMed.status,
      account_main: dependentMed.account_main,
      created_at: dependentMed.created_at,
      created_by: dependentMed.created_by,
      package_med: dependentMed.package_med,
      company: dependentMed.company,
      version: dependentMed.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });



    res.status(201).send(dependentMed);
  }
);

export { router as createMedDependentRouter };

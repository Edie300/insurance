import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  AccountMainStatus,
} from "@totum-insurance/common";

import { AccountMain } from "../models/account-main";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageFun } from "../models/package-funeral";
import { User } from "../models/user";

import { AccountMainCreatedPublisher } from "../events/publishers/account-main-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/accounts_main/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      payment_status,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      console.log("user not found")
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      console.log("company not found")
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      console.log("branch not found")
      throw new NotFoundError();
    }

    const account_id =
      (Math.random() * 100000).toFixed() +
      name[0].toUpperCase() +
      surname[0].toUpperCase();
    const created_at = new Date();
    const created_by = user._id;

    const accountMain = AccountMain.build({
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      payment_status,
      status: AccountMainStatus.Pending,
      created_at,
      created_by,
      company,
      branch,
    });

    await accountMain.save();

    new AccountMainCreatedPublisher(natsWrapper.client).publish({
      id: accountMain.id,
      attachments: accountMain.attachments,
      name: accountMain.name,
      surname: accountMain.surname,
      gender: accountMain.gender,
      account_id: accountMain.account_id,
      date_of_birth: accountMain.date_of_birth,
      marital_status: accountMain.marital_status,
      title: accountMain.title,
      email: accountMain.email,
      phone: accountMain.phone,
      address: accountMain.address,
      national_id: accountMain.national_id,
      passport: accountMain.passport,
      drivers_licence: accountMain.drivers_licence,
      status: accountMain.status,
      company: accountMain.company,
      branch: accountMain.branch,
      created_at: accountMain.created_at,
      created_by: accountMain.created_by,
      version: accountMain.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(201).send(accountMain);
  }
);

export { router as createMainAccountRouter };

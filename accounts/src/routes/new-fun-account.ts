import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  BadRequestError,
  AccountFunStatus,
  DependentFunStatus,
  PackageFunStatus
} from "@totum-insurance/common";

import moment from "moment";

import { AccountFun } from "../models/account-funeral";
import { DependentFun, DependentFunAttrs } from "../models/dependent-funeral";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageFun } from "../models/package-funeral";
import { User } from "../models/user";

import { AccountFunCreatedPublisher } from "../events/publishers/account-fun-created-publisher";
import { DependentFunCreatedPublisher } from "../events/publishers/dependent-fun-created-publisher";
import { AccountFunNextPaymentPublisher } from "../events/publishers/account-fun-next-payment-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/accounts_funeral/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const { attachments, package_fun, account_main, dependents } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      throw new NotFoundError();
    }

    // Check if package exists
    const getPackageFun = await PackageFun.findById(package_fun);

    if (!getPackageFun) {
      throw new NotFoundError();
    }

    if (getPackageFun.status != PackageFunStatus.Published) {
      console.log("package status", getPackageFun.status)
      throw new BadRequestError("package not published");
    }

    const created_at = new Date();
    const next_payment_date = moment(created_at).add(1,'minutes').toDate();
    const created_by = user._id;
    const policy_id = (Math.random() * 100000).toFixed();

    console.log("created_at", created_at)
    console.log("next_payment", next_payment_date)

    const accountFun = AccountFun.build({
      attachments,
      policy_id,
      payment_status: "0",
      balance: 0,
      premium: 0,
      next_payment_date,
      status: AccountFunStatus.Pending,
      company,
      branch,
      package_fun,
      account_main,
      created_at,
      created_by,
    });

    await accountFun.save();

    new AccountFunCreatedPublisher(natsWrapper.client).publish({
      id: accountFun.id,
      attachments: accountFun.attachments,
      policy_id: accountFun.policy_id,
      payment_status: accountFun.payment_status,
      balance: accountFun.balance,
      premium: accountFun.premium,
      next_payment_date: accountFun.next_payment_date,
      status: accountFun.status,
      company: accountFun.company,
      branch: accountFun.branch,
      package_fun: accountFun.package_fun,
      account_main: accountFun.account_main,
      created_at: accountFun.created_at,
      created_by: accountFun.created_by,
      version: accountFun.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    
    new AccountFunNextPaymentPublisher (natsWrapper.client).publish({
      id: accountFun.id, 
      next_payment_date: accountFun.next_payment_date,
    })


    // Add dependents in array to account

    dependents.forEach(
      async (dep: {
        name: string;
        surname: string;
        gender: string;
        date_of_birth: Date;
        marital_status: string;
        title: string;
        national_id: string;
        passport?: string;
        drivers_licence?: string;
        email?: string;
        phone: string;
        address: string;
        relationship: string;
        occupation: string;
      }) => {
        const {
          name,
          surname,
          gender,
          date_of_birth,
          marital_status,
          title,
          national_id,
          passport,
          drivers_licence,
          email,
          phone,
          address,
          relationship,
          occupation,
        } = dep;

        const dependentFun = DependentFun.build({
          name,
          surname,
          gender,
          date_of_birth,
          marital_status,
          title,
          national_id,
          passport,
          drivers_licence,
          email,
          phone,
          address,
          relationship,
          occupation,
          account_fun: accountFun._id,
          status: DependentFunStatus.Pending,
          account_main,
          created_at,
          created_by,
          package_fun,
          company,
        });

        await dependentFun.save();

        new DependentFunCreatedPublisher(natsWrapper.client).publish({
          id: dependentFun.id,
          attachments: dependentFun.attachments,
          name: dependentFun.name,
          surname: dependentFun.surname,
          gender: dependentFun.gender,
          date_of_birth: dependentFun.date_of_birth,
          marital_status: dependentFun.marital_status,
          title: dependentFun.title,
          national_id: dependentFun.national_id,
          passport: dependentFun.passport,
          drivers_licence: dependentFun.drivers_licence,
          email: dependentFun.email,
          phone: dependentFun.phone,
          address: dependentFun.address,
          relationship: dependentFun.relationship,
          occupation: dependentFun.occupation,
          account_fun: dependentFun.account_fun,
          status: dependentFun.status,
          account_main: dependentFun.account_main,
          created_at: dependentFun.created_at,
          created_by: dependentFun.created_by,
          package_fun: dependentFun.package_fun,
          company: dependentFun.company,
          version: dependentFun.version,
          user: {
            id:  req.currentUser!.id, 
            date: new Date(),
          },
        });
      }
    );



    res.status(201).send(accountFun);
  }
);

export { router as createFunAccountRouter };

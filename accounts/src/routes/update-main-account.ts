import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { AccountMain } from "../models/account-main";

import {  AccountMainUpdateddPublisher } from "../events/publishers/account-main-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/accounts_main/:id", async (req: Request, res: Response) => {
  const accountMain = await AccountMain.findById(req.params.id);

  if (!accountMain) {
    throw new NotFoundError();
  }

  accountMain.set({
    attachments: req.body.attachments,
    name: req.body.name,
    surname: req.body.surname,
    gender: req.body.gender,
    date_of_birth: req.body.date_of_birth,
    marital_status: req.body.marital_status,
    title: req.body.title,
    national_id: req.body.national_id,
    passport: req.body.passport,
    drivers_licence: req.body.drivers_licence,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    account_id: req.body.account_id,
    status: req.body.status,
    created_at: req.body.created_at,
    created_by: req.body.created_by,
    company: req.body.company,
    branch: req.body.branch,
  });

  await accountMain.save();

  new AccountMainUpdateddPublisher(natsWrapper.client).publish({
    id: accountMain.id,
    attachments: accountMain.attachments,
    name: accountMain.name,
    surname: accountMain.surname,
    gender: accountMain.gender,
    account_id: accountMain.account_id,
    date_of_birth: accountMain.date_of_birth,
    marital_status: accountMain.marital_status,
    title: accountMain.title,
    email: accountMain.email,
    phone: accountMain.phone,
    address: accountMain.address,
    national_id: accountMain.national_id,
    passport: accountMain.passport,
    drivers_licence: accountMain.drivers_licence,
    status: accountMain.status,
    company: accountMain.company,
    branch: accountMain.branch,
    created_at: accountMain.created_at,
    created_by: accountMain.created_by,
    version: accountMain.version,
    user: {
      id:  req.currentUser!.id, 
      date: new Date(),
    },
  });

  res.send(accountMain);
});

export { router as updateMainAccountRouter };

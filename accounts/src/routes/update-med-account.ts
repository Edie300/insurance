import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { AccountMed } from "../models/account-medical";

import { AccountMedUpdatedPublisher  } from "../events/publishers/account-med-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/accounts_medical/:id", async (req: Request, res: Response) => {
  const accountMed = await AccountMed.findById(req.params.id);

  if (!accountMed) {
    throw new NotFoundError();
  }

  accountMed.set({
    attachments: req.body.attachments,
    policy_id: req.body.policy_id,
    payment_status:  req.body.payment_status,
    balance: req.body.balance,
    premium: req.body.premium,
    status: req.body.status,
    account_main: req.body.account_main,
    company: req.body.company,
    branch: req.body.branch,
    created_at: req.body.created_at,
    created_by: req.body.created_by,
    package_med: req.body.package_fun,
  });

  await accountMed.save();

  new AccountMedUpdatedPublisher(natsWrapper.client).publish({
    id: accountMed.id,
    attachments: accountMed.attachments,
    policy_id: accountMed.policy_id,
    payment_status: accountMed.payment_status,
    balance: accountMed.balance,
    premium: accountMed.premium,
    status: accountMed.status,
    company: accountMed.company,
    branch: accountMed.branch,
    package_med: accountMed.package_med,
    account_main: accountMed.account_main,
    created_at: accountMed.created_at,
    created_by: accountMed.created_by,
    version: accountMed.version,
    user: {
      id:  req.currentUser!.id, 
      date: new Date(),
    },
  });

  res.send(accountMed);
});

export { router as updateMedAccountRouter };

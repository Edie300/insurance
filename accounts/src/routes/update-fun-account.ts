import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
} from "@totum-insurance/common";
import { AccountFun } from "../models/account-funeral";

import { AccountFunUpdatedPublisher  } from "../events/publishers/account-fun-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.put("/api/accounts_funeral/:id", async (req: Request, res: Response) => {
  const accountFun = await AccountFun.findById(req.params.id);

  if (!accountFun) {
    throw new NotFoundError();
  }

  accountFun.set({
    attachments: req.body.attachments,
    policy_id: req.body.policy_id,
    payment_status:  req.body.payment_status,
    balance: req.body.balance,
    premium: req.body.premium,
    status: req.body.status,
    account_main: req.body.account_main,
    company: req.body.company,
    branch: req.body.branch,
    created_at: req.body.created_at,
    created_by: req.body.created_by,
    package_fun: req.body.package_fun,
  });

  await accountFun.save();

  new AccountFunUpdatedPublisher(natsWrapper.client).publish({
    id: accountFun.id,
    attachments: accountFun.attachments,
    policy_id: accountFun.policy_id,
    payment_status: accountFun.payment_status,
    next_payment_date: accountFun.next_payment_date,
    balance: accountFun.balance,
    premium: accountFun.premium,
    status: accountFun.status,
    company: accountFun.company,
    branch: accountFun.branch,
    package_fun: accountFun.package_fun,
    account_main: accountFun.account_main,
    created_at: accountFun.created_at,
    created_by: accountFun.created_by,
    version: accountFun.version,
    user: {
      id:  req.currentUser!.id, 
      date: new Date(),
    },
  });

  res.send(accountFun);
});

export { router as updateFunAccountRouter };

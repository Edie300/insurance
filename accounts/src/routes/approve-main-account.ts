import express, { Request, Response } from "express";
import {
  requireAuth,
  NotFoundError,
  NotAuthorizedError,
  AccountMainStatus
} from '@totum-insurance/common';

import { AccountMain } from '../models/account-main';

import {  AccountMainUpdateddPublisher } from "../events/publishers/account-main-updated-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.patch('/api/accounts_main/approve/:id', async (req: Request, res: Response) => {
  
    const { id } = req.params;

    const accountMain = await AccountMain.findById(id);

    if (!accountMain) {
      throw new NotFoundError();
    }

    accountMain.status = AccountMainStatus.Active;
    await accountMain.save();

    new AccountMainUpdateddPublisher(natsWrapper.client).publish({
      id: accountMain.id,
      attachments: accountMain.attachments,
      name: accountMain.name,
      surname: accountMain.surname,
      gender: accountMain.gender,
      account_id: accountMain.account_id,
      date_of_birth: accountMain.date_of_birth,
      marital_status: accountMain.marital_status,
      title: accountMain.title,
      email: accountMain.email,
      phone: accountMain.phone,
      address: accountMain.address,
      national_id: accountMain.national_id,
      passport: accountMain.passport,
      drivers_licence: accountMain.drivers_licence,
      status: accountMain.status,
      company: accountMain.company,
      branch: accountMain.branch,
      created_at: accountMain.created_at,
      created_by: accountMain.created_by,
      version: accountMain.version,
      user: {
        id:  req.currentUser!.id, 
        date: new Date(),
      },
    });

    res.status(204).send(accountMain);
  }
);

export { router as approveMainAccountRouter };

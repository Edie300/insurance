import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  DependentMedStatus,
  VehicleMotorStatus,
} from "@totum-insurance/common";

import { AccountMotor } from "../models/account-motor";
import { Vehicle } from "../models/vehicle";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageMed } from "../models/package-medical";
import { User } from "../models/user";

import { VehicleMotorCreatedPublisher } from "../events/publishers/vehicle-motor-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/vehicles-motor/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const {
      attachments,
      make,
      model,
      year,
      reg_number,
      color,
      engine_number,
      chassis_number,
      tax_class,
      body_type,
      principal_driver,
      other_drivers,
      account_motor,
    } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company} = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      console.log("company not found")
      throw new NotFoundError();
    }

    // Check if account Funeral exists
    const accountMotor = await AccountMotor.findById(account_motor);

    if (!accountMotor) {
      console.log("account not found")
      throw new NotFoundError();
    }


    const { account_main, package_motor } = accountMotor

    const created_at = new Date();
    const created_by = user._id;

    const vehicle = Vehicle.build({
      attachments,
      make,
      model,
      year,
      reg_number,
      color,
      engine_number,
      chassis_number,
      tax_class,
      body_type,
      principal_driver,
      other_drivers,
      account_motor, 
      status: VehicleMotorStatus.Pending,
      account_main,
      created_at,
      created_by,
      package_motor,
      company,
    });

    await vehicle.save();

    new VehicleMotorCreatedPublisher(natsWrapper.client).publish({
      attachments: vehicle.attachments,
      make: vehicle.make,
      model: vehicle.model,
      year: vehicle.year,
      reg_number: vehicle.reg_number,
      color: vehicle.color,
      engine_number: vehicle.engine_number,
      chassis_number: vehicle.chassis_number,
      tax_class: vehicle.tax_class,
      body_type: vehicle.body_type,
      principal_driver: vehicle.principal_driver,
      other_drivers: vehicle.other_drivers,
      created_at: vehicle.created_at,
      created_by: vehicle.created_by,
      status: vehicle.status, 
      account_main: vehicle.account_main,
      account_motor: vehicle.account_motor,
      company: vehicle.company,
      package_motor: vehicle.package_motor,
      version: vehicle.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });



    res.status(201).send(vehicle);
  }
);

export { router as createMotorVehicleRouter };

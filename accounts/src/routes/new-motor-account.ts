import express, { Request, Response } from "express";
import {
  validateRequest,
  NotFoundError,
  requireAuth,
  NotAuthorizedError,
  BadRequestError,
  AccountMotorStatus,
  VehicleMotorStatus,
  PackageFunStatus,
} from "@totum-insurance/common";

import moment from "moment";

import { AccountMotor } from "../models/account-motor";
import { Vehicle } from "../models/vehicle";
import { Company } from "../models/company";
import { Branch } from "../models/branch";
import { PackageMotor } from "../models/package-motor";
import { User } from "../models/user";

import { AccountMotorCreatedPublisher } from "../events/publishers/account-motor-created-publisher";
import { VehicleMotorCreatedPublisher } from "../events/publishers/vehicle-motor-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/accounts_motor/create",
  requireAuth,
  async (req: Request, res: Response) => {
    const { attachments, package_motor, account_main, vehicles } = req.body;

    // find admin user
    const user = await User.findById(req.currentUser!.id);
    if (!user) {
      throw new NotFoundError();
    }

    const { company, branch } = user;

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      throw new NotFoundError();
    }

    // Check if branch exists
    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      throw new NotFoundError();
    }

    // Check if package exists
    const packageMotor = await PackageMotor.findById(package_motor);

    if (!packageMotor) {
      throw new NotFoundError();
    }

    const created_at = new Date();
    const created_by = user._id;
    const policy_id = (Math.random() * 100000).toFixed();

    const accountMotor = AccountMotor.build({
      attachments,
      policy_id,
      payment_status: "0",
      balance: 0,
      premium: 0,
      status: AccountMotorStatus.Pending,
      company,
      branch,
      package_motor,
      account_main,
      created_at,
      created_by,
    });

    await accountMotor.save();

    new AccountMotorCreatedPublisher(natsWrapper.client).publish({
      id: accountMotor.id,
      attachments: accountMotor.attachments,
      policy_id: accountMotor.policy_id,
      payment_status: accountMotor.payment_status,
      balance: accountMotor.balance,
      premium: accountMotor.premium,
      status: accountMotor.status,
      company: accountMotor.company,
      branch: accountMotor.branch,
      package_motor: accountMotor.package_motor,
      account_main: accountMotor.account_main,
      created_at: accountMotor.created_at,
      created_by: accountMotor.created_by,
      version: accountMotor.version,
      user: {
        id: req.currentUser!.id,
        date: new Date(),
      },
    });

    // Add dependents in array to account

    vehicles.forEach(
      async (v: {
        attachments: {}[];
        make: string;
        model: string;
        year: Date;
        reg_number: string;
        color: string;
        engine_number: string;
        chassis_number: string;
        tax_class: string;
        body_type: string;
        principal_driver: string;
        other_drivers?: [
          {
            driver_name: string;
            driver_surname: string;
            drivers_licence: string;
            driver_id: string;
            driver_cell: string;
            driver_email: string;
            driver_address: string;
            driver_occupation: string;
            relationship: string;
          }
        ];
      }) => {
        const {
          attachments,
          make,
          model,
          year,
          reg_number,
          color,
          engine_number,
          chassis_number,
          tax_class,
          body_type,
          principal_driver,
          other_drivers,
        } = v;

        const vehicle = Vehicle.build({
          attachments,
          make,
          model,
          year,
          reg_number,
          color,
          engine_number,
          chassis_number,
          tax_class,
          body_type,
          principal_driver,
          other_drivers,
          account_motor: accountMotor._id,
          status: VehicleMotorStatus.Pending,
          account_main,
          created_at,
          created_by,
          package_motor,
          company,
        });

        await vehicle.save();

        new VehicleMotorCreatedPublisher(natsWrapper.client).publish({
          attachments: vehicle.attachments,
          make: vehicle.make,
          model: vehicle.model,
          year: vehicle.year,
          reg_number: vehicle.reg_number,
          color: vehicle.color,
          engine_number: vehicle.engine_number,
          chassis_number: vehicle.chassis_number,
          tax_class: vehicle.tax_class,
          body_type: vehicle.body_type,
          principal_driver: vehicle.principal_driver,
          other_drivers: vehicle.other_drivers,
          created_at: vehicle.created_at,
          created_by: vehicle.created_by,
          status: vehicle.status, 
          account_main: vehicle.account_main,
          account_motor: vehicle.account_motor,
          company: vehicle.company,
          package_motor: vehicle.package_motor,
          version: vehicle.version,
          user: {
            id: req.currentUser!.id,
            date: new Date(),
          },
        });
      }
    );

    res.status(201).send(accountMotor);
  }
);

export { router as createMotorAccountRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";

import { AccountMed } from "../models/account-medical";

const router = express.Router();

router.get(
  "/api/accounts_medical",

  async (req: Request, res: Response) => {
    const accountsMed = await AccountMed.find()
      .populate("account_main")
      .populate("package_med")
      .populate("company")
      .populate("branch")
      .populate("created_by");
    res.send(accountsMed);
  }

  // package_med: Types.ObjectId;
  // account_main: Types.ObjectId;
  // company: Types.ObjectId;
  // branch: Types.ObjectId;
  // created_at: Date;
  // created_by: Types.ObjectId;
);

export { router as indexMedAccountRouter };

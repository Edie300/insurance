import { Publisher, Subjects, AccountMainCreatedEvent } from "@totum-insurance/common";

export class AccountMainCreatedPublisher extends Publisher<AccountMainCreatedEvent> {
  subject: Subjects.AccountMainCreated = Subjects.AccountMainCreated;
}

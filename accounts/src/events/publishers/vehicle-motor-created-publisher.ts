import { Publisher, Subjects, VehicleMotorCreatedEvent } from "@totum-insurance/common";

export class VehicleMotorCreatedPublisher extends Publisher<VehicleMotorCreatedEvent> {
  subject: Subjects.VehicleMotorCreated = Subjects.VehicleMotorCreated;
}

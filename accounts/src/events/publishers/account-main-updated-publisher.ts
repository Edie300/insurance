import { Publisher, Subjects, AccountMainUpdatedEvent } from "@totum-insurance/common";

export class AccountMainUpdateddPublisher extends Publisher<AccountMainUpdatedEvent> {
  subject: Subjects.AccountMainUpdated = Subjects.AccountMainUpdated;
}

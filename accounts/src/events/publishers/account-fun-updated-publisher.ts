import { Publisher, Subjects, AccountFunUpdatedEvent } from "@totum-insurance/common";

export class AccountFunUpdatedPublisher extends Publisher<AccountFunUpdatedEvent> {
  subject: Subjects.AccountFunUpdated = Subjects.AccountFunUpdated;
}


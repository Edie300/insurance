import { Publisher, Subjects, DependentFunCreatedEvent } from "@totum-insurance/common";

export class DependentFunCreatedPublisher extends Publisher<DependentFunCreatedEvent> {
  subject: Subjects.DependentFunCreated = Subjects.DependentFunCreated;
}

import { Publisher, Subjects, AccountMedUpdatedEvent } from "@totum-insurance/common";

export class AccountMedUpdatedPublisher extends Publisher<AccountMedUpdatedEvent> {
  subject: Subjects.AccountMedUpdated = Subjects.AccountMedUpdated;
}


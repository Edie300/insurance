import { Publisher, Subjects, AccountFunNextPaymentEvent } from "@totum-insurance/common";

export class AccountFunNextPaymentPublisher extends Publisher<AccountFunNextPaymentEvent> {
  subject: Subjects.AccountFunNextPayment = Subjects.AccountFunNextPayment;
}

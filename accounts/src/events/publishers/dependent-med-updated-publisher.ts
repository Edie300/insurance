import { Publisher, Subjects, DependentMedUpdatedEvent } from "@totum-insurance/common";

export class DependentMedUpdatedPublisher extends Publisher<DependentMedUpdatedEvent> {
  subject: Subjects.DependentMedUpdated = Subjects.DependentMedUpdated;
}


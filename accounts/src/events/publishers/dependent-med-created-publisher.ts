import { Publisher, Subjects, DependentMedCreatedEvent } from "@totum-insurance/common";

export class DependentMedCreatedPublisher extends Publisher<DependentMedCreatedEvent> {
  subject: Subjects.DependentMedCreated = Subjects.DependentMedCreated;
}

import { Publisher, Subjects, AccountMedCreatedEvent } from "@totum-insurance/common";

export class AccountMedCreatedPublisher extends Publisher<AccountMedCreatedEvent> {
  subject: Subjects.AccountMedCreated = Subjects.AccountMedCreated;
}

import { Publisher, Subjects, AccountMotorCreatedEvent } from "@totum-insurance/common";

export class AccountMotorCreatedPublisher extends Publisher<AccountMotorCreatedEvent> {
  subject: Subjects.AccountMotorCreated = Subjects.AccountMotorCreated;
}

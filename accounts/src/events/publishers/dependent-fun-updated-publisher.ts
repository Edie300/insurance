import { Publisher, Subjects, DependentFunUpdatedEvent } from "@totum-insurance/common";

export class DependentFunUpdatedPublisher extends Publisher<DependentFunUpdatedEvent> {
  subject: Subjects.DependentFunUpdated = Subjects.DependentFunUpdated;
}


import { Publisher, Subjects, AccountFunCreatedEvent } from "@totum-insurance/common";

export class AccountFunCreatedPublisher extends Publisher<AccountFunCreatedEvent> {
  subject: Subjects.AccountFunCreated = Subjects.AccountFunCreated;
}

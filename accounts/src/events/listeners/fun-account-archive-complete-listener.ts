import { Message } from "node-nats-streaming";
import { Subjects, Listener, AccountFunWaitingCompleteEvent } from "@totum-insurance/common";

import { queueGroupName } from "./queue-group-name";

export class AccountFunWaitingCompleteListener extends Listener<AccountFunWaitingCompleteEvent> {
  subject: Subjects.AccountFunWaitingComplete = Subjects.AccountFunWaitingComplete;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunWaitingCompleteEvent["data"], msg: Message) {
    console.log("account-fun-waiting-complete-listener::", data);
    const {
      id,
      version,
    } = data;


    msg.ack();
  }
}

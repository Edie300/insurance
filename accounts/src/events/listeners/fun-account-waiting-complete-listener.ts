import { Message } from "node-nats-streaming";
import AWS from "aws-sdk";
import {
  Subjects,
  Listener,
  AccountFunWaitingCompleteEvent,
} from "@totum-insurance/common";
import { AccountFun } from "../../models/account-funeral";
import { AccountMain } from "../../models/account-main";
import { queueGroupName } from "./queue-group-name";

const awsConfig = {
  accessKeyId: "AKIAU262SNIMGNH3M2OD",
  secretAccessKey: "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",
  region: "us-east-1",
  apiVersion: "2010-12-01",
};

const SES = new AWS.SES(awsConfig);

export class AccountFunWaitingCompleteListener extends Listener<AccountFunWaitingCompleteEvent> {
  subject: Subjects.AccountFunWaitingComplete =
    Subjects.AccountFunWaitingComplete;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunWaitingCompleteEvent["data"], msg: Message) {
    console.log("account-fun-waiting-complete-listener::", data);
    const { id, version } = data;

    const accountFun = await AccountFun.findById(id)


    if (!accountFun) {
      throw new Error("account funeral not found");
    }

    const accountMain = await AccountMain.findById(accountFun.account_main);

    if (!accountMain) {
      throw new Error("account main not found");
    }

    const params = {
      Source: "totum1000@gmail.com",
      Destination: {
        ToAddresses: [accountMain.email],
      },
      ReplyToAddresses: ["totum1000@gmail.com"],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `
                  <html>
                    <h1>waiting period</h1>
                    <p>Your account waiting period has elapsed. You can now claim</p>
                    <i>totum.com</i>
                  </html>
                `,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: "Waiting Period",
        },
      },
    };

    const emailSent = SES.sendEmail(params).promise();
    emailSent
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });

    msg.ack();
  }
}

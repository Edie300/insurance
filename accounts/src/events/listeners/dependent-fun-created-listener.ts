import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  DependentFunCreatedEvent,
} from "@totum-insurance/common";
import {} from "../../models/package-funeral";
import { Premium } from "../../services/Premium";
import { AccountMain } from "../../models/account-main";
import { AccountFun } from "../../models/account-funeral";
import { PackageFun } from "../../models/package-funeral";
import { DependentFun } from "../../models/dependent-funeral";
import { queueGroupName } from "./queue-group-name";

import { AccountFunUpdatedPublisher  } from "../publishers/account-fun-updated-publisher";
import { natsWrapper } from "../../nats-wrapper";

export class DependentFunCreatedListener extends Listener<DependentFunCreatedEvent> {
  subject: Subjects.DependentFunCreated = Subjects.DependentFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: DependentFunCreatedEvent["data"], msg: Message) {
    console.log("dependent-fun-created-listener::", data);
    const {
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      status,
      account_main,
      account_fun,
      company,
      package_fun,
      created_at,
      created_by,
      version,
      user,
    } = data;

    const dependents = await DependentFun.find({ account_fun: account_fun });

    const holder = await AccountMain.findById(account_main);

    if (!holder) {
      throw new Error("main account not found");
    }

    const pkg = await PackageFun.findById(package_fun);

    if (!pkg) {
      throw new Error("package funeral not found");
    }

    //  Recalculate account premium
    const accountFun = await AccountFun.findById(account_fun);

    if (!accountFun) {
      throw new Error("account funeral not found");
    }

    const premium =  new Premium(dependents, holder, pkg)
    accountFun.set({
      premium: premium.calculatePremium(),
    });
    
    await accountFun.save();

    new AccountFunUpdatedPublisher(natsWrapper.client).publish({
      id: accountFun.id,
      attachments: accountFun.attachments,
      policy_id: accountFun.policy_id,
      payment_status: accountFun.payment_status,
      next_payment_date: accountFun.next_payment_date,
      balance: accountFun.balance,
      premium: accountFun.premium,
      status: accountFun.status,
      company: accountFun.company,
      branch: accountFun.branch,
      package_fun: accountFun.package_fun,
      account_main: accountFun.account_main,
      created_at: accountFun.created_at,
      created_by: accountFun.created_by,
      version: accountFun.version,
      user,
    });


    msg.ack();
  }
}

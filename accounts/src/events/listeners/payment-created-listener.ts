import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  PaymentCreatedEvent,
  AccountFunUpdatedEvent,
} from "@totum-insurance/common";
import { AccountFunUpdatedPublisher } from "../publishers/account-fun-updated-publisher";
import { natsWrapper } from "../../nats-wrapper";
import { AccountFun } from "../../models/account-funeral";
import { AccountMed } from "../../models/account-medical";
import { AccountMotor } from "../../models/account-motor";
import { Payment } from "../../models/payment";
import { queueGroupName } from "./queue-group-name";

export class PaymentCreatedListener extends Listener<PaymentCreatedEvent> {
  subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PaymentCreatedEvent["data"], msg: Message) {
    console.log("branch-created-listener::", data);
    const {
      id,
      payment_id,
      type,
      method,
      amount,
      payer_name,
      payer_surname,
      payer_phone,
      currency,
      premium,
      balance_before,
      balance_after,
      created_at,
      created_by,
      status,
      account_main,
      account,
      account_package,
      company,
      branch,
      user
    } = data;

    const payment = Payment.build({
      id,
      payment_id,
      type,
      method,
      amount,
      payer_name,
      payer_surname,
      payer_phone,
      currency,
      premium,
      balance_before,
      balance_after,
      created_at,
      created_by,
      status,
      account_main,
      account,
      account_package,
      company,
      branch,
    });
    await payment.save();

    // calculate account balances

    if (type === 0) {
      let accountFun = await AccountFun.findById(account);

      if (!accountFun) {
        throw new Error("account funeral not found");
      }

      accountFun.set({
        balance: accountFun.balance + amount,
      })
      accountFun.save();

      new AccountFunUpdatedPublisher(this.client).publish({
        id: accountFun.id,
        attachments: accountFun.attachments,
        policy_id: accountFun.policy_id,
        payment_status: accountFun.payment_status,
        next_payment_date: accountFun.next_payment_date,
        balance: accountFun.balance,
        premium: accountFun.premium,
        status: accountFun.status,
        company: accountFun.company,
        branch: accountFun.branch,
        package_fun: accountFun.package_fun,
        account_main: accountFun.account_main,
        created_at: accountFun.created_at,
        created_by: accountFun.created_by,
        version: accountFun.version,
        user
      });
    }

    msg.ack();
  }
}

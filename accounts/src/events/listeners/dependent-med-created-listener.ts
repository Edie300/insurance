import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  DependentMedCreatedEvent,
} from "@totum-insurance/common";
import {} from "../../models/package-funeral";
import { Premium } from "../../services/Premium";
import { AccountMain } from "../../models/account-main";
import { AccountMed } from "../../models/account-medical";
import { PackageMed } from "../../models/package-medical";
import { DependentMed } from "../../models/dependent-medical";
import { queueGroupName } from "./queue-group-name";

import { AccountMedUpdatedPublisher  } from "../publishers/account-med-updated-publisher";
import { natsWrapper } from "../../nats-wrapper";

export class DependentMedCreatedListener extends Listener<DependentMedCreatedEvent> {
  subject: Subjects.DependentMedCreated = Subjects.DependentMedCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: DependentMedCreatedEvent["data"], msg: Message) {
    console.log("dependent-med-created-listener::", data);
    const {
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      status,
      account_main,
      account_med,
      company,
      package_med,
      created_at,
      created_by,
      version,
      user,
    } = data;

    const dependents = await DependentMed.find({ account_med: account_med });

    const holder = await AccountMain.findById(account_main);

    if (!holder) {
      throw new Error("main account not found");
    }

    const pkg = await PackageMed.findById(package_med);

    if (!pkg) {
      throw new Error("package medical not found");
    }

    //  Recalculate account premium
    const accountMed = await AccountMed.findById(account_med);

    if (!accountMed) {
      throw new Error("account medical not found");
    }

    const premium =  new Premium(dependents, holder, pkg)
    accountMed.set({
      premium: premium.calculatePremium(),
    });
    
    await accountMed.save();

    new AccountMedUpdatedPublisher(natsWrapper.client).publish({
      id: accountMed.id,
      attachments: accountMed.attachments,
      policy_id: accountMed.policy_id,
      payment_status: accountMed.payment_status,
      balance: accountMed.balance,
      premium: accountMed.premium,
      status: accountMed.status,
      company: accountMed.company,
      branch: accountMed.branch,
      package_med: accountMed.package_med,
      account_main: accountMed.account_main,
      created_at: accountMed.created_at,
      created_by: accountMed.created_by,
      version: accountMed.version,
      user,
    });


    msg.ack();
  }
}

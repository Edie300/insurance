import { DependentFunStatus } from "@totum-insurance/common";
import moment from "moment";

// moment().diff(dependent.dob, "years", true)

interface Dependent {
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  relationship: string;
  occupation: string;
  status: number;
}

interface Dependents extends Array<Dependent> {}

interface Holder {
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  status: number
}

interface Package {
  child_max_age: number;
  child_max_age_school: number;
  family_rate: number;
  dependent_rate: number;
  individual_rates: { min_age: number; max_age: number; rate: number }[];
  child_rate: number;
  unbilled_dependents: string[];
}

export class Premium {
  public premium: number;
  public dependents: Dependents;
  public holder: Holder;
  public pkg: Package;

  constructor(dependents: Dependents, holder: Holder, pkg: Package) {
    this.premium = 0;
    this.dependents = dependents;
    this.holder = holder;
    this.pkg = pkg;
  }

  private isBilled(dep: Dependent): Boolean {
    if (this.pkg.unbilled_dependents.includes(dep.relationship)) {
      return false;
    }
    return true;
  }

  private individualRate(dep: Dependent | Holder): number {
    const range = this.pkg.individual_rates.filter((rate) => {
      return (
        rate.min_age < moment().diff(dep.date_of_birth, "years", true) &&
        rate.max_age > moment().diff(dep.date_of_birth, "years", true)
      );
    });

    if (range.length === 0) {
      return 0
    }
    return range[0].rate;
  }

  public calculatePremium(): number {
    // calculate rates for dependents
    this.dependents.forEach((dependent) => {
      if (!this.isBilled(dependent)) {
        console.log("dependent not billed", dependent.relationship)
        return this.premium;
      }

      this.premium = this.premium + this.individualRate(dependent);
    });

    // calculate rate for holder
    this.premium = this.premium + this.individualRate(this.holder);

    return this.premium;
  }
}

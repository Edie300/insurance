import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { PackageMotorStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface PackageMotorAttrs {
  id: string;
  attachments: {}[];
  name: string;
  description: string;
  type: string;
  act_of_god: boolean;
  bodily_damage: boolean;
  cash_in_lieu: number;
  property_damage: boolean;
  waiting_time: number;
  claim_processing_time: number;
  cash_back: boolean;
  cash_back_period: number;
  road_side_assistance: boolean;
  towing_services: boolean;
  premium_main: number;
  breakdown_fuel: number;
  airtime: number;
  personal_injury_protection: number;
  period_of_cover: number;
  full_vehicle_cover: boolean;
  policy_maturity: number;
  premium: number;
  currency: string;
  payment_type: string;
  status: PackageMotorStatus;
  company: Types.ObjectId;

}

// An interface that describes the properties
// that a User Model has
interface PackageMotorModel extends mongoose.Model<PackageMotorDoc> {
  build(attrs: PackageMotorAttrs): PackageMotorDoc;
}

// An interface that describes the properties
// that a User Document has
interface PackageMotorDoc extends mongoose.Document {
  attachments: {}[];
  name: string;
  description: string;
  type: string;
  act_of_god: boolean;
  bodily_damage: boolean;
  cash_in_lieu: number;
  property_damage: boolean;
  waiting_time: number;
  claim_processing_time: number;
  cash_back: boolean;
  cash_back_period: number;
  road_side_assistance: boolean;
  towing_services: boolean;
  premium_main: number;
  breakdown_fuel: number;
  airtime: number;
  personal_injury_protection: number;
  period_of_cover: number;
  full_vehicle_cover: boolean;
  policy_maturity: number;
  premium: number;
  currency: string;
  payment_type: string;
  status: PackageMotorStatus;
  company: Types.ObjectId;
  version: number;
}

const packageMotorSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: [true, "Please add a name"],
      trim: true,
      maxlength: [50, "Name can not be more than 50 characters"],
    },

    description: {
      type: String,
      required: [true, "Please add a description"],
      maxlength: [700, "Description can not be more than 700 characters"],
    },

    type: {
      type: String,
      enum: ["thirdParty", "fullCover", "fireAndTheft"],
    },

    act_of_god: {
      type: Boolean,
      default: false,
    },
    bodily_damage: {
      type: Boolean,
      default: false,
    },
    cash_in_lieu: {
      type: Number,
    },
    property_damage: {
      type: Boolean,
      default: false,
    },
    waiting_time: {
      type: Number,
    },
    claim_processing_time: {
      type: Number,
      default: 1,
    },
    cash_back: {
      type: Boolean,
      default: false,
    },
    cash_back_period: {
      type: Number,
      default: 60,
    },
    road_side_assistance: {
      type: Boolean,
      default: false,
    },
    towing_services: {
      type: Boolean,
      default: false,
    },
    premium_main: {
      type: Number,
    },
    breakdown_fuel: {
      type: Number,
    },
    airtime: {
      type: Number,
    },
    personal_injury_protection: {
      type: Number,
    },
    period_of_cover: {
      type: Number,
    },
    full_vehicle_cover: {
      type: Boolean,
      default: false,
    },
    policy_maturity: {
      type: Number,
    },

    premium: {
      type: Number,
      required: [true, "Please add a premium amount"],
    },
    currency: {
      type: String,
      enum: ["ZW", "US", "ZAR"],
    },
    payment_type: {
      type: String,
      enum: ["weekly", "yearly", "monthly"],
    },

    status: {
      type: String,
      enum: Object.values(PackageMotorStatus),
      default: PackageMotorStatus.Unpublished,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

packageMotorSchema.set("versionKey", "version");
packageMotorSchema.plugin(updateIfCurrentPlugin);

packageMotorSchema.statics.build = (attrs: PackageMotorAttrs) => {
  return new PackageMotor({
    id: attrs.id,
    attachments: attrs.attachments,
    name: attrs.name,
    description: attrs.description,
    type: attrs.type,
    act_of_god: attrs.act_of_god,
    bodily_damage: attrs.bodily_damage,
    cash_in_lieu: attrs.cash_in_lieu,
    property_damage: attrs.property_damage,
    waiting_time: attrs.waiting_time,
    claim_processing_time: attrs.claim_processing_time,
    cash_back: attrs.cash_back,
    cash_back_period: attrs.cash_back_period,
    road_side_assistance: attrs.road_side_assistance,
    towing_services: attrs.towing_services,
    premium_main: attrs.premium_main,
    breakdown_fuel: attrs.breakdown_fuel,
    airtime: attrs.airtime,
    personal_injury_protection: attrs.personal_injury_protection,
    period_of_cover: attrs.period_of_cover,
    full_vehicle_cover: attrs.full_vehicle_cover,
    policy_maturity: attrs.policy_maturity,
    premium: attrs.premium,
    currency: attrs.currency,
    payment_type: attrs.payment_type,
    status: attrs.status,
    company: attrs.company,
  });
};

const PackageMotor = mongoose.model<PackageMotorDoc, PackageMotorModel>(
  "PackageMotor",
  packageMotorSchema
);

export { PackageMotor };

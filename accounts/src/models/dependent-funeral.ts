import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { DependentFunStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
export interface DependentFunAttrs {
  attachments?: string[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport?: string;
  drivers_licence?: string;
  email?: string;
  phone: string;
  address: string;
  relationship: string;
  occupation: string;
  status?: DependentFunStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  account_main: Types.ObjectId;
  account_fun: Types.ObjectId;
  company: Types.ObjectId;
  package_fun: Types.ObjectId;
}

// An interface that describes the properties

interface DependentFunModel extends mongoose.Model<DependentFunDoc> {
  build(attrs: DependentFunAttrs): DependentFunDoc;
}

interface DependentFunDoc extends mongoose.Document {
  attachments: {}[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  email: string;
  phone: string;
  address: string;
  relationship: string;
  occupation: string;
  status: number;
  created_at: Date;
  created_by: Types.ObjectId;
  account_fun: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  package_fun: Types.ObjectId;
  version: number;
}

const dependentFunSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    gender: {
      type: String,
      enum: ["male", "female"],
      required: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
    },
    marital_status: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    national_id: {
      type: String,
      required: true,
    },
    passport: {
      type: String,
    },
    drivers_licence: {
      type: String,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(DependentFunStatus),
      default: DependentFunStatus.Pending,
    },
    relationship: {
      type: String,
      required: [true, "Please add relationship to policy holder "],
      enum: [
        "spouse",
        "child",
        "parent",
        "uncle",
        "aunt",
        "nephew",
        "niece",
        "sibling",
        "grandparent",
      ],
    },
    occupation: {
      type: String,
      required: [true, "Please add occupation "],
      enum: ["student", "employed", "unemployed"],
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    account_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountFun",
      required: true,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    package_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageFun",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

dependentFunSchema.set("versionKey", "version");
dependentFunSchema.plugin(updateIfCurrentPlugin);

dependentFunSchema.statics.build = (attrs: DependentFunAttrs) => {
  return new DependentFun(attrs);
};

const DependentFun = mongoose.model<DependentFunDoc, DependentFunModel>(
  "DependentFun",
  dependentFunSchema
);

export { DependentFun };

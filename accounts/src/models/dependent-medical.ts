import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { DependentMedStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface DependentMedAttrs {
  attachments?: string[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport?: string;
  drivers_licence?: string;
  email?: string;
  phone: string;
  address: string;
  relationship: string;
  occupation?: string;
  status: DependentMedStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  account_main: Types.ObjectId;
  account_med: Types.ObjectId;
  company: Types.ObjectId;
  package_med: Types.ObjectId;
}

// An interface that describes the properties

interface DependentMedModel extends mongoose.Model<DependentMedDoc> {
  build(attrs: DependentMedAttrs): DependentMedDoc;
}

interface DependentMedDoc extends mongoose.Document {
  attachments: {}[];
  name: string;
  surname: string;
  gender: string;
  date_of_birth: Date;
  marital_status: string;
  title: string;
  national_id: string;
  passport: string;
  drivers_licence: string;
  email: string;
  phone: string;
  address: string;
  relationship: string;
  occupation: string;
  status: DependentMedStatus;
  created_at: Date;
  created_by: Types.ObjectId;
  account_main: Types.ObjectId;
  account_med: Types.ObjectId;
  company: Types.ObjectId;
  package_med: Types.ObjectId;
  version: number;
}

const dependentMedSchema = new mongoose.Schema(
  {
    attachments: [],
    name: {
      type: String,
      required: true,
    },
    surname: {
      type: String,
      required: true,
    },
    gender: {
      type: String,
      enum: ["male", "female"],
      required: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
    },
    marital_status: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    national_id: {
      type: String,
      required: true,
    },
    passport: {
      type: String,
    },
    drivers_licence: {
      type: String,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(DependentMedStatus),
      default: DependentMedStatus.Pending,
    },
    relationship: {
      type: String,
      required: [true, "Please add relationship to policy holder "],
      enum: [
        "spouse",
        "child",
        "mother",
        "father",
        "uncle",
        "aunt",
        "nephew",
        "niece",
        "brother",
        "sister",
        "grandmother",
        "grandfather",
      ],
    },
    occupation: {
      type: String,
      required: [true, "Please add occupation "],
      enum: ["student", "employed", "unemployed"],
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    account_med: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMed",
      required: true,
    },
    package_med: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageFun",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

dependentMedSchema.set("versionKey", "version");
dependentMedSchema.plugin(updateIfCurrentPlugin);

dependentMedSchema.statics.build = (attrs: DependentMedAttrs) => {
  return new DependentMed(attrs);
};

const DependentMed = mongoose.model<DependentMedDoc, DependentMedModel>(
  "DependentMed",
  dependentMedSchema
);

export { DependentMed };

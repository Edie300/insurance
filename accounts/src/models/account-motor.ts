import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { AccountMotorStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface AcountMotorAttrs {
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountMotorStatus;
  package_motor: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
}

// An interface that describes the properties

interface AccountMotorModel extends mongoose.Model<AccountMotorDoc> {
  build(attrs: AcountMotorAttrs): AccountMotorDoc;
}

interface AccountMotorDoc extends mongoose.Document {
  id: string;
  attachments: {}[];
  policy_id: string;
  payment_status: string;
  balance: number;
  premium: number;
  status: AccountMotorStatus;
  package_motor: Types.ObjectId;
  account_main: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
  version: number;
}

const accountMotorSchema = new mongoose.Schema(
  {
    attachments: [],
    policy_id: {
      type: String,
      required: true,
    },

    balance: {
      type: Number,
      default: 0,
    },
    premium: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: Object.values(AccountMotorStatus),
      default: AccountMotorStatus.Pending,
    },
    payment_status: {
      type: Number,
      default: 0,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
    package_motor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageMotor",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        ret.type = 2;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

accountMotorSchema.set("versionKey", "version");
accountMotorSchema.plugin(updateIfCurrentPlugin);

accountMotorSchema.statics.build = (attrs: AcountMotorAttrs) => {
  return new AccountMotor(attrs);
};

const AccountMotor = mongoose.model<AccountMotorDoc, AccountMotorModel>(
  "AccountMotor",
  accountMotorSchema
);

export { AccountMotor };

import mongoose, {Types} from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { BranchStatus } from "@totum-insurance/common";

import { CompanyDoc } from "./company";

interface ActivityAttrs {
  activity_type: string;
  activity_data: string;
  date: Date;
  created_at: Date;
  user: string;
  entity_id: string;
}

// An interface that describes the properties
// that a User Model has
interface ActivityModel extends mongoose.Model<AttributeDoc> {
  build(attrs: ActivityAttrs): AttributeDoc;
}

// An interface that describes the properties
// that a User Document has
export interface AttributeDoc extends mongoose.Document {
  id: string;
  activity_type: string;
  activity_data: string;
  date: Date;
  created_at: Date;
  user: string;
  entity_id: string;
  version: number;
}

const activitySchema = new mongoose.Schema(
  {
    activity_type: {
      type: String,
      required: true,
    },
    activity_data: {
      type: String,
    },
    date: {
      type: Date,
      required: true,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    entity_id: {
      type: mongoose.Schema.Types.ObjectId,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

activitySchema.set("versionKey", "version");
activitySchema.plugin(updateIfCurrentPlugin);

activitySchema.statics.build = (attrs: ActivityAttrs) => {
  return new Activity(attrs);
};

const Activity = mongoose.model<AttributeDoc, ActivityModel>("Activity", activitySchema);

export { Activity };

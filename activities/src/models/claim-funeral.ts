import mongoose, { Types } from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { ClaimFunStatus } from "@totum-insurance/common";

// An interface that describes the properties
// that are requried to create a new User
interface ClaimFunAttrs {
  id: string;
  attachments: {}[];
  claim_id: string;
  claim_type: number;
  deceased_type: number;
  date_of_death: string;
  cause_of_death: string;
  date_of_funeral: string;
  place_of_burial: string;
  claiment_name: string;
  claiment_surname: string;
  claiment_title: string;
  claiment_date_of_birth: Date;
  claiment_email: string;
  claiment_address: string;
  claiment_cell: string;
  claiment_national_id: string;
  claiment_passport: string;
  claiment_relationship: string;
  bank_account_holder: string;
  bank_account_number: string;
  bank_name: string;
  bank_branch_code: string;
  bank_account_type: number;
  status: number;
  sum_assured: number;
  entity_id?: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
  approved_by?: string;
  rejected_by?: string;
  account_main: Types.ObjectId;
  account_fun: Types.ObjectId;
  package_fun: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface ClaimFunModel extends mongoose.Model<ClaimFunDoc> {
  build(attrs: ClaimFunAttrs): ClaimFunDoc;
}

// An interface that describes the properties
// that a User Document has
interface ClaimFunDoc extends mongoose.Document {
  attachments: {}[];
  claim_id: string;
  claim_type: number;
  deceased_type: number;
  date_of_death: string;
  cause_of_death: string;
  date_of_funeral: string;
  place_of_burial: string;
  claiment_name: string;
  claiment_surname: string;
  claiment_title: string;
  claiment_date_of_birth: Date;
  claiment_email: string;
  claiment_address: string;
  claiment_cell: string;
  claiment_national_id: string;
  claiment_passport: string;
  claiment_relationship: string;
  bank_account_holder: string;
  bank_account_number: string;
  bank_name: string;
  bank_branch_code: string;
  bank_account_type: number;
  status: number;
  sum_assured: number;
  processing_time?: number;
  entity_id?: Types.ObjectId;
  created_at: Date;
  created_by: Types.ObjectId;
  approved_by?: string;
  rejected_by?: string;
  account_main: Types.ObjectId;
  account_fun: Types.ObjectId;
  package_fun: Types.ObjectId;
  company: Types.ObjectId;
  branch: Types.ObjectId;
  version: number;
}

const claimFunSchema = new mongoose.Schema(
  {
    attachments: [],
    claim_id: { type: String, required: true },
    claim_type: { type: Number },
    deceased_type: { type: Number },
    date_of_death: { type: String },
    cause_of_death: { type: String },
    date_of_funeral: { type: String },
    place_of_burial: { type: String },
    claiment_name: { type: String },
    claiment_surname: { type: String },
    claiment_title: { type: String },
    claiment_date_of_birth: { type: Date },
    claiment_email: { type: String },
    claiment_address: { type: String },
    claiment_cell: { type: String },
    claiment_national_id: { type: String },
    claiment_passport: { type: String },
    claiment_relationship: { type: String },
    bank_account_holder: { type: String },
    bank_account_number: { type: String },
    bank_name: { type: String },
    bank_branch_code: { type: String },

    // 0: savings, 1: cheque, 2: current, 3: transmission
    bank_account_type: {
      type: Number,
      enum: [0, 1, 2, 3],
    },

    status: {
      type: String,
      enum: Object.values(ClaimFunStatus),
      default: ClaimFunStatus.Pending,
    },

    sum_assured: {
      type: Number,
    },

    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },

    // processing time is in hours
    processing_time: {
      type: Number,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    entity_id: {
      type: mongoose.Schema.Types.ObjectId,
    },
    approved_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    rejected_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    account_main: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountMain",
      required: true,
    },
    account_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AccountFun",
      required: true,
    },
    package_fun: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PackageFun",
      required: true,
    },

    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
      required: true,
    },
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Branch",
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

claimFunSchema.set("versionKey", "version");
claimFunSchema.plugin(updateIfCurrentPlugin);

claimFunSchema.statics.build = (attrs: ClaimFunAttrs) => {
  return new ClaimFun({
    _id: attrs.id,
    attachments: attrs.attachments,
    claim_id: attrs.claim_id,
    claim_type: attrs.claim_type,
    deceased_type: attrs.deceased_type,
    date_of_death: attrs.date_of_death,
    cause_of_death: attrs.cause_of_death,
    date_of_funeral: attrs.date_of_funeral,
    place_of_burial: attrs.place_of_burial,
    claiment_name: attrs.claiment_name,
    claiment_surname: attrs.claiment_surname,
    claiment_title: attrs.claiment_title,
    claiment_date_of_birth: attrs.claiment_date_of_birth,
    claiment_email: attrs.claiment_email,
    claiment_address: attrs.claiment_address,
    claiment_cell: attrs.claiment_cell,
    claiment_national_id: attrs.claiment_national_id,
    claiment_passport: attrs.claiment_passport,
    claiment_relationship: attrs.claiment_relationship,
    bank_account_holder: attrs.bank_account_holder,
    bank_account_number: attrs.bank_account_number,
    bank_name: attrs.bank_name,
    bank_branch_code: attrs.bank_branch_code,
    bank_account_type: attrs.bank_account_type,
    status: attrs.status,
    sum_assured: attrs.sum_assured,
    entity_id: attrs.entity_id,
    created_at: attrs.created_at,
    created_by: attrs.created_by,
    approved_by: attrs.approved_by,
    rejected_by: attrs.rejected_by,
    account_main: attrs.account_main,
    account_fun: attrs.account_fun,
    package_fun: attrs.package_fun,
    company: attrs.company,
    branch: attrs.branch,
  });
};

const ClaimFun = mongoose.model<ClaimFunDoc, ClaimFunModel>(
  "ClaimFun",
  claimFunSchema
);

export { ClaimFun };

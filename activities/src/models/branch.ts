import mongoose, {Types} from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { BranchStatus } from "@totum-insurance/common";

import { CompanyDoc } from "./company";

interface BranchAttrs {
  id: string;
  name: string;
  phones: string[];
  landlines: string[];
  address: string;
  status: BranchStatus;
  company: Types.ObjectId;
}

// An interface that describes the properties
// that a User Model has
interface BranchModel extends mongoose.Model<BranchDoc> {
  build(attrs: BranchAttrs): BranchDoc;
  findByEvent(event: {
    id: string;
    version: number;
  }): Promise<BranchDoc | null>;
}

// An interface that describes the properties
// that a User Document has
export interface BranchDoc extends mongoose.Document {
  name: string;
  phones: string[];
  landlines: string[];
  address: string;
  status: BranchStatus;
  version: number;
  company: Types.ObjectId;
}

const branchSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    phones: [String],
    landlines: [String],
    address: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(BranchStatus),
      default: BranchStatus.Created,
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company",
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

branchSchema.set("versionKey", "version");
branchSchema.plugin(updateIfCurrentPlugin);

branchSchema.statics.findByEvent = (event: { id: string; version: number }) => {
  return Branch.findOne({
    _id: event.id,
    version: event.version - 1,
  });
};

branchSchema.statics.build = (attrs: BranchAttrs) => {
  return new Branch({
    _id: attrs.id,
    name: attrs.name,
    phones: attrs.phones,
    landlines: attrs.landlines,
    address: attrs.address,
    status: attrs.status,
    company: attrs.company,
  });
};

const Branch = mongoose.model<BranchDoc, BranchModel>("Branch", branchSchema);

export { Branch };

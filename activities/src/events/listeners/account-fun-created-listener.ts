import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountFunCreatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { AccountFun } from "../../models/account-funeral";
import { queueGroupName } from "./queue-group-name";

export class AccountFunCreatedListener extends Listener<AccountFunCreatedEvent> {
  subject: Subjects.AccountFunCreated = Subjects.AccountFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountFunCreatedEvent["data"], msg: Message) {
    console.log("account-fun-created-listener::", data);
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
      user
    } = data;

    const accountFun = AccountFun.build({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_fun,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by
    });
    await accountFun.save();

    const activity = Activity.build({
      activity_type: Subjects.AccountFunCreated,
      activity_data: JSON.stringify({ policy_id }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

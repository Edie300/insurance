import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMedCreatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { AccountMed } from "../../models/account-medical";
import { queueGroupName } from "./queue-group-name";

export class AccountMedCreatedListener extends Listener<AccountMedCreatedEvent> {
  subject: Subjects.AccountMedCreated = Subjects.AccountMedCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMedCreatedEvent["data"], msg: Message) {
    console.log("account-med-created-listener::", data);
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
      user
     } = data;

    const accountMed = AccountMed.build({      
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
     });
    await accountMed.save();

    const activity = Activity.build({
      activity_type: Subjects.AccountMedCreated,
      activity_data: JSON.stringify({ policy_id }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

import mongoose, {Types, ObjectId} from "mongoose";
import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  ClaimFunCreatedEvent,
} from "@totum-insurance/common";
import { ClaimFun } from "../../models/claim-funeral";
import { AccountMain } from "../../models/account-main";
import { Activity } from "../../models/activity";
import { queueGroupName } from "./queue-group-name";

export class ClaimFunCreatedListener extends Listener<ClaimFunCreatedEvent> {
  subject: Subjects.ClaimFunCreated = Subjects.ClaimFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: ClaimFunCreatedEvent["data"], msg: Message) {
    console.log("claim-fun-created-listener::", data);
    const {
      id,
      attachments,
      claim_id,
      claim_type,
      deceased_type,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      status,
      sum_assured,
      entity_id,
      created_at,
      created_by,
      account_main,
      package_fun,
      account_fun,
      company,
      branch,
      user,
    } = data;

    const accountMain = await AccountMain.findById(account_main)

    if (!accountMain) {
      throw new Error("account main not found");
    }

    const { account_id } = accountMain

    const claimFun = ClaimFun.build({
      id,
      attachments,
      claim_id,
      claim_type,
      deceased_type,
      date_of_death,
      cause_of_death,
      date_of_funeral,
      place_of_burial,
      claiment_name,
      claiment_surname,
      claiment_title,
      claiment_date_of_birth,
      claiment_email,
      claiment_address,
      claiment_cell,
      claiment_national_id,
      claiment_passport,
      claiment_relationship,
      bank_account_holder,
      bank_account_number,
      bank_name,
      bank_branch_code,
      bank_account_type,
      status,
      sum_assured,
      entity_id,
      created_at,
      created_by,
      account_main,
      package_fun,
      account_fun,
      company,
      branch,
    });
    await claimFun.save();

    const activity = Activity.build({
      activity_type: Subjects.ClaimFunCreated,
      activity_data: JSON.stringify({deceased_type, account_id}),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

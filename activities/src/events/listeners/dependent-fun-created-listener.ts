import { Message } from 'node-nats-streaming';
import { Subjects, Listener, DependentFunCreatedEvent } from '@totum-insurance/common';
import { PackageFun } from '../../models/package-funeral';
import { queueGroupName } from './queue-group-name';

export class DependentFunCreatedListener extends Listener<DependentFunCreatedEvent> {
  subject: Subjects.DependentFunCreated = Subjects.DependentFunCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: DependentFunCreatedEvent['data'], msg: Message) {

    console.log("package-fun-created-listener::", data)
    const { 
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      relationship,
      occupation,
      status,
      account_main,
      account_fun,
      company,
      package_fun,
      created_at,
      created_by,
      version,
     } = data;

    msg.ack();
  }
}

import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMotorCreatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { AccountMotor } from "../../models/account-motor";
import { queueGroupName } from "./queue-group-name";

export class AccountMotorCreatedListener extends Listener<AccountMotorCreatedEvent> {
  subject: Subjects.AccountMotorCreated = Subjects.AccountMotorCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMotorCreatedEvent["data"], msg: Message) {
    console.log("account-motor-created-listener::", data);
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_motor,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by,
      user
    } = data;

    const accountMotor = AccountMotor.build({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_motor,
      company,
      branch,
      balance, 
      premium, 
      account_main, 
      created_at, 
      created_by
    });
    await accountMotor.save();

    const activity = Activity.build({
      activity_type: Subjects.AccountMotorCreated,
      activity_data: JSON.stringify({ policy_id }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

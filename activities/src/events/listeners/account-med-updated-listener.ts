import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMedUpdatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { AccountMed } from "../../models/account-medical";
import { queueGroupName } from "./queue-group-name";

export class AccountMedUpdatedListener extends Listener<AccountMedUpdatedEvent> {
  subject: Subjects.AccountMedUpdated = Subjects.AccountMedUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMedUpdatedEvent["data"], msg: Message) {
    console.log("account-med-updated-listener::", data);
    const accountMed = await AccountMed.findByEvent(data);

    if (!accountMed) {
      throw new Error("Account medical not found");
    }
    const {
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance,
      premium,
      account_main,
      created_at,
      created_by,
      user
    } = data;

    accountMed.set({
      id,
      attachments,
      policy_id,
      payment_status,
      status,
      package_med,
      company,
      branch,
      balance,
      premium,
      account_main,
      created_at,
      created_by,
    });
    await accountMed.save();

    const activity = Activity.build({
      activity_type: Subjects.AccountMedUpdated,
      activity_data: JSON.stringify({ policy_id }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  PackageFunUpdatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { PackageFun } from "../../models/package-funeral";
import { queueGroupName } from "./queue-group-name";

export class PackageFunUpdatedListener extends Listener<PackageFunUpdatedEvent> {
  subject: Subjects.PackageFunUpdated = Subjects.PackageFunUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: PackageFunUpdatedEvent["data"], msg: Message) {
    console.log("package-fun-updated-listener::", data);
    const packageFun = await PackageFun.findByEvent(data);

    if (!packageFun) {
      throw new Error("package funeral not found");
    }


    const {
      id,
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      created_at,
      created_by,
      status,
      casket,
      company,
      user,
    } = data;

    packageFun.set({
      id,
      attachments,
      name,
      description,
      policy_lapse_period,
      policy_archive_period,
      payment_grace_period,
      waiting_period,
      principal_member_min_age,
      principal_member_max_age,
      dependent_max_age,
      dependents_max_count,
      policy_maturity,
      child_max_age,
      child_max_age_school,
      sum_assured,
      hearse,
      bus,
      groceries,
      cash_in_lieu,
      accidental_death,
      mortuary_services,
      events_management,
      burial_services,
      service_area,
      family_rate,
      dependent_rate,
      individual_rates,
      child_rate,
      unbilled_dependents,
      currency,
      payment_type,
      created_at,
      created_by,
      status,
      company,
      casket,
    });
    await packageFun.save();

    const activity = Activity.build({
      activity_type: Subjects.PackageFunUpdated,
      activity_data: JSON.stringify({ name }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

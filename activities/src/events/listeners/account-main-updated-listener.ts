import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  AccountMainUpdatedEvent,
} from "@totum-insurance/common";
import { Activity } from "../../models/activity";
import { AccountMain } from "../../models/account-main";
import { queueGroupName } from "./queue-group-name";

export class AccountMainUpdatedListener extends Listener<AccountMainUpdatedEvent> {
  subject: Subjects.AccountMainUpdated = Subjects.AccountMainUpdated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: AccountMainUpdatedEvent["data"], msg: Message) {
    console.log("account-main-updated-listener::", data);
    
    const {
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
      user
    } = data;

    const accountMain = await AccountMain.findById(id);

    if (!accountMain) {
      throw new Error("account main not found");
    }

    accountMain.set({
      id,
      attachments,
      name,
      surname,
      gender,
      date_of_birth,
      marital_status,
      title,
      national_id,
      passport,
      drivers_licence,
      email,
      phone,
      address,
      account_id,
      status,
      created_at,
      created_by,
      company,
      branch,
    });

    await accountMain.save();

    const activity = Activity.build({
      activity_type: Subjects.AccountMainUpdated,
      activity_data: JSON.stringify({ account_id, name, surname, national_id }),
      date: user.date,
      created_at: new Date(),
      user: user.id,
      entity_id: id,
    });
    await activity.save();

    msg.ack();
  }
}

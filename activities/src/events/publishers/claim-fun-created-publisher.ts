import { Publisher, Subjects, ClaimFunCreatedEvent } from "@totum-insurance/common";

export class ClaimFunCreatedPublisher extends Publisher<ClaimFunCreatedEvent> {
  subject: Subjects.ClaimFunCreated = Subjects.ClaimFunCreated;
}

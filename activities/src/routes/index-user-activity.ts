import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  BadRequestError,
  requireAuth,
} from "@totum-insurance/common";
import { Activity } from "../models/activity";

const router = express.Router();

router.get(
  "/api/activities/:id/user",
  requireAuth,

  async (req: Request, res: Response) => {
    const activities = await Activity.find({ user: req.params.id }).populate(
      "user"
    ).sort({date: -1});

    res.send(activities);
  }
);

export { router as indexUserActivityRouter };

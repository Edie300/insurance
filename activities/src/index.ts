import mongoose from "mongoose";

import { app } from "./app";
import { natsWrapper } from './nats-wrapper';
import { AccountFunCreatedListener } from "./events/listeners/account-fun-created-listener";
import { AccountFunUpdatedListener } from "./events/listeners/account-fun-updated-listener";
import { AccountMedCreatedListener } from "./events/listeners/account-med-created-listener";
import { AccountMainCreatedListener } from "./events/listeners/account-main-created-listener";
import { AccountMainUpdatedListener } from "./events/listeners/account-main-updated-listener";
import { AccountMedUpdatedListener } from "./events/listeners/account-med-updated-listener";
import { AccountMotorCreatedListener } from "./events/listeners/account-motor-created-listener";
import { BranchCreatedListener } from "./events/listeners/branch-created-listener";
import { BranchUpdatedListener } from "./events/listeners/branch-updated-listener";
import { CasketCreatedListener } from "./events/listeners/casket-created-listener";
import { CasketUpdatedListener } from "./events/listeners/casket-updated-listener";
import { ClaimFunCreatedListener } from "./events/listeners/claim-fun-created-listener";
import { CompanyCreatedListener } from "./events/listeners/company-created-listener";
import { CompanyUpdatedListener } from "./events/listeners/company-updated-listener";
import { PackageFunCreatedListener } from "./events/listeners/package-fun-created-listener";
import { PackageFunUpdatedListener } from "./events/listeners/package-fun-updated-listener";
import { PackageMedCreatedListener } from "./events/listeners/package-med-created-listener";
import { PackageMedUpdatedListener } from "./events/listeners/package-med-updated-listener";
import { PackageMotorCreatedListener } from "./events/listeners/package-motor-created-listener";
import { PackageMotorUpdatedListener } from "./events/listeners/package-motor-updated-listener";
import { UserCreatedListener } from "./events/listeners/user-created-listener";
import { VehicleMotorCreatedListener } from "./events/listeners/vehicle-motor-created-listener";

const start = async () => {
  // if (!process.env.JWT_KEY) {
  //   throw new Error('JWT_KEY must be defined');
  // }
  if (!process.env.NATS_URL) {
    throw new Error("NATS_URL must be defined.");
  }
  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error("NATS_CLUSTER_ID must be defined.");
  }
  if (!process.env.NATS_CLIENT_ID) {
    throw new Error("NATS_CLIENT_ID must be defined.");
  }
  if (!process.env.MONGO_URI) {
    throw new Error("MONGO_URI must be defined");
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );

    // Following lines are required for gracefully shutting down client
    natsWrapper.client.on("close", () => {
      console.log("NATS connection closed");
      process.exit();
    });
    process.on("SIGINT", () => natsWrapper.client.close());
    process.on("SIGTERM", () => natsWrapper.client.close());


    new AccountFunCreatedListener(natsWrapper.client).listen();
    new AccountFunUpdatedListener(natsWrapper.client).listen();
    new AccountMedCreatedListener(natsWrapper.client).listen();
    new AccountMedUpdatedListener(natsWrapper.client).listen();
    new AccountMotorCreatedListener(natsWrapper.client).listen();
    new AccountMainCreatedListener(natsWrapper.client).listen();
    new AccountMainUpdatedListener(natsWrapper.client).listen();  
    new BranchCreatedListener(natsWrapper.client).listen();
    new BranchUpdatedListener(natsWrapper.client).listen();
    new CasketCreatedListener(natsWrapper.client).listen();
    new CasketUpdatedListener(natsWrapper.client).listen();
    new ClaimFunCreatedListener(natsWrapper.client).listen();
    new CompanyCreatedListener(natsWrapper.client).listen();
    new CompanyUpdatedListener(natsWrapper.client).listen();
    new PackageFunCreatedListener(natsWrapper.client).listen();
    new PackageFunUpdatedListener(natsWrapper.client).listen();
    new PackageMedCreatedListener(natsWrapper.client).listen();
    new PackageMedUpdatedListener(natsWrapper.client).listen();
    new PackageMotorCreatedListener(natsWrapper.client).listen();
    new PackageMotorUpdatedListener(natsWrapper.client).listen();
    new UserCreatedListener(natsWrapper.client).listen();
    new VehicleMotorCreatedListener(natsWrapper.client).listen();

    await mongoose.connect(process.env.MONGO_URI);
    console.log("Connected to MongoDb");
  } catch (err) {
    console.error(err);
  }

  app.listen(6555, () => {
    console.log("Listening on port 6555!!!!!!!!");
  });
};

start();

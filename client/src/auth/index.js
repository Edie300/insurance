import { API, BACKEND_API } from "../config";
import axios from "axios";

export const authenticate = (data, next) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("jwt", JSON.stringify(data));
    next();
  }
};

export const isAuthenticated = () => {
  if (typeof window == "undefined") {
    return false;
  }
  if (localStorage.getItem("jwt")) {
    return JSON.parse(localStorage.getItem("jwt"));
  }
  else {
    return false
  }
};

// export const signin = (user) => {
//   return axios.post(`${API}/auth/login`, user);
// };

export const signin = (user) => {
  return axios.post(`${BACKEND_API}:31596/api/users/signin`, user);
};

export const signup = (user) => {
  return axios.post(`${API}/auth/register`, user);
};

export const signout = (next) => {
  // if (typeof window !== "undefined") {
  //   localStorage.removeItem("jwt");
  //   next();
  //   // API call to backend here
  // }
  return axios.post(`${API}/auth/logout`);
};

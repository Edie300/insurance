import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createtFunDependent } from "../api/dependent-funeral";
import { createFunClaim } from "../api/claims-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

const ModalCreateClaim = ({
  show,
  onClose,
  dependents = [],
  accountFun = "",
  mainAccount = "23eqwxdfqwef",
  packageFun = "",
}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };

  const [claimFuneral, setClaimFuneral] = useState({
    attachments: "",
    claim_type: "",
    deceased_type: "",
    date_of_death: "",
    cause_of_death: "",
    date_of_funeral: "",
    place_of_burial: "",
    claiment_name: "",
    claiment_surname: "",
    claiment_title: "",
    claiment_date_of_birth: "",
    claiment_email: "",
    claiment_address: "",
    claiment_cell: "",
    claiment_national_id: "",
    claiment_passport: "",
    claiment_relationship: "",
    bank_account_holder: "",
    bank_account_number: "",
    bank_name: "",
    bank_branch_code: "",
    bank_account_type: "",
    entity_id: "",
    account_fun: accountFun,
  });

  const [loading, setLoading] = useState(false);

  const [checked, setChecked] = useState("");

  const handleDependentCheck = (event) => {
    console.log(event.target.value);
    let checkedDep = claimFuneral.entity_id;
    if (checkedDep === event.target.value) {
      checkedDep = "";
    } else {
      checkedDep = event.target.value;
    }
    setClaimFuneral({ ...claimFuneral, entity_id: checkedDep });
    console.log("claim", claimFuneral.entity_id);
  };

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setClaimFuneral({ ...claimFuneral, [name]: event.target.value });
  };

  // Handle select options

  const handleDeceasedType = (selected) => {
    let entityId = "";
    if (selected.value === 0) {
      entityId = mainAccount;
    } 
    setClaimFuneral({ ...claimFuneral,  deceased_type: selected.value, entity_id: entityId});
  };
  const handleBankAccountType = (selected) => {

    setClaimFuneral({ ...claimFuneral,  bank_account_type: selected.value});
  };

  const handleSubmitClaim = () => {
    setLoading(true);
    createFunClaim(token, claimFuneral)
      .then((res) => {
        console.log("response", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Create Claim</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">
            {step === 0 && (
              <div className="">
                <div className="packages__">
                  <div className="modal__">
                    <label className="form__label">Claim type</label>
                    <Select
                      defaultValue={claimFuneral.deceased_type}
                      onChange={handleDeceasedType}
                      options={[
                        { value: 0, label: "principal" },
                        { value: 1, label: "dependent" },
                      ]}
                    />
                  </div>

                  <div
                    className="claims__claiment"
                    style={{ marginTop: "2rem" }}
                  >
                    <div className="claims__">
                      <label className="form__label">Date of death</label>
                      <input
                        className="form__input"
                        type="date"
                        value={claimFuneral.date_of_death}
                        onChange={handleChange("date_of_death")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">Cause of death</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.cause_of_death}
                        onChange={handleChange("cause_of_death")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">Date of funeral</label>
                      <input
                        className="form__input"
                        type="date"
                        value={claimFuneral.date_of_funeral}
                        onChange={handleChange("date_of_funeral")}
                      />
                    </div>

                    <div className="claims__">
                      <label className="form__label">Place of burail</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.place_of_burial}
                        onChange={handleChange("place_of_burial")}
                      />
                    </div>
                  </div>
                  {claimFuneral.deceased_type === 1 && (
                    <>
                      <div class=" modal__table">
                        <div class=" modal__table-header">
                          <div class="modal__table-cell--check"></div>
                          <div class="accounts__table-cell">Name</div>
                          <div class="accounts__table-cell">ID number</div>
                          <div class="accounts__table-cell">Date of birth</div>
                          <div class="accounts__table-cell">gender</div>
                          <div class="accounts__table-cell">relationship</div>
                          <div class="accounts__table-cell">occupation</div>
                          <div class="accounts__table-cell">Date Added</div>
                          <div class="accounts__table-cell">Status</div>
                        </div>

                        {dependents.map((dependent, i) => (
                          <div class="modal__table-row" key={i}>
                            <div class=" modal__table-cell--check">
                              <input
                                checked={
                                  claimFuneral.entity_id === dependent.id
                                }
                                type="checkbox"
                                value={dependent.id}
                                onClick={handleDependentCheck}
                              />
                            </div>
                            <div class=" accounts__table-cell">
                              <p className="accounts__table-name">
                                {dependent.name} {dependent.surname}
                              </p>
                            </div>
                            <div class=" accounts__table-cell">
                              {dependent.national_id}
                            </div>
                            <div class=" accounts__table-cell">
                              {moment(dependent.date_of_birth).format("LL")}{" "}
                            </div>
                            <div class=" accounts__table-cell">
                              {dependent.gender}
                            </div>
                            <div class=" accounts__table-cell">
                              {dependent.relationship}
                            </div>
                            <div class=" accounts__table-cell">
                              {dependent.occupation}
                            </div>
                            <div class=" accounts__table-cell">
                              {moment(dependent.created_at).format("LLL")}
                            </div>
                            <div class=" accounts__table-cell">
                              {(() => {
                                switch (dependent.status) {
                                  case "0":
                                    return (
                                      <span className="badge badge__green">
                                        active
                                      </span>
                                    );
                                  case "1":
                                    return (
                                      <span className="badge badge__green">
                                        deceased
                                      </span>
                                    );
                                }
                              })()}
                            </div>
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back"></div>
                    <div className="packages__next">
                      <button className="btn btn--blue" onClick={nextStep}>
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 1 && (
              <>
                <div className="modal__">
                  <div className="claims__claiment">
                    <div className="claims__">
                      <label className="form__label">Claiment name</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_name}
                        onChange={handleChange("claiment_name")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment surname</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_surname}
                        onChange={handleChange("claiment_surname")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment title</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_title}
                        onChange={handleChange("claiment_title")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">
                        Claiment date of birth
                      </label>
                      <input
                        className="form__input"
                        type="date"
                        value={claimFuneral.claiment_date_of_birth}
                        onChange={handleChange("claiment_date_of_birth")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment email</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_email}
                        onChange={handleChange("claiment_email")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment phone</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_cell}
                        onChange={handleChange("claiment_cell")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment ID</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_national_id}
                        onChange={handleChange("claiment_national_id")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">Claiment passport</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_passport}
                        onChange={handleChange("claiment_passport")}
                      />
                    </div>
                    <div className="modal__">
                      <label className="form__label">
                        Claiment relationship
                      </label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.claiment_relationship}
                        onChange={handleChange("claiment_relationship")}
                      />
                    </div>
                  </div>
                </div>

                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button className="btn btn--blue-grey" onClick={prevStep}>
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button className="btn btn--blue" onClick={nextStep}>
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}

            {step === 2 && (
              <>
                <div className="modal__review">
                  <div className="claims__claiment">
                    <div className="claims__">
                      <label className="form__label">bank account holder</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.bank_account_holder}
                        onChange={handleChange("bank_account_holder")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">bank account number</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.bank_account_number}
                        onChange={handleChange("bank_account_number")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">bank name</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.bank_name}
                        onChange={handleChange("bank_name")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">branch code</label>
                      <input
                        className="form__input"
                        type="text"
                        value={claimFuneral.bank_branch_code}
                        onChange={handleChange("bank_branch_code")}
                      />
                    </div>
                    <div className="claims__">
                      <label className="form__label">account type</label>

                      <Select
                      defaultValue={claimFuneral.bank_account_type}
                      onChange={handleBankAccountType}
                      options={[
                        { value: 0, label: "savings" },
                        { value: 1, label: "cheque" },
                        { value: 2, label: "current" },
                        { value: 3, label: "transmission" },
                      ]}
                    />
                    </div>
                  </div>
                  <div className="modal__"></div>
                </div>

                {/* <p>{JSON.stringify(claimFuneral, null, 2)}</p> */}
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmitClaim}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button className="btn btn--blue-grey" onClick={prevStep}>
                        Back
                      </button>
                    </div>
                    <div className="modal__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 3 && (
              <div className="">
                <div className="success">
                  <div className="success__container">
                    <CheckSuccess />
                    <div className="success__heading">
                      <p>Success</p>
                    </div>
                    <div className="success__paragraph">
                      Claim details have been submitted
                    </div>
                    <div className="success__options">
                      <div className="success__option">
                        <button className="btn btn--blue-grey">Action 1</button>
                      </div>
                      <div className="success__option">
                        <button className="btn btn--blue-border">
                          Action 2
                        </button>
                      </div>
                      <div className="success__option">
                        <button
                          className="btn btn--blue"
                          type="button"
                          onClick={() => setStep(0)}
                        >
                          Create claim
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateClaim;

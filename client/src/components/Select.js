import React from "react";
import Select from "react-select";



const Selector = ({options, value, handleChange}) => {
  return (
    <div>
      <Select classNamePrefix='filter' value={value} options={options} onChange={handleChange}/>
    </div>
  );
};
export default Selector;

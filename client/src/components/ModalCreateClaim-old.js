import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createMainAccount } from "../api/account-main";
import { getPackagesFun } from "../api/package-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Funeral } from ".././assets/svg/Totum_Icon_Funeral.svg";
import { ReactComponent as Health } from ".././assets/svg/Totum_Icon_Health.svg";
import { ReactComponent as Motor } from ".././assets/svg/Totum_Icon_Motor.svg";
import { ReactComponent as Life } from ".././assets/svg/Totum_Icon_Life.svg";
import { ReactComponent as Home } from ".././assets/svg/Totum_Icon_Home.svg";
import { ReactComponent as Travel } from ".././assets/svg/Totum_Icon_Travel.svg";
import { ReactComponent as Appliance } from ".././assets/svg/Totum_Icon_Appliance.svg";
import { ReactComponent as Pet } from ".././assets/svg/Totum_Icon_Pet.svg";
import { ReactComponent as Cyber } from ".././assets/svg/Totum_Icon_Cyber.svg";
import { ReactComponent as Pension } from ".././assets/svg/Totum_Icon_Pension.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";

const ModalCreateAccount = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);

  useEffect(() => {}, []);

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);

  // Handle insurance type check
  const [insurance, setInsurance] = useState();

  const handleToggle = (t) => () => {
    const isInsurance = insurance === t;

    if (isInsurance) {
      setInsurance();
    } else {
      setInsurance(t);
    }
  };

  //----------------------------- MAIN ACCOUNT------------------------------------
  // account holder details
  const [account, setAccount] = useState({
    attachments: "",
    name: "",
    surname: "",
    gender: "",
    date_of_birth: "",
    marital_status: "",
    title: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    email: "",
    phone: "",
    address: "",
    payment_status: "",
  });

  // main account creating response
  const [main, setMain] = useState({});
  const handleChange = (name) => (event) => {
    event.preventDefault();
    setAccount({ ...account, [name]: event.target.value });
  };

  // Handle select options

  const handleAccountTitle = (selected) => {
    setAccount({ ...account, title: selected.value });
  };

  const handleAccountMarital = (selected) => {
    setAccount({ ...account, marital_status: selected.value });
  };

  const handleAccountGender = (selected) => {
    setAccount({ ...account, gender: selected.value });
  };

  const submitMainAccount = (event) => {
    console.log("submit clicked", account);
    setLoading(true);

    // put delay for testing
    setTimeout(function () {
      setLoading(false);
      nextStep();
    }, 5000);

    return;
    event.preventDefault();
    console.log("account", account);
    createMainAccount(token, account)
      .then((res) => {
        console.log("response===>", res.data);
        setMain(res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
      });
  };

  // ----------------------- FUNERAL ACCOUNT-----------------------------
  const [funeral, setFuneral] = useState({
    attachments: [],
    package_fun: "",
    account_main: "",
    dependents: [],
  });

  const [funPackages, setFunPackages] = useState([]);
  const funeralPackages = () => {
    getPackagesFun(token)
      .then((res) => {
        console.log("fun-packages===>", res.data);
        setFunPackages(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    funeralPackages();
  }, []);

  // Insurance package
  const [packageFun, setPackageFun] = useState({
    package_fun: " ",
  });
  const [packageMee, setPackageMee] = useState({
    package_med: " ",
  });
  const [packageMotor, setPackageMotor] = useState({
    package_motor: " ",
  });

  // account dependents
  const [dep, setDep] = useState({
    name: "",
    surname: "",
    gender: "",
    date_of_birth: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    address: "  ",
    marital_status: "",
    title: "",
    phone: "",
    email: "",
    occupation: "",
    relationship: "",
  });
  const [dependents, setDependents] = useState([]);

  // account vehicles
  const [vehicles, setVehicles] = useState([]);

  const [title, setTitle] = useState("");

  // Documents Array
  const [attachments, setAttachments] = useState([]);

  const handleChangeInput = (id, event) => {
    const newInputFields = dependents.map((i) => {
      if (id === i.id) {
        i[event.target.name] = event.target.value;
      }
      return i;
    });

    setDependents(newInputFields);
  };

  const handleChangeDep = (name) => (event) => {
    event.preventDefault();
    setDep({ ...dep, [name]: event.target.value });
  };

  const handleAddDependent = () => {
    // event.preventDefault();
    const dept = dep;

    const _dependents = dependents;
    _dependents.push(dep);
    setDependents(_dependents);

    console.log("dependents", dependents);
    setDep({
      name: "",
      surname: "",
      gender: "",
      date_of_birth: "2022-03-01",
      national_id: "",
      passport: "",
      drivers_licence: "",
      address: "  ",
      marital_status: "",
      title: "",
      phone: "",
      email: "",
      occupation: "",
      relationship: "",
    });
  };

  const handleRemoveDependent = (index) => {
    const values = [...dependents];
    values.splice(index, 1);
    setDependents(values);
  };

  const SelectType = () => (
    <>
      <p
        style={{
          width: "100%",
          color: "blue",
          textAlign: "center",
          fontSize: "1.4rem",
          marginBottom: "2rem",
        }}
      >
        account has been created, add policies
      </p>
      <div className="modal__select">
        {category.includes(0) && (
          <div
            className={
              insurance === 0 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(0)}
          >
            <Funeral />
            <p className="">Funeral</p>
          </div>
        )}

        {category.includes(1) && (
          <div
            className={
              insurance === 1 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(1)}
          >
            <Health />
            <p>Medical</p>
          </div>
        )}

        {category.includes(2) && (
          <div
            className={
              insurance === 2 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(2)}
          >
            <Motor />
            <p>Motor</p>
          </div>
        )}
        {category.includes(3) && (
          <div
            className={
              insurance === 3 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(3)}
          >
            <Life />
            <p>Life</p>
          </div>
        )}
        {category.includes(4) && (
          <div
            className={
              insurance === 4 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(4)}
          >
            <Home />
            <p>Home</p>
          </div>
        )}
        {category.includes(5) && (
          <div
            className={
              insurance === 5 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(5)}
          >
            <Appliance />
            <p className="">Appliance</p>
          </div>
        )}
        {category.includes(6) && (
          <div
            className={
              insurance === 6 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(6)}
          >
            <Travel />
            <p className="">Travel</p>
          </div>
        )}
        {category.includes(7) && (
          <div
            className={
              insurance === 7 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(7)}
          >
            <Pet />
            <p className="">Pet</p>
          </div>
        )}

        {category.includes(8) && (
          <div
            className={
              insurance === 8 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(8)}
          >
            <Cyber />
            <p className="">Cyber</p>
          </div>
        )}
        {category.includes(9) && (
          <div
            className={
              insurance === 9 ? "modal__option-checked" : "modal__option"
            }
            onClick={handleToggle(9)}
          >
            <Pension />
            <p className="">Pension</p>
          </div>
        )}
      </div>
      <div className="modal__footer">
        <div className="modal__buttons">
          <div className="modal__next">
            <button className="btn btn--blue-border" onClick={nextStep}>
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__heading">
          <div className="modal__information">
            <h2>New account</h2>
          </div>

          <div onClick={onClose} className="modal__close">
            <IconClose />
          </div>
        </div>
        <div className="modal__container">
          <form className="">
            {step === 0 && (
              <>
                <div className="modal__client">
                  <div className="modal__client-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={account.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="modal__surname">
                    <label className="form__label">Surname</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("surname")}
                      value={account.surname}
                    />
                  </div>
                  <div className="modal__gender">
                    <label className="form__label">Gender</label>
                    <Select
                      defaultValue={account.gender}
                      onChange={handleAccountGender}
                      options={[
                        { value: "male", label: "Male" },
                        { value: "female", label: "Female" },
                      ]}
                    />
                  </div>
                  <div className="modal__client-title">
                    <label className="form__label">
                      Title <span>{title}</span>
                    </label>
                    <Select
                      defaultValue={account.title}
                      onChange={handleAccountTitle}
                      options={[
                        { value: "mr", label: "Mr" },
                        { value: "mrs", label: "Mrs" },
                        { value: "miss", label: "Miss" },
                        { value: "dr", label: "Dr" },
                        { value: "prof", label: "Prof" },
                      ]}
                    />
                  </div>
                  <div className="modal__dob">
                    <label className="form__label">Date of birth</label>
                    <input
                      className="form__input"
                      type="date"
                      onChange={handleChange("date_of_birth")}
                      value={account.date_of_birth}
                    />
                  </div>
                  <div className="modal__id">
                    <label className="form__label">ID number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("national_id")}
                      value={account.national_id}
                    />
                  </div>
                  <div className="modal__passport">
                    <label className="form__label">Passport number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("passport")}
                      value={account.passport}
                    />
                  </div>
                  <div className="modal__drivers">
                    <label className="form__label">
                      Drivers license number
                    </label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("drivers_licence")}
                      value={account.drivers_licence}
                    />
                  </div>
                  <div className="modal__marital">
                    <label className="form__label">Marital status</label>
                    <Select
                      defaultValue={account.marital_status}
                      onChange={handleAccountMarital}
                      options={[
                        { value: "single", label: "Single" },
                        { value: "married", label: "Married" },
                        { value: "divorced", label: "Divorced" },
                      ]}
                    />
                  </div>
                  <div className="modal__phone">
                    <label className="form__label">Phone</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("phone")}
                      value={account.phone}
                    />
                  </div>
                  <div className="modal__email">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("email")}
                      value={account.email}
                    />
                  </div>
                  <div className="modal__address">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("address")}
                      value={account.address}
                    />
                  </div>
                </div>

                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={submitMainAccount}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
              </>
            )}
            {step === 1 && <SelectType />}

            {step === 2 && (
              <div className="">
                <p
                  style={{
                    width: "100%",
                    color: "blue",
                    textAlign: "center",
                    fontSize: "1.4rem",
                    marginBottom: "2rem",
                  }}
                >
                  select package
                </p>
                <div class=" modal__table">
                  <div class=" modal__table-header">
                    <div class="modal__table-cell--check"></div>
                    <div class="modal__table-cell">Name</div>
                    <div class="modal__table-cell">Casket</div>
                    <div class="modal__table-cell">Waiting Period</div>
                    <div class="modal__table-cell">Groceries</div>
                    <div class="modal__table-cell">Sum Assured</div>
                    <div class="modal__table-cell">Cash In Lieu</div>
                    <div class="modal__table-cell">Currency</div>
                  </div>

                  {funPackages.map((funPkg, i) => (
                    <div class="modal__table-row">
                      <div class=" modal__table-cell--check">
                        <input type="checkbox" />
                      </div>
                      <div class=" modal__table-cell">
                        {" "}
                        <p className="accounts__table-name">{funPkg.name}</p>
                      </div>
                      <div class=" modal__table-cell">casket name</div>
                      <div class=" modal__table-cell">
                        {funPkg.waiting_period} months
                      </div>
                      <div class=" modal__table-cell">{funPkg.groceries}</div>
                      <div class=" modal__table-cell">{funPkg.sum_assured}</div>
                      <div class=" modal__table-cell">
                        {funPkg.cash_in_lieu}
                      </div>
                      <div class=" modal__table-cell">{funPkg.currency}</div>
                    </div>
                  ))}
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 3 && (
              <>
                <p
                  style={{
                    width: "100%",
                    color: "blue",
                    textAlign: "center",
                    fontSize: "1.4rem",
                    marginBottom: "2rem",
                  }}
                >
                  add dependents to policy
                </p>
                <div className="modal__dependents">
                  <div className="modal__dependents-add">
                    <div className="modal__dependents-form">
                      <div className="modal__dpt-name">
                        <label className="form__label">Name</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.name}
                          onChange={handleChangeDep("name")}
                        />
                      </div>
                      <div className="modal__dpt-surname">
                        <label className="form__label">Surname</label>
                        <input
                          className="form__input"
                          type="text"
                          onChange={handleChangeDep("surname")}
                          value={dep.surname}
                        />
                      </div>
                      <div className="modal__dpt-gender">
                        <label className="form__label">Gender</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.gender}
                          onChange={handleChangeDep("gender")}
                        />
                      </div>
                      <div className="modal__dpt-title">
                        <label className="form__label">Title</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.title}
                          onChange={handleChangeDep("title")}
                        />
                      </div>
                      <div className="modal__dpt-dob">
                        <label className="form__label">Date of birth</label>
                        <input
                          className="form__input"
                          type="date"
                          value={dep.date_of_birth}
                          onChange={handleChangeDep("date_of_birth")}
                        />
                      </div>
                      <div className="modal__dpt-id">
                        <label className="form__label">ID number</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.national_id}
                          onChange={handleChangeDep("national_id")}
                        />
                      </div>
                      <div className="modal__dpt-passport">
                        <label className="form__label">Passport number</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.passport}
                          onChange={handleChangeDep("passport")}
                        />
                      </div>
                      <div className="modal__dpt-drivers">
                        <label className="form__label">
                          Drivers license number
                        </label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.drivers_licence}
                          onChange={handleChangeDep("drivers_licence")}
                        />
                      </div>
                      <div className="modal__dpt-marital">
                        <label className="form__label">Maritul status</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.marital_status}
                          onChange={handleChangeDep("marital_status")}
                        />
                      </div>
                      <div className="modal__dpt-phone">
                        <label className="form__label">Phone</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.phone}
                          onChange={handleChangeDep("phone")}
                        />
                      </div>
                      <div className="modal__dpt-email">
                        <label className="form__label">Email</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.email}
                          onChange={handleChangeDep("email")}
                        />
                      </div>
                      <div className="modal__dpt-address">
                        <label className="form__label">Address</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.address}
                          onChange={handleChangeDep("address")}
                        />
                      </div>
                      <div className="modal__dpt-relationship">
                        <label className="form__label">Relationship</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.relationship}
                          onChange={handleChangeDep("relationship")}
                        />
                      </div>
                    </div>
                    <div className="modal__dependents-btn">
                      <button
                        className="btn btn--green"
                        type="button"
                        onClick={() => handleAddDependent()}
                      >
                        Add dependent
                      </button>
                    </div>
                  </div>
                  <div className="modal__dependents-list">
                    {dependents.map((dependent, i) => (
                      <div className="modal__dpt-card" key={i}>
                        <div>
                          <p>Name</p>
                          <span>{dependent.name}</span>
                        </div>
                        <div>
                          <p>Surname</p>
                          <span>{dependent.surname}</span>
                        </div>
                        <div>
                          <p>Gender</p>
                          <span>{dependent.gender}</span>
                        </div>
                        <div>
                          <p>Date of Birth</p>
                          <span>{dependent.date_of_birth}</span>
                        </div>
                        <div>
                          <p>Relationship</p>
                          <span>{dependent.relationship}</span>
                        </div>
                        <div className="modal__dept-options">
                          <p className="modal__dept-more">More</p>
                          <p
                            className="modal__dept-remove"
                            onClick={() => handleRemoveDependent(i)}
                          >
                            Remove
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                  <div className="modal__footer">
                    <div className="modal__buttons">
                      <div className="modal__back">
                        <button
                          className="btn btn--blue-border"
                          onClick={prevStep}
                        >
                          Back
                        </button>
                      </div>
                      <div className="modal__next">
                        <button
                          className="btn btn--blue-border"
                          onClick={nextStep}
                        >
                          Next
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}

            {step === 4 && (
              <div className="modal__documents">
                <p
                  style={{
                    width: "100%",
                    color: "blue",
                    textAlign: "center",
                    fontSize: "1.4rem",
                    marginBottom: "2rem",
                  }}
                >
                  upload files for account
                </p>
                <label className="form__label">Attachments</label>
                <div className="">
                  <Upload />
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 5 && (
              <>
                <div className="modal__review">
                  <p
                    style={{
                      width: "100%",
                      color: "blue",
                      textAlign: "center",
                      fontSize: "1.4rem",
                      marginBottom: "2rem",
                    }}
                  >
                    Review details
                  </p>
                  <div className="modal__review-grid">
                    <div className="modal__item">
                      <p className="modal__title">Name</p>
                      <span>John</span>
                    </div>
                    <div className="modal__item">
                      <p className="modal__title">Name</p>
                      <span>John</span>
                    </div>
                  </div>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}
            {step === 6 && (
              <div className="">
                <CheckSuccess />
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

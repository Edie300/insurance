import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { getCompanyCaskets } from "../api/caskets";
import { editPackageFun } from "../api/package-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

const ModalCreatePackage = ({ show, onClose, pkg }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [caskets, setCaskets] = useState([]);
  const [packageId, setPackageId] = useState(pkg.id);
  const [funeralPackage, setFuneralPackage] = useState({
    attachments: pkg.attachments,
    name: pkg.name,
    description: pkg.description,
    policy_lapse_period: pkg.policy_lapse_period,
    policy_archive_period: pkg.policy_archive_period,
    payment_grace_period: pkg.payment_grace_period,
    waiting_period: pkg.waiting_period,
    principal_member_min_age: pkg.principal_member_min_age,
    principal_member_max_age: pkg.principal_member_max_age,
    dependent_max_age: pkg.dependent_max_age,
    dependents_max_count: pkg.dependents_max_count,
    policy_maturity: pkg.policy_maturity,
    child_max_age: pkg.child_max_age,
    child_max_age_school: pkg.child_max_age_school,
    sum_assured: pkg.sum_assured,
    hearse: pkg.hearse,
    bus: pkg.bus,
    groceries: pkg.groceries,
    cash_in_lieu: pkg.cash_in_lieu,
    accidental_death: pkg.accidental_death,
    mortuary_services: pkg.mortuary_services,
    events_management: pkg.events_management,
    burial_services: pkg.burial_services,
    premium_family: pkg.premium_family,
    premium_dependent: pkg.premium_dependent,
    currency: pkg.currency,
    payment_type: pkg.payment_type,
    casket: pkg.casket.id,
  });

  const init = () => {
    console.log("package to be edited===>", pkg)
    getCompanyCaskets(company.id, token)
      .then((res) => {
        console.log("caskets==>", res.data);
        setCaskets(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    init();
  }, []);

  // form steps
  const [step, setStep] = useState(1);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);

  // Handle insurance type check
  const [insurance, setInsurance] = useState();


  // ---------------------[FUNERAL PACKAGE]------------------------------

  const handleChangeFun = (name) => (event) => {
    event.preventDefault();
    setFuneralPackage({ ...funeralPackage, [name]: event.target.value });
  };

  // handle casket select

  const handleCasketSelect = (selected) => {
    setFuneralPackage({ ...funeralPackage, casket: selected.value });
  };
  // handle checks
  const handleCheckFunHearse = () => {
    let currentValue = funeralPackage.hearse;
    setFuneralPackage({ ...funeralPackage, hearse: !currentValue });
  };

  const handleCheckFunBus = () => {
    let currentValue = funeralPackage.bus;
    setFuneralPackage({ ...funeralPackage, bus: !currentValue });
  };

  const handleCheckFunGroceries = () => {
    let currentValue = funeralPackage.groceries;
    setFuneralPackage({ ...funeralPackage, groceries: !currentValue });
  };

  const handleCheckFunAccidental = () => {
    let currentValue = funeralPackage.accidental_death;
    setFuneralPackage({ ...funeralPackage, accidental_death: !currentValue });
  };

  const handleCheckFunMortuary = () => {
    let currentValue = funeralPackage.mortuary_services;
    setFuneralPackage({ ...funeralPackage, mortuary_services: !currentValue });
  };

  const handleCheckFunEvents = () => {
    let currentValue = funeralPackage.events_management;
    setFuneralPackage({ ...funeralPackage, events_management: !currentValue });
  };

  const handleCheckFunBurial = () => {
    let currentValue = funeralPackage.burial_services;
    setFuneralPackage({ ...funeralPackage, burial_services: !currentValue });
  };

  // ---------------------[HANDLESUBMIT]----------------------------------------

  const handleSubmitPackage = () => {
    console.log("edited package", funeralPackage);
    setLoading(true)
    editPackageFun(token, funeralPackage, packageId)
      .then((res) => {
        setLoading(false)
        console.log("new package==>", res.data);
        nextStep();
      })
      .catch((error) => {
        setLoading(false)
        console.log("::Error::", error);
      });
  };

 
  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="packages__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Edit Package</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">

            {step === 1 && (
              <div className="">
                <div className="packages__details">
                  <div className="packages__name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={funeralPackage.name}
                      onChange={handleChangeFun("name")}
                    />
                  </div>

                  <div className="packages__casket">
                    <label className="form__label">Casket</label>
                    <Select
                      defaultValue={funeralPackage.casket.id}
                      onChange={handleCasketSelect}
                      options={caskets.map((casket) => ({
                        value: casket.id,
                        label: casket.name,
                      }))}
                    />
                  </div>
                  <div className="packages__waiting">
                    <label className="form__label">waiting period</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.waiting_period}
                      onChange={handleChangeFun("waiting_period")}
                    />
                  </div>
                  <div className="packages__payment">
                    <label className="form__label">payment grace period</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.payment_grace_period}
                      onChange={handleChangeFun("payment_grace_period")}
                    />
                  </div>
                  <div className="packages__lapse">
                    <label className="form__label">Lapse period</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.policy_lapse_period}
                      onChange={handleChangeFun("policy_lapse_period")}
                    />
                  </div>
                  <div className="packages__archive">
                    <label className="form__label">Archive period</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.policy_archive_period}
                      onChange={handleChangeFun("policy_archive_period")}
                    />
                  </div>
                  <div className="packages__principal-min">
                    <label className="form__label">
                      Principal member maximum age
                    </label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.principal_member_max_age}
                      onChange={handleChangeFun("principal_member_max_age")}
                    />
                  </div>
                  <div className="packages__principal-max">
                    <label className="form__label">
                      Principal member minmum age
                    </label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.principal_member_min_age}
                      onChange={handleChangeFun("principal_member_min_age")}
                    />
                  </div>
                  <div className="packages__dependent-max">
                    <label className="form__label">dependent maximum age</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.dependent_max_age}
                      onChange={handleChangeFun("dependent_max_age")}
                    />
                  </div>
                  <div className="packages__dependent-limit">
                    <label className="form__label">dependents limit</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.dependents_max_count}
                      onChange={handleChangeFun("dependents_max_count")}
                    />
                  </div>
                  <div className="packages__maturity">
                    <label className="form__label">Policy maturity</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.policy_maturity}
                      onChange={handleChangeFun("policy_maturity")}
                    />
                  </div>
                  <div className="packages__child-max">
                    <label className="form__label">Child maximum age</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.child_max_age}
                      onChange={handleChangeFun("child_max_age")}
                    />
                  </div>
                  <div className="packages__child-max-school">
                    <label className="form__label">
                      Child at school maximum age
                    </label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.child_max_age_school}
                      onChange={handleChangeFun("child_max_age_school")}
                    />
                  </div>
                  <div className="packages__sum-assured">
                    <label className="form__label">Sum assured</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.sum_assured}
                      onChange={handleChangeFun("sum_assured")}
                    />
                  </div>
                </div>
                <div className="packages__description">
                  <label className="form__label">description</label>
                  <textarea
                    className="form__input"
                    style={{ height: "4rem", width: "100%" }}
                    type="text"
                    value={funeralPackage.description}
                    onChange={handleChangeFun("description")}
                  />
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 2 && (
              <div className="packages__documents">
                <div className="packages__benefits">
                  <div className="packages__check">
                    <input
                      type="checkbox"
                      value={funeralPackage.hearse}
                      checked={funeralPackage.hearse}
                      onChange={handleCheckFunHearse}
                    />

                    <div className="packages__info">
                      <p>Hearse</p>
                      <span>somthing abt hearse</span>
                    </div>
                  </div>
                  <div className="packages__check">
                    <input
                      type="checkbox"
                      className=""
                      type="checkbox"
                      value={funeralPackage.bus}
                      checked={funeralPackage.bus}
                      onChange={handleCheckFunBus}
                    />

                    <div className="packages__info">
                      <p>Bus</p>
                      <span>somthing abt bus</span>
                    </div>
                  </div>
                  <div className="packages__check">
                    <input
                      className=""
                      type="checkbox"
                      value={funeralPackage.groceries}
                      checked={funeralPackage.groceries}
                      onChange={handleCheckFunGroceries}
                    />

                    <div className="packages__info">
                      <p>groceries</p>
                      <span>somthing abt groceries</span>
                    </div>
                  </div>
                  <div className="packages__check">
                    <input
                      className=""
                      type="checkbox"
                      value={funeralPackage.accidental_death}
                      checked={funeralPackage.accidental_death}
                      onChange={handleCheckFunAccidental}
                    />

                    <div className="packages__info">
                      <p>accidental death</p>
                      <span>somthing abt accidental </span>
                    </div>
                  </div>
                  <div className="packages__check">
                    <input
                      className=""
                      type="checkbox"
                      value={funeralPackage.events_management}
                      checked={funeralPackage.events_management}
                      onChange={handleCheckFunEvents}
                    />

                    <div className="packages__info">
                      <p>events management</p>
                      <span>somthing abt events management</span>
                    </div>
                  </div>
                  <div className="packages__check">
                    <input
                      className=""
                      type="checkbox"
                      value={funeralPackage.mortuary_services}
                      checked={funeralPackage.mortuary_services}
                      onChange={handleCheckFunMortuary}
                    />

                    <div className="packages__info">
                      <p>mortuary services</p>
                      <span>somthing abt mortuary services</span>
                    </div>
                  </div>

                  <div className="packages__check">
                    <input
                      className=""
                      type="checkbox"
                      value={funeralPackage.burial_services}
                      checked={funeralPackage.burial_services}
                      onChange={handleCheckFunBurial}
                    />

                    <div className="packages__info">
                      <p>burial services</p>
                      <span>somthing abt burial services</span>
                    </div>
                  </div>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 3 && (
              <div className="packages__documents">
                <div className="packages__premium">
                  <div className="packages__payment-type">
                    <label className="form__label">payment type</label>
                    <Select
                      defaultValue={funeralPackage.currency}
                      // onChange={handleCompanyType}
                      options={[
                        { value: 1, label: "monthly" },
                        { value: 2, label: "quarterly" },
                        { value: 3, label: "yearly" },
                      ]}
                    />
                  </div>
                  <div className="packages__currency">
                    <label className="form__label">Currency</label>
                    <Select
                      defaultValue={funeralPackage.currency}
                      // onChange={handleCompanyType}
                      options={[
                        { value: "ZW", label: "ZW" },
                        { value: "US", label: "US" },
                      ]}
                    />
                  </div>
                  <div className="packages__family">
                    <label className="form__label">Premium family</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.premium_family}
                      onChange={handleChangeFun("premium_family")}
                    />
                  </div>
                  <div className="packages__dependent">
                    <label className="form__label">Premium dependent</label>
                    <input
                      className="form__input"
                      type="number"
                      value={funeralPackage.premium_dependent}
                      onChange={handleChangeFun("premium_dependent")}
                    />
                  </div>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 4 && (
              <>
                <div className="packages__review">
                  <p
                    style={{
                      width: "100%",
                      color: "blue",
                      textAlign: "center",
                      fontSize: "1.4rem",
                      marginBottom: "2rem",
                    }}
                  >
                    Review details
                  </p>
                </div>
                <div className="packages__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmitPackage}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Update package"
                    )}
                  </button>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 5 && (
              <div className="">
                <CheckSuccess />
                <p>{JSON.stringify(funeralPackage, null, 2)}</p>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreatePackage;

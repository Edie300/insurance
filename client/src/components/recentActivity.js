import React from "react";

const RecentActivity = ({}) => {
  return (
    <div class="">
      <h4 class="centered mt">RECENT ACTIVITY</h4>

      <div class="desc">
        <div class="thumb">
          <span class="badge bg-theme">
            <i class="fa fa-clock-o"></i>
          </span>
        </div>
        <div class="details">
          <p>
            <muted>Just Now</muted>
            <br />
            <a href="#">Paul Rudd</a> has been registered.
            <br />
          </p>
        </div>
      </div>

      <div class="desc">
        <div class="thumb">
          <span class="badge bg-theme">
            <i class="fa fa-clock-o"></i>
          </span>
        </div>
        <div class="details">
          <p>
            <muted>2 Minutes Ago</muted>
            <br />
            <a href="#">James Brown</a> made payment.
            <br />
          </p>
        </div>
      </div>

      <div class="desc">
        <div class="thumb">
          <span class="badge bg-theme">
            <i class="fa fa-clock-o"></i>
          </span>
        </div>
        <div class="details">
          <p>
            <muted>3 Hours Ago</muted>
            <br />
            <a href="#">Diana Kennedy</a> added a depedent.
            <br />
          </p>
        </div>
      </div>

      <div class="desc">
        <div class="thumb">
          <span class="badge bg-theme">
            <i class="fa fa-clock-o"></i>
          </span>
        </div>
        <div class="details">
          <p>
            <muted>7 Hours Ago</muted>
            <br />
            <a href="#">Brando Page</a> initiated a claim
            <br />
          </p>
        </div>
      </div>
    </div>
  );
};
export default RecentActivity;

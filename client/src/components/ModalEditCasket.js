import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { editCasket } from "../api/caskets";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

const ModalCreateAccount = ({ show, onClose, casket }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);
  const [casketId, setCasketId] = useState(casket.id);
  const [values, setValues] = useState({
    images: casket.images,
    name: casket.name,
    quantity: casket.quantity,
    description: casket.description,
  });

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setValues({ ...values, [name]: event.target.value });
  };

  const handleAddFiles = (image) => {
    const currentFiles = values.images;
    currentFiles.push(image);
    setValues({ ...values, images: currentFiles });
  };

  const handleSubmit = () => {
    setLoading(true);
    editCasket(token, values, casketId)
      .then((res) => {
        console.log("response===>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Edit Casket</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">
            {step === 0 && (
              <div className="">
                <div className="casket__information">
                  <div className="casket__casket-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={values.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="casket__quantity">
                    <label className="form__label">Quantity</label>
                    <input
                      className="form__input"
                      type="number"
                      value={values.quantity}
                      onChange={handleChange("quantity")}
                    />
                  </div>
                </div>
                <div className="casket__description">
                  <label className="form__label">Description</label>
                  <textarea
                    className="form__input"
                    rows="10"
                    type="text"
                    style={{ height: "20rem" }}
                    value={values.description}
                    onChange={handleChange("description")}
                  />
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 1 && (
              <div className="packages__documents">
                <label className="form__label">Attachments</label>
                <div className="">
                  <Upload handleAddFiles={handleAddFiles} />
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 2 && (
              <>
                <div className="packages__review">
                  <p
                    style={{
                      width: "100%",
                      color: "blue",
                      textAlign: "center",
                      fontSize: "1.4rem",
                      marginBottom: "2rem",
                    }}
                  >
                    Review details
                  </p>
                </div>
                <div className="packages__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmit}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 3 && (
              <div className="">
                <CheckSuccess />
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createtFunDependent } from "../api/dependent-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

const ModalCreateDependent = ({
  show,
  onClose,
  accountFun = "",
  packageFun = "",
}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);
  const [dependent, setDependent] = useState({
    name: "",
    surname: "",
    gender: "",
    date_of_birth: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    address: "  ",
    marital_status: "",
    title: "",
    phone: "",
    email: "",
    occupation: "",
    relationship: "",
    account_fun: accountFun,
    package_fun: packageFun,
  });

  const handleChangeDep = (name) => (event) => {
    event.preventDefault();
    setDependent({ ...dependent, [name]: event.target.value });
  };

  // Handle dependent select options

  const handleDepTitle = (selected) => {
    setDependent({ ...dependent, title: selected.value });
  };

  const handleDepGender = (selected) => {
    setDependent({ ...dependent, gender: selected.value });
  };

  const handleDepMaritul = (selected) => {
    setDependent({ ...dependent, marital_status: selected.value });
  };

  const handleDepRelationship = (selected) => {
    setDependent({ ...dependent, relationship: selected.value });
  };

  const handleDepOccupation = (selected) => {
    setDependent({ ...dependent, occupation: selected.value });
  };

  const handleSubmitDependent = () => {
    setLoading(true);
    createtFunDependent(token, dependent)
      .then((res) => {
        console.log("response===>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Register dependent</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">
            {step === 0 && (
              <div className="">
                <div className="packages__details">
                  <div className="modal__dpt-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.name}
                      onChange={handleChangeDep("name")}
                    />
                  </div>
                  <div className="modal__dpt-surname">
                    <label className="form__label">Surname</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChangeDep("surname")}
                      value={dependent.surname}
                    />
                  </div>
                  <div className="modal__dpt-gender">
                    <label className="form__label">Gender</label>
                    <Select
                      defaultValue={dependent.gender}
                      onChange={handleDepGender}
                      options={[
                        { value: "male", label: "Male" },
                        { value: "female", label: "Female" },
                      ]}
                    />
                  </div>
                  <div className="modal__dpt-title">
                    <label className="form__label">Title</label>
                    <Select
                      defaultValue={dependent.gender}
                      onChange={handleDepTitle}
                      options={[
                        { value: "mr", label: "Mr" },
                        { value: "mrs", label: "Mrs" },
                        { value: "miss", label: "Miss" },
                        { value: "dr", label: "Dr" },
                        { value: "prof", label: "Prof" },
                      ]}
                    />
                  </div>
                  <div className="modal__dpt-dob">
                    <label className="form__label">Date of birth</label>
                    <input
                      className="form__input"
                      type="date"
                      value={dependent.date_of_birth}
                      onChange={handleChangeDep("date_of_birth")}
                    />
                  </div>
                  <div className="modal__dpt-id">
                    <label className="form__label">ID number</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.national_id}
                      onChange={handleChangeDep("national_id")}
                    />
                  </div>
                  <div className="modal__dpt-passport">
                    <label className="form__label">Passport number</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.passport}
                      onChange={handleChangeDep("passport")}
                    />
                  </div>
                  <div className="modal__dpt-drivers">
                    <label className="form__label">
                      Drivers license number
                    </label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.drivers_licence}
                      onChange={handleChangeDep("drivers_licence")}
                    />
                  </div>
                  <div className="modal__dpt-marital">
                    <label className="form__label">Maritul status</label>
                    <Select
                      defaultValue={dependent.marital_status}
                      onChange={handleDepMaritul}
                      options={[
                        { value: "single", label: "Single" },
                        { value: "married", label: "Married" },
                        { value: "divorced", label: "Divorced" },
                      ]}
                    />
                  </div>
                  <div className="modal__dpt-phone">
                    <label className="form__label">Phone</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.phone}
                      onChange={handleChangeDep("phone")}
                    />
                  </div>
                  <div className="modal__dpt-email">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.email}
                      onChange={handleChangeDep("email")}
                    />
                  </div>
                  <div className="modal__dpt-address">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      value={dependent.address}
                      onChange={handleChangeDep("address")}
                    />
                  </div>
                  <div className="modal__dpt-relationship">
                    <label className="form__label">Relationship</label>
                    <Select
                      defaultValue={dependent.relationship}
                      onChange={handleDepRelationship}
                      options={[
                        { value: "child", label: "child" },
                        { value: "spouse", label: "spouse" },
                        { value: "parent", label: "parent" },
                        { value: "uncle", label: "uncle" },
                        { value: "nephew", label: "nephew" },
                      ]}
                    />
                  </div>
                  <div className="modal__occupation">
                    <label className="form__label">Occupation</label>
                    <Select
                      defaultValue={dependent.occupation}
                      onChange={handleDepOccupation}
                      options={[
                        { value: "student", label: "student" },
                        { value: "employed", label: "employed" },
                        { value: "unemployed", label: "unemployed" },
                      ]}
                    />
                  </div>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 1 && (
              <>
                <div className="modal__review">
                  <div className="modal__">
                    {/* <p>{JSON.stringify(dependent, null, 2)}</p> */}

                    <div class=" account__policy">
                      <div className="account__item">
                        <p className="">Name</p>
                        <span>{dependent.name}</span>
                      </div>
                      <div className="account__item">
                        <p className="">Surname</p>
                        <span>{dependent.surname}</span>
                      </div>
                      <div className="account__item">
                        <p className="">gender</p>
                        <span>{dependent.gender}</span>
                      </div>
                      <div className="account__item">
                        <p className="">date of birth</p>
                        <span>{dependent.date_of_birth}</span>
                      </div>
                      <div className="account__item">
                        <p className="">marital status</p>
                        <span>{dependent.marital_status}</span>
                      </div>
                      <div className="account__item">
                        <p className="">passport</p>
                        <span>{dependent.passport}</span>
                      </div>
                      <div className="account__item">
                        <p className="">national id</p>
                        <span>{dependent.national_id}</span>
                      </div>
                      <div className="account__item">
                        <p className="">drivers licence</p>
                        <span>{dependent.drivers_licence}</span>
                      </div>
                      <div className="account__item">
                        <p className="">email</p>
                        <span>{dependent.email}</span>
                      </div>
                      <div className="account__item">
                        <p className="">phone</p>
                        <span>{dependent.phone}</span>
                      </div>
                      <div className="account__item">
                        <p className="">address</p>
                        <span>{dependent.address}</span>
                      </div>
                      <div className="account__item">
                        <p className="">relationship</p>
                        <span>{dependent.relationship}</span>
                      </div>
                      <div className="account__item">
                        <p className="">occupation</p>
                        <span>{dependent.occupation}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmitDependent}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                    <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 2 && (
              <div className="">
                <div className="success">
                  <div className="success__container">
                    <CheckSuccess />
                    <div className="success__heading">
                      <p>Success</p>
                    </div>
                    <div className="success__paragraph">
                      Dependent details have been submitted
                    </div>
                    <div className="success__options">
                      <div className="success__option">
                        <button className="btn btn--blue-grey">Action 1</button>
                      </div>
                      <div className="success__option">
                        <button className="btn btn--blue-border">
                          Action 2
                        </button>
                      </div>
                      <div className="success__option">
                        <button
                          className="btn btn--blue"
                          type="button"
                          onClick={() => setStep(0)}
                        >
                          Create Dep
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateDependent;

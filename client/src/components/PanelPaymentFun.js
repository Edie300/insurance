import React, { useEffect, useState } from "react";
import moment from "moment";

import IconClose from "./iconClose";
import ModalLoader from "./ModalLoader";
import Payment from "../company/paymentFuneral/CreatePayAccountForm"

import { createFuneralDependent } from "../api/dependentFuneral";
import { isAuthenticated } from "../auth";

const PanelAddFunDependents = ({ account, show, onClose }) => {
  const {
    user: { company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

 
  const [loader, setLoader] = useState(false);



  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);




  const handleSubmit = () => {
    setLoader(true)
    
  
  }

  if (!show) {
    return null;
  }

  return (
    <div className="overlay">
      <div className="panel" onClick={(e) => e.stopPropagation()}>
        <div className="panel__close" onClick={onClose}>
          <IconClose />
        </div>
      <Payment/>

        {loader && (
        <ModalLoader
          title='Registering'
          message={`Adding dependent`}
        />
      )}
      </div>

    </div>
  );
};
export default PanelAddFunDependents;

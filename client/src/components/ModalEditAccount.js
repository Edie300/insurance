import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { EditMainAccount } from "../api/account-main";
import { createFunAccount } from "../api/account-funeral";
import { getPackagesFun } from "../api/package-funeral";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";


const ModalCreateAccount = ({ show, onClose, start = 0, }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);

  useEffect(() => {}, []);

  // form steps
  const [step, setStep] = useState(start);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);


  //----------------------------- MAIN ACCOUNT------------------------------------
  const [account, setAccount] = useState({
    attachments: ["docs"],
    name: "thabo",
    surname: "moyo",
    gender: "male",
    date_of_birth: "2022-03-01",
    marital_status: "single",
    title: "mr",
    national_id: "0987653a09",
    passport: "12345",
    drivers_licence: "67890",
    email: "smith@gmail.com",
    phone: "+26307345346777",
    address: "1234 nkulumane bulawayo",
    payment_status: "",
  });

  // main account creating response
  const [main, setMain] = useState({});
  const handleChange = (name) => (event) => {
    event.preventDefault();
    setAccount({ ...account, [name]: event.target.value });
  };

  // Handle select options

  const handleAccountTitle = (selected) => {
    setAccount({ ...account, title: selected.value });
  };

  const handleAccountMarital = (selected) => {
    setAccount({ ...account, marital_status: selected.value });
  };

  const handleAccountGender = (selected) => {
    setAccount({ ...account, gender: selected.value });
  };

  const submitMainAccount = (event) => {
    console.log("submit clicked", account);
    setLoading(true);

    event.preventDefault();
    console.log("account", account);
    EditMainAccount(token, account)
      .then((res) => {
        console.log("response===>", res.data);
        setMain(res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Edit account</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="accounts__modal-container">
          <form className="">
            {step === 0 && (
              <>
                <p className="modal__step">Account information</p>
                <div className="modal__client">
                  <div className="modal__client-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={account.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="modal__surname">
                    <label className="form__label">Surname</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("surname")}
                      value={account.surname}
                    />
                  </div>
                  <div className="modal__gender">
                    <label className="form__label">Gender</label>
                    <Select
                      defaultValue={account.gender}
                      onChange={handleAccountGender}
                      options={[
                        { value: "male", label: "Male" },
                        { value: "female", label: "Female" },
                      ]}
                    />
                  </div>
                  <div className="modal__client-title">
                    <label className="form__label">Title</label>
                    <Select
                      defaultValue={account.title}
                      onChange={handleAccountTitle}
                      options={[
                        { value: "mr", label: "Mr" },
                        { value: "mrs", label: "Mrs" },
                        { value: "miss", label: "Miss" },
                        { value: "dr", label: "Dr" },
                        { value: "prof", label: "Prof" },
                      ]}
                    />
                  </div>
                  <div className="modal__dob">
                    <label className="form__label">Date of birth</label>
                    <input
                      className="form__input"
                      type="date"
                      onChange={handleChange("date_of_birth")}
                      value={account.date_of_birth}
                    />
                  </div>
                  <div className="modal__id">
                    <label className="form__label">ID number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("national_id")}
                      value={account.national_id}
                    />
                  </div>
                  <div className="modal__passport">
                    <label className="form__label">Passport number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("passport")}
                      value={account.passport}
                    />
                  </div>
                  <div className="modal__drivers">
                    <label className="form__label">
                      Drivers license number
                    </label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("drivers_licence")}
                      value={account.drivers_licence}
                    />
                  </div>
                  <div className="modal__marital">
                    <label className="form__label">Marital status</label>
                    <Select
                      defaultValue={account.marital_status}
                      onChange={handleAccountMarital}
                      options={[
                        { value: "single", label: "Single" },
                        { value: "married", label: "Married" },
                        { value: "divorced", label: "Divorced" },
                      ]}
                    />
                  </div>
                  <div className="modal__phone">
                    <label className="form__label">Phone</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("phone")}
                      value={account.phone}
                    />
                  </div>
                  <div className="modal__email">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("email")}
                      value={account.email}
                    />
                  </div>
                  <div className="modal__address">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("address")}
                      value={account.address}
                    />
                  </div>
                </div>

                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={submitMainAccount}
                    disable={loading}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Create account"
                    )}
                  </button>
                </div>
              </>
            )}

            {step === 1 && (
              <div className="">
                <CheckSuccess />
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { getCompanyCaskets } from "../api/caskets";
import { createPackageFun } from "../api/package-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

import FormPackageMed from "./FormPackageMed";
import FormPackageFun from "./FormPackageFun";
import FormPackageMotor from "./FormPackageMotor";

import { ReactComponent as Funeral } from ".././assets/svg/Totum_Icon_Funeral.svg";
import { ReactComponent as Health } from ".././assets/svg/Totum_Icon_Health.svg";
import { ReactComponent as Motor } from ".././assets/svg/Totum_Icon_Motor.svg";
import { ReactComponent as Life } from ".././assets/svg/Totum_Icon_Life.svg";
import { ReactComponent as Home } from ".././assets/svg/Totum_Icon_Home.svg";
import { ReactComponent as Travel } from ".././assets/svg/Totum_Icon_Travel.svg";
import { ReactComponent as Appliance } from ".././assets/svg/Totum_Icon_Appliance.svg";
import { ReactComponent as Pet } from ".././assets/svg/Totum_Icon_Pet.svg";
import { ReactComponent as Cyber } from ".././assets/svg/Totum_Icon_Cyber.svg";
import { ReactComponent as Pension } from ".././assets/svg/Totum_Icon_Pension.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const ModalCreatePackage = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);
 

  // form steps
  const [mainStep, setMainStep] = useState(0);
  const nextStep = () => {
    setMainStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setMainStep((cur) => cur - 1);
  };

  const resetMainStep = () => {
    setMainStep(0);
  };
  const [loading, setLoading] = useState(false);

  // Handle insurance type check
  const [insurance, setInsurance] = useState();

  const handleToggle = (t) => () => {
    const isInsurance = insurance === t;

    if (isInsurance) {
      setInsurance();
    } else {
      setInsurance(t);
    }
  };

  

  const SelectType = () => (
    <>
      <div className="packages__select">
        {category.includes(0) && (
          <div
            className={
              insurance === 0 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(0)}
          >
            <div>
              <Funeral />
            </div>
            <p className="">Funeral</p>
          </div>
        )}

        {category.includes(1) && (
          <div
            className={
              insurance === 1 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(1)}
          >
            <div>
              <Health />
            </div>
            <p>Medical</p>
          </div>
        )}

        {category.includes(2) && (
          <div
            className={
              insurance === 2 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(2)}
          >
            <div>
              <Motor />
            </div>
            <p>Motor</p>
          </div>
        )}
        {category.includes(3) && (
          <div
            className={
              insurance === 3 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(3)}
          >
            <div>
              <Life />
            </div>
            <p>Life</p>
          </div>
        )}
        {category.includes(4) && (
          <div
            className={
              insurance === 4 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(4)}
          >
            <div>
              <Home />
            </div>
            <p>Home</p>
          </div>
        )}
        {category.includes(5) && (
          <div
            className={
              insurance === 5 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(5)}
          >
            <div>
              <Appliance />
            </div>
            <p className="">Appliance</p>
          </div>
        )}
        {category.includes(6) && (
          <div
            className={
              insurance === 6 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(6)}
          >
            <div>
              <Travel />
            </div>
            <p className="">Travel</p>
          </div>
        )}
        {category.includes(7) && (
          <div
            className={
              insurance === 7 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(7)}
          >
            <div>
              <Pet />
            </div>
            <p className="">Pet</p>
          </div>
        )}

        {category.includes(8) && (
          <div
            className={
              insurance === 8 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(8)}
          >
            <div>
              <Cyber />
            </div>
            <p className="">Cyber</p>
          </div>
        )}
        {category.includes(9) && (
          <div
            className={
              insurance === 9 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(9)}
          >
            <div>
              <Pension />
            </div>
            <p className="">Pension</p>
          </div>
        )}
      </div>
      <div className="packages__footer">
        <div className="packages__buttons">
          <div className="packages__next">
            <button className="btn btn--blue" onClick={nextStep}>
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="packages__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Create Package</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">
            {mainStep === 0 && <SelectType />}
            {mainStep === 1 && insurance === 0 && (< FormPackageFun start={()=>setMainStep(0)}/>)}
            {mainStep === 1 && insurance === 1 && (<FormPackageMed start={()=>setMainStep(0)}/>)}
            {mainStep === 1 && insurance === 2 && (<FormPackageMotor start={()=>setMainStep(0)}/>)}
  

          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreatePackage;

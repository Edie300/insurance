import React, { useState, useEffect } from "react";
import axios from "axios";
import Dropzone from "./Dropzone";
import Progress from "./Progress";

import { isAuthenticated } from "../auth";
import "./Upload.css";

const Upload = ({handleAddFiles, url="/upload"}) => {
  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [values, setValues] = useState({
    files: [],
    uploading: false,
    uploadProgress: {},
    successfullUploaded: false,
  });

  const { files, uploading, uploadProgress, successfullUploaded } = values;

  const onFilesAdded = (files) => {
    const currentFiles = [...values.files];
    setValues({ ...values, files: currentFiles.concat(files) });
  };

  const renderProgress = (file) => {
    const uploadProgress = values.uploadProgress[file.name];
    if (values.uploading || values.successfullUploaded) {
      return (
        <div className="ProgressWrapper">
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0} />
          {/* <span style={{ color: "red" }}>remove</span> */}
        </div>
      );
    }
  };

  const renderActions = () => {
    if (values.successfullUploaded) {
      return (
        <button
          onClick={() =>
            setValues({ ...values, files: [], successfullUploaded: false })
          }
        >
          Clear
        </button>
      );
    } else {
      return (
        <button
          disabled={values.files.length < 0 || values.uploading}
          onClick={uploadFiles}
        >
          Upload
        </button>
      );
    }
  };

  const uploadFiles = async () => {
    setValues({ ...values, uploadProgress: {}, uploading: true });
    const promises = [];
    values.files.forEach((file) => {
      promises.push(sendRequest(file));
    });
    try {
      await Promise.all(promises);

      setValues({ ...values, successfullUploaded: true, uploading: false });
    } catch (e) {
      // Not Production ready! Do some error handling here instead...
      setValues({ ...values, successfullUploaded: true, uploading: false });
    }
  };

  // const sendRequest = (file) => {
  //   return new Promise((resolve, reject) => {
  //     const req = new XMLHttpRequest();

  //     const formData = new FormData();
  //     formData.append("file", file, file.name);

  //     req.open("POST", "http://localhost:8000/upload");
  //     req.send(formData);
  //   });
  // };



  const sendRequest = async (file) => {
    const formData = new FormData();
    formData.append("image", file, file.name);

    try {
      let { data } = await axios.post(
        url,
        formData,
        {
          headers: { Authorization: `Bearer ${token}` },
        },
        {
          onUploadProgress: (e) => {
            const copy = { ...values.uploadProgress };
            copy[file.name] = {
              state: "pending",
              percentage: (e.loaded / e.total) * 100,
            };
            setValues({ ...values, uploadProgress: copy });
          },
          // setProgress(Math.round((100 * e.loaded) / e.total)),
        }
      );
      console.log("upload response", data);
      handleAddFiles(data)
    } catch (err) {
      console.log(err);
    }

    // return new Promise((resolve, reject) => {
    //   const req = new XMLHttpRequest();

    //   req.upload.addEventListener("progress", (event) => {
    //     if (event.lengthComputable) {
    //       const copy = { ...values.uploadProgress };
    //       copy[file.name] = {
    //         state: "pending",
    //         percentage: (event.loaded / event.total) * 100,
    //       };
    //       setValues({...values, uploadProgress: copy });
    //     }
    //   });

    //   req.upload.addEventListener("load", (event) => {
    //     const copy = { ...values.uploadProgress };
    //     copy[file.name] = { state: "done", percentage: 100 };
    //     setValues({...values, uploadProgress: copy });
    //     resolve(req.response);
    //   });

    //   req.upload.addEventListener("error", (event) => {
    //     const copy = { ...values.uploadProgress };
    //     copy[file.name] = { state: "error", percentage: 0 };
    //     setValues({...values, uploadProgress: copy });
    //     reject(req.response);
    //   });

    //   const formData = new FormData();
    //   formData.append("image", file, file.name);

    //   console.log("form data===>", formData)

    //   // req.open(
    //   //   "POST",
    //   //   "http://146.190.224.66:30869/api/caskets/upload_image"
    //   // );
    //   // req.send(formData);
    // });
  };

  return (
    <div className="Upload">
      {/* <span className="Title">Upload Files</span> */}
      <div className="Content">
        <div>
          <Dropzone
            onFilesAdded={onFilesAdded}
            disabled={values.uploading || values.successfullUploaded}
            uploadFiles={uploadFiles}
          />
        </div>
        <div className="Files">
          {values.files.map((file) => {
            return (
              <div key={file.name} className="Row">
                <span className="Filename">{file.name}</span>
                {renderProgress(file)}
              </div>
            );
          })}
        </div>
      </div>
      <div className="Actions">{renderActions()}</div>
    </div>
  );
};

export default Upload;

import React, { useState, useEffect } from "react";
import IconClose from "./iconClose";


const ModalEmail = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__heading">
          <div className="modal__information">
            <h2>New Email</h2>
            <p>Fill details required to send mail</p>
          </div>

          <div onClick={onClose} className="modal__close">
            <IconClose />
          </div>
        </div>
        <div className="modal__container">
        </div>
      </div>
    </div>
  );
};

export default ModalEmail;

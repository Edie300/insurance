import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createCasket } from "../api/caskets";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Check } from ".././assets/svg/check.svg";

const ModalCreateAccount = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
      setValues({
        images: [],
        name: "",
        quantity: 0,
        description: "",
      })
      setStep(0);
    }
  };

  const closeModal = () => {
    onClose();
    setValues({
      images: [],
      name: "",
      quantity: 0,
      description: "",
    })
    setStep(0);
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);

  const [values, setValues] = useState({
    images: [],
    name: "",
    quantity: 0,
    description: "",
  });

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setValues({ ...values, [name]: event.target.value });
  };

  const handleAddFiles = (image) => {
    const currentFiles = values.images;
    currentFiles.push(image);
    setValues({ ...values, images: currentFiles });
  };

  const handleSubmit = () => {
    setLoading(true);
    createCasket(values, token)
      .then((res) => {
        console.log("response===>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={closeModal}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Create Casket</h2>
          </div>
          <div onClick={closeModal} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="packages__container">
          <form className="">
            {step === 0 && (
              <div className="">
                <div className="casket__information">
                  <div className="casket__casket-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={values.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="casket__quantity">
                    <label className="form__label">Quantity</label>
                    <input
                      className="form__input"
                      type="number"
                      value={values.quantity}
                      onChange={handleChange("quantity")}
                    />
                  </div>
                </div>
                <div className="casket__description">
                  <label className="form__label">Description</label>
                  <textarea
                    className="form__input"
                    rows="10"
                    type="text"
                    style={{ height: "20rem" }}
                    value={values.description}
                    onChange={handleChange("description")}
                  />
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back"></div>
                    <div className="packages__next">
                      <button className="btn btn--blue" onClick={nextStep}>
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 1 && (
              <div className="packages__documents">
                <label className="form__label">Attachments</label>
                <div className="">
                  <Upload
                    handleAddFiles={handleAddFiles}
                    url="http://146.190.224.66:30869/api/caskets/upload_image"
                  />
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button className="btn btn--blue-grey" onClick={prevStep}>
                        Back
                      </button>
                    </div>
                    <div className="packages__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 2 && (
              <>
                <div className="casket__review">
                  <div className="casket__about">
                    <div className="casket__about-name">
                      <p>Name</p>
                      <span>{values.name}</span>
                    </div>
                    <div className="casket__about-quantity">
                      <p>Quantity</p>
                      <span>{values.quantity}</span>
                    </div>
                    <div className="casket__about-description">
                      <p>Description</p>
                      <span>{values.description}</span>
                    </div>
                  </div>
                  <p className="casket__image-heading">Images</p>
                  <div className="casket__images-review">
                    {values.images.length > 0 &&
                      values.images.map((image, i) => (
                        <div className="casket__image-box">
                          <img src={image.Location} alt="" />
                        </div>
                      ))}
                  </div>
                </div>
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmit}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="packages__footer">
                  <div className="packages__buttons">
                    <div className="packages__back">
                      <button className="btn btn--blue-grey" onClick={prevStep}>
                        Back
                      </button>
                    </div>
                    <div className="packages__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 3 && (
              <div className="caskets__success">
                <div className="success">
                  <div className="success__container">
                    <CheckSuccess />
                    <div className="success__heading">
                      <p>Success</p>
                    </div>
                    <div className="success__paragraph">
                      Casket details have been submitted
                    </div>
                    <div className="success__options">
                      <div className="success__option">
                        <button className="btn btn--blue-grey">Action 1</button>
                      </div>
                      <div className="success__option">
                        <button className="btn btn--blue-border">
                          Action 2
                        </button>
                      </div>
                      <div className="success__option">
                        <button
                          className="btn btn--blue"
                          type="button"
                          onClick={() => setStep(0)}
                        >
                          Add casket
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

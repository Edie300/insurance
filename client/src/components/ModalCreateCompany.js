import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import Resizer from "react-image-file-resizer";
import { isAuthenticated } from "../auth";
import { createCompany, uploadLogo, deleteLogo } from "../api/companies";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Funeral } from ".././assets/svg/Totum_Icon_Funeral.svg";
import { ReactComponent as Health } from ".././assets/svg/Totum_Icon_Health.svg";
import { ReactComponent as Motor } from ".././assets/svg/Totum_Icon_Motor.svg";
import { ReactComponent as Life } from ".././assets/svg/Totum_Icon_Life.svg";
import { ReactComponent as Home } from ".././assets/svg/Totum_Icon_Home.svg";
import { ReactComponent as Travel } from ".././assets/svg/Totum_Icon_Travel.svg";
import { ReactComponent as Appliance } from ".././assets/svg/Totum_Icon_Appliance.svg";
import { ReactComponent as Pet } from ".././assets/svg/Totum_Icon_Pet.svg";
import { ReactComponent as Cyber } from ".././assets/svg/Totum_Icon_Cyber.svg";
import { ReactComponent as Pension } from ".././assets/svg/Totum_Icon_Pension.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";

const ModalCreateCompany = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);

  useEffect(() => {}, []);

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };

  const [preview, setPreview] = useState("");
  const [loading, setLoading] = useState(false);
  const [uploading, setUpLoading] = useState(false);
  const [values, setValues] = useState({
    attachments: [""],
    logo: {},
    name: "",
    tax_number: "",
    phones: [],
    landlines: [],
    email: "",
    address: "",
    insurance: [],
    type: 1,
  });
  const [phone, setPhone] = useState("");
  const [landline, setLandline] = useState("");


  const handleChange = (name) => (event) => {
    event.preventDefault();
    setValues({ ...values, [name]: event.target.value });
  };

  // Handle company logo upload
  const handleLogoUpload = (e) => {
    let file = e.target.files[0];
    setPreview(window.URL.createObjectURL(file));
    const data = new FormData();
    data.append("image", file);

    uploadLogo(data, token)
      .then((res) => {
        setValues({ ...values, logo: res.data });
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  // Handle company logo delete
  const handleLogoDelete = () => {
    deleteLogo(values.logo, token)
      .then((res) => {
        setValues({ ...values, logo: {} });
        setPreview("");
        setLoading(false);
      })
      .catch((error) => {
        console.log("signin.js", error);
        setLoading(false);
      });
  };

  // Handle phones
  const handlePhones = (event) => {
    event.preventDefault();
    setPhone(event.target.value);
  };

  const handleAddPhone = () => {
    // event.preventDefault();
    const currentPhones = [...values.phones];
    currentPhones.push(phone);
    setValues({ ...values, phones: currentPhones });

    console.log("phones", values.phones);
    setPhone("");
  };
  const handleRemovePhone = (index) => {
    const currentPhones = [...values.phones];
    currentPhones.splice(index, 1);
    setValues({ ...values, phones: currentPhones });
  };

  // Handle landlines
  const handleLandline = (event) => {
    event.preventDefault();
    setLandline(event.target.value);
  };

  const handleAddLandline = () => {
    // event.preventDefault();
    const currentLandlines = [...values.landlines];
    currentLandlines.push(landline);
    setValues({ ...values, landlines: currentLandlines });
    setLandline("");
  };
  const handleRemoveLandline = (index) => {
    const currentLandlines = [...values.landlines];
    currentLandlines.splice(index, 1);
    setValues({ ...values, landlines: currentLandlines });
  };

  // Handle select options
  const handleCompanyType = (selected) => {
    setValues({ ...values, type: selected.value });
  };


  // handle insurance type select
  const handleInsuranceSelect = (t) => () => {
    // return first index or -1
    const currentInsuranceId = values.insurance.indexOf(t);
    const newCheckedInsuranceId = [...values.insurance];

    // if currently checked was not already in checked state > push
    // elese pull/take

    if (currentInsuranceId === -1) {
      newCheckedInsuranceId.push(t);
    } else {
      newCheckedInsuranceId.splice(currentInsuranceId, 1);
    }
    setValues({ ...values, insurance: newCheckedInsuranceId});
  
  };

  const handleSubmit = () => {
    setLoading(true)
    createCompany(values, token)
    .then((res) => {
      setValues({
        attachments: [""],
        logo: {},
        name: "",
        tax_numbe: "",
        phones: [],
        landlines: [],
        email: "",
        address: "",
        insurance: [],
        type: 1,
      });
      setPreview("");
      setLoading(false);
      resetStep()
    })
    .catch((error) => {
      console.log("register company===>", error);
      setLoading(false);
    });
  }

  const SelectType = () => (
    <>
      <div className="packages__select">
        <div
          className={
            values.insurance.indexOf(0) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(0)}
        >
          <div>
            <Funeral />
          </div>
          <p className="">Funeral</p>
        </div>

        <div
          className={
            values.insurance.indexOf(1) === -1 ? "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(1)}
        >
          <div>
            <Health />
          </div>
          <p>Medical</p>
        </div>

        <div
          className={
            values.insurance.indexOf(2) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(2)}
        >
          <div>
            <Motor />
          </div>
          <p>Motor</p>
        </div>

        <div
          className={
            values.insurance.indexOf(3) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(3)}
        >
          <div>
            <Life />
          </div>
          <p>Life</p>
        </div>

        <div
          className={
            values.insurance.indexOf(4) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(4)}
        >
          <div>
            <Home />
          </div>
          <p>Home</p>
        </div>

        <div
          className={
            values.insurance.indexOf(5) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(5)}
        >
          <div>
            <Appliance />
          </div>
          <p className="">Appliance</p>
        </div>

        <div
          className={
            values.insurance.indexOf(6) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(6)}
        >
          <div>
            <Travel />
          </div>
          <p className="">Travel</p>
        </div>

        <div
          className={
            values.insurance.indexOf(7) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(7)}
        >
          <div>
            <Pet />
          </div>
          <p className="">Pet</p>
        </div>

        <div
          className={
            values.insurance.indexOf(8) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(8)}
        >
          <div>
            <Cyber />
          </div>
          <p className="">Cyber</p>
        </div>

        <div
          className={
            values.insurance.indexOf(9) === -1 ?  "packages__option": "packages__option-checked"
          }
          onClick={handleInsuranceSelect(9)}
        >
          <div>
            <Pension />
          </div>
          <p className="">Pension</p>
        </div>
      </div>
      <div className="companies__footer">
        <div className="companies__buttons">
          <div className="companies__back">
            <button className="btn btn--blue-border" onClick={prevStep}>
              Back
            </button>
          </div>
          <div className="companies__next">
            <button className="btn btn--blue-border" onClick={nextStep}>
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="companies__modal" onClick={(e) => e.stopPropagation()}>
        <div className="companies__heading">
          <div className="companies__information">
            <h2>New company</h2>
          </div>
          <div onClick={onClose} className="companies__close">
            <IconClose />
          </div>
        </div>
        <div className="companies__container">
          <pre>{JSON.stringify(values)}</pre>
          <form className="">
            {step === 0 && (
              <>
                <div style={{ display: "flex", width: "100%" }}>
                  <div
                    className=""
                    style={{
                      width: "10rem",
                      height: "10rem",
                      backgroundColor: "pink",
                    }}
                  >
                    <img
                      src={preview}
                      style={{ width: "10rem", height: "10rem" }}
                    />
                  </div>
                  <button type="button" onClick={()=>handleLogoDelete()}>Delete logo</button>
                </div>
                <div className="companies__details">
                  <div className="companies__">
                    <label className="form__label">Logo</label>
                    <input
                      className="form__input"
                      type="file"
                      onChange={handleLogoUpload}
                      accept="image/*"
                    />
                  </div>
                  <div className="companies__">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("name")}
                      value={values.name}
                    />
                  </div>
                  <div className="companies__">
                    <label className="form__label">Tax number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("tax_number")}
                      value={values.tax_number}
                    />
                  </div>
                  <div className="companies__">
                    <label className="form__label">Company type</label>
                    <Select
                      defaultValue={values.type}
                      onChange={handleCompanyType}
                      options={[
                        { value: 0, label: "Admin" },
                        { value: 1, label: "Insurer" },
                      ]}
                    />
                  </div>
                </div>
                <div className="companies__footer">
                  <div className="companies__buttons">
                    <div className="companies__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="companies__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}
            {step === 1 && (
              <>
                <div className="companies__contacts">
                  <div
                    className="companies__"
                    style={{ display: "flex", flexDirection: "row" }}
                  >
                    <div className="">
                      <label className="form__label">phone</label>
                      <input
                        className="form__input"
                        type="text"
                        onChange={handlePhones}
                        value={phone}
                      />
                    </div>
                    <div className="">
                      <button
                        className="btn btn--blue"
                        type="button"
                        onClick={() => handleAddPhone()}
                      >
                        add
                      </button>
                    </div>
                  </div>
                  {values.phones.map((phone, i) => (
                    <div style={{ display: "flex" }} key={i}>
                      <span>{phone}</span>
                      <p
                        style={{ color: "red", marginLeft: "2rem" }}
                        onClick={() => handleRemovePhone(i)}
                      >
                        remove
                      </p>
                    </div>
                  ))}

                  <div
                    className="companies__"
                    style={{ display: "flex", flexDirection: "row" }}
                  >
                    <div className="">
                      <label className="form__label">landline</label>
                      <input
                        className="form__input"
                        type="text"
                        onChange={handleLandline}
                        value={landline}
                      />
                    </div>
                    <div className="">
                      <button
                        className="btn btn--blue"
                        type="button"
                        onClick={() => handleAddLandline()}
                      >
                        add
                      </button>
                    </div>
                  </div>
                  {values.landlines.map((landline, i) => (
                    <div style={{ display: "flex" }} key={i}>
                      <span>{landline}</span>
                      <p
                        style={{ color: "red", marginLeft: "2rem" }}
                        onClick={() => handleRemoveLandline(i)}
                      >
                        remove
                      </p>
                    </div>
                  ))}
                  <div className="companies__">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="email"
                      onChange={handleChange("email")}
                      value={values.email}
                    />
                  </div>
                  <div className="companies__">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("address")}
                      value={values.address}
                    />
                  </div>
                </div>
                <div className="companies__footer">
                  <div className="companies__buttons">
                    <div className="companies__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="companies__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}
            {step === 2 && <SelectType />}

            {step === 3 && (
              <div className="companies__documents">
                <label className="form__label">Attachments</label>
                <div className="">
                  <Upload />
                </div>
                <div className="companies__footer">
                  <div className="companies__buttons">
                    <div className="companies__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="companies__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 4 && (
              <>
                <div className="companies__review">
                  <p
                    style={{
                      width: "100%",
                      color: "blue",
                      textAlign: "center",
                      fontSize: "1.4rem",
                      marginBottom: "2rem",
                    }}
                  >
                    Review details
                  </p>
                </div>
                <div className="companies__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmit}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="companies__footer">
                  <div className="companies__buttons">
                    <div className="companies__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="companies__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 5 && (
              <div className="">
                <CheckSuccess />
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateCompany;

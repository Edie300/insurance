import React, { useEffect } from "react";
import moment from "moment";


const PanelClient = ({
  show,
  onClose,
  name,
  surname,
  policyId,
  productType,
  registrationDate,
  insuredPersons,
  agesOfInsured,
  amount,
  status,
  lastPayment,
  isPaid,
  duration,
}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>

        <div className="modal" onClick={(e) => e.stopPropagation()}>
          <div className="modal__close">
            <h4 className="modal__name">
              {name} {surname}
            </h4>
            <h4 onClick={onClose} className="modal__close-text">
              x
            </h4>
          </div>

          <div className="modal__content">
            <div className="modal__item">
              <p className="modal__heading">Policy ID</p>
              <span>{policyId}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Insured persons</p>
              <span>{insuredPersons} persons</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Product type</p>
              <span>{productType}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Registration Date</p>
              <span>{moment(registrationDate).format("lll")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading"> Ages of Insured persons</p>
              {agesOfInsured.map((age, i) => (
                <span key={i}>
                  {moment().diff(Date.parse(age), "years")} yrs
                </span>
              ))}
            </div>
            <div className="modal__item">
              <p className="modal__heading">Monthly subscription</p>
              <span>{amount}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Duration</p>
              <span>{moment(duration).fromNow(true)}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Last payment</p>
              <span>{moment(lastPayment).format("lll")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Subscription status</p>
              {(() => {
                switch (isPaid) {
                  case true:
                    return <span className="text-green">Up to date</span>;
                  case false:
                    return <span className="text-warning">Overdue</span>;
                }
              })()}
            </div>
            <div className="modal__item">
              <p className="modal__heading">policy status</p>

              {(() => {
                switch (status) {
                  case 0:
                    return <span className="text-green">Active</span>;
                  case 1:
                    return <span className="text-green">Suspended</span>;
                }
              })()}
            </div>
            <div className="modal__item">
              <p className="modal__heading">Options</p>
              <span className="text-blue">Show option list</span>
            </div>
          </div>
        </div>
    </div>
  );
};

export default PanelClient;

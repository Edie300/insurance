import React, { useEffect } from "react";

import IconClose from "./iconClose";

import IMG from "../assets/images/id00_1.jpg";
import ShowImage from "../core/ShowImage";

import moment from "moment";

const PanelFunPayments = ({ show, onClose}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }

  return (
    <div className="overlay">
      <div className="panel" onClick={(e) => e.stopPropagation()}>
        <div className="panel__close" onClick={onClose}>
          <IconClose />
        </div>
        <div className="panel__content">
          <div className="panel__left">
          
          </div>
          <div className="panel__right">
            <div className="panel__personal">
              <h2 className="panel__title">PAYMENTS</h2>
              <div className="panel__details">
                <p className="">Name</p>
                <p className="">
                rthrtych
                </p>
              </div>
              <div className="panel__details">
                <p className="">Surname</p>
                <p className="">retwerty</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default PanelFunPayments;

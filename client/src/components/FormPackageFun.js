import React, { useState, useEffect } from "react";
import _ from "lodash" ;
import { Link } from "react-router-dom";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { getCompanyCaskets } from "../api/caskets";
import { createPackageFun, uploadAttachments } from "../api/package-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import Uploader from "./Uploader";

import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const FormPackageFun = ({ start }) => {
  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(1);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };

  const [caskets, setCaskets] = useState([]);
  const [loading, setLoading] = useState(false);
  const [funeralPackage, setFuneralPackage] = useState({
    attachments: [],
    name: "",
    description: "",
    policy_lapse_period: null,
    policy_archive_period: null,
    payment_grace_period: null,
    waiting_period: null,
    principal_member_min_age: null,
    principal_member_max_age: null,
    dependent_max_age: null,
    dependents_max_count: null,
    policy_maturity: null,
    child_max_age: null,
    child_max_age_school: null,
    sum_assured: null,
    hearse: false,
    bus: false,
    groceries: false,
    cash_in_lieu: null,
    accidental_death: false,
    mortuary_services: false,
    events_management: false,
    burial_services: false,
    currency: "ZW",
    service_area: "local",
    family_rate: null,
    dependent_rate: null,
    premium_fixed: null,
    individual_rates: [],
    child_rate: null,
    unbilled_dependents: [],
    payment_type: 0,
    casket: "",
  });

  const [images, setImages] = useState([]);

  // package__rates
  const [rate, setRate] = useState({
    min_age: 0,
    max_age: 0,
    rate: 0,
  });

  const init = () => {
    getCompanyCaskets(company.id, token)
      .then((res) => {
        console.log("caskets==>", res.data);
        setCaskets(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    init();
  }, []);

  // ----------------------[HANDLE ATTACHMENTS]-------------------------

  const handleUpload = () => {
    let attachments = _.cloneDeep(images);
    console.log("images==>", images);

 
    attachments.forEach((image) => {
      delete image["file"];
    });
    console.log("attachments==>", attachments);

    uploadAttachments(token, attachments)
    .then((res) => {
      console.log("attachments==>", res.data);
      setFuneralPackage({ ...funeralPackage, attachments: res.data});
     
    })
    .catch((error) => {
      console.log("::Error::", error);
    });
  };

  // ---------------------[FUNERAL PACKAGE]------------------------------

  const handleChangeFun = (name) => (event) => {
    event.preventDefault();
    setFuneralPackage({ ...funeralPackage, [name]: event.target.value });
  };

  // handle casket select

  const handleCasketSelect = (selected) => {
    setFuneralPackage({ ...funeralPackage, casket: selected.value });
  };

  const handleCurrencySelect = (selected) => {
    setFuneralPackage({ ...funeralPackage, currency: selected.value });
  };
  const handleServiceAreaSelect = (selected) => {
    setFuneralPackage({ ...funeralPackage, service_area: selected.value });
  };

  // handle checks
  const handleCheckFunHearse = () => {
    let currentValue = funeralPackage.hearse;
    setFuneralPackage({ ...funeralPackage, hearse: !currentValue });
  };

  const handleCheckFunBus = () => {
    let currentValue = funeralPackage.bus;
    setFuneralPackage({ ...funeralPackage, bus: !currentValue });
  };

  const handleCheckFunGroceries = () => {
    let currentValue = funeralPackage.groceries;
    setFuneralPackage({ ...funeralPackage, groceries: !currentValue });
  };

  const handleCheckFunAccidental = () => {
    let currentValue = funeralPackage.accidental_death;
    setFuneralPackage({ ...funeralPackage, accidental_death: !currentValue });
  };

  const handleCheckFunMortuary = () => {
    let currentValue = funeralPackage.mortuary_services;
    setFuneralPackage({ ...funeralPackage, mortuary_services: !currentValue });
  };

  const handleCheckFunEvents = () => {
    let currentValue = funeralPackage.events_management;
    setFuneralPackage({ ...funeralPackage, events_management: !currentValue });
  };

  const handleCheckFunBurial = () => {
    let currentValue = funeralPackage.burial_services;
    setFuneralPackage({ ...funeralPackage, burial_services: !currentValue });
  };

  // ---------------------[HANDLE RATES]---------------------------------------
  const handleChangeRates = (name) => (event) => {
    event.preventDefault();
    setRate({ ...rate, [name]: event.target.value });
  };

  const handleAddRate = () => {
    // event.preventDefault();
    const currentRates = [...funeralPackage.individual_rates];
    currentRates.push(rate);
    setFuneralPackage({ ...funeralPackage, individual_rates: currentRates });

    console.log("rates", funeralPackage.individual_rates);
    setRate({
      min_age: 0,
      max_age: 0,
      rate: 0,
    });
  };

  // ---------------------[HANDLE UNBILLED]----------------------------------------
  const handleUnbilled = (dep) => {
    const currentUnbilled = funeralPackage.unbilled_dependents.indexOf(dep);
    const newUnbilled = [...funeralPackage.unbilled_dependents];

    if (currentUnbilled === -1) {
      newUnbilled.push(dep);
    } else {
      newUnbilled.splice(currentUnbilled, 1);
    }
    setFuneralPackage({ ...funeralPackage, unbilled_dependents: newUnbilled });
  };

  // ---------------------[HANDLESUBMIT]----------------------------------------

  const handleSubmitPackage = () => {
    createPackageFun(token, funeralPackage)
      .then((res) => {
        console.log("new package==>", res.data);
        nextStep();
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  return (
    <div className=" ">
      {step === 1 && (
        <div className="">
          <div className="packages__details">
            <div className="packages__name">
              <label className="form__label">Name</label>
              <input
                className="form__input"
                type="text"
                value={funeralPackage.name}
                onChange={handleChangeFun("name")}
              />
            </div>

            <div className="packages__casket">
              <label className="form__label">Casket</label>
              <Select
                defaultValue={funeralPackage.casket}
                onChange={handleCasketSelect}
                options={caskets.map((casket) => ({
                  value: casket.id,
                  label: casket.name,
                }))}
              />
            </div>
            <div className="packages__waiting">
              <label className="form__label">waiting period</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.waiting_period}
                onChange={handleChangeFun("waiting_period")}
              />
            </div>
            <div className="packages__payment">
              <label className="form__label">payment grace period</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.payment_grace_period}
                onChange={handleChangeFun("payment_grace_period")}
              />
            </div>
            <div className="packages__lapse">
              <label className="form__label">Lapse period</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.policy_lapse_period}
                onChange={handleChangeFun("policy_lapse_period")}
              />
            </div>
            <div className="packages__archive">
              <label className="form__label">Archive period</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.policy_archive_period}
                onChange={handleChangeFun("policy_archive_period")}
              />
            </div>
            <div className="packages__principal-min">
              <label className="form__label">
                Principal member maximum age
              </label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.principal_member_max_age}
                onChange={handleChangeFun("principal_member_max_age")}
              />
            </div>
            <div className="packages__principal-max">
              <label className="form__label">Principal member minmum age</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.principal_member_min_age}
                onChange={handleChangeFun("principal_member_min_age")}
              />
            </div>
            <div className="packages__dependent-max">
              <label className="form__label">dependent maximum age</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.dependent_max_age}
                onChange={handleChangeFun("dependent_max_age")}
              />
            </div>
            <div className="packages__dependent-limit">
              <label className="form__label">dependents limit</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.dependents_max_count}
                onChange={handleChangeFun("dependents_max_count")}
              />
            </div>
            <div className="packages__maturity">
              <label className="form__label">Policy maturity</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.policy_maturity}
                onChange={handleChangeFun("policy_maturity")}
              />
            </div>
            <div className="packages__child-max">
              <label className="form__label">Child maximum age</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.child_max_age}
                onChange={handleChangeFun("child_max_age")}
              />
            </div>
            <div className="packages__child-max-school">
              <label className="form__label">Child at school maximum age</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.child_max_age_school}
                onChange={handleChangeFun("child_max_age_school")}
              />
            </div>
            <div className="packages__sum-assured">
              <label className="form__label">Sum assured</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.sum_assured}
                onChange={handleChangeFun("sum_assured")}
              />
            </div>
            <div className="packages__service-area">
              <label className="form__label">Service area</label>
              <Select
                defaultValue={funeralPackage.service_area}
                onChange={handleServiceAreaSelect}
                options={[
                  { value: "local", label: "local" },
                  { value: "national", label: "national" },
                  { value: "international", label: "international" },
                ]}
              />
            </div>
          </div>
          <div className="packages__description">
            <label className="form__label">description</label>
            <textarea
              className="form__input"
              style={{ height: "4rem", width: "100%" }}
              type="text"
              value={funeralPackage.description}
              onChange={handleChangeFun("description")}
            />
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button
                  className="btn btn--blue-grey"
                  type="button"
                  onClick={() => start()}
                >
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 2 && (
        <div className="packages__documents">
          <div className="packages__benefits">
            <div className="packages__check">
              <input
                type="checkbox"
                value={funeralPackage.hearse}
                checked={funeralPackage.hearse}
                onChange={handleCheckFunHearse}
              />

              <div className="packages__info">
                <p>Hearse</p>
                <span>somthing abt hearse</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                type="checkbox"
                className=""
                type="checkbox"
                value={funeralPackage.bus}
                checked={funeralPackage.bus}
                onChange={handleCheckFunBus}
              />

              <div className="packages__info">
                <p>Bus</p>
                <span>somthing abt bus</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={funeralPackage.groceries}
                checked={funeralPackage.groceries}
                onChange={handleCheckFunGroceries}
              />

              <div className="packages__info">
                <p>groceries</p>
                <span>somthing abt groceries</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={funeralPackage.accidental_death}
                checked={funeralPackage.accidental_death}
                onChange={handleCheckFunAccidental}
              />

              <div className="packages__info">
                <p>accidental death</p>
                <span>somthing abt accidental </span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={funeralPackage.events_management}
                checked={funeralPackage.events_management}
                onChange={handleCheckFunEvents}
              />

              <div className="packages__info">
                <p>events management</p>
                <span>somthing abt events management</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={funeralPackage.mortuary_services}
                checked={funeralPackage.mortuary_services}
                onChange={handleCheckFunMortuary}
              />

              <div className="packages__info">
                <p>mortuary services</p>
                <span>somthing abt mortuary services</span>
              </div>
            </div>

            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={funeralPackage.burial_services}
                checked={funeralPackage.burial_services}
                onChange={handleCheckFunBurial}
              />

              <div className="packages__info">
                <p>burial services</p>
                <span>somthing abt burial services</span>
              </div>
            </div>
          </div>
          <div className="packages__unbilled">
            <div className="search__status">
              <p className="search__label">Unbilled dependets</p>
              <div className="search__radios">
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("child")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "child"
                    )}
                  />
                  <span>child</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("spouse")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "spouse"
                    )}
                  />
                  <span>spouse</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("uncle")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "uncle"
                    )}
                  />
                  <span>uncle</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("aunt")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "aunt"
                    )}
                  />
                  <span>aunt</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("nephew")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "nephew"
                    )}
                  />
                  <span>nephew</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("niece")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "niece"
                    )}
                  />
                  <span>niece</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("sibling")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "sibling"
                    )}
                  />
                  <span>sibling</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("grandparent")}
                    checked={funeralPackage.unbilled_dependents.includes(
                      "grandparent"
                    )}
                  />
                  <span>grandparent</span>
                </div>
              </div>
            </div>
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 3 && (
        <div className="packages__documents">
          <div className="packages__premium">
            <div className="packages__payment-type">
              <label className="form__label">payment type</label>
              <Select
                defaultValue={funeralPackage.payment_type}
                // onChange={handleCompanyType}
                options={[
                  { value: 1, label: "monthly" },
                  { value: 2, label: "quarterly" },
                  { value: 3, label: "yearly" },
                ]}
              />
            </div>
            <div className="packages__currency">
              <label className="form__label">Currency</label>
              <Select
                defaultValue={funeralPackage.currency}
                onChange={handleCurrencySelect}
                options={[
                  { value: "ZW", label: "ZW" },
                  { value: "US", label: "US" },
                ]}
              />
            </div>
            <div className="packages__family">
              <label className="form__label">Family rate</label>
              <input
                className="form__input"
                type="number"
                value={funeralPackage.family_rate}
                onChange={handleChangeFun("family_rate")}
              />
            </div>
          </div>
          <div className="packages__individual-rates">
            <div className="packages__form-rates">
              <div className="packages__family">
                <label className="form__label">minimum age</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.min_age}
                  onChange={handleChangeRates("min_age")}
                />
              </div>

              <div className="packages__family">
                <label className="form__label">maximum age</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.max_age}
                  onChange={handleChangeRates("max_age")}
                />
              </div>

              <div className="packages__family">
                <label className="form__label">rate</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.rate}
                  onChange={handleChangeRates("rate")}
                />
              </div>
              <div className="packages__rates-button">
                <button
                  className="btn btn--blue"
                  type="button"
                  onClick={() => handleAddRate()}
                >
                  Add rate
                </button>
              </div>
            </div>
            <div className="packages__table-rates">
              <div class=" modal__table">
                <div class=" modal__table-header">
                  <div class="modal__table-cell">Min age</div>
                  <div class="modal__table-cell">Max age</div>
                  <div class="modal__table-cell">Rate</div>
                </div>
                {funeralPackage.individual_rates.map((rate, i) => (
                  <div class="modal__table-row" key={i}>
                    <div class=" modal__table-cell">{rate.min_age} years</div>
                    <div class=" modal__table-cell">{rate.max_age} years</div>
                    <div class=" modal__table-cell">{rate.rate}</div>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 4 && (
        <div className="">
          <Uploader  images={images} setImages={setImages} maxNumber={3}/>
          <p>{JSON.stringify(funeralPackage.attachments)}</p>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" 
                type="button" 
                onClick={nextStep}
                // onClick={()=>handleUpload()}
                >
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 5 && (
        <>
          <div className="packages__review">
            <div className="packages__info">
              <p className="package__section">About package</p>
              <div class="package__details">
                <div className="package__item">
                  <p className="">Package</p>
                  <span>{funeralPackage.name}</span>
                </div>
                <div className="package__item">
                  <p className="">Casket</p>
                  <span>{funeralPackage.casket}</span>
                </div>
                <div className="package__item">
                  <p className="">lapse period</p>
                  <span>{funeralPackage.policy_lapse_period} months</span>
                </div>
                <div className="package__item">
                  <p className="">archive period</p>
                  <span>{funeralPackage.policy_archive_period} months</span>
                </div>
                <div className="package__item">
                  <p className="">payment grace period</p>
                  <span>{funeralPackage.payment_grace_period} months</span>
                </div>
                <div className="package__item">
                  <p className="">Waiting period</p>
                  <span>{funeralPackage.waiting_period} months</span>
                </div>
                <div className="package__item">
                  <p className="">Principal minimum age</p>
                  <span>{funeralPackage.principal_member_min_age} years</span>
                </div>
                <div className="package__item">
                  <p className="">Principal maximum age</p>
                  <span>{funeralPackage.principal_member_max_age} years</span>
                </div>
                <div className="package__item">
                  <p className="">Dependent maximum age</p>
                  <span>{funeralPackage.dependent_max_age} years</span>
                </div>
                <div className="package__item">
                  <p className="">Dependent limit</p>
                  <span>{funeralPackage.dependents_max_count}</span>
                </div>
                <div className="package__item">
                  <p className="">policy maturity</p>
                  <span>{funeralPackage.policy_maturity} years</span>
                </div>
                <div className="package__item">
                  <p className="">Child maximum age</p>
                  <span>{funeralPackage.child_max_age} years</span>
                </div>
                <div className="package__item">
                  <p className="">Child at school maximum age</p>
                  <span>{funeralPackage.child_max_age_school} years</span>
                </div>
                <div className="package__item">
                  <p className="">Sum assured</p>
                  <span>{funeralPackage.sum_assured}</span>
                </div>
                <div className="package__item">
                  <p className="">Cash in lieu</p>
                  <span>{funeralPackage.cash_in_lieu}</span>
                </div>
                <div className="package__item">
                  <p className="">groceries</p>
                  <span>{funeralPackage.groceries}</span>
                </div>
                <div className="package__item">
                  <p className="">service area</p>
                  <span>{funeralPackage.service_area}</span>
                </div>
              </div>
              <p className="package__section">Benefits</p>
              <div className="packages__benefits">
                <div className="package__benefit">
                  {funeralPackage.hearse ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">hearse</p>
                </div>
                <div className="package__benefit">
                  {funeralPackage.bus ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">bus</p>
                </div>
                <div className="package__benefit">
                  {funeralPackage.accidental_death ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">accidenta death</p>
                </div>
                <div className="package__benefit">
                  {funeralPackage.mortuary_services ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">Mortuary services</p>
                </div>
                <div className="package__benefit">
                  {funeralPackage.events_management ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">Events management</p>
                </div>
                <div className="package__benefit">
                  {funeralPackage.burial_services ? (
                    <div className="package__true">
                      <Check />
                    </div>
                  ) : (
                    <div className="package__false">
                      <X />
                    </div>
                  )}
                  <p className="">burial services</p>
                </div>
              </div>
              <p className="package__section">Premium</p>
              <div className="package__premiums">
                <div className="package__item">
                  <p className="">payment type</p>
                  <span>{funeralPackage.payment_type}</span>
                </div>
                <div className="package__item">
                  <p className="">currency</p>
                  <span>{funeralPackage.currency}</span>
                </div>
                <div className="package__item">
                  <p className="">family premium</p>
                  <span>{funeralPackage.premium_family}</span>
                </div>
                <div className="package__item">
                  <p className="">dependent</p>
                  <span>{funeralPackage.premium_dependent}</span>
                </div>
              </div>
            </div>
          </div>

          <div className="modal__submit-btn">
            <button
              className="btn btn--blue"
              type="button"
              onClick={()=>handleSubmitPackage()}
            >
              {loading ? (
                <div class="loading loading--full-height"></div>
              ) : (
                "Submit"
              )}
            </button>
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next"></div>
            </div>
          </div>
        </>
      )}
      {step === 6 && (
        <div className="caskets__success">
          <div className="success">
            <div className="success__container">
              <CheckSuccess />
              <div className="success__heading">
                <p>Success</p>
              </div>
              <div className="success__paragraph">
                New Package details have been submitted
              </div>
              <div className="success__options">
                <div className="success__option">
                  <button className="btn btn--blue-grey">Action 1</button>
                </div>
                <div className="success__option">
                  <button className="btn btn--blue-border">Action 2</button>
                </div>
                <div className="success__option">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={() => start()}
                  >
                    Add package
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default FormPackageFun;

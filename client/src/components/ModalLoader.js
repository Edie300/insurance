import React from "react";

const ModalLoader = ({ title, message }) => {
  return (
    <div className="overlay--1">
      <div className="loader__modal">
        <div className="loader__header">{title}</div>
        <div className="loader__content">{message}</div>
        {/* <div className="loader__actions">
          <a className="loader__success" href="#">
            Yes, do it
          </a>
          <a href="#">No, cancel</a>
        </div> */}
        <div className="loader__loader-bar">
          <div className="loader__bar"></div>
        </div>
      </div>
    </div>
  );
};
export default ModalLoader;

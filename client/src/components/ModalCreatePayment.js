import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { makePayment } from "../api/payments";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import ECOCASH from ".././assets/images/ecocash.png";
import ONEMONEY from ".././assets/images/onemoney.png";
import VISA from ".././assets/images/visa.png";
import MASTERCARD from ".././assets/images/mastercard.png";
import TELECASH from ".././assets/images/telecash.png";
import ZIMSWITCH from ".././assets/images/zimswitch.png";

const ModalCreateAccount = ({
  show,
  onClose,
  start = 1,
  premium = "",
  mainAccount = "",
  accountFun = "",
  accountPackage = "",
}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    console.log("payment", payment);
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, surname, email, role, company, branch },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);
  const [payment, setPayment] = useState({
    type: "",
    method: "",
    amount: 0,
    payer_name: "",
    payer_surname: "",
    payer_phone: "",
    currency: "",
    premium: "",
    account_main: mainAccount,
    account: accountFun,
    account_package: accountPackage,
  });

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setPayment({ ...payment, [name]: event.target.value });
  };

  // form steps
  const [step, setStep] = useState(start);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);

  const submitPayment = () => {
    nextStep();
  };

  const printPayment = () => {
    nextStep();
  };

  const handleSubmitPayment = () => {
    nextStep();
    // setLoading(true);
    // makePayment(token, payment)
    //   .then((res) => {
    //     console.log("response", res.data);
    //     setLoading(false);
       
    //   })
    //   .catch((error) => {
    //     console.log("::Error::", error);
    //     setLoading(false);
    //   });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>New payment</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="accounts__modal-container">
          <form className="">
            {step === 0 && (
              <>
                <div className="claims__search-box">
                  <input
                    className="form__input"
                    type="text"
                    placeholder="account number"
                  />
                  <button className="btn">Search</button>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-border"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </>
            )}

            {step === 1 && (
              <div className="">
                {/* <p>{JSON.stringify(payment)}</p> */}
                <div className="payments__information">
                  <div className="payments__method">
                    <div className="payments__image">
                      <img src={ECOCASH} alt="ecocash" />
                    </div>
                    <div className="payments__image">
                      <img src={ONEMONEY} alt="onemoney" />
                    </div>
                    <div className="payments__image">
                      <img src={TELECASH} alt="telecash" />
                    </div>
                    <div className="payments__image">
                      <img src={ZIMSWITCH} alt="zimswitch" />
                    </div>
                    <div className="payments__image">
                      <img src={VISA} alt="visa" />
                    </div>
                    <div className="payments__image">
                      <img src={MASTERCARD} alt="mastercard" />
                    </div>
                  </div>
                  <div className="payments__form">
                    <div className="modal__client-name">
                      <label className="form__label">Amount</label>
                      <input
                        className="form__input"
                        type="number"
                        value={payment.amount}
                        onChange={handleChange("amount")}
                      />
                    </div>
                    <div className="modal__client-name">
                      <label className="form__label">Currency</label>
                      <input
                        className="form__input"
                        type="text"
                        value={payment.currency}
                        onChange={handleChange("currency")}
                      />
                    </div>
                    <div className="modal__client-name">
                      <label className="form__label">Payer name</label>
                      <input
                        className="form__input"
                        type="text"
                        value={payment.payer_name}
                        onChange={handleChange("payer_name")}
                      />
                    </div>
                    <div className="modal__client-name">
                      <label className="form__label">Payer surname</label>
                      <input
                        className="form__input"
                        type="text"
                        value={payment.payer_surname}
                        onChange={handleChange("payer_surname")}
                      />
                    </div>
                    <div className="modal__client-name">
                      <label className="form__label">Payer phone number</label>
                      <input
                        className="form__input"
                        type="text"
                        value={payment.payer_phone}
                        onChange={handleChange("payer_phone")}
                      />
                    </div>
                  </div>
                </div>
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleSubmitPayment}
                    disable={loading}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "make payment"
                    )}
                  </button>
                </div>
              </div>
            )}

            {step === 2 && (
              <>
                <div className="payments__invoice">
                  <div className="payments__top">
                    <div className="payments__company">
                      <p className="payments__company-name">{company.name}</p>
                      <p className="payments__company-address">
                        {company.address}
                      </p>
                      <p className="payments__company-email">{company.email}</p>
                      {company.phones.map((phone, i) => (
                        <p className="payments__company-phone" key={i}>
                          {phone}
                        </p>
                      ))}
                    </div>
                    <div className="payments__invoice-title">
                      <p className="">Invoice</p>
                    </div>
                  </div>
                  <div className="payments__branch">
                    <div className="payments__tailor">
                      <p className="payments__user">
                        {name} {surname}
                      </p>
                      <p className="payments__branch-name">{branch.name}</p>
                      <p className="payments__branch-address">
                        {branch.address}
                      </p>
                      <p className="payments__branch-email">{branch.email}</p>
                      {branch.phones.map((phone, i) => (
                        <p className="payments__branch-phone" key={i}>
                          {phone}
                        </p>
                      ))}
                    </div>
                    <div className="paymnets__invoice-details">
                      <div className="payments__invoice-item">
                        <p className="">invoice number</p>
                        <span>3124214323</span>
                      </div>
                      <div className="payments__invoice-item">
                        <p className="">invoice date</p>
                        <span>12-03-22</span>
                      </div>
                    </div>
                  </div>
                  <div className="payments__details">
                    <div class=" payments__table">
                      <div class=" payments__table-header">
                        <div class="payments__table-cell">Account number</div>
                        <div class="payments__table-cell">Package</div>
                        <div class="payments__table-cell">Type</div>
                        <div class="payments__table-cell">Currency</div>
                        <div class="payments__table-cell">Premium</div>
                        <div class="payments__table-cell">Amount</div>
                      </div>
                    </div>
                    <div class="payments__table-row">
                      <div class=" payments__table-cell">214353465EM</div>
                      <div class=" payments__table-cell">family package</div>
                      <div class=" payments__table-cell">funeral</div>
                      <div class=" payments__table-cell">zw</div>
                      <div class="payments__table-cell">100.00</div>
                      <div class="payments__table-cell">100.00</div>
                    </div>
                  </div>
                  <div className="payments__bottom">
                    <div className="payments__terms">
                      <h3>Terms and Conditions</h3>
                      <p>
                        Thank you for your business. We do expect payment within
                        one month, so please pay in time to avoid unnecessary
                        inconvinience.
                      </p>
                    </div>
                    <div className="payments__calculate">
                      <div className="payments__balance-due">
                        <p>balance</p>
                        <span>100</span>
                      </div>
                      <div className="payments__balance">
                        <p>balance</p>
                        <span>100</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="payments__print">
                  <button
                    className="btn btn--blue-border"
                    type="button"
                    onClick={printPayment}
                    disable={loading}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Print"
                    )}
                  </button>
                </div>
              </>
            )}
            {step === 3 && (
              <div className="">
                <div className="success">
                  <div className="success__container">
                    <CheckSuccess />
                    <div className="success__heading">
                      <p>Success</p>
                    </div>
                    <div className="success__paragraph">
                      Payment details have been submitted
                    </div>
                    <div className="success__options">
                      <div className="success__option">
                        <button className="btn btn--blue-grey">Action 1</button>
                      </div>
                      <div className="success__option">
                        <button className="btn btn--blue-border">
                          Action 2
                        </button>
                      </div>
                      <div className="success__option">
                        <button
                          className="btn btn--blue"
                          type="button"
                          onClick={() => setStep(1)}
                        >
                          new payment
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

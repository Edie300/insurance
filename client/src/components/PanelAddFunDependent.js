import React, { useEffect, useState } from "react";
import moment from "moment";

import IconClose from "./iconClose";
import ModalLoader from "../components/ModalLoader";

import { createFuneralDependent } from "../api/dependentFuneral";
import { isAuthenticated } from "../auth";

const PanelAddFunDependents = ({ account, show, onClose }) => {
  const {
    user: { _id, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [dependent, setDependent] = useState({
    name: "",
    surname: "",
    gender: "",
    dob: "",
    national_id: "",
    address: "",
    marital_status: "",
    title: "",
    passport: "",
    phone: "",
    email: "",
    relationship: "",
  });
  const [loader, setLoader] = useState(false);

  const {
    name,
    surname,
    gender,
    dob,
    national_id,
    passport,
    address,
    marital_status,
    title,
    phone,
    email,
    relationship,
  } = dependent;

  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const handleChange = (name) => (event) => {
    setDependent({ ...dependent, [name]: event.target.value });
  };

  const handleDob = (date) => {
    setDependent({
      ...dependent,
      dob: date,
    });
  };


  const handleSubmit = () => {
    setLoader(true)
    console.log(account._id, dependent)
    createFuneralDependent(token, account._id, dependent)
    .then((res) => {
      setDependent({
        name: "",
        surname: "",
        gender: "",
        dob: "",
        national_id: "",
        address: "",
        marital_status: "",
        title: "",
        passport: "",
        phone: "",
        email: "",
        relationship: "",
      })

      setLoader(false)
    })
    .catch((error) => {
      console.log("error", error);
    });
  }

  if (!show) {
    return null;
  }

  return (
    <div className="overlay">
      <div className="panel" onClick={(e) => e.stopPropagation()}>
        <div className="panel__close" onClick={onClose}>
          <IconClose />
        </div>
        <div className="panel__content">
          <h4 className="panel__name u-margin-bottom-small">Add Dependent</h4>

          <div className="panel__block">
            <div className="form__group panel__item--2">
              <label className="form__label">Name</label>
              <input
                onChange={handleChange("name")}
                className="form__input"
                type="text"
                value={name}
              />
            </div>
            <div className="form__group panel__item--2">
              <label className="form__label">Surname</label>
              <input
                onChange={handleChange("surname")}
                className="form__input"
                type="text"
                value={surname}
              />
            </div>
            <div className="form__group panel__item">
              <label className="form__label">Title</label>
              <div className="form__select">
                <select onChange={handleChange("title")}>
                  <option value="">Select title</option>

                  <option value="Mr">Mr</option>
                  <option value="Mrs">Mrs</option>
                  <option value="Miss">Miss</option>
                  <option value="Dr">Dr</option>
                  <option value="Prof">Prof</option>
                </select>
              </div>
            </div>
          </div>

          <div className="panel__block">
            <div className="form__group panel__item">
              <label className="form__label">Date fo Birth</label>
              <input
              onChange={handleChange("dob")}
              className="form__input"
              type="date"
              value={dob}
              name="dob"
            />
            </div>
            <div className="form__group panel__item">
              <label className="form__label">Gender</label>
              <div className="form__select">
                <select onChange={handleChange("gender")}>
                  <option value="">Select gender</option>

                  <option value="male">male</option>
                  <option value="female">female</option>
                </select>
              </div>
            </div>
            <div className="form__group panel__item">
              <label className="form__label">Relationship</label>
              <input
                onChange={handleChange("relationship")}
                className="form__input"
                type="text"
                value={relationship}
              />
            </div>
          </div>

          <div className="panel__block">
            <div className="form__group panel__item">
              <label className="form__label">Marital status</label>
              <div className="form__select">
                <select onChange={handleChange("marital_status")}>
                  <option value="">Select marital status</option>

                  <option value="Single">Single</option>
                  <option value="Married">Married</option>
                  <option value="Divorced">Divorced</option>
                </select>
              </div>
            </div>
            <div className="form__group panel__item--2">
              <label className="form__label">National ID</label>
              <input
                onChange={handleChange("national_id")}
                className="form__input"
                type="text"
                value={national_id}
              />
            </div>
            <div className="form__group panel__item--2">
              <label className="form__label">Passport number</label>
              <input
                onChange={handleChange("passport")}
                className="form__input"
                type="text"
                value={passport}
              />
            </div>
          </div>

          <div className="panel__block">
            <div className="form__group panel__item">
              <label className="form__label">Mobile number</label>
              <input
                onChange={handleChange("phone")}
                className="form__input"
                type="text"
                value={phone}
              />
            </div>
            <div className="form__group panel__item--2">
              <label className="form__label">Email</label>
              <input
                onChange={handleChange("email")}
                className="form__input"
                type="text"
                value={email}
              />
            </div>
          </div>
          <div className="panel__block">
            <div className="form__group panel__item">
              <label className="form__label">Address</label>
              <input
                onChange={handleChange("address")}
                className="form__input"
                type="text"
                value={address}
              />
            </div>
          </div>
          <div className="u-width-1 u-margin-top-medium">
            <button className="btn btn--blue" onClick={handleSubmit}>Add</button>
          </div>
        </div>
        {loader && (
        <ModalLoader
          title='Registering'
          message={`Adding dependent`}
        />
      )}
      </div>

    </div>
  );
};
export default PanelAddFunDependents;

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createPackageMed } from "../api/package-medical";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const FormPackageMed = ({ start }) => {
  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(1);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };

  const [loading, setLoading] = useState(false);
  const [medicalPackage, setMedicalPackage] = useState({
    attachments: [],
    name: "",
    description: "",
    annual_global_limit: 0,
    general_practitioners: 0,
    number_of_initial_visits: 0,
    medical_specialist: 0,
    hospitalization_limit: 0,
    waiting_time: 0,
    claim_processing_time: 0,
    private_hospitalization: 0,
    private_hospitalization_CV19: 0,
    public_hospitalization: 0,
    ambulance_services: false,
    air_evacuation: false,
    number_of_dependents: 0,
    age_limit_max: 0,
    age_limit_min: 0,
    premium_add: 0,
    drugs_3ben: 0,
    drugs_abv_3ben: 0,
    chronic_drug_limit: 0,
    drugs_cv19: 0,
    drugs_acute: 0,
    dental: 0,
    optical: 0,
    psychiatric: 0,
    physiotherapy: 0,
    prosthetics: 0,
    pathology: 0,
    specialized_radiology: 0,
    blood_transfusion: 0,
    maternity: 0,
    family_planning: 0,
    infertility: 0,
    hearing_aids: 0,
    hearing_scan: 0,
    funeral_cashback: 0,
    premium_waver: 0,
    wellness: 0,
    tandCs: "",
    policy_maturity: 0,
    currency: "",
    payment_type: "",
    service_area: "local",
    child_max_age: "",
    child_max_age_school: "",
    waiting_periods: [],
    family_rate: 0,
    dependent_rate: 0,
    individual_rates: [],
    child_rate: 0,
    unbilled_dependents: [],
  });

  // package__rates
  const [rate, setRate] = useState({
    min_age: 0,
    max_age: 0,
    rate: 0,
  });

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setMedicalPackage({ ...medicalPackage, [name]: event.target.value });
  };

  // handle select

  const handleServiceAreaSelect = (selected) => {
    setMedicalPackage({ ...medicalPackage, service_area: selected.value });
  };
  const handleCurrencySelect = (selected) => {
    setMedicalPackage({ ...medicalPackage, currency: selected.value });
  };

  const handlePaymentType = (selected) => {
    setMedicalPackage({ ...medicalPackage, payment_type: selected.value });
  };

  // handle checks
  const handleCheckAmbulance = () => {
    let currentValue = medicalPackage.ambulance_services;
    setMedicalPackage({ ...medicalPackage, ambulance_services: !currentValue });
  };

  const handleCheckAirAvaccuation = () => {
    let currentValue = medicalPackage.air_evacuation;
    setMedicalPackage({ ...medicalPackage, air_evacuation: !currentValue });
  };

  // ---------------------[HANDLE RATES]---------------------------------------
  const handleChangeRates = (name) => (event) => {
    event.preventDefault();
    setRate({ ...rate, [name]: event.target.value });
  };

  const handleAddRate = () => {
    // event.preventDefault();
    const currentRates = [...medicalPackage.individual_rates];
    currentRates.push(rate);
    setMedicalPackage({ ...medicalPackage, individual_rates: currentRates });

    console.log("rates", medicalPackage.individual_rates);
    setRate({
      min_age: 0,
      max_age: 0,
      rate: 0,
    });
  };

  // ---------------------[HANDLE UNBILLED]----------------------------------------
  const handleUnbilled = (dep) => {
    const currentUnbilled = medicalPackage.unbilled_dependents.indexOf(dep);
    const newUnbilled = [...medicalPackage.unbilled_dependents];

    if (currentUnbilled === -1) {
      newUnbilled.push(dep);
    } else {
      newUnbilled.splice(currentUnbilled, 1);
    }
    setMedicalPackage({ ...medicalPackage, unbilled_dependents: newUnbilled });
  };

  // ---------------------[HANDLESUBMIT]----------------------------------------

  const handleSubmitPackage = () => {
    setLoading(true);
    createPackageMed(token, medicalPackage)
      .then((res) => {
        console.log("new package==>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  return (
    <>
      {step === 1 && (
        <div className="">
          <div className="packages__four-columns">
            <div className="packages__">
              <label className="form__label">Name</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.name}
                onChange={handleChange("name")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">Description</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.description}
                onChange={handleChange("description")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">annual global limit</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.annual_global_limit}
                onChange={handleChange("annual_global_limit")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">general practitioners</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.general_practitioners}
                onChange={handleChange("general_practitioners")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">number of initial visits</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.number_of_initial_visits}
                onChange={handleChange("number_of_initial_visits")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">medical specialist</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.medical_specialist}
                onChange={handleChange("medical_specialist")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">hospitalization limit</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.hospitalization_limit}
                onChange={handleChange("hospitalization_limit")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">claim processing time</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.claim_processing_time}
                onChange={handleChange("claim_processing_time")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">private hospitalization</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.private_hospitalization}
                onChange={handleChange("private_hospitalization")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">public hospitalization</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.public_hospitalization}
                onChange={handleChange("public_hospitalization")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">number of dependents</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.number_of_dependents}
                onChange={handleChange("number_of_dependents")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">age limit max</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.age_limit_max}
                onChange={handleChange("age_limit_max")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">age limit min</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.age_limit_min}
                onChange={handleChange("age_limit_min")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">drugs 3ben</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.drugs_3ben}
                onChange={handleChange("drugs_3ben")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">drugs above 3ben</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.drugs_abv_3ben}
                onChange={handleChange("drugs_abv_3ben")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">chronic drug limit</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.chronic_drug_limit}
                onChange={handleChange("chronic_drug_limit")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">drugs acute</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.drugs_acute}
                onChange={handleChange("drugs_acute")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">dental</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.dental}
                onChange={handleChange("dental")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">optical</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.optical}
                onChange={handleChange("optical")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">psychiatric</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.psychiatric}
                onChange={handleChange("psychiatric")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">prosthetics</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.prosthetics}
                onChange={handleChange("prosthetics")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">pathology</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.pathology}
                onChange={handleChange("pathology")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">specialized_radiology</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.specialized_radiology}
                onChange={handleChange("specialized_radiology")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">blood_transfusion</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.blood_transfusion}
                onChange={handleChange("blood_transfusion")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">maternity</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.maternity}
                onChange={handleChange("maternity")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">family planning</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.family_planning}
                onChange={handleChange("family_planning")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">infertility</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.infertility}
                onChange={handleChange("infertility")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">hearing aids</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.hearing_aids}
                onChange={handleChange("hearing_aids")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">hearing scan</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.hearing_scan}
                onChange={handleChange("hearing_scan")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">funeral cashback</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.funeral_cashback}
                onChange={handleChange("funeral_cashback")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">premium waver</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.premium_waver}
                onChange={handleChange("premium_waver")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">wellness</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.wellness}
                onChange={handleChange("wellness")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">policy_maturity</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.policy_maturity}
                onChange={handleChange("policy_maturity")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">child max age</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.child_max_age}
                onChange={handleChange("child_max_age")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">child max age school</label>
              <input
                className="form__input"
                type="text"
                value={medicalPackage.child_max_age_school}
                onChange={handleChange("child_max_age_school")}
              />
            </div>

            <div className="packages__">
              <label className="form__label">Service area</label>
              <Select
                defaultValue={medicalPackage.service_area}
                onChange={handleServiceAreaSelect}
                options={[
                  { value: "local", label: "local" },
                  { value: "national", label: "national" },
                  { value: "international", label: "international" },
                ]}
              />
            </div>
          </div>

          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button
                  className="btn btn--blue-grey"
                  type="button"
                  onClick={() => start()}
                >
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 2 && (
        <div className="packages__documents">
          <div className="packages__benefits">
            <div className="packages__check">
              <input
                type="checkbox"
                value={medicalPackage.ambulance_services}
                checked={medicalPackage.ambulance_services}
                onChange={handleCheckAmbulance}
              />

              <div className="packages__info">
                <p>Ambulance services</p>
                <span>somthing abt Ambulance</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                type="checkbox"
                className=""
                type="checkbox"
                value={medicalPackage.air_evacuation}
                checked={medicalPackage.air_evacuation}
                onChange={handleCheckAirAvaccuation}
              />

              <div className="packages__info">
                <p>Air evacuation</p>
                <span>somthing abt air evacuation</span>
              </div>
            </div>
          </div>
          <div className="packages__unbilled">
            <div className="search__status">
              <p className="search__label">Unbilled dependets</p>
              <div className="search__radios">
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("child")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "child"
                    )}
                  />
                  <span>child</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("spouse")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "spouse"
                    )}
                  />
                  <span>spouse</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("uncle")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "uncle"
                    )}
                  />
                  <span>uncle</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("aunt")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "aunt"
                    )}
                  />
                  <span>aunt</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("nephew")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "nephew"
                    )}
                  />
                  <span>nephew</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("niece")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "niece"
                    )}
                  />
                  <span>niece</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("sibling")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "sibling"
                    )}
                  />
                  <span>sibling</span>
                </div>
                <div className="search__radio-box">
                  <input
                    className=""
                    type="radio"
                    onClick={() => handleUnbilled("grandparent")}
                    checked={medicalPackage.unbilled_dependents.includes(
                      "grandparent"
                    )}
                  />
                  <span>grandparent</span>
                </div>
              </div>
            </div>
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 3 && (
        <div className="packages__documents">
          <div className="packages__premium">
            <div className="packages__payment-type">
              <label className="form__label">payment type</label>
              <Select
                defaultValue={medicalPackage.payment_type}
                onChange={handlePaymentType}
                options={[
                  { value: "monthly", label: "monthly" },
                  { value: "quarterly", label: "quarterly" },
                  { value: "yearly", label: "yearly" },
                ]}
              />
            </div>
            <div className="packages__currency">
              <label className="form__label">Currency</label>
              <Select
                defaultValue={medicalPackage.currency}
                onChange={handleCurrencySelect}
                options={[
                  { value: "ZW", label: "ZW" },
                  { value: "US", label: "US" },
                ]}
              />
            </div>
            <div className="packages__family">
              <label className="form__label">Family rate</label>
              <input
                className="form__input"
                type="number"
                value={medicalPackage.family_rate}
                onChange={handleChange("family_rate")}
              />
            </div>
          </div>
          <div className="packages__individual-rates">
            <div className="packages__form-rates">
              <div className="packages__family">
                <label className="form__label">minimum age</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.min_age}
                  onChange={handleChangeRates("min_age")}
                />
              </div>

              <div className="packages__family">
                <label className="form__label">maximum age</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.max_age}
                  onChange={handleChangeRates("max_age")}
                />
              </div>

              <div className="packages__family">
                <label className="form__label">rate</label>
                <input
                  className="form__input"
                  type="number"
                  value={rate.rate}
                  onChange={handleChangeRates("rate")}
                />
              </div>
              <div className="packages__rates-button">
                <button
                  className="btn btn--blue"
                  type="button"
                  onClick={() => handleAddRate()}
                >
                  Add rate
                </button>
              </div>
            </div>
            <div className="packages__table-rates">
              <div class=" modal__table">
                <div class=" modal__table-header">
                  <div class="modal__table-cell">Min age</div>
                  <div class="modal__table-cell">Max age</div>
                  <div class="modal__table-cell">Rate</div>
                </div>
                {medicalPackage.individual_rates.map((rate, i) => (
                  <div class="modal__table-row" key={i}>
                    <div class=" modal__table-cell">{rate.min_age} years</div>
                    <div class=" modal__table-cell">{rate.max_age} years</div>
                    <div class=" modal__table-cell">{rate.rate}</div>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      {step === 4 && (
        <>
          <p>{JSON.stringify(medicalPackage)}</p>
          <div className="modal__submit-btn">
            <button
              className="btn btn--blue"
              type="button"
              onClick={handleSubmitPackage}
            >
              {loading ? (
                <div class="loading loading--full-height"></div>
              ) : (
                "Submit"
              )}
            </button>
          </div>
        </>
      )}

      {step === 5 && (
        <div className="caskets__success">
          <div className="success">
            <div className="success__container">
              <CheckSuccess />
              <div className="success__heading">
                <p>Success</p>
              </div>
              <div className="success__paragraph">
                New Package details have been submitted
              </div>
              <div className="success__options">
                <div className="success__option">
                  <button className="btn btn--blue-grey">Action 1</button>
                </div>
                <div className="success__option">
                  <button className="btn btn--blue-border">Action 2</button>
                </div>
                <div className="success__option">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={() => setStep(1)}
                  >
                    Add package
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default FormPackageMed;

import React, { useState, useEffect } from "react";
import Select from "react-select";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

const ModalSearch = ({ show, onClose, query, branches, setQuery, handleApply, handleReset}) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const handleBranch = (selected) => {
    setQuery({ ...query, branch: selected.value });
  };

  const handleStatus = (s) => {
    const currentStatus = query.status.indexOf(s);
    const newStatus = [...query.status];

    if (currentStatus === -1) {
      newStatus.push(s);
    } else {
      newStatus.splice(currentStatus, 1);
    }
    setQuery({ ...query, status: newStatus });
  };

  const handleGender = (g) => {
    const currentGender = query.gender.indexOf(g);
    const newGender = [...query.gender];

    if (currentGender === -1) {
      newGender.push(g);
    } else {
      newGender.splice(currentGender, 1);
    }
    setQuery({ ...query, gender: newGender });
  };


  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Filters</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="search__content">
          <div className="search__selectors">
            <div className="search__item">
              <p className="search__label">Branch</p>
              <Select
                defaultValue={query.branch}
                onChange={handleBranch}
                options={branches.map((branch) => ({
                  value: branch.id,
                  label: branch.name,
                }))}
              />
            </div>
          </div>
          <div className="search__status">
            <p className="search__label">Status</p>
            <div className="search__radios">
              <div className="search__radio-box">
                <input
                  className=""
                  type="radio"
                  onClick={() => handleStatus("0")}
                  checked={query.status.includes("0")}
                />
                <span>pending</span>
              </div>
              <div className="search__radio-box">
                <input
                  className=""
                  type="radio"
                  onClick={() => handleStatus("1")}
                  checked={query.status.includes("1")}
                />
                <span>active</span>
              </div>
              <div className="search__radio-box">
                <input
                  className=""
                  type="radio"
                  onClick={() => handleStatus("2")}
                  checked={query.status.includes("2")}
                />
                <span>deleted</span>
              </div>
            </div>
          </div>

          <div className="search__status">
            <p className="search__label">Gender</p>
            <div className="search__radios">
              <div className="search__radio-box">
                <input
                  className=""
                  type="radio"
                  onClick={() => handleGender("male")}
                  checked={query.gender.includes("male")}
                />
                <span>male</span>
              </div>
              <div className="search__radio-box">
                <input
                  className=""
                  type="radio"
                  onClick={() => handleStatus("female")}
                  checked={query.status.includes("female")}
                />
                <span>female</span>
              </div>
            </div>
          </div>
          <div className="search__status">
            <p className="search__label">Sort by</p>
            <div className="search__radios">
              <div className="search__radio-box">
                <input className="" type="radio" />
                <span>date registered</span>
              </div>
              <div className="search__radio-box">
                <input className="" type="radio" />
                <span>name</span>
              </div>
            </div>
          </div>
        </div>

        <div className="search__footer">
          <div className="search__remember">
            <input type="checkbox" checked />
            <p>Remember these filters</p>
          </div>
          <div className="search__btns">
            <button className="btn btn--blue-text" onClick={handleReset}>Reset</button>
            <button className="btn btn--blue-border">Cancel</button>
            <button className="btn btn--blue" onClick={handleApply}>Apply</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalSearch;

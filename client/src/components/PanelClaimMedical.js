import React, { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import moment from "moment";

import IconClose from "./iconClose";

import { updateMedicalClaim } from "../api/claimMedical";
import { isAuthenticated } from "../auth";

const PanelClaimMedical = ({ claim, show, onClose }) => {
  const {
    user: { _id, name, surname, email, role, company, employee_id },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const [values, setValues] = useState({
    success: false,
    error: false,
  });

  const handleClaimApprove = () => {
    const data = {
      status: 1,
      approver_name: name,
      approver_surname: surname,
      approver_employee_id: employee_id,
    };
    updateMedicalClaim(token, claim._id, data)
      .then((res) => {
        console.log("response", res.data.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  if (!show) {
    return null;
  }

  return (
    <div className="overlay">
      <CSSTransition
        in={show}
        appear={true}
        timeout={700}
        classNames="slide"
        unmountOnExit
      >
        <div className="panel" onClick={(e) => e.stopPropagation()}>
          <div className="panel__close" onClick={onClose}>
            <IconClose />
          </div>

          <h4 className="modal__name">Patient Details</h4>
          <div className="modal__content">
            <div className="modal__item">
              <p className="modal__heading">Name</p>
              <span>
                {claim.patient_name} {claim.patient_surname}
              </span>
            </div>

            <div className="modal__item">
              <p className="modal__heading">Date of Birth </p>
              <span> {moment(claim.patient_dob).format("LL")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Age </p>
              <span>{moment().diff(Date.parse(claim.patient_dob), "years")}  years</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Address</p>
              <span> {claim.patient_address}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">ID number</p>
              <span> {claim.patient_national_id}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Passport number</p>
              <span>{claim.patient_passport}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Relationship to member</p>
              <span>{claim.patient_relationship}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Contact number</p>
              <span>{claim.patient_phone}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Email</p>
              <span>{claim.patient_email}</span>
            </div>

          </div>
          <h4 className="modal__name">Claiment Details</h4>
          <div className="modal__content">
            <div className="modal__item">
              <p className="modal__heading">Name</p>
              <span>
                {claim.claiment_name} {claim.claiment_surname}
              </span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Relationship to Patient </p>
              <span>{claim.claiment_relationship}</span>
            </div>

            <div className="modal__item">
              <p className="modal__heading">Date of Birth </p>
              <span>{claim.claiment_dob}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Contact Number </p>
              <span>{claim.claiment_cell}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Email </p>
              <span>{claim.claiment_email}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Address </p>
              <span>{claim.claiment_address}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">ID number </p>
              <span>{claim.claiment_national_id}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Passport Number </p>
              <span>{claim.claiment_passport}</span>
            </div>
          </div>
          <h4 className="modal__name">Bank Details</h4>
          <div className="modal__content">
            <div className="modal__item">
              <p className="modal__heading">Account Holder</p>
              <span>{claim.bank_account_holder}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Bank name </p>
              <span>{claim.bank_name}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Account number </p>
              <span>{claim.bank_account_number}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Branch code </p>
              <span>{claim.bank_branch_code}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Account type </p>
              <span>{claim.bank_account_type}</span>
            </div>
          </div>

          {(() => {
            switch (claim.status) {
              case 0:
                return (
                  <div className="u-width-1">
                    <button
                      className="btn btn--blue"
                      onClick={handleClaimApprove}
                    >
                      Approve
                    </button>
                  </div>
                );
              case 1:
                return <span className="text-green">Approved</span>;
            }
          })()}
        </div>
      </CSSTransition>
    </div>
  );
};
export default PanelClaimMedical;

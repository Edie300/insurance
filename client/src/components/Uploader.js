import React, { useState } from "react";
import _ from "lodash" 
import ImageUploading from "react-images-uploading";
import { ReactComponent as Remove } from ".././assets/svg/x.svg";
import { ReactComponent as Update } from ".././assets/svg/edit.svg";

const Uploader = ({images, setImages, maxNumber = 2 }) => {

  
  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    console.log(imageList, addUpdateIndex);
    setImages(imageList);
  };


  return (
    <div className="App">
      <ImageUploading
        multiple
        value={images}
        onChange={onChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
          imageList,
          onImageUpload,
          onImageRemoveAll,
          onImageUpdate,
          onImageRemove,
          isDragging,
          dragProps,
        }) => (
          <div className="">
            <button
              className="uploader__dropzone"
              type="button"
              style={isDragging ? { color: "red" } : null}
              onClick={() => onImageUpload()}
              {...dragProps}
            >
              Click or Drop here
            </button>
            &nbsp;
            <button onClick={onImageRemoveAll}>Remove all images</button>
            <div className="uploader__images">
              {imageList.map((image, index) => (
                <div key={index} className="uploader__item">
                  <img src={image.data_url} alt="" />
                  <div className=" uploader__btn-wrapper">
                    <div
                      className="uploader__update"
                      type="button"
                      onClick={() => onImageUpdate(index)}
                    >
                      <Update />
                    </div>
                    <div
                      className="uploader__remove"
                      type="button"
                      onClick={() => onImageRemove(index)}
                    >
                      <Remove />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </ImageUploading>
    </div>
  );
};

export default Uploader;

import React from "react";

const Loader = ({message=""}) => {
  return (
    <div className="loader__container">
      <div class="loading loading--full-height"></div>
  <div className="loader__message">{message}</div>
    </div>
  );
};
export default Loader;

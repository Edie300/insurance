import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { getCompanyCaskets } from "../api/caskets";
import { createPackageMotor } from "../api/package-motor";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";

import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const FormPackageMotor = ({ start }) => {
  const {
    user: { name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(1);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };

  const [caskets, setCaskets] = useState([]);
  const [loading, setLoading] = useState(false);
  const [motorPackage, setMotorPackage] = useState({
    attachments: [],
    name: "",
    description: "",
    type: "",
    act_of_god: false,
    bodily_damage: false,
    cash_in_lieu: 0,
    property_damage: false,
    waiting_time: 0,
    claim_processing_time: 0,
    cash_back: false,
    cash_back_period: 0,
    road_side_assistance: false,
    towing_services: false,
    premium_main: 0,
    breakdown_fuel: 0,
    airtime: 0,
    personal_injury_protection: 0,
    period_of_cover: 0,
    full_vehicle_cover: false,
    policy_maturity: 0,
    premium: 0,
    currency: "",
  });

  // ---------------------[FUNERAL PACKAGE]------------------------------

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setMotorPackage({ ...motorPackage, [name]: event.target.value });
  };

  // handle select

  const handleCurrencySelect = (selected) => {
    setMotorPackage({ ...motorPackage, currency: selected.value });
  };

  const handleType = (selected) => {
    setMotorPackage({ ...motorPackage, type: selected.value });
  };

  const handlePaymentType = (selected) => {
    setMotorPackage({ ...motorPackage, payment_type: selected.value });
  };

  // handle checks
  const handleCheckAct = () => {
    let currentValue = motorPackage.act_of_god;
    setMotorPackage({ ...motorPackage, act_of_god: !currentValue });
  };
  const handleCheckBodily = () => {
    let currentValue = motorPackage.bodily_damage;
    setMotorPackage({ ...motorPackage, bodily_damage: !currentValue });
  };
  const handleCheckProperty = () => {
    let currentValue = motorPackage.property_damage;
    setMotorPackage({ ...motorPackage, property_damage: !currentValue });
  };
  const handleCheckCash = () => {
    let currentValue = motorPackage.cash_back;
    setMotorPackage({ ...motorPackage, cash_back: !currentValue });
  };
  const handleCheckRoad = () => {
    let currentValue = motorPackage.road_side_assistance;
    setMotorPackage({ ...motorPackage, road_side_assistance: !currentValue });
  };
  const handleCheckTowing = () => {
    let currentValue = motorPackage.towing_services;
    setMotorPackage({ ...motorPackage, towing_services: !currentValue });
  };
  const handleCheckCover = () => {
    let currentValue = motorPackage.full_vehicle_cover;
    setMotorPackage({ ...motorPackage, full_vehicle_cover: !currentValue });
  };

  // ---------------------[HANDLESUBMIT]----------------------------------------

  const handleSubmitPackage = () => {
    setLoading(true);
    createPackageMotor(token, motorPackage)
      .then((res) => {
        console.log("new package==>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  return (
    <div className=" ">
      {step === 1 && (
        <div className="">
          <div className="packages__four-columns">
            <div className="packages__">
              <label className="form__label">Name</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.name}
                onChange={handleChange("name")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">type</label>
              <Select
                defaultValue={motorPackage.type}
                onChange={handleType}
                options={[
                  { value: "thirdParty", label: "thirdParty" },
                  { value: "fullCover", label: "fullCover" },
                  { value: "fireAndTheft", label: "fireAndTheft" },
                ]}
              />
              {/* <input
                className="form__input"
                type="text"
                value={motorPackage.type}
                onChange={handleChange("type")}
              /> */}
            </div>
            <div className="packages__">
              <label className="form__label">cash in lieu</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.cash_in_lieu}
                onChange={handleChange("cash_in_lieu")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">waiting time</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.waiting_time}
                onChange={handleChange("waiting_time")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">claim processing time</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.claim_processing_time}
                onChange={handleChange("claim_processing_time")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">cash back period</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.cash_back_period}
                onChange={handleChange("cash_back_period")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">premium main</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.premium_main}
                onChange={handleChange("premium_main")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">breakdown fuel</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.breakdown_fuel}
                onChange={handleChange("breakdown_fuel")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">airtime</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.airtime}
                onChange={handleChange("airtime")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">personal injury protection</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.personal_injury_protection}
                onChange={handleChange("personal_injury_protection")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">period_of_cover</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.period_of_cover}
                onChange={handleChange("period_of_cover")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">policy maturity</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.policy_maturity}
                onChange={handleChange("policy_maturity")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">premium</label>
              <input
                className="form__input"
                type="text"
                value={motorPackage.premium}
                onChange={handleChange("premium")}
              />
            </div>
            <div className="packages__">
              <label className="form__label">Currency</label>
              <Select
                defaultValue={motorPackage.currency}
                onChange={handleCurrencySelect}
                options={[
                  { value: "ZW", label: "ZW" },
                  { value: "US", label: "US" },
                ]}
              />
            </div>
            <div className="packages__payment-type">
              <label className="form__label">payment type</label>
              <Select
                defaultValue={motorPackage.payment_type}
                onChange={handlePaymentType}
                options={[
                  { value: "monthly", label: "monthly" },
                  { value: "quarterly", label: "quarterly" },
                  { value: "yearly", label: "yearly" },
                ]}
              />
            </div>
          </div>
          <div className="packages__description">
            <label className="form__label">description</label>
            <textarea
              className="form__input"
              style={{ height: "4rem", width: "100%" }}
              type="text"
              value={motorPackage.description}
              onChange={handleChange("description")}
            />
          </div>
          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button
                  className="btn btn--blue-grey"
                  type="button"
                  onClick={() => start()}
                >
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

      {step === 2 && (
        <div className="packages__documents">
          <div className="packages__benefits">
            <div className="packages__check">
              <input
                type="checkbox"
                value={motorPackage.act_of_god}
                checked={motorPackage.act_of_god}
                onChange={handleCheckAct}
              />

              <div className="packages__info">
                <p>Act of God</p>
                <span>somthing abt act of god</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                type="checkbox"
                className=""
                type="checkbox"
                value={motorPackage.bodily_damage}
                checked={motorPackage.bodily_damage}
                onChange={handleCheckBodily}
              />

              <div className="packages__info">
                <p>bodily damage</p>
                <span>somthing abt bodily damage</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={motorPackage.cash_back}
                checked={motorPackage.cash_back}
                onChange={handleCheckCash}
              />

              <div className="packages__info">
                <p>cash back</p>
                <span>somthing abt cash back</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={motorPackage.road_side_assistance}
                checked={motorPackage.road_side_assistance}
                onChange={handleCheckRoad}
              />

              <div className="packages__info">
                <p>road side assistance</p>
                <span>somthing abt road side assistance </span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={motorPackage.towing_services}
                checked={motorPackage.towing_services}
                onChange={handleCheckTowing}
              />

              <div className="packages__info">
                <p>towing services</p>
                <span>somthing abt towing services</span>
              </div>
            </div>
            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={motorPackage.full_vehicle_cover}
                checked={motorPackage.full_vehicle_cover}
                onChange={handleCheckCover}
              />

              <div className="packages__info">
                <p>full vehicle cover</p>
                <span>somthing abt mortuary full vehicle cover</span>
              </div>
            </div>

            <div className="packages__check">
              <input
                className=""
                type="checkbox"
                value={motorPackage.property_damage}
                checked={motorPackage.property_damage}
                onChange={handleCheckProperty}
              />

              <div className="packages__info">
                <p>property damage</p>
                <span>somthing abt burial property damage</span>
              </div>
            </div>
          </div>

          <div className="packages__footer">
            <div className="packages__buttons">
              <div className="packages__back">
                <button className="btn btn--blue-grey" onClick={prevStep}>
                  Back
                </button>
              </div>
              <div className="packages__next">
                <button className="btn btn--blue" onClick={nextStep}>
                  Next
                </button>
              </div>
            </div>
          </div>
        </div>
      )}

   
      {step === 3 && (
        <>
          <div className="packages__review">
            <div className="packages__info">
              <p className="package__section">About package</p>
            </div>

            <div className="modal__submit-btn">
              <button
                className="btn btn--blue"
                type="button"
                onClick={handleSubmitPackage}
              >
                {loading ? (
                  <div class="loading loading--full-height"></div>
                ) : (
                  "Submit"
                )}
              </button>
            </div>

            <div className="packages__footer">
              <div className="packages__buttons">
                <div className="packages__back">
                  <button className="btn btn--blue-grey" onClick={prevStep}>
                    Back
                  </button>
                </div>
                <div className="packages__next"></div>
              </div>
            </div>
          </div>
        </>
      )}
      {step === 4 && (
        <div className="caskets__success">
          <div className="success">
            <div className="success__container">
              <CheckSuccess />
              <div className="success__heading">
                <p>Success</p>
              </div>
              <div className="success__paragraph">
                New Package details have been submitted
              </div>
              <div className="success__options">
                <div className="success__option">
                  <button className="btn btn--blue-grey">Action 1</button>
                </div>
                <div className="success__option">
                  <button className="btn btn--blue-border">Action 2</button>
                </div>
                <div className="success__option">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={() => start()}
                  >
                    Create package
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default FormPackageMotor;

import React, { useState, useEffect } from "react";
import moment from "moment";
import Select from "react-select";
import { createUser } from "../api/users";
import { getCompanyBranches } from "../api/companies";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { isAuthenticated } from "../auth";

import IMG from ".././assets/images/img.jpg";

const ModalCreateAccount = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // form steps
  const [step, setStep] = useState(1);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const handleChange = (name) => (event) => {
    setUser({ ...user, [name]: event.target.value });
  };

  // user details
  const [user, setUser] = useState({
    attachments: [],
    name: "",
    surname: "",
    employee_id: "",
    gender: "",
    title: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    date_of_birth: "",
    phone: "",
    address: "",
    email: "",
    department: "",
    role: "",
    branch: "",
    permissions_accounts: [],
    permissions_packages: [],
    permissions_claims: [],
    permissions_users: [],
  });
  const [loading, setLoading] = useState(false);
  const [branches, setBranches] = useState([]);
  const [tab, setTab] = useState(1);

  const init = () => {
    setLoading(true);
    getCompanyBranches(token, company.id)
      .then((res) => {
        console.log("branches", res.data);
        setBranches(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  // handle select inputs
  const handleTitle = (selected) => {
    setUser({ ...user, title: selected.value });
  };

  const handleGender = (selected) => {
    setUser({ ...user, gender: selected.value });
  };

  const handleBranchSelect = (selected) => {
    setUser({ ...user, branch: selected.value });
  };

  // handle permissions

  const handleUserPermission = (p) => () => {
    const currentPermission = user.permissions_users.indexOf(p);
    const newPermissions = [...user.permissions_users];

    if (currentPermission === -1) {
      newPermissions.push(p);
    } else {
      newPermissions.splice(currentPermission, 1);
    }
    setUser({ ...user, permissions_users: newPermissions });
  };

  const handlePackagePermission = (p) => () => {
    const currentPermission = user.permissions_packages.indexOf(p);
    const newPermissions = [...user.permissions_packages];

    if (currentPermission === -1) {
      newPermissions.push(p);
    } else {
      newPermissions.splice(currentPermission, 1);
    }
    setUser({ ...user, permissions_packages: newPermissions });
  };

  const handleAccountPermission = (p) => () => {
    const currentPermission = user.permissions_accounts.indexOf(p);
    const newPermissions = [...user.permissions_accounts];

    if (currentPermission === -1) {
      newPermissions.push(p);
    } else {
      newPermissions.splice(currentPermission, 1);
    }
    setUser({ ...user, permissions_accounts: newPermissions });
  };

  const handleClaimPermission = (p) => () => {
    const currentPermission = user.permissions_claims.indexOf(p);
    const newPermissions = [...user.permissions_claims];

    if (currentPermission === -1) {
      newPermissions.push(p);
    } else {
      newPermissions.splice(currentPermission, 1);
    }
    setUser({ ...user, permissions_claims: newPermissions });
  };

  const submit = (event) => {
    setLoading(true);
    event.preventDefault();
    console.log("user", user);
    createUser(user, token)
      .then((res) => {
        console.log("response===>", res.data);
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  const Review = () => <div className="modal__review"></div>;

  const Submit = () => (
    <div className="">
      <CheckSuccess />
    </div>
  );
  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>Create User</h2>
          </div>
          <div onClick={onClose} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="accounts__modal-container">
          <form className="">
            {step === 1 && (
              <div className="">
                <div className="users__details">
                  <div className="users__name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="users__surname">
                    <label className="form__label">Surname</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.surname}
                      onChange={handleChange("surname")}
                    />
                  </div>
                  <div className="users__gender">
                    <label className="form__label">Gender</label>
                    <Select
                      defaultValue={user.gender}
                      onChange={handleGender}
                      options={[
                        { value: "male", label: "Male" },
                        { value: "female", label: "Female" },
                      ]}
                    />
                  </div>
                  <div className="users__dob">
                    <label className="form__label">Date of birth</label>
                    <input
                      className="form__input"
                      type="date"
                      value={user.date_of_birth}
                      onChange={handleChange("date_of_birth")}
                    />
                  </div>
                  <div className="users__title">
                    <label className="form__label">Title</label>
                    <Select
                      defaultValue={user.title}
                      onChange={handleTitle}
                      options={[
                        { value: "mr", label: "Mr" },
                        { value: "mrs", label: "Mrs" },
                        { value: "miss", label: "Miss" },
                        { value: "dr", label: "Dr" },
                        { value: "prof", label: "Prof" },
                      ]}
                    />
                  </div>
                  <div className="users__number">
                    <label className="form__label">Employe number</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.employee_id}
                      onChange={handleChange("employee_id")}
                    />
                  </div>
                  <div className="users__national">
                    <label className="form__label">National ID</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.national_id}
                      onChange={handleChange("national_id")}
                    />
                  </div>
                  <div className="users__passport">
                    <label className="form__label">Passport</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.passport}
                      onChange={handleChange("passport")}
                    />
                  </div>
                  <div className="users__licence">
                    <label className="form__label">Drivers licence</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.drivers_licence}
                      onChange={handleChange("drivers_licence")}
                    />
                  </div>
                  <div className="users__phone">
                    <label className="form__label">Phone</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.phone}
                      onChange={handleChange("phone")}
                    />
                  </div>
                  <div className="users__email">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.email}
                      onChange={handleChange("email")}
                    />
                  </div>
                  <div className="users__address">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      value={user.address}
                      onChange={handleChange("address")}
                    />
                  </div>
                  <div className="users__branch">
                    <label className="form__label">Branch</label>
                    <Select
                      defaultValue={user.branch}
                      onChange={handleBranchSelect}
                      options={branches.map((branch) => ({
                        value: branch.id,
                        label: branch.name,
                      }))}
                    />
                  </div>
                </div>

                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                     
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 2 && (
              <div className="modal__documents">
                <label className="form__label">Attachments</label>
                <div className="">
                  <Upload />
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                       className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 3 && (
              <div className="users__permissions">
                <div className="users__perm-grid">
                  <p className="users__permission">Users</p>
                  <div className="users__perm-box">
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleUserPermission("create")}
                        checked={user.permissions_users.includes("create")}
                      />
                      <div className="users__info">
                        <p>Create</p>
                        <span>register new system users</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleUserPermission("edit")}
                        checked={user.permissions_users.includes("edit")}
                      />
                      <div className="users__info">
                        <p>Edit</p>
                        <span>edit system users</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleUserPermission("delete")}
                        checked={user.permissions_users.includes("delete")}
                      />

                      <div className="users__info">
                        <p>Delete</p>
                        <span>remove system users</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleUserPermission("suspend")}
                        checked={user.permissions_users.includes("suspend")}
                      />
                      <div className="users__info">
                        <p>suspend</p>
                        <span>suspend user</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="users__perm-grid">
                  <p className="users__permission">Packages</p>
                  <div className="users__perm-box">
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handlePackagePermission("create")}
                        checked={user.permissions_packages.includes("create")}
                      />
                      <div className="users__info">
                        <p>Create</p>
                        <span>add new packages</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handlePackagePermission("edit")}
                        checked={user.permissions_packages.includes("edit")}
                      />
                      <div className="users__info">
                        <p>Edit</p>
                        <span>add new packages</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handlePackagePermission("delete")}
                        checked={user.permissions_packages.includes("delete")}
                      />
                      <div className="users__info">
                        <p>Delete</p>
                        <span>remove packages from system packages</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handlePackagePermission("publish")}
                        checked={user.permissions_packages.includes("publish")}
                      />
                      <div className="users__info">
                        <p>publish</p>
                        <span>publish packages</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="users__perm-grid">
                  <p className="users__permission">Accounts</p>
                  <div className="users__perm-box">
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleAccountPermission("create")}
                        checked={user.permissions_accounts.includes("create")}
                      />
                      <div className="users__info">
                        <p>Create</p>
                        <span>create customer accounts</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleAccountPermission("edit")}
                        checked={user.permissions_accounts.includes("edit")}
                      />
                      <div className="users__info">
                        <p>Edit</p>
                        <span>edit customer accounts</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleAccountPermission("delete")}
                        checked={user.permissions_accounts.includes("delete")}
                      />

                      <div className="users__info">
                        <p>Delete</p>
                        <span>edit customer accounts</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleAccountPermission("archive")}
                        checked={user.permissions_accounts.includes("archive")}
                      />
                      <div className="users__info">
                        <p>Archive</p>
                        <span>edit customer accounts</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="users__perm-grid">
                  <p className="users__permission">Claims</p>
                  <div className="users__perm-box">
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleClaimPermission("create")}
                        checked={user.permissions_claims.includes("create")}
                      />

                      <div className="users__info">
                        <p>Create</p>
                        <span>create claims</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleClaimPermission("edit")}
                        checked={user.permissions_claims.includes("edit")}
                      />

                      <div className="users__info">
                        <p>Edit</p>
                        <span>edit account claims</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleClaimPermission("delete")}
                        checked={user.permissions_claims.includes("delete")}
                      />

                      <div className="users__info">
                        <p>Delete</p>
                        <span>delete account claims</span>
                      </div>
                    </div>
                    <div className="users__check">
                      <input
                        type="checkbox"
                        onClick={handleClaimPermission("approve")}
                        checked={user.permissions_claims.includes("approve")}
                      />
                      <div className="users__info">
                        <p>Approve</p>
                        <span>delete account claims</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {step === 4 && (
              <div className="">
                <p>Review details</p>
                {/* <div className="modal__">
                  <div className="account__nav">
                    <ul className="account__nav-list">
                      <p
                        className={
                          tab === 1
                            ? "account__nav-item--active"
                            : "account__nav-item"
                        }
                        onClick={() => setTab(1)}
                      >
                        User
                      </p>
                      <p
                        className={
                          tab === 2
                            ? "account__nav-item--active"
                            : "account__nav-item"
                        }
                        onClick={() => setTab(2)}
                      >
                        Attachments
                      </p>
                      <p
                        className={
                          tab === 3
                            ? "account__nav-item--active"
                            : "account__nav-item"
                        }
                        onClick={() => setTab(3)}
                      >
                        Permissions
                      </p>
                    </ul>
                  </div>
                  <div className="">
                    {tab === 1 && (
                      <div className="users__container">
                        <div class=" account__policy">
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                          <div className="account__item">
                            <p className="">Name</p>
                            <span>fasdfads</span>
                          </div>
                        </div>
                      </div>
                    )}
                    {tab === 2 && (
                      <div className="users__container">
                        <div class=" account__policy">
                          <div className="users__file">
                            <img src={IMG} alt="image" />
                          </div>
                          <div className="users__file">
                            <img src={IMG} alt="image" />
                          </div>
                          <div className="users__file">
                            <img src={IMG} alt="image" />
                          </div>
                          <div className="users__file">
                            <img src={IMG} alt="image" />
                          </div>
                        </div>
                      </div>
                    )}
                    {tab === 3 && (
                      <div className="users__container">ambulance</div>
                    )}
                  </div>
                </div> */}
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={submit}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                         className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next"></div>
                  </div>
                </div>
              </div>
            )}
            {step === 5 &&         <div className="caskets__success">
          <div className="success">
            <div className="success__container">
              <CheckSuccess />
              <div className="success__heading">
                <p>Success</p>
              </div>
              <div className="success__paragraph">
                New Package details have been submitted
              </div>
              <div className="success__options">
                <div className="success__option">
                  <button className="btn btn--blue-grey">Action 1</button>
                </div>
                <div className="success__option">
                  <button className="btn btn--blue-border">Action 2</button>
                </div>
                <div className="success__option">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={() => setStep(1)}
                  >
                    Create User
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

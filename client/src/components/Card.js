import React from "react";
import { Link } from "react-router-dom";

const Card = ({
  title = "Account balance",
  value = 1234567,
  description = "+1.2% for the last 5 days",
  link="/"
}) => {
  return (
    <div className="card ">
      <div className="card__title-box">
        <p className="card__title">{title} </p>
      </div>
      <div className="card__value-box">
        <h3 className="card__value">
          {value}
        </h3>
      </div>
      <div className="card__footer">
        <Link to={link} className="card__link">View</Link>
      </div>
    </div>
  );
};
export default Card;

import React from "react";
import Avatar from "../assets/images/avatar_00.jpg";

const UserAvatar = () => {
  return (
    <div className="header">
      <img src={Avatar} />
    </div>
  );
};
export default UserAvatar;

import React, { useEffect, useState } from "react";
import moment from "moment";

import IconClose from "./iconClose";
import IconOption from "./iconOption";

const ModalFunDependents = ({ dependents, show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }

  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__close">
          <h4 className="modal__name">Dependents</h4>
          <div onClick={onClose} className="modal__close-icon">
            <IconClose />
          </div>
        </div>
        <div className="">

          <div className="">
            <table className="table no-wrap">
              <thead className="table__header-text">
                <tr className="">
                  <th>Name</th>
                  <th>Surname</th>
                  <th>Relationship</th>
                  <th>Date of Birth</th>
                  <th>Gender</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody className="table__body-text">
                {dependents.map((dp, i) => (
                  <tr className="" key={i} data-id={dp._id}>
                    <td>{dp.name}</td>
                    <td>{dp.surname}</td>
                    <td>{dp.relationship}</td>
                    <td>{moment(dp.dob).format("LL")}</td>
                    <td>{dp.gender}</td>
                    <td className="text-green">active</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ModalFunDependents;

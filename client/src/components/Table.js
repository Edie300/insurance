import React from "react";
import { Link } from "react-router-dom";
import IconOption from "./iconOption"

const Table = ({ onShow}) => {
  return (
    <div class="">
      <table class="table no-wrap">
        <thead className="table__header-text">
          <tr className="">
            <th>Account</th>
            <th>Policy Holder</th>
            <th>Package</th>
            <th>Type</th>
            <th>Fee</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <p  className="" onClick={onShow}>
                EM12345667
              </p>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
            <IconOption/>
            </td>
          </tr>
          <tr className="">
            <td>
              <Link to={"/"} className="">
                EM12345667
              </Link>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
            <IconOption/>
            </td>
          </tr>
          <tr className="">
            <td>
              <Link to={"/"} className="">
                EM12345667
              </Link>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
            <IconOption/>
            </td>
          </tr>
          <tr className="">
            <td>
              <Link to={"/"} className="">
                EM12345667
              </Link>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
            <IconOption/>
            </td>
          </tr>
          <tr className="">
            <td>
              <Link to={"/"} className="">
                EM12345667
              </Link>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
            <IconOption/>
            </td>
          </tr>
          <tr className="">
            <td>
              <Link to={"/"} className="">
                EM12345667
              </Link>
            </td>
            <td>Edwin Muvandi</td>
            <td>Family cover </td>
            <td>Funeral</td>
            <td>$1200</td>
            <td>
              <IconOption/>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default Table;

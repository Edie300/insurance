import React from "react";

const Invoice = () => {
  return (
    <div class="col-lg-12 mt">
      <div class="row content-panel shadow1 m-l-20 m-t-20 m-r-20 m-b-20 p-l-40 p-t-40">
        <div class="col-lg-10 col-lg-offset-1">
          <div class="invoice-body">
            <div class="pull-left">
              <address>
                <strong>Doves pvt ltd</strong>
                <br />
                1234 Ave
                <br />
                Harare
                <br />
                Zimbabwe
                <br />
                <abbr title="Phone">P:</abbr> (+263) 456-7890
              </address>
            </div>

            <div class="pull-right">
              <h2>Invoice</h2>
            </div>

            <div class="clearfix"></div>
            <br />
            <br />
            <br />
            <div class="row">
              <div class="col-md-9">
                <h4>Edwin Muvandi</h4>
                <address>
                  12345 Nkulumane
                  <br />
                  Bulawayo
                  <br />
                  Zimbabwe
                  <br />
                  <abbr title="Phone">P:</abbr> (+263) 777 123 456
                </address>
              </div>

              <div class="col-md-3">
                <br />
                <div>
                  <div class="pull-left"> CASHIER : </div>
                  <div class="pull-right"> Thomas Sithole </div>
                  <div class="clearfix"></div>
                </div>
                <div>
                  <div class="pull-left"> INVOICE NO : </div>
                  <div class="pull-right"> 000283 </div>
                  <div class="clearfix"></div>
                </div>
                <div>
                  <div class="pull-left"> INVOICE DATE : </div>
                  <div class="pull-right"> 15/03/20 </div>
                  <div class="clearfix"></div>
                </div>

                <br />
                <div class="well well-small blue">
                  <div class="pull-left"> Total Due : </div>
                  <div class="pull-right"> 8,000 ZW </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>

            <table class="table">
              <thead>
                <tr>
                  <th  style={{width: '60px'}} class="text-center">
                    ACCOUNT HOLDER
                  </th>
                  <th class="text-left">POLICY NAME</th>
                  <th  style={{width: '140px'}} class="text-right">
                    ACCOUNT NUMBER
                  </th>
                  <th  style={{width: '90px'}} class="text-right">
                    AMOUNT
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">Edwin Muvandi</td>
                  <td>Family cover</td>
                  <td class="text-right">EM123456789</td>
                  <td class="text-right">$429.00</td>
                </tr>
                <tr>
                  <td colspan="2" rowspan="4">
                    <h4>Terms and Conditions</h4>
                    <p>
                      Thank you for your business. Please pay your mothly
                      payments in time to avoid your account being suspended.
                    </p>
                  </td>
                
                </tr>
                <tr>
                  <td class="text-right no-border">
                    <strong>VAT Included in Total</strong>
                  </td>
                  <td class="text-right">$0.00</td>
                </tr>
                <tr>
                  <td class="text-right no-border">
                    <div class="well well-small blue">
                      <strong>Total</strong>
                    </div>
                  </td>
                  <td class="text-right">
                    <strong>$429.00</strong>
                  </td>
                </tr>
              </tbody>
            </table>
            <br />
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Invoice;

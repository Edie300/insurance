import React, { useState, useEffect } from "react";
import moment from "moment";
import Select from "./Select";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Folder } from ".././assets/svg/folder.svg";

const ModalEmail = ({ show, onClose }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  // account data
  const [values, setValues] = useState({
    name: "",
    surname: "",
    email: "",
  });

  // Documents Array
  const [documents, setDocuments] = useState([]);

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setValues({ ...values, [name]: event.target.value });
  };

  const Submit = () => (
    <div className="">
      <CheckSuccess />
    </div>
  );
  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__heading">
          <div className="modal__information">
            <h2>New Email</h2>
            <p>Fill details required to send mail</p>
          </div>

          <div onClick={onClose} className="modal__close">
            <IconClose />
          </div>
        </div>
        <div className="modal__container">
          <form className="">
            <>
              <div className="modal__client">
                <div className="modal__client-name">
                  <label className="form__label">Name</label>
                  <input className="form__input" type="text" />
                </div>
                <div className="modal__surname">
                  <label className="form__label">Surname</label>
                  <input className="form__input" type="text" />
                </div>
              </div>
            </>
          </form>
        </div>
        <div className="modal__footer">
          <div className="modal__buttons">
            <div className="modal__back">
              <button className="btn btn--blue-border">Cancel</button>
            </div>
            <div className="modal__next">
              <button className="btn btn--blue">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalEmail;

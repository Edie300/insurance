import React, { useState, useEffect } from "react";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import Select from "react-select";
import { isAuthenticated } from "../auth";
import { createMainAccount } from "../api/account-main";
import { createFunAccount } from "../api/account-funeral";
import { getPackagesFun } from "../api/package-funeral";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Funeral } from ".././assets/svg/Totum_Icon_Funeral.svg";
import { ReactComponent as Health } from ".././assets/svg/Totum_Icon_Health.svg";
import { ReactComponent as Motor } from ".././assets/svg/Totum_Icon_Motor.svg";
import { ReactComponent as Life } from ".././assets/svg/Totum_Icon_Life.svg";
import { ReactComponent as Home } from ".././assets/svg/Totum_Icon_Home.svg";
import { ReactComponent as Travel } from ".././assets/svg/Totum_Icon_Travel.svg";
import { ReactComponent as Appliance } from ".././assets/svg/Totum_Icon_Appliance.svg";
import { ReactComponent as Pet } from ".././assets/svg/Totum_Icon_Pet.svg";
import { ReactComponent as Cyber } from ".././assets/svg/Totum_Icon_Cyber.svg";
import { ReactComponent as Pension } from ".././assets/svg/Totum_Icon_Pension.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";

const ModalCreateAccount = ({ show, onClose, start = 0, mainAccount = "" }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
      setStep(0);
    }
  };
  const closeModal = () => {
    onClose();
    setStep(0);
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  // set insurance type states
  const [category, setCategory] = useState(company.insurance);

  useEffect(() => {}, []);

  // form steps
  const [step, setStep] = useState(start);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  const resetStep = () => {
    setStep(0);
  };
  const [loading, setLoading] = useState(false);

  // Handle insurance type check
  const [insurance, setInsurance] = useState();

  const handleToggle = (t) => () => {
    const isInsurance = insurance === t;

    if (isInsurance) {
      setInsurance();
    } else {
      setInsurance(t);
    }
  };

  //----------------------------- MAIN ACCOUNT------------------------------------
  // account holder details
  const [account, setAccount] = useState({
    attachments: ["docs"],
    name: "",
    surname: "",
    gender: "",
    date_of_birth: "",
    marital_status: "",
    title: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    email: "",
    phone: "",
    address: "",
    payment_status: "",
  });

  // main account creating response
  const [main, setMain] = useState({});
  const handleChange = (name) => (event) => {
    event.preventDefault();
    setAccount({ ...account, [name]: event.target.value });
  };

  // Handle select options

  const handleAccountTitle = (selected) => {
    setAccount({ ...account, title: selected.value });
  };

  const handleAccountMarital = (selected) => {
    setAccount({ ...account, marital_status: selected.value });
  };

  const handleAccountGender = (selected) => {
    setAccount({ ...account, gender: selected.value });
  };

  const submitMainAccount = (event) => {
    console.log("submit clicked", account);
    setLoading(true);

    event.preventDefault();
    console.log("account", account);
    createMainAccount(token, account)
      .then((res) => {
        console.log("response===>", res.data);
        setMain(res.data);
        setFuneral({ ...funeral, account_main: res.data.id });
        setLoading(false);
        nextStep();
      })
      .catch((error) => {
        console.log("Error===>", error);
        setLoading(false);
      });
  };

  // ----------------------- FUNERAL ACCOUNT-----------------------------
  const [funeral, setFuneral] = useState({
    attachments: [],
    package_fun: "",
    account_main: mainAccount,
    dependents: [],
  });

  const [funPackages, setFunPackages] = useState([]);
  const funeralPackages = () => {
    getPackagesFun(token)
      .then((res) => {
        console.log("fun-packages===>", res.data);
        setFunPackages(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    funeralPackages();
  }, []);

  // handle check packages
  const [checked, setChecked] = useState("");
  const handlePackageCheck = (event) => {
    let checkedPackage = checked;
    if (event.target.checked) {
      checkedPackage = event.target.value;
    } else {
      checkedPackage = "";
    }
    setFuneral({ ...funeral, package_fun: checkedPackage });
  };

  // Insurance package
  const [packageFun, setPackageFun] = useState({
    package_fun: " ",
  });
  const [packageMee, setPackageMee] = useState({
    package_med: " ",
  });
  const [packageMotor, setPackageMotor] = useState({
    package_motor: " ",
  });

  // account dependents
  const [dep, setDep] = useState({
    name: "",
    surname: "",
    gender: "",
    date_of_birth: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    address: "  ",
    marital_status: "",
    title: "",
    phone: "",
    email: "",
    occupation: "",
    relationship: "",
  });
  const [dependents, setDependents] = useState([]);

  // account vehicles
  const [vehicles, setVehicles] = useState([]);

  const [title, setTitle] = useState("");

  // Documents Array
  const [attachments, setAttachments] = useState([]);

  const handleChangeInput = (id, event) => {
    const newInputFields = dependents.map((i) => {
      if (id === i.id) {
        i[event.target.name] = event.target.value;
      }
      return i;
    });

    setDependents(newInputFields);
  };

  const handleChangeDep = (name) => (event) => {
    event.preventDefault();
    setDep({ ...dep, [name]: event.target.value });
  };

  // Handle dependent select options

  const handleDepTitle = (selected) => {
    setDep({ ...dep, title: selected.value });
  };

  const handleDepGender = (selected) => {
    setDep({ ...dep, gender: selected.value });
  };

  const handleDepMaritul = (selected) => {
    setDep({ ...dep, marital_status: selected.value });
  };

  const handleDepRelationship = (selected) => {
    setDep({ ...dep, relationship: selected.value });
  };

  const handleDepOccupation = (selected) => {
    setDep({ ...dep, occupation: selected.value });
  };

  const handleAddDependent = () => {
    // event.preventDefault();
    const currentDeps = [...funeral.dependents];
    currentDeps.push(dep);
    setFuneral({ ...funeral, dependents: currentDeps });

    console.log("dependents", funeral.dependents);
    setDep({
      name: "",
      surname: "",
      gender: "",
      date_of_birth: "",
      national_id: "",
      passport: "",
      drivers_licence: "",
      address: "  ",
      marital_status: "",
      title: "",
      phone: "",
      email: "",
      occupation: "",
      relationship: "",
    });
  };

  const handleRemoveDependent = (index) => {
    const currentDeps = [...funeral.dependents];
    currentDeps.splice(index, 1);
    setFuneral({ ...funeral, dependents: currentDeps });
  };

  // --------------------------ADD SUB ACCOUNTS------------------------------

  const handleAddSubAccount = () => {
    setLoading(true);

    switch (insurance) {
      case 0:
        console.log("funeral==>", funeral);
        createFunAccount(token, funeral)
          .then((res) => {
            console.log("response===>", res.data);
            setLoading(false);
            nextStep();
          })
          .catch((error) => {
            console.log("Error===>", error);
            setLoading(false);
          });
      case 1:
    }
  };

  const SelectType = () => (
    <>
      <p className="modal__step">Insurance type</p>
      <div className="modal__select">
        {category.includes(0) && (
          <div
            className={
              insurance === 0 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(0)}
          >
            <div>
              <Funeral />
            </div>
            <p className="">Funeral</p>
          </div>
        )}

        {category.includes(1) && (
          <div
            className={
              insurance === 1 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(1)}
          >
            <div>
              <Health />
            </div>
            <p>Medical</p>
          </div>
        )}

        {category.includes(2) && (
          <div
            className={
              insurance === 2 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(2)}
          >
            <div>
              <Motor />
            </div>
            <p>Motor</p>
          </div>
        )}
        {category.includes(3) && (
          <div
            className={
              insurance === 3 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(3)}
          >
            <div>
              <Life />
            </div>
            <p>Life</p>
          </div>
        )}
        {category.includes(4) && (
          <div
            className={
              insurance === 4 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(4)}
          >
            <div>
              <Home />
            </div>
            <p>Home</p>
          </div>
        )}
        {category.includes(5) && (
          <div
            className={
              insurance === 5 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(5)}
          >
            <div>
              <Appliance />
            </div>
            <p className="">Appliance</p>
          </div>
        )}
        {category.includes(6) && (
          <div
            className={
              insurance === 6 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(6)}
          >
            <div>
              <Travel />
            </div>
            <p className="">Travel</p>
          </div>
        )}
        {category.includes(7) && (
          <div
            className={
              insurance === 7 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(7)}
          >
            <div>
              <Pet />
            </div>
            <p className="">Pet</p>
          </div>
        )}

        {category.includes(8) && (
          <div
            className={
              insurance === 8 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(8)}
          >
            <div>
              <Cyber />
            </div>
            <p className="">Cyber</p>
          </div>
        )}
        {category.includes(9) && (
          <div
            className={
              insurance === 9 ? "packages__option-checked" : "packages__option"
            }
            onClick={handleToggle(9)}
          >
            <div>
              <Pension />
            </div>
            <p className="">Pension</p>
          </div>
        )}
      </div>
      <div className="modal__footer">
        <div className="modal__buttons">
          <div className="modal__next">
            <button className="btn btn--blue-border" onClick={nextStep}>
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={closeModal}>
      <div className="accounts__modal" onClick={(e) => e.stopPropagation()}>
        <div className="accounts__modal-header">
          <div className="accounts__modal-title">
            <h2>New account</h2>
          </div>
          <div onClick={closeModal} className="accounts__close">
            <IconClose />
          </div>
        </div>
        <div className="accounts__modal-container">
          <form className="">
            {step === 0 && (
              <>
                <p className="modal__step">Account information</p>
                <div className="modal__client">
                  <div className="modal__client-name">
                    <label className="form__label">Name</label>
                    <input
                      className="form__input"
                      type="text"
                      value={account.name}
                      onChange={handleChange("name")}
                    />
                  </div>
                  <div className="modal__surname">
                    <label className="form__label">Surname</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("surname")}
                      value={account.surname}
                    />
                  </div>
                  <div className="modal__gender">
                    <label className="form__label">Gender</label>
                    <Select
                      defaultValue={account.gender}
                      onChange={handleAccountGender}
                      options={[
                        { value: "male", label: "Male" },
                        { value: "female", label: "Female" },
                      ]}
                    />
                  </div>
                  <div className="modal__client-title">
                    <label className="form__label">
                      Title <span>{title}</span>
                    </label>
                    <Select
                      defaultValue={account.title}
                      onChange={handleAccountTitle}
                      options={[
                        { value: "mr", label: "Mr" },
                        { value: "mrs", label: "Mrs" },
                        { value: "miss", label: "Miss" },
                        { value: "dr", label: "Dr" },
                        { value: "prof", label: "Prof" },
                      ]}
                    />
                  </div>
                  <div className="modal__dob">
                    <label className="form__label">Date of birth</label>
                    <input
                      className="form__input"
                      type="date"
                      onChange={handleChange("date_of_birth")}
                      value={account.date_of_birth}
                    />
                  </div>
                  <div className="modal__id">
                    <label className="form__label">ID number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("national_id")}
                      value={account.national_id}
                    />
                  </div>
                  <div className="modal__passport">
                    <label className="form__label">Passport number</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("passport")}
                      value={account.passport}
                    />
                  </div>
                  <div className="modal__drivers">
                    <label className="form__label">
                      Drivers license number
                    </label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("drivers_licence")}
                      value={account.drivers_licence}
                    />
                  </div>
                  <div className="modal__marital">
                    <label className="form__label">Marital status</label>
                    <Select
                      defaultValue={account.marital_status}
                      onChange={handleAccountMarital}
                      options={[
                        { value: "single", label: "Single" },
                        { value: "married", label: "Married" },
                        { value: "divorced", label: "Divorced" },
                      ]}
                    />
                  </div>
                  <div className="modal__phone">
                    <label className="form__label">Phone</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("phone")}
                      value={account.phone}
                    />
                  </div>
                  <div className="modal__email">
                    <label className="form__label">Email</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("email")}
                      value={account.email}
                    />
                  </div>
                  <div className="modal__address">
                    <label className="form__label">Address</label>
                    <input
                      className="form__input"
                      type="text"
                      onChange={handleChange("address")}
                      value={account.address}
                    />
                  </div>
                </div>

                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={submitMainAccount}
                    disable={loading}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Create account"
                    )}
                  </button>
                </div>
              </>
            )}
            {step === 1 && <SelectType />}

            {step === 2 && (
              <div className="">
                <p className="modal__step">Funeral packages</p>
                <div class=" modal__table">
                  <div class=" modal__table-header">
                    <div class="modal__table-cell--check"></div>
                    <div class="modal__table-cell">Name</div>
                    <div class="modal__table-cell">Casket</div>
                    <div class="modal__table-cell">Waiting Period</div>
                    <div class="modal__table-cell">Groceries</div>
                    <div class="modal__table-cell">Sum Assured</div>
                    <div class="modal__table-cell">Cash In Lieu</div>
                    <div class="modal__table-cell">Currency</div>
                  </div>

                  {funPackages.map((funPkg, i) => (
                    <div class="modal__table-row" key={i}>
                      <div class=" modal__table-cell--check">
                        <input
                          checked={funeral.package_fun === funPkg.id}
                          type="checkbox"
                          value={funPkg.id}
                          onClick={handlePackageCheck}
                        />
                      </div>
                      <div class=" modal__table-cell">
                        <p className="accounts__table-name">{funPkg.name}</p>
                      </div>
                      <div class=" modal__table-cell">casket name</div>
                      <div class=" modal__table-cell">
                        {funPkg.waiting_period} months
                      </div>
                      <div class=" modal__table-cell">{funPkg.groceries}</div>
                      <div class=" modal__table-cell">{funPkg.sum_assured}</div>
                      <div class=" modal__table-cell">
                        {funPkg.cash_in_lieu}
                      </div>
                      <div class=" modal__table-cell">{funPkg.currency}</div>
                    </div>
                  ))}
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                    <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next">
                      <button
                        className="btn btn--blue-border"
                        onClick={nextStep}
                      >
                        Next
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {step === 3 && (
              <>
                <p className="modal__step">Dependents</p>
                <div className="modal__dependents">
                  <div className="modal__dependents-add">
                    <div className="modal__dependents-form">
                      <div className="modal__dpt-name">
                        <label className="form__label">Name</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.name}
                          onChange={handleChangeDep("name")}
                        />
                      </div>
                      <div className="modal__dpt-surname">
                        <label className="form__label">Surname</label>
                        <input
                          className="form__input"
                          type="text"
                          onChange={handleChangeDep("surname")}
                          value={dep.surname}
                        />
                      </div>
                      <div className="modal__dpt-gender">
                        <label className="form__label">Gender</label>
                        <Select
                          defaultValue={dep.gender}
                          onChange={handleDepGender}
                          options={[
                            { value: "male", label: "Male" },
                            { value: "female", label: "Female" },
                          ]}
                        />
                      </div>
                      <div className="modal__dpt-title">
                        <label className="form__label">Title</label>
                        <Select
                          defaultValue={dep.gender}
                          onChange={handleDepTitle}
                          options={[
                            { value: "mr", label: "Mr" },
                            { value: "mrs", label: "Mrs" },
                            { value: "miss", label: "Miss" },
                            { value: "dr", label: "Dr" },
                            { value: "prof", label: "Prof" },
                          ]}
                        />
                      </div>
                      <div className="modal__dpt-dob">
                        <label className="form__label">Date of birth</label>
                        <input
                          className="form__input"
                          type="date"
                          value={dep.date_of_birth}
                          onChange={handleChangeDep("date_of_birth")}
                        />
                      </div>
                      <div className="modal__dpt-id">
                        <label className="form__label">ID number</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.national_id}
                          onChange={handleChangeDep("national_id")}
                        />
                      </div>
                      <div className="modal__dpt-passport">
                        <label className="form__label">Passport number</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.passport}
                          onChange={handleChangeDep("passport")}
                        />
                      </div>
                      <div className="modal__dpt-drivers">
                        <label className="form__label">
                          Drivers license number
                        </label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.drivers_licence}
                          onChange={handleChangeDep("drivers_licence")}
                        />
                      </div>
                      <div className="modal__dpt-marital">
                        <label className="form__label">Maritul status</label>
                        <Select
                          defaultValue={dep.marital_status}
                          onChange={handleDepMaritul}
                          options={[
                            { value: "single", label: "Single" },
                            { value: "married", label: "Married" },
                            { value: "divorced", label: "Divorced" },
                          ]}
                        />
                      </div>
                      <div className="modal__dpt-phone">
                        <label className="form__label">Phone</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.phone}
                          onChange={handleChangeDep("phone")}
                        />
                      </div>
                      <div className="modal__dpt-email">
                        <label className="form__label">Email</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.email}
                          onChange={handleChangeDep("email")}
                        />
                      </div>
                      <div className="modal__dpt-address">
                        <label className="form__label">Address</label>
                        <input
                          className="form__input"
                          type="text"
                          value={dep.address}
                          onChange={handleChangeDep("address")}
                        />
                      </div>
                      <div className="modal__dpt-relationship">
                        <label className="form__label">Relationship</label>
                        <Select
                          defaultValue={dep.relationship}
                          onChange={handleDepRelationship}
                          options={[
                            { value: "child", label: "child" },
                            { value: "spouse", label: "spouse" },
                            { value: "parent", label: "parent" },
                            { value: "uncle", label: "uncle" },
                            { value: "nephew", label: "nephew" },
                          ]}
                        />
                      </div>
                      <div className="modal__occupation">
                        <label className="form__label">Occupation</label>
                        <Select
                          defaultValue={dep.occupation}
                          onChange={handleDepOccupation}
                          options={[
                            { value: "student", label: "student" },
                            { value: "employed", label: "employed" },
                            { value: "unemployed", label: "unemployed" },
                          ]}
                        />
                      </div>
                    </div>
                    <div className="modal__dependents-btn">
                      <button
                        className="btn btn--green"
                        type="button"
                        onClick={() => handleAddDependent()}
                      >
                        Add dependent
                      </button>
                    </div>
                  </div>
                  <div className="modal__dependents-list">
                    {funeral.dependents.length === 0 && (
                      <div className="modal__no-dependents">
                        <p>No dependents</p>
                        <span>fill form to add policy dependents</span>
                      </div>
                    )}
                    {funeral.dependents.length > 0 &&
                      funeral.dependents.map((dependent, i) => (
                        <div className="modal__dpt-card" key={i}>
                          <div>
                            <p>Name</p>
                            <span>{dependent.name}</span>
                          </div>
                          <div>
                            <p>Surname</p>
                            <span>{dependent.surname}</span>
                          </div>
                          <div>
                            <p>Gender</p>
                            <span>{dependent.gender}</span>
                          </div>
                          <div>
                            <p>Date of Birth</p>
                            <span>{dependent.date_of_birth}</span>
                          </div>
                          <div>
                            <p>Relationship</p>
                            <span>{dependent.relationship}</span>
                          </div>
                          <div className="modal__dept-options">
                            <p className="modal__dept-more">More</p>
                            <p
                              className="modal__dept-remove"
                              onClick={() => handleRemoveDependent(i)}
                            >
                              Remove
                            </p>
                          </div>
                        </div>
                      ))}
                  </div>
                  <div className="modal__footer">
                    <div className="modal__buttons">
                      <div className="modal__back">
                      <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                      </div>
                      <div className="modal__next">
                        <button
                          className="btn btn--blue-border"
                          onClick={nextStep}
                        >
                          Next
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}

            {step === 4 && (
              <>
                <p className="modal__step">Attachments</p>
                <div className="modal__documents">
                  <div className="">
                    <Upload />
                  </div>
                  <div className="modal__footer">
                    <div className="modal__buttons">
                      <div className="modal__back">
                      <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                      </div>
                      <div className="modal__next">
                        <button
                          className="btn btn--blue-border"
                          onClick={nextStep}
                        >
                          Next
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
            {step === 5 && (
              <>
                <p className="modal__step">Review details</p>
                <div className="modal__review">
               
                </div>
                <div className="modal__submit-btn">
                  <button
                    className="btn btn--blue"
                    type="button"
                    onClick={handleAddSubAccount}
                  >
                    {loading ? (
                      <div class="loading loading--full-height"></div>
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
                <div className="modal__footer">
                  <div className="modal__buttons">
                    <div className="modal__back">
                      <button
                        className="btn btn--blue-grey"
                        onClick={prevStep}
                      >
                        Back
                      </button>
                    </div>
                    <div className="modal__next"></div>
                  </div>
                </div>
              </>
            )}
            {step === 6 && (
              <div className="">
                 <div className="success">
                  <div className="success__container">
                    <CheckSuccess />
                    <div className="success__heading">
                      <p>Success</p>
                    </div>
                    <div className="success__paragraph">
                     Customer details have been submitted
                    </div>
                    <div className="success__options">
                      <div className="success__option">
                        <button className="btn btn--blue-grey">Action 1</button>
                      </div>
                      <div className="success__option">
                        <button className="btn btn--blue-border" >
                          Action 2
                        </button>
                      </div>
                      <div className="success__option">
                        <button
                          className="btn btn--blue"
                          type="button"
                          onClick={() => setStep(0)}
                        >
                          Create account
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ModalCreateAccount;

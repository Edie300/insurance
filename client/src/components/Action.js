import React, { useState, useEffect } from "react";
import moment from "moment";
import Select from "./Select";
import Upload from "./Upload";
import IconClose from "./iconClose";
import CheckSuccess from "./CheckSuccess";
import { ReactComponent as Folder } from ".././assets/svg/folder.svg";

const Action = ({ show, onClose, action, title, paragraph }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  // form steps
  const [step, setStep] = useState(0);
  const nextStep = () => {
    setStep((cur) => cur + 1);
  };

  const prevStep = () => {
    setStep((cur) => cur - 1);
  };

  // account data
  const [values, setValues] = useState({
    name: "",
    surname: "",
    email: "",
  });

  // Documents Array
  const [documents, setDocuments] = useState([]);

  const handleChange = (name) => (event) => {
    event.preventDefault();
    setValues({ ...values, [name]: event.target.value });
  };

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="action" onClick={(e) => e.stopPropagation()}>
        <div className="action__heading">
          <div className="action__information">
            <p>{title}</p>
          </div>

          <div onClick={onClose} className="action__close">
            <IconClose />
          </div>
        </div>
        <div className="action__container">
          <p className="action__paragraph">{paragraph}</p>
        </div>

        <div className="action__footer">
          <div className="action__cancel">
            <button className="btn btn--blue-text" onClick={onClose}>
              Cancel
            </button>
          </div>
          <div className="action__publish">
            <button className="btn btn--blue" onClick={action}>Publish</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Action;

import React, { useEffect } from "react";
import { CSSTransition } from "react-transition-group";

import IconClose from "../components/iconClose";
import Loader from "../components/Loader";
import ShowImage from ".././core/ShowImage";

import moment from "moment";

const PanelClient = ({ show, onClose, client, loading }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }

  return (
    <div className="overlay">
      <CSSTransition
        in={show}
        appear={true}
        timeout={700}
        classNames="slide"
        unmountOnExit
      >
        <div className="panel" onClick={(e) => e.stopPropagation()}>
          <div className="panel__close" onClick={onClose}>
            <IconClose />
          </div>

          {(() => {
            switch (loading) {
              case true:
                return (
                  <div className="">
                    <Loader message="Fetching client details" />
                  </div>
                );
              case false:
                return (
                  <div className="overlay" onClick={onClose}>
                    <div className="modal" onClick={(e) => e.stopPropagation()}>
                      <div className="panel__content">
                        <div className="panel__img-box">
                          <ShowImage url="clients/photo" id={client._id} />
                        </div>
                        <div className="panel__details u-margin-top-medium">
                          <div className="panel__item">
                            <p className="panel__heading">Name</p>
                            <span>{client.name}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Surname</p>
                            <span>{client.surname}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Date of Birth</p>
                            <span>{moment(client.dob).format("LL")}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Gender</p>
                            <span>{client.gender}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">National ID</p>
                            <span>{client.national_id}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Passport</p>
                            <span>{client.passport}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Drivers Licence</p>
                            <span>{client.drivers_licence}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Marital Status</p>
                            <span>{client.marital_status}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Tilte</p>
                            <span>{client.title}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Mobile No</p>
                            <span>{client.cell}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Email</p>
                            <span>{client.email}</span>
                          </div>
                          <div className="panel__item">
                            <p className="panel__heading">Address</p>
                            <span>{client.address}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
            }
          })()}
        </div>
      </CSSTransition>
    </div>
  );
};
export default PanelClient;

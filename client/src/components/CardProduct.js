import React from "react";
import ShowImage from ".././core/ShowImage";
import IconOption from "../components/iconOption";

const CardProduct = ({
  onShow,
  name,
  description,
  url,
  id,
}) => {
  return (
    <div className="products__box">
      <div className="card-product">
        <div className="card-product__item item-image">
          <ShowImage url={url} id={id} />
        </div>
        <div className="card-product__item item-contents">
          <h3 className="card-product__name" onClick={onShow}>
            {name}
          </h3>

          <p className="card-product__price">{description}</p>
          <IconOption />
        </div>
      </div>
    </div>
  );
};
export default CardProduct;

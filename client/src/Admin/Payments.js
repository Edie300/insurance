import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { Switch, Route, Link, useRouteMatch, Redirect } from "react-router-dom";
import moment from "moment";
import PrivateRoute from "../auth/PrivateRoute";
import { isAuthenticated } from "../auth";

import Navigation from "./navigation";
import Header from "./header";

import CardWithBorder from "../components/CardWithBorder";
import Select from "../components/Select";
import ModalCreateClaim from "../components/ModalCreateClaim";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Payments = () => {
  const { path } = useRouteMatch();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewClaim, setShowNewClaim] = useState(false);

  // Create new account modal

  const accounts = () => <div className="accounts"></div>;

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Payments</span>
        </div>
        <div className="accounts__header">
          <h2 className="accounts__heading">Payments</h2>
        </div>

        <div className="content__pages">
          <div className="accounts__container">
            <div className="accounts__options">
              <div className="accounts__button-box">
                <button
                  className="btn btn--green"
                  onClick={() => setShowNewClaim(true)}
                >
                  Create Claim
                </button>
              </div>
            </div>
            <div className="accounts__filter">
              <div className="accounts__search-box">
                <input className="form__input" type="text" />
              </div>
              <div
                className="accounts__slider"
              >
         
              </div>
            </div>
            <div class=" accounts__table">
              <div class=" accounts__table-header">
                <div class="accounts__table-cell">Account ID</div>
                <div class="accounts__table-cell">Name</div>
                <div class="accounts__table-cell">ID Number</div>
                <div class="accounts__table-cell">gender</div>
                <div class="accounts__table-cell">Phone</div>
                <div class="accounts__table-cell">Status</div>
              </div>

              <div class="accounts__table-row">
                <div class=" accounts__table-cell">aerce</div>
                <div class=" accounts__table-cell">
                  <p className="accounts__table-name">wilson mathuthu</p>
                </div>
                <div class=" accounts__table-cell">cerferc</div>
                <div class=" accounts__table-cell">erfde</div>
                <div class=" accounts__table-cell">srctgfre</div>
                <div class=" accounts__table-cell">
                  {(() => {
                    switch (0) {
                      case 0:
                        return <span className="text-blue ">pending</span>;
                      case 1:
                        return <span className="text-green">active</span>;
                      case 2:
                        return <span className="text-warning">lapsed</span>;
                      case 3:
                        return <span className="text-danger">archived</span>;
                    }
                  })()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalCreateClaim
        onClose={() => {
          setShowNewClaim(false);
        }}
        show={showNewClaim}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Payments;

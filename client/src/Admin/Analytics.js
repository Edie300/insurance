import React, { useState, useEffect } from "react";
import Header from "./header";
import Navigation from "./navigation";

import Avatar from "../components/UserAvatar";

import Card from "../components/Card";
import Bar from "../core/ChartBar";
import Line from "../core/ChartLine";
import Doughnut from "../core/ChartDoughnut";
import Pie from "../core/ChartPie";
import Polar from "../core/ChartPolar";
import Radar from "../core/ChartRadar";

import { getClaimPerYear} from "../api/claimFuneral"
import { isAuthenticated } from "../auth";


const Analytics = () => {

  const [labels, setLabels] = useState([]);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const token = isAuthenticated().token;

  const init = () => {
    setLoading(true)
    getClaimPerYear(token)
      .then((res) => {
        console.log("response", res.data.data);
        setLabels(res.data.data.labels);
        setData(res.data.data.data);
        setLoading(false)
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false)
      });
  };

  useEffect(() => {
    init();
  }, []);
  const content = () => (
    <div className="content">
      <h1 className="heading-primary">Analytics</h1>
      <div className="row u-margin-top-medium">
        {!loading && <Bar labels={labels} data={data} />}       
      </div>



    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
    
      {content()}
    </div>
  );
};
export default Analytics;

import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";

import ModalCreateUser from "../components/ModalCreateUser";
import { getCompanyUsers } from "../api/users";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateClaim from "../components/ModalCreateClaim";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Users = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showCreateUser, setShowCreateUser] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  const init = () => {
    setLoading(true);
    getCompanyUsers(token, company.id)
      .then((res) => {
        console.log("response", res.data);
        setUsers(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const [data, setData] = useState({
    name: "",
    surname: "",
    gender: "",
    dob: "",
    national_id: "",
    passport: "",
    drivers_licence: "",
    address: "  ",
    marital_status: "",
    title: "",
    cell: "",
    email: "",
    package: "",
  });

  // Documents Array
  const [documents, setDocuments] = useState([]);

  const handleChange = (name) => (event) => {
    setData({ ...data, [name]: event.target.value });
  };

  const viewClaim = (id) => {
    history.push(`${path}/${id}`);
  };

  const accountTable = () => (
    <div className="claims__container">
      <div className="claims__header">
        <h3 className="claims__title">
          Users <span>({users.length})</span>
        </h3>
        <div className="claims__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowCreateUser(true)}
            >
              Register user
            </button>
          </div>
        </div>
      </div>
      <div className="claims__filter">
        <div className="claims__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="account number"
          />
          <button className="btn">Search</button>
        </div>
        <div className="claims__settings" onClick={() => setShowFilters(true)}>
          <Settings />
        </div>
        <div className="claims__pagination">
          <div className="claims__previous">
            <Chevron />
          </div>
          <div className="claims__page">
            <p>2</p>
          </div>
          <div className="claims__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" claims__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Employee ID</div>
          <div class="accounts__table-cell">Name</div>
          <div class="accounts__table-cell">gender</div>
          <div class="accounts__table-cell">email</div>
          <div class="accounts__table-cell">branch</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching users"} />}

        {!loading && users.length === 0 && (
          <div className="claims__no-accounts">
            <p>No users</p>
            <span>
              There are no users available to display. To start registering
              users click register
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => setShowCreateUser(true)}
              >
                Register user
              </button>
            </div>
          </div>
        )}

        {!loading &&
          users.length > 0 &&
          users.map((user, i) => (
            <div
              class="accounts__table-row"
              key={user.id}
              onClick={() => viewClaim(user.id)}
            >
              <div class=" accounts__table-cell">{user.employee_id}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                  {user.name} {user.surname}
                </p>
              </div>
              <div class=" accounts__table-cell">{user.gender}</div>
              <div class=" accounts__table-cell">{user.email}</div>
              <div class=" accounts__table-cell">{user.branch.name}</div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (user.status) {
                    case "0":
                      return <span className="text-green">active</span>;
                    case "1":
                      return <span className="text-warning">suspended</span>;
                    case "2":
                      return <span className="text-danger">deleted</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Users</span>
        </div>
        <div className="claims__">
          <h2 className="claims__heading">Users</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreateUser
        onClose={() => {
          setShowCreateUser(false);
        }}
        show={showCreateUser}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Users;
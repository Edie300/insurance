import React, { useState, useEffect } from "react";
import { Switch, Route, NavLink, useRouteMatch } from "react-router-dom";
import { isAuthenticated } from "../auth";
import Navigation from "./navigation";
import Header from "./header";



const Resources = () => {
  const { path } = useRouteMatch();
  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(2);

  const content = () => (
    <div className="content">
      <div className="content__breadcrum">
        <span className="content__current">Resources</span>
      </div>
      <div className="accounts__header">
        <h2 className="accounts__heading">Resources</h2>
      </div>
      <div className="account__nav">
        <ul className="account__nav-list">
          <p className="account__nav-item" onClick={() => setTab(1)}>
            Caskets
          </p>
          <p className="account__nav-item" onClick={() => setTab(2)}>
            Hearses
          </p>
          <p className="account__nav-item" onClick={() => setTab(3)}>
            Ambulances
          </p>
          <p className="account__nav-item" onClick={() => setTab(4)}>
            Buses
          </p>
          <p className="account__nav-item" onClick={() => setTab(5)}>
            Fuel
          </p>
          <p className="account__nav-item" onClick={() => setTab(6)}>
            Tents
          </p>
          <p className="account__nav-item" onClick={() => setTab(7)}>
            Mobile Toilets
          </p>
        </ul>
      </div>
      <div className="account"></div>
    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
      {content()}
    </div>
  );
};
export default Resources;

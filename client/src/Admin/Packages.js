import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import { getCompanyPackagesFun } from "../api/package-funeral";
import ModalCreatePackage from "../components/ModalCreatePackage";
import ModalCreateClaim from "../components/ModalCreateClaim";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Packages = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewPackage, setShowNewPackage] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [packages, setPackages] = useState([]);
  const [loading, setLoading] = useState(false);

  const init = () => {
    setLoading(true)
    getCompanyPackagesFun(company.id, token)
      .then((res) => {
        console.log("response", res.data);
        setPackages(res.data);
        setLoading(false)
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false)
      });
  };

  useEffect(() => {
    init();
  }, []);


  const viewPackage = (id) => {
    history.push(`${path}/funeral/${id}`);
  };


  const accountTable = () => (
    <div className="claims__container">
      <div className="claims__header">
        <h3 className="claims__title">
          Packages <span>({packages.length})</span>
        </h3>
        <div className="claims__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewPackage(true)}
            >
              Create package
            </button>
          </div>
        </div>
      </div>
      <div className="claims__filter">
        <div className="claims__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="search packages"
          />
          <button className="btn">Search</button>
        </div>
        <div className="claims__settings" onClick={() => setShowFilters(true)}>
          <Settings />
        </div>
        <div className="claims__pagination">
          <div className="claims__previous">
            <Chevron />
          </div>
          <div className="claims__page">
            <p>2</p>
          </div>
          <div className="claims__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" claims__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Package ID</div>
          <div class="accounts__table-cell">Name</div>
          <div class="accounts__table-cell">type</div>
          <div class="accounts__table-cell">Company</div>
          <div class="accounts__table-cell">currency</div>
          <div class="accounts__table-cell">Date Created</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching packages"} />}

        {!loading && packages.length < 1 && (
          <div className="claims__no-accounts">
            <p>No packages</p>
            <span>
              There are no packages available to display. To create package click
              create package
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => setShowNewPackage(true)}
              >
                Create package
              </button>
            </div>
          </div>
        )}

        {!loading &&
          packages.length > 0 &&
          packages.map((pkg, i) => (
            <div
              class="accounts__table-row"
              onClick={() => viewPackage(pkg.id)}
            >
              <div class=" accounts__table-cell">12345</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">{pkg.name}</p>
              </div>
              <div class=" accounts__table-cell">funeral</div>
              <div class=" accounts__table-cell">{pkg.company.name}</div>
              <div class=" accounts__table-cell">{pkg.currency}</div>
              <div class=" accounts__table-cell">
                {moment(pkg.created_at).format("LL")}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (pkg.status) {
                    case "0":
                      return <span className="text-warning ">inactive</span>;
                    case "1":
                      return <span className="text-green">active</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Packages</span>
        </div>
        <div className="claims__">
          <h2 className="claims__heading">Packages</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreatePackage
        onClose={() => {
          setShowNewPackage(false);
        }}
        show={showNewPackage}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Packages;


import React from "react";
import Navigation from "./navigation";
import MultiStepForm from "../test/MultiStepForm";

const Settings = () => {
  const content = () => (
    <div className="content">
      <h1 className="heading-primary">Settings</h1>
      <div className="sub-navigation u-margin-top-small">
          <ul className="sub-navigation__list">
            <li className="sub-navigation__item ">
              Policies
            </li>
            <li className="sub-navigation__item sub-navigation__item--active">Create</li>
          </ul>
        </div>
      <div className="registration u-margin-top-small">
        <div className="registration__forms">
          <MultiStepForm />
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Navigation />
     
      {content()}
    </div>
  );
};
export default Settings;

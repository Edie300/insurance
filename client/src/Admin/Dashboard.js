import React from "react";
import { Link } from "react-router-dom";

import Navigation from "./navigation";
import Header from "./header";
import Card from "../components/Card";

import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Dashboard = () => {
  const content = () => (
    <div className="content">
      <div className="content__breadcrum">
        <span className="content__current">Dashboard</span>
      </div>
      <h1 className="heading-primary">Overview</h1>
      <div className="home__cards ">
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
      {content()}
    </div>
  );
};
export default Dashboard;

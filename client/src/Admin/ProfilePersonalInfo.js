import React from "react";

const ProfilePersonalInfo = () => {
  const content = () => (
    <div className="content">
      <h2>Personal information</h2>
    </div>
  );

  return <div>{content()}</div>;
};
export default ProfilePersonalInfo;

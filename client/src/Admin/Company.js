import React, { useState, useEffect } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getSingleCompany } from "../api/companies";

import Navigation from "./navigation";
import Header from "./header";
import ShowImage from "../core/ShowImage";

import ModalCreateCompany from "../components/ModalCreateCompany";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Funeral } from ".././assets/svg/Totum_Icon_Funeral.svg";
import { ReactComponent as Health } from ".././assets/svg/Totum_Icon_Health.svg";
import { ReactComponent as Motor } from ".././assets/svg/Totum_Icon_Motor.svg";
import { ReactComponent as Life } from ".././assets/svg/Totum_Icon_Life.svg";
import { ReactComponent as Home } from ".././assets/svg/Totum_Icon_Home.svg";
import { ReactComponent as Travel } from ".././assets/svg/Totum_Icon_Travel.svg";
import { ReactComponent as Appliance } from ".././assets/svg/Totum_Icon_Appliance.svg";
import { ReactComponent as Pet } from ".././assets/svg/Totum_Icon_Pet.svg";
import { ReactComponent as Cyber } from ".././assets/svg/Totum_Icon_Cyber.svg";
import { ReactComponent as Pension } from ".././assets/svg/Totum_Icon_Pension.svg";

const Company = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(2);
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  const getCompany = () => {
    setLoading(true);
    getSingleCompany(id, token)
      .then((res) => {
        console.log("response", res.data);
        setData(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    getCompany();
  }, []);

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            companies
            <Chevron />
          </Link>
          <span className="content__current">{data.name}</span>
        </div>
        <div className="accounts__header">
          <h2 className="accounts__heading">{data.name}</h2>
        </div>

        <div className="content__pages">
          <div className="accounts__container">
            {data && data.logo && (
              <div className="company__about">
                <div className="company__logo">
                  <ShowImage url={data.logo.Location} alt={data.name} />
                </div>
                <div className="company__details">
                  <div className="modal__item">
                    <p className="modal__heading">Name</p>
                    <span>{data.name}</span>
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Tax Number</p>
                    <span>{data.tax_number}</span>
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Company type</p>
                    {(() => {
                      switch (data.type) {
                        case 0:
                          return <span className="text-blue ">admin</span>;
                        case 1:
                          return <span className="text-green">insure</span>;
                      }
                    })()}
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Status</p>
                    {(() => {
                      switch (data.status) {
                        case "0":
                          return <span>inactive</span>;
                        case "1":
                          return <span>active</span>;
                      }
                    })()}
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Email</p>
                    <span>{data.email}</span>
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Address</p>
                    <span>{data.address}</span>
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">Phones</p>
                    {data.phones.map((phone, i) => (
                      <span>{phone}</span>
                    ))}
                  </div>
                  <div className="modal__item">
                    <p className="modal__heading">landlines</p>
                    {data.landlines.map((landlines, i) => (
                      <span>{landlines}</span>
                    ))}
                  </div>
                </div>
              </div>
            )}
            {data && data.insurance && (
              <div className="company__insurance">
                <div
                  className={
                    data.insurance.indexOf(0) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Funeral />
                  </div>
                  <p className="">Funeral</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(1) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Health />
                  </div>
                  <p>Medical</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(2) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Motor />
                  </div>
                  <p>Motor</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(3) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Life />
                  </div>
                  <p>Life</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(4) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Home />
                  </div>
                  <p>Home</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(5) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Appliance />
                  </div>
                  <p className="">Appliance</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(6) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Travel />
                  </div>
                  <p className="">Travel</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(7) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Pet />
                  </div>
                  <p className="">Pet</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(8) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Cyber />
                  </div>
                  <p className="">Cyber</p>
                </div>

                <div
                  className={
                    data.insurance.indexOf(9) === -1
                      ? "company__insure--hide"
                      : "company__insure--show"
                  }
                >
                  <div>
                    <Pension />
                  </div>
                  <p className="">Pension</p>
                </div>
              </div>
            )}
          </div>
          <div className="account__nav">
            <ul className="account__nav-list">
              <p className="account__nav-item" onClick={() => setTab(1)}>
                Caskets
              </p>
              <p className="account__nav-item" onClick={() => setTab(2)}>
                Hearses
              </p>
              <p className="account__nav-item" onClick={() => setTab(3)}>
                Ambulances
              </p>
              <p className="account__nav-item" onClick={() => setTab(4)}>
                Buses
              </p>
              <p className="account__nav-item" onClick={() => setTab(5)}>
                Fuel
              </p>
              <p className="account__nav-item" onClick={() => setTab(6)}>
                Tents
              </p>
              <p className="account__nav-item" onClick={() => setTab(7)}>
                Mobile Toilets
              </p>
            </ul>
          </div>
        </div>
      </div>
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Company;

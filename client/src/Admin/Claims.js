import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateClaim from "../components/ModalCreateClaim";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Claims = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewClaim, setShowNewClaim] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [claims, setClaims] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getMainAccounts(token)
      .then((res) => {
        console.log("response", res.data);
        setClaims(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const viewClaim = (id) => {
    history.push(`${path}/${id}`);
  };

  const accountTable = () => (
    <div className="claims__container">
      <div className="claims__header">
        <h3 className="claims__title">
          Claims <span>({claims.length})</span>
        </h3>
        <div className="claims__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewClaim(true)}
            >
              New claim
            </button>
          </div>
        </div>
      </div>
      <div className="claims__filter">
        <div className="claims__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="account number"
          />
          <button className="btn">Search</button>
        </div>
        <div
          className="claims__settings"
          onClick={() => setShowFilters(true)}
        >
          <Settings />
        </div>
        <div className="claims__pagination">
          <div className="claims__previous">
            <Chevron />
          </div>
          <div className="claims__page">
            <p>2</p>
          </div>
          <div className="claims__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" claims__table">
        <div class=" claims__table-header">
          <div class="claims__table-cell">Account ID</div>
          <div class="claims__table-cell">Type</div>
          <div class="claims__table-cell">Company</div>
          <div class="claims__table-cell">Claiment</div>
          <div class="claims__table-cell">Package</div>
          <div class="claims__table-cell">Date</div>
          <div class="claims__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching claims"} />}

        {!loading && claims.length < 1 && (
          <div className="claims__no-accounts">
            <p>No claims</p>
            <span>
              There are no claims available to display. To make a claim
               click new claim
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => setShowNewClaim(true)}
              >
                New claim
              </button>
            </div>
          </div>
        )}

        {!loading &&
          claims.length > 0 &&
          claims.map((claim, i) => (
            <div
              class="claims__table-row"
              key={claim.id}
              onClick={() => viewClaim(claim.id)}
            >
              <div class=" claims__table-cell">{claim.account_id}</div>
              <div class=" claims__table-cell">
                <p className="claims__table-name">
                  {claim.claiment_name} {claim.claiment_surname}
                </p>
              </div>
              <div class=" claims__table-cell">{claim.national_id}</div>
              <div class=" claims__table-cell">something...</div>
              <div class=" claims__table-cell">something...</div>
              <div class=" claims__table-cell">something...</div>
              <div class=" claims__table-cell">
                {(() => {
                  switch (claim.status) {
                    case 0:
                      return <span className="text-blue ">pending</span>;
                    case 1:
                      return <span className="text-green">active</span>;
                    case 2:
                      return <span className="text-warning">lapsed</span>;
                    case 3:
                      return <span className="text-danger">archived</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Claims</span>
        </div>
        <div className="claims__">
          <h2 className="claims__heading">Claims</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreateClaim
        onClose={() => {
          setShowNewClaim(false);
        }}
        show={showNewClaim}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Claims;

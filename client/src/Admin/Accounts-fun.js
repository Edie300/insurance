import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useHistory,
  useParams,
} from "react-router-dom";
import moment from "moment";
import PrivateRoute from "../auth/PrivateRoute";
import { isAuthenticated } from "../auth";
import { getFunAccount } from "../api/account-funeral";

import Navigation from "./navigation";
import Header from "./header";

import Card from "../components/Card";
import ModalCreateAccount from "../components/ModalCreateAccount";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const AccountFun = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(1);
  const [account, setAccount] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getFunAccount(token, id)
      .then((res) => {
        console.log("response", res.data);
        setAccount(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const policy = () => (
    <div class=" account__policy">
      <div className="account__item">
        <p className="">Package</p>
        <span>{account.package_fun.id}</span>
      </div>
      <div className="account__item">
        <p className="">Balance</p>
        <span>{account.id}</span>
      </div>
      <div className="account__item">
        <p className="">Premium</p>
        <span>{account.id}</span>
      </div>
      <div className="account__item">
        <p className="">Created At</p>
        <span>{account.created_at}</span>
      </div>
      <div className="account__item">
        <p className="">Status</p>
        <span>{account.status}</span>
      </div>
    </div>
  );
  const dependents = () => (
    <div className="account__dependents">
      <div className="account__options">
        <div className="account__create">
          <button className="btn btn--blue">Add dependent</button>
        </div>
      </div>
      <div class=" account__table">
        <div class=" account__table-header">
          <div class="account__table-cell">Name</div>
          <div class="account__table-cell">ID number</div>
          <div class="account__table-cell">Date of birth</div>
          <div class="account__table-cell">gender</div>
          <div class="account__table-cell">relationship</div>
          <div class="account__table-cell">Date Added</div>
          <div class="account__table-cell">Status</div>
        </div>

        <div class="account__table-row">
          <div class=" account__table-cell">Simon phiri</div>
          <div class=" account__table-cell">098366a76</div>
          <div class=" account__table-cell"> 2 july 98</div>
          <div class=" account__table-cell">male</div>
          <div class=" account__table-cell">son</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">active</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">Simon phiri</div>
          <div class=" account__table-cell">098366a76</div>
          <div class=" account__table-cell"> 2 july 98</div>
          <div class=" account__table-cell">male</div>
          <div class=" account__table-cell">son</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">active</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">Simon phiri</div>
          <div class=" account__table-cell">098366a76</div>
          <div class=" account__table-cell"> 2 july 98</div>
          <div class=" account__table-cell">male</div>
          <div class=" account__table-cell">son</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">active</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">Simon phiri</div>
          <div class=" account__table-cell">098366a76</div>
          <div class=" account__table-cell"> 2 july 98</div>
          <div class=" account__table-cell">male</div>
          <div class=" account__table-cell">son</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">active</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">Simon phiri</div>
          <div class=" account__table-cell">098366a76</div>
          <div class=" account__table-cell"> 2 july 98</div>
          <div class=" account__table-cell">male</div>
          <div class=" account__table-cell">son</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">active</div>
        </div>
      </div>
    </div>
  );

  const claims = () => (
    <div className="account__dependents">
      <div className="account__options">
        <div className="account__create">
          <button className="btn btn--blue">Create claim</button>
        </div>
      </div>
      <div class=" account__table">
        <div class=" account__table-header">
          <div class="account__table-cell">Claim ID</div>
          <div class="account__table-cell">Deceased Name</div>
          <div class="account__table-cell">Claim type</div>
          <div class="account__table-cell">Claiment Name</div>
          <div class="account__table-cell">Sum Assured</div>
          <div class="account__table-cell">Currency</div>
          <div class="account__table-cell">Date created</div>
          <div class="account__table-cell">Status</div>
        </div>

        <div class="account__table-row">
          <div class=" account__table-cell">56345i</div>
          <div class=" account__table-cell">Artwell Phiri</div>
          <div class=" account__table-cell">dependent</div>
          <div class=" account__table-cell">max Phiri</div>
          <div class=" account__table-cell">20000</div>
          <div class=" account__table-cell">zw</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">pending</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">56345i</div>
          <div class=" account__table-cell">Artwell Phiri</div>
          <div class=" account__table-cell">dependent</div>
          <div class=" account__table-cell">max Phiri</div>
          <div class=" account__table-cell">20000</div>
          <div class=" account__table-cell">zw</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">pending</div>
        </div>
        <div class="account__table-row">
          <div class=" account__table-cell">56345i</div>
          <div class=" account__table-cell">Artwell Phiri</div>
          <div class=" account__table-cell">dependent</div>
          <div class=" account__table-cell">max Phiri</div>
          <div class=" account__table-cell">20000</div>
          <div class=" account__table-cell">zw</div>
          <div class=" account__table-cell">3 may 2022</div>
          <div class=" account__table-cell">pending</div>
        </div>
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            Accounts
            <Chevron />
          </Link>
          <Link to="/accounts" className="content__link">
            9485RT
            <Chevron />
          </Link>
          <span className="content__current">2342354</span>
        </div>
        <div className="accounts__header">
          <h2 className="accounts__heading">funeral</h2>
        </div>
        <div className="account__nav">
          <ul className="account__nav-list">
            <p className="account__nav-item" onClick={() => setTab(1)}>
              Policy details
            </p>
            <p className="account__nav-item" onClick={() => setTab(2)}>
              Dependents
            </p>
            <p className="account__nav-item" onClick={() => setTab(3)}>
              Claims
            </p>
            <p className="account__nav-item" onClick={() => setTab(4)}>
              Payments
            </p>
          </ul>
        </div>
        <div className="account">
          {tab === 1 && policy()}
          {tab === 2 && dependents()}
          {tab === 3 && claims()}
        </div>
      </div>
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default AccountFun;

import React from "react";
import { Link } from "react-router-dom";

import Navigation from "./navigation";
import Header from "./header";
import Card from "../components/Card";

import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Home = () => {
  const content = () => (
    <div className="content">
      <div className="content__breadcrum">
      

        <span className="content__current">Dashboard</span>
      </div>
      <h1 className="heading-primary">Overview</h1>
      <div className="home__cards ">
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
      </div>

      <div class="table__limiter">
        <div className="table__heading">
          <div className="table__information">
            <h2>Recent activity</h2>
            <p>something about your activity</p>
          </div>
          {/* <div className="table__options">
            <button className="btn btn--blue-border">option 1</button>
            <button className="btn btn--blue">option 2</button>
          </div> */}
        </div>
        <div class=" table__wrapper">
          <div class="table">
            <div class="table__header">
              <div class="table__cell">Transaction</div>
              <div class="table__cell">Amount</div>
              <div class="table__cell">Status</div>
              <div class="table__cell">Date</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">Payment for Thomas</div>
              <div class="table__cell">$200,000</div>
              <div class="table__cell">Success</div>
              <div class="table__cell">July 11, 2021</div>
            </div>

            <div class="table__row">
              <div class="table__cell">Payment for Thomas</div>
              <div class="table__cell">$200,000</div>
              <div class="table__cell">Success</div>
              <div class="table__cell">July 11, 2021</div>
            </div>

            <div class="table__row">
              <div class="table__cell">Payment for Thomas</div>
              <div class="table__cell">$200,000</div>
              <div class="table__cell">Success</div>
              <div class="table__cell">July 11, 2021</div>
            </div>

            <div class="table__row">
              <div class="table__cell">Payment for Thomas</div>
              <div class="table__cell">$200,000</div>
              <div class="table__cell">Success</div>
              <div class="table__cell">July 11, 2021</div>
            </div>

            <div class="table__row">
              <div class="table__cell">Payment for Thomas</div>
              <div class="table__cell">$200,000</div>
              <div class="table__cell">Success</div>
              <div class="table__cell">July 11, 2021</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
      {content()}
    </div>
  );
};
export default Home;

import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useParams,
  useHistory,
} from "react-router-dom";
import moment from "moment";
import PrivateRoute from "../auth/PrivateRoute";
import { isAuthenticated } from "../auth";
import { getMainAccount, getSubAccounts } from "../api/account-main";

import Navigation from "./navigation";
import Header from "./header";

import Card from "../components/Card";

import ModalEmail from "../components/ModalEmail";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as PaperClip } from ".././assets/svg/paperclip.svg";

const Account = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(2);
  const [showNewEmail, setShowNewEmail] = useState(false);
  const [account, setAccount] = useState([]);
  const [accountId, setAccountId] = useState({ account_main: id });
  const [policies, setPolicies] = useState([]);
  const [loading, setLoading] = useState(true);

  const getAccount = () => {
    setLoading(true);
    getMainAccount(token, id)
      .then((res) => {
        console.log("response", res.data);
        setAccount(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const fetchSubAccounts = () => {
    setLoading(true);

    getSubAccounts(token, accountId)
      .then((res) => {
        console.log("response", res.data);
        setPolicies(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    getAccount();
  }, []);

  useEffect(() => {
    fetchSubAccounts();
  }, []);

  const viewAccount = (fun_id) => {
    history.push(`funeral/${fun_id}`);
  };

  const noPolicies = () => (
    <div className="account__no-policies">
      <p>No Policies</p>
      <span>no policies to display for this account</span>
    </div>
  );

  const accountPolicies = () => (
    <div class=" account__table">
      <div class=" account__table-header">
        <div class="account__table-cell">Package</div>
        <div class="account__table-cell">Type</div>
        <div class="account__table-cell">Payment Status</div>
        <div class="account__table-cell">Premium</div>
        <div class="account__table-cell">balance</div>
        <div class="account__table-cell">Created</div>
        <div class="account__table-cell">Status</div>
      </div>
      {policies.map((policy, i) => (
        <div
          class="account__table-row"
          key={policy.id}
          onClick={() => viewAccount(policy.id)}
        >
          <div class=" account__table-cell">
            {(() => {
              switch (policy.type) {
                case 0:
              return <span className="">{policy.package_fun.name}</span>;
                case 1:
                  return <span className="">{policy.package_med.name}</span>;
                case 2:
                  return <span className="">{policy.package_motor.name}</span>;
              }
            })()}
          </div>
          <div class=" account__table-cell">
            {(() => {
              switch (policy.type) {
                case 0:
                  return <span className="">Funeral</span>;
                case 1:
                  return <span className="">medical</span>;
                case 2:
                  return <span className="">motor</span>;
              }
            })()}
          </div>
          <div class=" account__table-cell">{policy.payment_status}</div>
          <div class=" account__table-cell">{policy.premium}</div>
          <div class=" account__table-cell">{policy.balance}</div>
          <div class=" account__table-cell">
            {moment(policy.created_at).format("LL")}
          </div>
          <div class=" account__table-cell">
            {(() => {
              switch (policy.status) {
                case 0:
                  return <span className="text-blue ">pending</span>;
                case 1:
                  return <span className="text-green">active</span>;
                case 2:
                  return <span className="text-warning">lapsed</span>;
                case 3:
                  return <span className="text-danger">archived</span>;
              }
            })()}
          </div>
        </div>
      ))}
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            Accounts
            <Chevron />
          </Link>
          <span className="content__current">{account.account_id}</span>
        </div>
        <div className="accounts__header">
          <h2 className="accounts__heading">{account.account_id}</h2>
        </div>
        <div className="content__pages">
          <div className="account">
            <div className="account__heading">
              <h3 className="">Customer details</h3>
              <p>these are the custmer details</p>
            </div>
            <div class=" account__client">
              <div className="account__item">
                <p className="">Name</p>
                <span>{account.name}</span>
              </div>
              <div className="account__item">
                <p className="">Surname</p>
                <span>{account.surname}</span>
              </div>
              <div className="account__item">
                <p className="">Gender</p>
                <span>{account.gender}</span>
              </div>
              <div className="account__item">
                <p className="">Date of Birth</p>
                <span>{moment(account.date_of_birth).format("LL")}</span>
              </div>
              <div className="account__item">
                <p className="">Title</p>
                <span>{account.title}</span>
              </div>
              <div className="account__item">
                <p className="">Marital status</p>
                <span>{account.marital_status}</span>
              </div>

              <div className="account__item">
                <p className="">National ID</p>
                <span>{account.national_id}</span>
              </div>
              <div className="account__item">
                <p className="">Passport Number</p>
                <span>{account.passport}</span>
              </div>

              <div className="account__item">
                <p className="">Drivers licence</p>
                <span>{account.drivers_license}</span>
              </div>
              <div className="account__item">
                <p className="">phone</p>
                <span>{account.phone}</span>
              </div>
              <div className="account__item">
                <p className="">email</p>
                <span>{account.email}</span>
              </div>
              <div className="account__item">
                <p className="">Address</p>
                <span>{account.address}</span>
              </div>
            </div>
            <div className="account__attach-container">
              <h4 className="account__name">Attachments</h4>
              <div className="account__attachments-box">
                <div className="account__attachments">
                  <div className="account__file">
                    <PaperClip />
                    <p>birth_document_0884575a07.pdf</p>
                  </div>
                  <p className="account__download">Download</p>
                </div>
                <div className="account__attachments">
                  <div className="account__file">
                    <PaperClip />
                    <p>birth_document_0884575a07.pdf</p>
                  </div>
                  <p className="account__download">Download</p>
                </div>
                <div className="account__attachments">
                  <div className="account__file">
                    <PaperClip />
                    <p>birth_document_0884575a07.pdf</p>
                  </div>
                  <p className="account__download">Download</p>
                </div>
              </div>
            </div>
          </div>
          <div className="account">
            <div className="account__heading">
              <h3 className="">Policies</h3>
              <p>these are polices under this account</p>
            </div>
            <div className="account__accounts">
              <div className="account__options">
                <div className="account__create">
                  <button className="btn btn--blue">Create Account</button>
                </div>
              </div>
              <div className="account__box">{accountPolicies()}</div>
            </div>
          </div>
        </div>
      </div>
      <ModalEmail
        onClose={() => {
          setShowNewEmail(false);
        }}
        show={showNewEmail}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Account;
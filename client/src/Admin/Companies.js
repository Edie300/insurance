import React, { useState, useEffect } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";
import { getAllCompanies } from "../api/companies";

import Navigation from "./navigation";
import Header from "./header";
import ShowImage from ".././core/ShowImage";

import ModalCreateCompany from "../components/ModalCreateCompany";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Slider } from ".././assets/svg/sliders.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Companies = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewCompany, setShowNewCompany] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [companies, setCompanies] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getAllCompanies(token)
      .then((res) => {
        console.log("response", res.data);
        setCompanies(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const viewCompany = (id) => {
    history.push(`${path}/${id}`);
  };

  const accountTable = () => (
    <div className="accounts__container">
      <div className="accounts__header">
        <h3 className="accounts__title">
          Companies <span>({companies.length})</span>
        </h3>
        <div className="accounts__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Send email</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewCompany(true)}
            >
              Register Company
            </button>
          </div>
        </div>
      </div>
      <div className="accounts__filter">
        <div className="accounts__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="account number,  phone, national ID"
          />
          <button className="btn">Search</button>
        </div>
        <div
          className="accounts__settings"
          onClick={() => setShowFilters(true)}
        >
          <Settings />
        </div>
        <div className="accounts__pagination">
          <div className="accounts__previous">
            <Chevron />
          </div>
          <div className="accounts__page">
            <p>2</p>
          </div>
          <div className="accounts__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Account ID</div>
          <div class="accounts__table-cell">Name</div>
          <div class="accounts__table-cell">Email</div>
          <div class="accounts__table-cell">Tax Number</div>
          <div class="accounts__table-cell">Type</div>
          <div class="accounts__table-cell">Status</div>
        </div>
        {companies.map((company, i) => (
          <div
            class="accounts__table-row"
            key={company.id}
            onClick={() => viewCompany(company.id)}
          >
            <div class=" accounts__table-cell">
              123124
              {/* <div className="accounts__table-image">
                <ShowImage url={company.logo.Location} alt={company.name} />
              </div> */}
            </div>
            <div class=" accounts__table-cell">
              <p className="accounts__table-name">{company.name}</p>
            </div>
            <div class=" accounts__table-cell">{company.email}</div>
            <div class=" accounts__table-cell">{company.tax_number}</div>
            <div class=" accounts__table-cell">
              {(() => {
                switch (company.type) {
                  case 0:
                    return <span className="text-blue ">admin</span>;
                  case 1:
                    return <span className="text-green">insure</span>;
                }
              })()}
            </div>
            <div class=" accounts__table-cell">
              {(() => {
                switch (company.status) {
                  case "0":
                    return <span>inactive</span>;
                  case "1":
                    return <span>active</span>;
                }
              })()}
            </div>
          </div>
        ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Companies</span>
        </div>
        <div className="accounts__">
          <h2 className="accounts__heading">Companies</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreateCompany
        onClose={() => {
          setShowNewCompany(false);
        }}
        show={showNewCompany}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Companies;

import { API, BACKEND_API } from "../config";
import axios from "axios";

export const getAllPackages = (token, companyId) => {
  return axios.get(`${BACKEND_API}:32580/api/packages/companies/${companyId}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



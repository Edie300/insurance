import { API } from "../config";
import axios from "axios";

export const getAllBuses = (token) => {
  return axios.get(`${API}/clients`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const createBus = (token, formData) => {
  return axios.post(`${API}/buses`, formData, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getBus = (token, id) => {
  return axios.get(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getCompanyBuses = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/buses`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const updateBus = (token, id) => {
  return axios.put(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const deleteBus = (token, id) => {
  return axios.delete(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

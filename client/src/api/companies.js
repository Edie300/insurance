import { BACKEND_API} from "../config";
import axios from "axios";



export const createCompany = (data, token) => {
  return axios.post(`${BACKEND_API}:30365/api/companies/create`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};


export const getAllCompanies = (token) => {
  return axios.get(`${BACKEND_API}:30365/api/companies`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getSingleCompany = (id, token) => {
  return axios.get(`${BACKEND_API}:30365/api/companies/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const uploadLogo = (data, token) => {
  return axios.post(`${BACKEND_API}:30365/api/companies/upload_logo`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const deleteLogo = (data, token) => {
  return axios.post(`${BACKEND_API}:30365/api/companies/remove_logo`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyBranches = (token, companyId) => {
  return axios.get(`${BACKEND_API}:30365/api/branches/companies/${companyId}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
import { API, BACKEND_API } from "../config";
import axios from "axios";
import queryString from "query-string";

export const getPackagesFun = (token) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_funeral`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyPackagesFun = (id, token) => {
  return axios.get(
    `${BACKEND_API}:32580/api/packages_funeral/companies/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const createPackageFun = (token, packageFun) => {
  return axios.post(
    `${BACKEND_API}:32580/api/packages_funeral/create`,
    packageFun,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const getPackageFun = (token, id) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_funeral/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const editPackageFun = (token, pkg, id) => {
  return axios.put(`${BACKEND_API}:32580/api/packages_funeral/${id}`, pkg, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const publishPackageFun = (token, id) => {
  console.log("token",token)
  return axios.patch(`${BACKEND_API}:32580/api/packages_funeral/publish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const unpublishPackageFun = (token, id) => {
  return axios.patch(
    `${BACKEND_API}:32580/api/packages_funeral/unpublish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const uploadAttachments = (token, attachments) => {
  return axios.post(
    `${BACKEND_API}:32580/api/packages/upload_attachments`, attachments,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

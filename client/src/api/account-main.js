import { BACKEND_API, REACT_APP_API_URL } from "../config";
import axios from "axios";
import queryString from "query-string";

export const getMainAccounts = (token, params) => {
  const query = queryString.stringify(params);
  const qry = queryString.stringify(params);
  console.log("query::", query);
  console.log("qry::", qry);
  return axios.get(`${BACKEND_API}:31717/api/accounts_main`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createMainAccount = (token, account) => {
  return axios.post(`${BACKEND_API}:31717/api/accounts_main/create`, account, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getMainAccount = (token, id) => {
  return axios.get(`${BACKEND_API}:31717/api/accounts_main/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const EditMainAccount = (token, id, account) => {
  return axios.put(`${BACKEND_API}:31717/api/accounts_main/${id}`, account, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getSubAccounts = (token, data) => {
  return axios.post(`${BACKEND_API}:31717/api/accounts_sub_accounts`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const approveMainAccount = (token, id) => {
  return axios.patch(`${BACKEND_API}:31717/api/accounts_main/approve/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

import { API, BACKEND_API } from "../config";
import axios from "axios";

export const getAllCaskets = (token) => {
  return axios.get(`${BACKEND_API}:30869/api/caskets`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyCaskets = (id, token) => {
  return axios.get(`${BACKEND_API}:30869/api/caskets/companies/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createCasket = (data, token) => {
  return axios.post(`${BACKEND_API}:30869/api/caskets/create`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getCasket = (id, token) => {
  return axios.get(`${BACKEND_API}:30869/api/caskets/${id}`,{
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
export const editCasket = ( token, casket, id, ) => {
  return axios.put(`${BACKEND_API}:30869/api/caskets/${id}`, casket, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const uploadCasketImage = (data, token) => {
  return axios.post(`${BACKEND_API}:30869/api/caskets/upload_logo`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const deleteCasketImage = (data, token) => {
  return axios.post(`${BACKEND_API}:30869/api/caskets/remove_logo`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

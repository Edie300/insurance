import { API } from "../config";
import axios from "axios";

export const getAllClients = (token) => {
  return axios.get(`${API}/clients`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const createClient = (token) => {
  return axios.post(`${API}/clients`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getClient = (token, id) => {
  return axios.get(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getCompanyClients = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/clients`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const updateClient = (token, id) => {
  return axios.put(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const deleteClient = (token, id) => {
  return axios.delete(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

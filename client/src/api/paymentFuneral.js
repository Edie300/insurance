import { API } from "../config";
import axios from "axios";

export const getAllFuneralPayments = (token) => {
  return axios.get(`${API}/payments/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyFuneralPayments = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/payments/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createFuneralPayment = (token, accountId, data) => {
  return axios.post(`${API}/accounts/funerals/${accountId}/payments/funerals`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getSingleFuneralPackage = (token, packageId) => {
  return axios.get(`${API}/payments/funerals/${packageId}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



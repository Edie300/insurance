import { BACKEND_API , REACT_APP_API_URL } from "../config";
import axios from "axios";
import queryString from "query-string"

export const getAccountClaims = (token, id) => {
  return axios.get(`${BACKEND_API }:31816/api/claims_funeral/account_fun/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getAllFunClaims = (token) => {
  return axios.get(`${BACKEND_API}:31816/api/claims_funeral`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createFunClaim = (token, claim) => {
  return axios.post(`${BACKEND_API}:31816/api/claims_funeral/create`, claim, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getClaim = (token, id) => {
  return axios.get(`${BACKEND_API }:31816/api/claims_funeral/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};




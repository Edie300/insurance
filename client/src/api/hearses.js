import { API } from "../config";
import axios from "axios";

export const getAllHearses = (token) => {
  return axios.get(`${API}/hearses`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const createHearse = (token, formData) => {
  return axios.post(`${API}/hearses`, formData, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getHearse = (token, id) => {
  return axios.get(`${API}/hearses/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const getCompanyHearses = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/hearses`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const updateHearse = (token, id) => {
  return axios.put(`${API}/hearses/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const deleteHearse = (token, id) => {
  return axios.delete(`${API}/clients/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

import { API, BACKEND_API} from "../config";
import axios from "axios";



export const createUser = (data, token) => {
  return axios.post(`${BACKEND_API}:31596/api/users/create`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};


export const getAllUsers = (token) => {
  return axios.get(`${BACKEND_API}:31596/api/users`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyUsers = (token, companyId) => {
  return axios.get(`${BACKEND_API}:31596/api/users/companies/${companyId}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};




import { API } from "../config";
import axios from "axios";

export const getAllMedicalPackages = (token) => {
  return axios.get(`${API}/packages/medicals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyMedicalPackages = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/packages/medicals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createCompanyMedicalPackage = (token, companyId, formData) => {
  return axios.post(`${API}/companies/${companyId}/packages/medicals`, formData, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



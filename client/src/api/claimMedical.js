import { API } from "../config";
import axios from "axios";

export const getAllMedicalClaims = (token) => {
  return axios.get(`${API}/claims/medicals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyMedicalClaims = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/claims/medicals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createMedicalClaim = (token, companyId, formData) => {
  return axios.post(`${API}/companies/${companyId}/claims/medicals`, formData, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const updateMedicalClaim = (token, claimId, data) => {
  return axios.put(`${API}/claims/medicals/${claimId}`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



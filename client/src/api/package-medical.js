import { API, BACKEND_API } from "../config";
import axios from "axios";
import queryString from "query-string";

export const getPackagesMed = (token) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_medical`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyPackagesMed = (id, token) => {
  return axios.get(
    `${BACKEND_API}:32580/api/packages_medical/companies/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const createPackageMed = (token, packageMed) => {
  return axios.post(
    `${BACKEND_API}:32580/api/packages_medical/create`,
    packageMed,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const getPackageMed = (token, id) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_medical/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const editPackageFun = (token, pkg, id) => {
  return axios.put(`${BACKEND_API}:32580/api/packages_funeral/${id}`, pkg, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const publishPackageFun = (token, id) => {
  return axios.patch(`${BACKEND_API}:32580/api/packages_funeral/publish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const unpublishPackageFun = (token, id) => {
  return axios.patch(
    `${BACKEND_API}:32580/api/packages_funeral/unpublish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

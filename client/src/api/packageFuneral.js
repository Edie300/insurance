import { API } from "../config";
import axios from "axios";

export const getAllFuneralPackages = (token) => {
  return axios.get(`${API}/packages/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyFuneralPackages = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/packages/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createCompanyFuneralPackage = (token, companyId, data) => {
  return axios.post(`${API}/companies/${companyId}/packages/funerals`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getSingleFuneralPackage = (token, packageId) => {
  return axios.get(`${API}/packages/funerals/${packageId}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



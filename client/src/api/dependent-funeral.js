
import { API, BACKEND_API } from "../config";
import axios from "axios";
import queryString from "query-string"


export const getAccountFunDependents = (token, id) => {
  return axios.get(`${BACKEND_API}:31717/api/accounts_funeral/dependents/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createtFunDependent = (token, dependent) => {
  return axios.post(`${BACKEND_API}:31717/api/dependents_funeral/create`, dependent, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
















// OLD API







export const getFuneralAccounts = (token, params) => {
  const query = queryString.stringify(params)
  console.log("query::", query)
  return axios.get(`${API}/accounts/funerals?${query}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createFuneralAccount = (token, formData) => {
  return axios.post(`${API}/accounts/funerals`, formData, {
    headers: {
      Authorization: `Bearer ${token}`
    },
  });
};

export const getCompanyFuneralAccounts = (token, companyId,  params) => {
  const query = queryString.stringify(params)
  console.log("query::", query)
  console.log("company Id::", companyId)
  return axios.get(`${API}/companies/${companyId}/accounts/funerals?${query}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getFuneralAccount = (token, id) => {
  return axios.get(`${API}/accounts/funerals/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getClientFuneralAccount = (token, id) => {
  return axios.get(`${API}/accounts/funerals/client/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const deleteFuneralAccount = (token, id) => {
  return axios.delete(`${API}/accounts/funerals/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

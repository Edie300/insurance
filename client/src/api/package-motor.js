import { API, BACKEND_API } from "../config";
import axios from "axios";
import queryString from "query-string";

export const getPackagesMotor = (token) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_motor`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyPackagesMotor = (id, token) => {
  return axios.get(
    `${BACKEND_API}:32580/api/packages_motor/companies/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const createPackageMotor = (token, packageMed) => {
  return axios.post(
    `${BACKEND_API}:32580/api/packages_motor/create`,
    packageMed,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const getPackageMotor = (token, id) => {
  return axios.get(`${BACKEND_API}:32580/api/packages_motor/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const editPackagemotor = (token, pkg, id) => {
  return axios.put(`${BACKEND_API}:32580/api/packages_motor/${id}`, pkg, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const publishPackagemotor = (token, id) => {
  return axios.patch(`${BACKEND_API}:32580/api/packages_motor/publish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

export const unpublishPackagemotor = (token, id) => {
  return axios.patch(
    `${BACKEND_API}:32580/api/packages_motor/unpublish/${id}`,
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );
};

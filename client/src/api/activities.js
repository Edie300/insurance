import { API, BACKEND_API } from "../config";
import axios from "axios";
import queryString from "query-string"


export const getUserActivities = (token, id) => {
  return axios.get(`${BACKEND_API}:31846/api/activities/${id}/user`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
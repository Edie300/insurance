import { API } from "../config";
import axios from "axios";

export const getAllFuneralDependents = (token) => {
  return axios.get(`${API}/fundependents`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyFuneralDependents = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/fundependents`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createFuneralDependent = (token, accountId, dependent) => {
  return axios.post(`${API}/accounts/funerals/${accountId}/fundependents`, dependent, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getAccountFuneralDependents = (token, accountId) => {
  return axios.get(`${API}/accounts/funerals/${accountId}/fundependents`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getFuneralDependentDob = (token, id) => {
  return axios.get(`${API}/fundependents/dob/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

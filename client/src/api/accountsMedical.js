import { API } from "../config";
import axios from "axios";
import queryString from "query-string"

export const getMedicalAccounts = (token, params) => {
  const query = queryString.stringify(params)
  console.log("query::", query)
  return axios.get(`${API}/accounts/medicals?${query}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createMedicalAccount = (token, formData) => {
  return axios.post(`${API}/accounts/medicals`, formData, {
    headers: {
      Authorization: `Bearer ${token}`
    },
  });
};

export const getCompanyMedicalAccounts = (token, companyId,  params) => {
  const query = queryString.stringify(params)
  console.log("query::", query)
  console.log("company Id::", companyId)
  return axios.get(`${API}/companies/${companyId}/accounts/medicals?${query}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getMedicalAccount = (token, id) => {
  return axios.get(`${API}/accounts/medicals/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getClientMedicalAccount = (token, id) => {
  return axios.get(`${API}/accounts/medicals/client/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};
export const deletemedicalAccount = (token, id) => {
  return axios.delete(`${API}/accounts/medicals/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

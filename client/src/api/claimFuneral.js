import { API } from "../config";
import axios from "axios";

export const getAllFuneralClaims = (token) => {
  return axios.get(`${API}/claims/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getCompanyFuneralClaims = (token, companyId) => {
  return axios.get(`${API}/companies/${companyId}/claims/funerals`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const createFuneralClaim = (token, companyId, formData) => {
  return axios.post(`${API}/companies/${companyId}/claims/funerals`, formData, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const updateFuneralClaim = (token, claimId, data) => {
  return axios.put(`${API}/claims/funerals/${claimId}`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
};

export const getClaimPerYear = (token) => {
  return axios.post(`${API}/claims/funerals/claim-ratio`, {
    headers: { Authorization: `Bearer ${token}` },
  });
};



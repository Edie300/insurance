import React from "react";
import { Link } from "react-router-dom";

const Recover = () => {
  return (
    <div className="recover">
      <div className="recover__back">
        <div className="recover__btn-box u-width-1">
          <button className="btn btn--blue-border ">Back</button>
        </div>
      </div>

      <div className="recover__main">
        <div className="recover__main__form">
          <h3 className="heading-primary u-margin-bottom-small">Access Recovery</h3>
          <p className="recover__text">
            We will send you an email with a link to restore access
          </p>

          <form action="#" className="form recover__form u-margin-top-small">
            <div className="form__group u-margin-right-small">
              <input
                className="form__input"
                type="email"
                name="state"
                placeholder="Your Email address"
                required
              />
            </div>

            <div className="u-width-1 recover__btn ">
              <button className="btn btn--blue">Restore</button>
            </div>
          </form>

          <p className="recover__text">
            If you do not remember neither password nor email <br /> and the
            account really belongs to you.
            <br /> You can contact our &nbsp;
            <span>
              <Link to={'/'} className="text-blue">
                support
              </Link>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};
export default Recover;

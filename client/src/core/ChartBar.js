import React from "react";
import { Bar, Doughnut, Line, Pie, Polar, Radar } from "react-chartjs-2";

const BarChart = ({ labels, data }) => {
  return (
    <div className="u-height-1">
      <Bar
        data={{
          labels: labels,
          datasets: [
            {
              label: "Claims per year",
              backgroundColor: "#6262ff",
              borderColor: "transparent",
              borderWidth: 1,
              hoverBackgroundColor: "#0000ff",
              hoverBorderColor: "transparent",
              data: data,
            },
          ],
        }}
        options={{
          tooltips: {
            enabled: false,
          },
          legend: {
            labels: {
              fontFamily: "Poppins",
              fontSize: 14,
            },
          },
          scales: {
            yAxes: [
              {
                display: true,
                ticks: {
                  suggestedMin: 0,
                  fontFamily: "Poppins",
                },
              },
            ],
            xAxes: [
              {
                display: true,
                ticks: {
                  suggestedMin: 0,
                  fontFamily: "Poppins",
                },
              },
            ],
          },

          maintainAspectRatio: false,
        }}
      />
    </div>
  );
};
export default BarChart;

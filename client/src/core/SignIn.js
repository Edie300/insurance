import React, { useState,  useContext} from "react";

import { signin, authenticate } from "../auth/index";



import { isAuthenticated } from "../auth";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";

import ModalLoader from "../components/ModalLoader";

const SignIn = () => {
  const [values, setValues] = useState({
    email: "",
    password: "",
    error: "",
    loading: false,
    redirectToReferrer: false,
  });

  const { user } = isAuthenticated();

  const { email, password, error, loading, redirectToReferrer } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  

  const submit = (event) => {
    event.preventDefault();
    console.log({ email, password });
    setValues({ ...values, loading: true });
    signin({ email, password })
      .then((res) => {
        authenticate(res.data, () => {
          setValues({ ...values, success: true, loading: false, redirectToReferrer: true });
        });

        // dispatch({
        //   type: "LOGIN",
        //   payload: res.data.user,
        // });

        window.localStorage.setItem('user', JSON.stringify(res.data.user))



      })
      .catch((error) => {
        console.log("signin.js", error);
        setValues({ ...values, error: error, success: false, loading: false });
      });
  };

  const showError = () => (
    <div
      className="alert alert--danger"
      style={{ display: error ? "" : "none" }}
    >
      <p>Please provide a valid email and password</p>
    </div>
  );

  // const showloading = () =>
  //   loading && (
  //     <ModalLoader
  //       title="Signing in..."
  //       message="Wait a minute while we are logging you in"
  //     />
  //   );

  const redirectUser = () => {
    if (redirectToReferrer) {
      if (user && user.company.type !== 0) {
        return <Redirect to="/admin/dashboard" />;
      } else {
        return <Redirect to="/home" />;
      }
    }
    // if (isAuthenticated()) {
    //   return <Redirect to="/" />;
    // }
  };

  return (
    <div className="sign-in">
      <div class="sign-in__back">
        <div className="sign-in__btn-box u-width-1">
          <button class="btn btn--blue-border ">Back to main</button>
        </div>
      </div>

      <div className="sign-in__main">
        {showError()}
        {redirectUser()}
        <div className="sign-in__main__form">
          <h3 className="heading-primary u-margin-bottom-small">Sign In</h3>
          <form action="#" class="form">
            <div class="form__group">
              <input
                onChange={handleChange("email")}
                class="form__input"
                type="email"
                placeholder="Email"
                value={email}
                required
              />
            </div>
            <div class="form__group">
              <input
                onChange={handleChange("password")}
                class="form__input"
                type="password"
                placeholder="Password"
                value={password}
                required
              />
            </div>
            <Link
              to={"/recovery"}
              className="text-small text-blue text-semi-bold"
            >
              I do not remember password
            </Link>
            <div class="sign-in__btn u-margin-bottom-small u-margin-top-small u-width-1 ">
              <button onClick={submit} className="btn btn--blue" disabled={loading || !email || !password}>
                {loading ?("Loading...") : ( "Sign In") }               
              </button>
            </div>
            <Link to={"/"} className="text-small text-blue text-semi-bold">
              I do not have an account yet
            </Link>
          </form>
        </div>
      </div>
    </div>
  );
};
export default SignIn;

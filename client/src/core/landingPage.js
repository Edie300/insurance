import React, { useState, useEffect } from "react";
import { signin, authenticate } from "../auth/index";

import { isAuthenticated } from "../auth";
import { Redirect } from "react-router";

const Landing = () => {
  const [values, setValues] = useState({
    email: "user00@gmail.com",
    password: "12345",
    error: "",
    loading: false,
    redirectToReferrer: false,
  });

  const { user } = isAuthenticated();

  const { email, password, error, loading, redirectToReferrer } = values;

  const handleChange = (name) => (event) => {
    setValues({ ...values, error: false, [name]: event.target.value });
  };

  const submit = (event) => {
    event.preventDefault();
    console.log({ email, password });
    setValues({ ...values, loading: true });
    signin({ email, password })
      .then((res) => {
        authenticate(res.data, () => {
          setValues({ ...values, success: true, redirectToReferrer: true });
        });
      })
      .catch((error) => {
        console.log("signin.js", error);
        setValues({ ...values, error: error, success: false, loading: false });
      });
  };

  const showError = () => (
    <div
      className="alert alert-danger"
      style={{ display: error ? "" : "none" }}
    >
      email and password do not match
    </div>
  );

  const showloading = () =>
    loading && (
      <div className="alert alert-info">
        <h2 className="">Loading...</h2>
      </div>
    );

  const redirectUser = () => {
    if (redirectToReferrer) {
      if (user && user.role === "admin") {
        return <Redirect to="/dashboard" />;
      } else {
        return <Redirect to="/explore" />;
      }
    }
    if (isAuthenticated()) {
      return <Redirect to="/" />;
    }
  };

  return (
    <div className="landing-page">
      <div className="row">
        <div className="col-1-of-2">
          <div className="landing-page__sign-in">
            <form action="#" class="form">
              <div class="form__group">
                <input
                  onChange={handleChange("email")}
                  class="form__input"
                  type="email"
                  placeholder="Email"
                  value={email}
                  required
                />
              </div>
              <div class="form__group">
                <input
                  onChange={handleChange("password")}
                  class="form__input"
                  type="password"
                  placeholder="Password"
                  value={password}
                  required
                />
              </div>
              <p className="paragraph">I do not remember password</p>
              <div class="w-size3 trans-0-4 mx-auto">
                <button
                  onClick={submit}
                  class="btn btn--green"
                >
                  Sign In
                </button>
              </div>
              <p className="paragraph">
                I do not have an account yet
              </p>
            </form>
          </div>
        </div>
        <div className="col-1-of-2">
          {showError()}
          {showloading()}
          {redirectUser()}
        </div>
      </div>
    </div>
  );
};
export default Landing;

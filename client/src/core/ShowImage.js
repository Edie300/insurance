import React from "react";
import { API } from "../config";

const ShowImage = ({ url, alt="image" }) => {
  return (
    <img src={`${url}`} alt={alt} className="avater__image" />
  );
};
export default ShowImage;

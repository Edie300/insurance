import React from "react";
import { Bar, Doughnut, Line, Pie, Polar, Radar } from 'react-chartjs-2';

const doughnut = {
  labels: [
    'Red',
    'Green',
    'Yellow',
  ],
  datasets: [
    {
      data: [300, 50, 100],
      backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
      ],
      hoverBackgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
      ],
    }],
};

const options = {
  tooltips: {
    enabled: false,
  },
  maintainAspectRatio: false
}

const DoughnutChart = () => {
  return (
    <div className="u-height-1">
     <Doughnut data={doughnut} />
    </div>
  );
};
export default DoughnutChart;

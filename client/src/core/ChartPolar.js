import React from "react";
import { Bar, Doughnut, Line, Pie, Polar, Radar } from 'react-chartjs-2';

const polar = {
  datasets: [
    {
      data: [
        11,
        16,
        7,
        3,
        14,
      ],
      backgroundColor: [
        '#FF6384',
        '#4BC0C0',
        '#FFCE56',
        '#E7E9ED',
        '#36A2EB',
      ],
      label: 'My dataset' // for legend
    }],
  labels: [
    'Red',
    'Green',
    'Yellow',
    'Grey',
    'Blue',
  ],
};
const options = {
  tooltips: {
    enabled: false,
  },
  maintainAspectRatio: false
}
const PolarChart = () => {
  return (
    <div className="u-height-1">
       <Polar data={polar} options={options}/>
    </div>
  );
};
export default PolarChart;

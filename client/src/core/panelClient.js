import React, { useEffect } from "react";
import moment from "moment";
import IconClose from "../components/iconClose";
import { API } from "../config";
import ShowImage from "./ShowImage";

const PanelClient = ({ show, onClose, account }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__close">
          <h4 className="modal__name">
            {account.name} {account.surname}
          </h4>
          <div onClick={onClose} className="modal__close-icon">
            <IconClose />
          </div>
        </div>
        <div className="modal__content">
          <div className="modal__item-image">
            <p className="modal__heading">Identity document</p>
            <div className="modal__image-box">
              <ShowImage url="accounts/funerals/photo" id={account._id} />
            </div>
          </div>
          <div className="modal__grid">
            <div className="modal__item">
              <p className="modal__heading">Title</p>
              <span>{account.title}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Date of birth</p>
              <span>{moment(account.dob).format("LL")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Gender</p>
              <span>{account.gender}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Marital status</p>
              <span>{account.marital_status}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">ID number</p>
              <span>{account.national_id}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Passport number</p>
              <span>{account.passport}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Mobile</p>
              <span>{account.phone}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Email</p>
              <span>{account.email}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Address</p>
              <span>{account.address}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Policy ID</p>
              <span>{account.policy_id}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Package name</p>
              <span>{account.package.name}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Currency</p>
              <span>{account.package.currency}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Monthly premium</p>
              <span>{account.premium}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Insured persons</p>
              <span>{account.dependents.length} persons</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Product type</p>
              <span>{}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Registration Date</p>
              <span>{moment(account.createdAt).format("LL")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading"> Ages of Insured persons</p>
              {account.dependents.map((dep, i) => (
                <span key={i}>
                  {moment().diff(Date.parse(dep.dob), "years")} yrs
                </span>
              ))}
            </div>

            <div className="modal__item">
              <p className="modal__heading">Duration</p>
              <span>{moment(account.createdAt).fromNow(true)}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Last payment</p>
              <span>{moment(account.last_payment).format("LL")}</span>
            </div>
            <div className="modal__item">
              <p className="modal__heading">Subscription status</p>
              {(() => {
                switch (account.is_paid) {
                  case true:
                    return <span className="text-green">Up to date</span>;
                  case false:
                    return <span className="text-warning">Overdue</span>;
                }
              })()}
            </div>
            <div className="modal__item">
              <p className="modal__heading">policy status</p>

              {(() => {
                switch (account.status) {
                  case 0:
                    return <span className="text-green">Active</span>;
                  case 1:
                    return <span className="text-green">Suspended</span>;
                }
              })()}
            </div>
            <div className="modal__item">
              <p className="modal__heading">Options</p>
              <span className="text-blue">Show option list</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PanelClient;

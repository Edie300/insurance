import React, { useEffect } from "react";
import moment from "moment";
import IconX from ".././components/iconX";
import IconCheck from ".././components/iconCheck";
import IconClose from "../components/iconClose";
import ShowImage from ".././core/ShowImage";

const PanelProduct = ({ show, onClose, packageDetails }) => {
  const closeOnEscapeKeyDown = (e) => {
    if ((e.charCode || e.keyCode) === 27) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.addEventListener("keydown", closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener("keydown", closeOnEscapeKeyDown);
    };
  }, []);

  if (!show) {
    return null;
  }
  return (
    <div className="overlay" onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        <div className="modal__close">
          <h4 className="modal__name">{packageDetails.name}</h4>
          <div onClick={onClose} className="modal__close-icon">
            <IconClose/>
          </div>
        </div>

        <div className="modal__content">
          <div className="modal__item">
            <p className="modal__heading">Casket name</p>
            <span>{packageDetails.casket.name}</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Total accounts</p>
            <span>400</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Active accounts</p>
            <span>600</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Created</p>
            <span>{moment(packageDetails.createdAt).format("lll")}</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Casket photo</p>
            <div className="modal__image">
              <ShowImage url="caskets/photo" id={packageDetails.casket._id} />
            </div>
          </div>
          <div className="modal__item">
            <p className="modal__heading"> Last updated</p>
            <span>{moment(packageDetails.updatedAt).format("lll")}</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Benefits</p>
            <div className="modal__features">
              {packageDetails.hearse ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Hearse</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Hearse</span>
                </div>
              )}

              {packageDetails.bus ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Bus</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Bus</span>
                </div>
              )}

              {packageDetails.accidental_death ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Accidental death cover</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Accidental death cover</span>
                </div>
              )}

              {packageDetails.mortuary_services ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Mortuary services</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Mortuary services</span>
                </div>
              )}

              {packageDetails.burial_services ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Burial services</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Burial services</span>
                </div>
              )}
              {packageDetails.events_management ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Events management</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Events management</span>
                </div>
              )}

              {packageDetails.sum_assured ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Sum assured ({packageDetails.sum_assured})</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Sum assured</span>
                </div>
              )}

              {packageDetails.groceries ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Groceries: ({packageDetails.groceries})</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Groceries</span>
                </div>
              )}

              {packageDetails.cash_in_lieu ? (
                <div className="modal__features-item">
                  <IconCheck />
                  <span>Cash in lieu: ({packageDetails.cash_in_lieu})</span>
                </div>
              ) : (
                <div className="modal__features-item">
                  <IconX />
                  <span>Cash in lieu</span>
                </div>
              )}
            </div>
          </div>
          <div className="modal__item">
            <p className="modal__heading">last updated</p>
            <span> {moment(packageDetails.updatedAt).format("lll")}</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Max Dependents</p>
            <span>{packageDetails.number_of_dependents}</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Policy maturity</p>
            <span>{packageDetails.policy_maturity} yrs</span>
          </div>
          <div className="modal__item">
            <p className="modal__heading">Options</p>
            <span className="text-blue">Show option list</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PanelProduct;

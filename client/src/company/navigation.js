import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { isAuthenticated } from "../auth";
import ShowImage from ".././core/ShowImage";

import IconHome from "../components/iconHome";
import IconSettings from "../components/iconSettings";
import IconUsers from "../components/iconUsers";
import IconUser from "../components/iconUser";
import IconAnalytics from "../components/iconAnalytics";
import IconPayments from "../components/iconPayments";
import IconList from "../components/iconList";
import IconClaims from "../components/iconClaims";
import IconProducts from "../components/iconProducts";

import IMG from "../assets/images/nyaradzo.png";

const Navigation = () => {
  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  useEffect(() => {
    console.log("company::", company)
  }, []);


  return (
    <div className="navigation">
      <div className="navigation__logo-box">
        <ShowImage url={company.logo.Location} alt={company.name} />
      </div>
      <nav className="navigation__nav">
        <ul className="navigation__list">
          <li className="navigation__item navigation__item--active">
            <NavLink
              to="/home"
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconHome />
              <span>Dashboard</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/payments"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconPayments />
              <span>Payments</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/accounts"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconList />
              <span>Accounts</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/claims"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconClaims />
              <span>Claims</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/packages"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconProducts />
              <span>Packages</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/resources"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconProducts />
              <span>Resources</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/analytics"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconAnalytics />
              <span>Analytics</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/users"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconUsers />
              <span>Users</span>
            </NavLink>
          </li>
        </ul>

        {/* <ul className="navigation__list u-margin-top-small">
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/profile"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconUser />
              <span>Profile</span>
            </NavLink>
          </li>
          <li className="navigation__item navigation__item--active">
            <NavLink
              to={"/settings"}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
              className="navigation__link"
              activeClassName="navigation__link--active"
            >
              <IconSettings />
              <span>Settings</span>
            </NavLink>
          </li>
        </ul> */}
      </nav>
    </div>
  );
};
export default Navigation;

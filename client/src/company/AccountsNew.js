import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import { Switch, Route, Link, useRouteMatch, Redirect } from "react-router-dom";
import moment from "moment";
import PrivateRoute from "../auth/PrivateRoute";
import { isAuthenticated } from "../auth";

import Navigation from "./navigation";
import Header from "./header";

import Card from "../components/Card";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Clients = () => {
  const { path } = useRouteMatch();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const accounts = () => (
    <div className="accounts">
      <h1 className="heading-primary u-margin-bottom-extra-small">
        New accounts
      </h1>

      <div class="table__limiter">
        <div class=" table__wrapper">
          <div class="table">
            <div class="table__header">
              <div class="table__cell">Account ID</div>
              <div class="table__cell">Name</div>
              <div class="table__cell">Date</div>
              <div class="table__cell">User</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

            <div class=" table__row">
              <div class="table__cell">10000BC</div>
              <div class="table__cell">John smith</div>
              <div class="table__cell">July 11, 2021</div>
              <div class="table__cell">Frank</div>
            </div>

           
          </div>
        </div>
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            Accounts
            <Chevron />
          </Link>

          <span className="content__current">New registrations</span>
        </div>

        <div className="content__pages">{accounts()}</div>
      </div>
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Clients;

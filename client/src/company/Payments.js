import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreatePayment from "../components/ModalCreatePayment";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Payments = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewPayment, setShowNewPayment] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [payments, setPayments] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getMainAccounts(token)
      .then((res) => {
        console.log("response", res.data);
        setPayments(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const viewClaim = (id) => {
    history.push(`${path}/${id}`);
  };

  const accountTable = () => (
    <div className="payments__container">
      <div className="payments__header">
        <h3 className="payments__title">
          Payments <span>({payments.length})</span>
        </h3>
        <div className="payments__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewPayment(true)}
            >
              New payment
            </button>
          </div>
        </div>
      </div>
      <div className="payments__filter">
        <div className="payments__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="account number"
          />
          <button className="btn">Search</button>
        </div>
        <div
          className="payments__settings"
          onClick={() => setShowFilters(true)}
        >
          <Settings />
        </div>
        <div className="payments__pagination">
          <div className="payments__previous">
            <Chevron />
          </div>
          <div className="payments__page">
            <p>2</p>
          </div>
          <div className="payments__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Account ID</div>
          <div class="accounts__table-cell">Type</div>          
          <div class="accounts__table-cell">Amount</div>
          <div class="accounts__table-cell">Balance</div>
          <div class="accounts__table-cell">Method</div>
          <div class="accounts__table-cell">Date</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching claims"} />}

        {!loading && payments.length < 1 && (
          <div className="payments__no-accounts">
            <p>No payments</p>
            <span>
              There are no payments available to display. To make a payment
               click new payment
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => setShowNewPayment(true)}
              >
                New claim
              </button>
            </div>
          </div>
        )}

        {!loading &&
          payments.length > 0 &&
          payments.map((payment, i) => (
            <div
              class="accounts__table-row"
              key={payment.id}
              onClick={() => viewClaim(payment.id)}
            >
              <div class=" accounts__table-cell">{payment.account_id}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                something...
                </p>
              </div>
              <div class=" accounts__table-cell">something...</div>
              <div class=" accounts__table-cell">something...</div>
              <div class=" accounts__table-cell">something...</div>
              <div class=" accounts__table-cell">something...</div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (payment.status) {
                    case 0:
                      return <span className="text-blue ">pending</span>;
                    case 1:
                      return <span className="text-green">active</span>;
                    case 2:
                      return <span className="text-warning">lapsed</span>;
                    case 3:
                      return <span className="text-danger">archived</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Payments</span>
        </div>
        <div className="payments__">
          <h2 className="payments__heading">Payments</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreatePayment
        onClose={() => {
          setShowNewPayment(false);
        }}
        show={showNewPayment}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Payments;

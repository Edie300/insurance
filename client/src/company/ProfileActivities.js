import React from "react";

const ProfileActivities = () => {
  const content = () => (
    <div className="content">
      <h2>Personal Activities</h2>
    </div>
  );

  return <div>{content()}</div>;
};
export default ProfileActivities;

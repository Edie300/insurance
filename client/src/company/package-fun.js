import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useHistory,
  useParams,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";

import ModalEditFunPackage from "../components/ModalEditFunPackage";
import Action from "../components/Action";

import { publishPackageFun } from "../api/package-funeral";
import { unpublishPackageFun } from "../api/package-funeral";

import { getPackageFun } from "../api/package-funeral";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const PackageFun = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [pkg, setPkg] = useState({});
  const [loading, setLoading] = useState(false);
  const [reload, setReload] = useState(false);
  const [showEditPackage, setShowEditPackage] = useState(false);
  const [showPublishPackage, setShowPublishPackage] = useState(false);
  const [showDeletePackage, setShowDeletePackage] = useState(false);

  const init = () => {
    setLoading(true);
    getPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
        setPkg(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, [setReload]);

  const publish = () => {
    publishPackageFun(token, id)
      .then((res) => {

      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const unpublish = () => {
    unpublishPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const packageDetails = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package details</h3>
        <div className="account__options">
          {pkg && pkg.status === "1" && (
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => unpublish()}
              >
                Unpublish
              </button>
            </div>
          )}
          {pkg && pkg.status === "0" && (
            <div className="">
              <button
                className="btn btn--blue-grey"
                type="button"
                onClick={() => setShowPublishPackage(true)}
              >
                Publish
              </button>
            </div>
          )}
          <div className="">
            <button
              className="btn btn--blue-grey"
              type="button"
              onClick={() => setShowEditPackage(true)}
            >
              Edit
            </button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Delete</button>
          </div>
        </div>
      </div>
      {loading && <Loader message={"Fetching package details"} />}
      {!loading && pkg && (
        <div className="package__container">
          <div class="package__details">
            <div className="package__item">
              <p className="">Package</p>
              <span>{pkg.name}</span>
            </div>
            <div className="package__item">
              <p className="">Casket</p>
              {pkg && pkg.casket && <span>{pkg.casket.name}</span>}
            </div>
            <div className="package__item">
              <p className="">lapse period</p>
              <span>{pkg.policy_lapse_period} months</span>
            </div>
            <div className="package__item">
              <p className="">archive period</p>
              <span>{pkg.policy_archive_period} months</span>
            </div>
            <div className="package__item">
              <p className="">payment grace period</p>
              <span>{pkg.payment_grace_period} months</span>
            </div>
            <div className="package__item">
              <p className="">Waiting period</p>
              <span>{pkg.waiting_period} months</span>
            </div>
            <div className="package__item">
              <p className="">Principal minimum age</p>
              <span>{pkg.principal_member_min_age} years</span>
            </div>
            <div className="package__item">
              <p className="">Principal maximum age</p>
              <span>{pkg.principal_member_max_age} years</span>
            </div>
            <div className="package__item">
              <p className="">Dependent maximum age</p>
              <span>{pkg.dependent_max_age} years</span>
            </div>
            <div className="package__item">
              <p className="">Dependent limit</p>
              <span>{pkg.dependents_max_count}</span>
            </div>
            <div className="package__item">
              <p className="">policy maturity</p>
              <span>{pkg.policy_maturity} years</span>
            </div>
            <div className="package__item">
              <p className="">Child maximum age</p>
              <span>{pkg.child_max_age} years</span>
            </div>
            <div className="package__item">
              <p className="">Child at school maximum age</p>
              <span>{pkg.child_max_age_school} years</span>
            </div>
            <div className="package__item">
              <p className="">Sum assured</p>
              <span>{pkg.sum_assured}</span>
            </div>
            <div className="package__item">
              <p className="">Cash in lieu</p>
              <span>{pkg.cash_in_lieu}</span>
            </div>
            <div className="package__item">
              <p className="">groceries</p>
              <span>{pkg.groceries}</span>
            </div>
            <div className="package__item">
              <p className="">Date created</p>
              <span>{moment(pkg.created_at).format("LLL")}</span>
            </div>
            <div className="package__item">
              <p className="">created by</p>
              {pkg && pkg.created_by && (
                <span>
                  {pkg.created_by.name} {pkg.created_by.surname}
                </span>
              )}
            </div>
            <div className="package__item">
              <p className="">Status</p>

              {(() => {
                switch (pkg.status) {
                  case "0":
                    return (
                      <span className="badge badge__blue">unpublished</span>
                    );
                  case "1":
                    return (
                      <span className="badge badge__green">published</span>
                    );
                  case "2":
                    return <span className="badge badge__red">deleted</span>;
                }
              })()}
            </div>
          </div>
        </div>
      )}
    </div>
  );

  const packageBenefits = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package benefits</h3>
      </div>
      <div className="package__container">
        <div className="package__benefits">
          <div className="package__benefit">
            {pkg.hearse ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">hearse</p>
          </div>
          <div className="package__benefit">
            {pkg.bus ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">bus</p>
          </div>
          <div className="package__benefit">
            {pkg.accidental_death ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">accidenta death</p>
          </div>
          <div className="package__benefit">
            {pkg.mortuary_services ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">Mortuary services</p>
          </div>
          <div className="package__benefit">
            {pkg.events_management ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">Events management</p>
          </div>
          <div className="package__benefit">
            {pkg.burial_services ? (
              <div className="package__true">
                <Check />
              </div>
            ) : (
              <div className="package__false">
                <X />
              </div>
            )}
            <p className="">burial services</p>
          </div>
        </div>
      </div>
    </div>
  );

  const packageRates = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package Rates</h3>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Minimum age</div>
          <div class="accounts__table-cell">Maximum age</div>
          <div class="accounts__table-cell">Rate</div>
        </div>

        {!loading &&
          pkg &&
          pkg.individual_rates &&
          pkg.individual_rates.length > 0 &&
          pkg.individual_rates.map((rate, i) => (
            <div class="accounts__table-row" key={i}>
              <div class=" accounts__table-cell">{rate.min_age}</div>
              <div class=" accounts__table-cell">{rate.max_age}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">{rate.rate}</p>
              </div>
            </div>
          ))}
      </div>

      <div className="package__container">
        <div className="package__premiums">
          <div className="package__item">
            <p className="">payment type</p>
            <span>{pkg.payment_type}</span>
          </div>
          <div className="package__item">
            <p className="">currency</p>
            <span>{pkg.currency}</span>
          </div>
          <div className="package__item">
            <p className="">family premium</p>
            <span>{pkg.premium_family}</span>
          </div>
          <div className="package__item">
            <p className="">dependent</p>
            <span>{pkg.premium_dependent}</span>
          </div>
        </div>
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/packages" className="content__link">
            Packages
            <Chevron />
          </Link>

          <span className="content__current">funeral package (2342354)</span>
        </div>
        <div className="accounts__">
          {pkg && <h2 className="accounts__heading">{pkg.name}</h2>}
        </div>

        <div className="content__pages">
          {packageDetails()}
          {packageBenefits()}
          {packageRates()}
        </div>
      </div>
      {!loading && pkg && pkg.casket && (
        <ModalEditFunPackage
          onClose={() => {
            setShowEditPackage(false);
          }}
          show={showEditPackage}
          pkg={pkg}
        />
      )}

      {!loading && pkg && (
        <Action
          onClose={() => {
            setShowPublishPackage(false);
          }}
          show={showPublishPackage}
          action={publish}
          title={"Do you want to publish?"}
          paragraph={
            "  Once package has been published, it will now be available to create policies"
          }
        />
      )}
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default PackageFun;

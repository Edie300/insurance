import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import moment from "moment";
import Navigation from "./navigation";
import Header from "./header";
import Card from "../components/Card";
import { getUserActivities } from "../api/activities";

import { isAuthenticated } from "../auth";

import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Home = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [activities, setActivities] = useState([]);
  const [loading, setLoading] = useState(false);

  const init = () => {
    setLoading(true);
    getUserActivities(token, id)
      .then((res) => {
        console.log("response", res.data);
        setActivities(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const activity = (type, data, id) => {
    let _data = JSON.parse(data);
    switch (type) {
      case "package-fun:created":
        return (
          <p>
            Created funeral package{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.name}</span>
          </p>
        );
      case "package-fun:updated":
        return (
          <p>
            Updated funeral package{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.name}</span>
          </p>
        );


      case "account-main:created":
        return (
          <p>
            Created new account{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.account_id}</span>
          </p>
        );
      case "account-fun:created":
        return (
          <p>
            Created funeral policy{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.policy_id}</span>
          </p>
        );

      case "account-fun:updated":
        return (
          <p>
            Updated funeral policy{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.policy_id}</span>
          </p>
        );
      case "package-med:created":
        return (
          <p>
            Created medical package{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.name}</span>
          </p>
        );
      case "account-med:created":
        return (
          <p>
            Created medical policy{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.policy_id}</span>
          </p>
        );

      case "package-motor:created":
        return (
          <p>
            Created motor package{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.name}</span>
          </p>
        );

      case "casket-created":
        return (
          <p>
            Created casket{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.name}</span>
          </p>
        );

      case "claim-fun:created":
        return (
          <p>
            Created funeral claim for account{" "}
            <span onClick={() => viewEntity(type, id)}>{_data.account_id}</span>
          </p>
        );
    }
  };

  const viewEntity = (type, id) => {
    if (type === "casket-created") {
      return history.push(`/resources/caskets/${id}`);
    }
    if (type === "package-fun:created" || type === "package-fun:updated") {
      return history.push(`/packages/funeral/${id}`);
    }

    if (type === "package-med:created" || type === "package-med:updated") {
      return history.push(`/packages/medical/${id}`);
    }

    if (type === "package-motor:created" || type === "package-motor:updated") {
      return history.push(`/packages/motor/${id}`);
    }

    // "account-main:created"

    if (type === "account-main:created") {
      return history.push(`/accounts/${id}`);
    }

    if (type === "account-fun:created") {
      return history.push(`/accounts/funeral/${id}`);
    }

    if (type === "account-med:created") {
      return history.push(`/accounts/medical/${id}`);
    }

    if (type === "claim-fun:created") {
      return history.push(`/home`);
    }
  };

  // const viewPackage = (type, id) => {
  //   switch (type) {
  //     case 0:
  //       return history.push(`${path}/funeral/${id}`);
  //     case 1:
  //       return history.push(`${path}/medical/${id}`);
  //     case 2:
  //       return history.push(`${path}/motor/${id}`);
  //   }
  // };

  const content = () => (
    <div className="content">
      <div className="content__breadcrum">
        <span className="content__current">Dashboard</span>
      </div>
      <h1 className="heading-primary">Overview</h1>
      <div className="home__cards ">
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
        <div className="home__item">
          <Card />
        </div>
      </div>
      <div className="accounts__container">
        <div class=" activities__table">
          <div class=" activities__table-header">
            <div class="activities__table-event">Activity</div>
            <div class="activities__table-date">Date</div>
          </div>
          {!loading &&
            activities.length > 0 &&
            activities.map((act, i) => (
              <div class="activities__table-row">
                <div class=" activities__table-event">
                  {activity(
                    act.activity_type,
                    act.activity_data,
                    act.entity_id
                  )}
                </div>
                <div class=" activities__table-date">
                  {moment(act.date).format("lll")}
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
      {content()}
    </div>
  );
};
export default Home;

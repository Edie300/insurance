import React, { useState, useEffect } from "react";
import {
  Switch,
  Route,
  NavLink,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";
import { getCompanyCaskets } from "../api/caskets";
import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateCasket from "../components/ModalCreateCasket";

const Resources = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(1);
  const [caskets, setCaskets] = useState([]);
  const [showNewCasket, setShowNewCasket] = useState(false);
  const [loading, setLoading] = useState(false);

  const init = () => {
    setLoading(true);
    getCompanyCaskets(company.id, token)
      .then((res) => {
        console.log("response", res.data);
        setCaskets(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const viewCasket = (id) => {
    history.push(`${path}/caskets/${id}`);
  };

  const content = () => (
    <div className="content">
      <div className="content__breadcrum">
        <span className="content__current">Resources</span>
      </div>
      <div className="accounts__">
        <h2 className="accounts__heading">Resources</h2>
      </div>
      <div className="account__nav">
        <ul className="account__nav-list">
          <p
            className={
              tab === 1 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(1)}
          >
            Caskets
          </p>
          <p
            className={
              tab === 2 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(2)}
          >
            Hearses
          </p>
          <p
            className={
              tab === 3 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(3)}
          >
            Ambulances
          </p>
          <p
            className={
              tab === 4 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(4)}
          >
            Buses
          </p>
          <p
            className={
              tab === 5 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(5)}
          >
            Fuel
          </p>
          <p
            className={
              tab === 6 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(6)}
          >
            Tents
          </p>
          <p
            className={
              tab === 7 ? "account__nav-item--active" : "account__nav-item"
            }
            onClick={() => setTab(7)}
          >
            Mobile Toilets
          </p>
        </ul>
      </div>
      <div className="resources__">
        {tab === 1 && (
          <div className="accounts__container">
            <div className="accounts__header">
              <h3 className="accounts__title">
                Caskets <span>({caskets.length})</span>
              </h3>
              <div className="accounts__options">
                <div className="">
                  <button
                    className="btn btn--blue btn--disabled"
                    type="button"
                    onClick={() => setShowNewCasket(true)}
                  >
                    Create Casket
                  </button>
                </div>
              </div>
            </div>

            <div class=" accounts__table">
              <div class=" accounts__table-header">
                <div class="accounts__table-cell">Name</div>
                <div class="accounts__table-cell">Quantity</div>
                <div class="accounts__table-cell">Date created</div>
                <div class="accounts__table-cell">Created by</div>
                <div class="accounts__table-cell">Status</div>
              </div>

              {loading && <Loader message={"Fetching accounts"} />}

              {!loading && caskets.length < 1 && (
                <div className="accounts__no-accounts">
                  <p>No caskets</p>
                  <span>
                    There are no caskets to manage. to start adding caskets
                    click create casket
                  </span>
                  <div className="">
                    <button
                      className="btn btn--blue-grey"
                      onClick={() => setShowNewCasket(true)}
                    >
                      Create casket
                    </button>
                  </div>
                </div>
              )}
              {caskets.map((casket, i) => (
                <div
                  class="accounts__table-row"
                  key={casket.id}
                  onClick={() => viewCasket(casket.id)}
                >
                  <div class=" accounts__table-cell">
                    <p className="accounts__table-name">{casket.name}</p>
                  </div>
                  <div class=" accounts__table-cell">{casket.quantity}</div>
                  <div class=" accounts__table-cell">
                    {moment(casket.created_at).format("LL")}
                  </div>
                  <div class=" accounts__table-cell">
                    {casket.created_by.name} {casket.created_by.surname}
                  </div>

                  <div class=" accounts__table-cell">
                    {(() => {
                      switch (casket.status) {
                        case "0":
                          return <span className="badge badge__green">active</span>;
                        case "1":
                          return <span className="badge badge__orange">out of order</span>;
                        case "2":
                          return <span className="badge badge__orange">Deleted</span>;
                      }
                    })()}
                  </div>
                </div>
              ))}
            </div>
            <ModalCreateCasket
              onClose={() => {
                setShowNewCasket(false);
              }}
              show={showNewCasket}
            />
          </div>
        )}
        {tab === 2 && <div className="accounts__container">hearses</div>}
        {tab === 3 && <div className="accounts__container">ambulance</div>}
        {tab === 4 && <div className="accounts__container">buses</div>}
        {tab === 5 && <div className="accounts__container">fuel</div>}
        {tab === 6 && <div className="accounts__container">tents</div>}
        {tab === 7 && <div className="accounts__container">mobile toilets</div>}
      </div>
    </div>
  );

  return (
    <div>
      <Navigation />
      <Header />
      {content()}
    </div>
  );
};
export default Resources;

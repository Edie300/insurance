import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useParams,
  useHistory,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccount, getSubAccounts } from "../api/account-main";
import { getCasket } from "../api/caskets";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ShowImage from "../core/ShowImage";

import ModalEditCasket from "../components/ModalEditCasket";

import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const Casket = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(2);
  const [showEditCasket, setShowEditCasket] = useState(false);
  const [casket, setCasket] = useState();
  const [casketId, setCasketId] = useState(id);
  const [policies, setPolicies] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getCasket(casketId, token)
      .then((res) => {
        console.log("response", res.data);
        setCasket(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, []);

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/resources" className="content__link">
            Resources
            <Chevron />
          </Link>
          {casket && (
            <span className="content__current">casket ({casket.name})</span>
          )}
        </div>
        <div className="accounts__">
          {casket && <h2 className="accounts__heading">{casket.name}</h2>}
        </div>

        <div className="account__container">
          <div className="account__header">
            <h3 className="account__title">Casket details</h3>
            <div className="account__options">
              <div className="">
                <button
                  className="btn btn--blue-grey"
                  onClick={() => setShowEditCasket(true)}
                >
                  Edit
                </button>
              </div>
              <div className="">
                <button className="btn btn--blue-grey">Delete</button>
              </div>
            </div>
          </div>
          {loading && <Loader message={"Fetching policy details"} />}
          {!loading && casket && (
            <>
              <div className="casket__info">
                <div className="casket__status">
                  <div className="account__item">
                    <p className="">Status</p>
                    {(() => {
                      switch (casket.status) {
                        case "0":
                          return (
                            <span className="badge badge__green">active</span>
                          );
                        case "1":
                          return (
                            <span className="badge badge__orange">
                              out of order
                            </span>
                          );
                        case "2":
                          return (
                            <span className="badge badge__orange">Deleted</span>
                          );
                      }
                    })()}
                  </div>
                </div>
                <div className="casket__details">
                  <div className="casket__item">
                    <p className="">Name</p>
                    <span>{casket.name}</span>
                  </div>
                  <div className="casket__item">
                    <p className="">Quantity</p>
                    <span>{casket.quantity}</span>
                  </div>
                </div>
              </div>
              <div className="casket__descr">
                <p>Description</p>
                <span>{casket.description}</span>
              </div>

              <p className="casket__name">Images</p>
              <div className="casket__images">
                {casket &&
                  casket.images.map((image, i) => (
                    <div className="casket__image-box" key={i}>
                      <ShowImage url={image.Location} alt={casket.name} />
                    </div>
                  ))}
              </div>
            </>
          )}
        </div>
      </div>
      {casket && (
        <ModalEditCasket
          onClose={() => {
            setShowEditCasket(false);
          }}
          show={showEditCasket}
          casket={casket}
        />
      )}
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Casket;

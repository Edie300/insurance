import React from "react";

const ProfileSecurity = () => {
  const content = () => (
    <div className="content">
      <h2>Personal Security</h2>
    </div>
  );

  return <div>{content()}</div>;
};
export default ProfileSecurity;

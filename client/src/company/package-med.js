import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useHistory,
  useParams,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";

import ModalEditFunPackage from "../components/ModalEditFunPackage";
import Action from "../components/Action";

import { publishPackageFun } from "../api/package-funeral";
import { unpublishPackageFun } from "../api/package-funeral";

import { getPackageMed } from "../api/package-medical";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const PackageMed = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [pkg, setPkg] = useState({});
  const [loading, setLoading] = useState(false);
  const [reload, setReload] = useState(false);
  const [showEditPackage, setShowEditPackage] = useState(false);
  const [showPublishPackage, setShowPublishPackage] = useState(false);
  const [showDeletePackage, setShowDeletePackage] = useState(false);

  const init = () => {
    setLoading(true);
    getPackageMed(token, id)
      .then((res) => {
        console.log("response", res.data);
        setPkg(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    init();
  }, [setReload]);

  const publish = () => {
    publishPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const unpublish = () => {
    unpublishPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const packageDetails = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package details</h3>
        <div className="account__options">
          {pkg && pkg.status === "1" && (
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => unpublish()}
              >
                Unpublish
              </button>
            </div>
          )}
          {pkg && pkg.status === "0" && (
            <div className="">
              <button
                className="btn btn--blue-grey"
                type="button"
                onClick={() => setShowPublishPackage(true)}
              >
                Publish
              </button>
            </div>
          )}
          <div className="">
            <button
              className="btn btn--blue-grey"
              type="button"
              onClick={() => setShowEditPackage(true)}
            >
              Edit
            </button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Delete</button>
          </div>
        </div>
      </div>
      {loading && <Loader message={"Fetching package details"} />}
      {!loading && pkg && (
        <div className="package__container">
          <div class="package__details">
            <div className="package__item">
              <p className="">Name</p>
              <span>{pkg.name}</span>
            </div>
            <div className="package__item">
              <p className="">Annual global limit</p>
             <span>{pkg.annual_global_limit}</span>
            </div>
            <div className="package__item">
              <p className="">General practioner</p>
              <span>{pkg.general_practitioners}</span>
            </div>
            <div className="package__item">
              <p className="">number of initial visits</p>
              <span>{pkg.number_of_initial_visits} </span>
            </div>
            <div className="package__item">
              <p className="">medical specialist</p>
              <span>{pkg.medical_specialist}</span>
            </div>
            <div className="package__item">
              <p className="">hospitalization limit</p>
              <span>{pkg.hospitalization_limit}</span>
            </div>
            <div className="package__item">
              <p className="">Waiting time</p>
              <span>{pkg.waiting_time} months</span>
            </div>
            <div className="package__item">
              <p className="">Claim processing time</p>
              <span>{pkg.claim_processing_time} hrs</span>
            </div>
            <div className="package__item">
              <p className="">private hospitalization</p>
              <span>{pkg.private_hospitalization}</span>
            </div>
            <div className="package__item">
              <p className="">Private hospitalization CV19</p>
              <span>{pkg.private_hospitalization_CV19}</span>
            </div>
            <div className="package__item">
              <p className="">public hospitalization</p>
              <span>{pkg.public_hospitalization} years</span>
            </div>
            <div className="package__item">
              <p className="">Number of dependents</p>
              <span>{pkg.number_of_dependents} years</span>
            </div>
            <div className="package__item">
              <p className="">Child at school maximum age</p>
              <span>{pkg.child_max_age_school} years</span>
            </div>
            <div className="package__item">
              <p className="">Maximum age limit</p>
              <span>{pkg.age_limit_max}</span>
            </div>
            <div className="package__item">
              <p className="">Minimum age limit</p>
              <span>{pkg.age_limit_min}</span>
            </div>
            <div className="package__item">
              <p className="">drugs 3ben</p>
              <span>{pkg.drugs_3ben}</span>
            </div>
            <div className="package__item">
              <p className="">drugs abv 3ben</p>
              <span>{pkg.drugs_abv_3ben}</span>
            </div>
            <div className="package__item">
              <p className="">chronic drug limit</p>
              <span>{pkg.chronic_drug_limit}</span>
            </div>
            <div className="package__item">
              <p className="">drugs cv19</p>
              <span>{pkg.drugs_cv19}</span>
            </div>
            <div className="package__item">
              <p className="">drugs acute</p>
              <span>{pkg.drugs_acute}</span>
            </div>
            <div className="package__item">
              <p className="">dental</p>
              <span>{pkg.dental}</span>
            </div>
            <div className="package__item">
              <p className="">drugs 3ben</p>
              <span>{pkg.drugs_3ben}</span>
            </div>
            <div className="package__item">
              <p className="">optical</p>
              <span>{pkg.optical}</span>
            </div>
            <div className="package__item">
              <p className="">psychiatric</p>
              <span>{pkg.psychiatric}</span>
            </div>
            <div className="package__item">
              <p className="">physiotherapy</p>
              <span>{pkg.physiotherapy}</span>
            </div>
            <div className="package__item">
              <p className="">prosthetics</p>
              <span>{pkg.prosthetics}</span>
            </div>
            <div className="package__item">
              <p className="">pathology</p>
              <span>{pkg.pathology}</span>
            </div>
            <div className="package__item">
              <p className="">specialized_radiology</p>
              <span>{pkg.specialized_radiology}</span>
            </div>
            <div className="package__item">
              <p className="">blood_transfusion</p>
              <span>{pkg.blood_transfusion}</span>
            </div>
            <div className="package__item">
              <p className="">maternity</p>
              <span>{pkg.maternity}</span>
            </div>
            <div className="package__item">
              <p className="">family planning</p>
              <span>{pkg.maternity}</span>
            </div>
            <div className="package__item">
              <p className="">infertility</p>
              <span>{pkg.infertility}</span>
            </div>
            <div className="package__item">
              <p className="">hearing aids</p>
              <span>{pkg.hearing_aids}</span>
            </div>
            <div className="package__item">
              <p className="">hearing_scan</p>
              <span>{pkg.hearing_scan}</span>
            </div>
            <div className="package__item">
              <p className="">funeral cashback</p>
              <span>{pkg.funeral_cashback}</span>
            </div>
            <div className="package__item">
              <p className="">premium_waver</p>
              <span>{pkg.premium_waver}</span>
            </div>
            <div className="package__item">
              <p className="">wellness</p>
              <span>{pkg.wellness}</span>
            </div>
            <div className="package__item">
              <p className="">policy_maturity</p>
              <span>{pkg.policy_maturity} years</span>
            </div>
            <div className="package__item">
              <p className="">service area</p>
              <span>{pkg.service_area}</span>
            </div>
            <div className="package__item">
              <p className="">child max age</p>
              <span>{pkg.child_max_age}</span>
            </div>

            <div className="package__item">
              <p className="">child max age school</p>
              <span>{pkg.child_max_age_school}</span>
            </div>

            <div className="package__item">
              <p className="">wellness</p>
              <span>{pkg.wellness}</span>
            </div>

            <div className="package__item">
              <p className="">Date created</p>
              <span>{moment(pkg.created_at).format("LLL")}</span>
            </div>
            <div className="package__item">
              <p className="">created by</p>
              {pkg && pkg.created_by && (
                <span>
                  {pkg.created_by.name} {pkg.created_by.surname}
                </span>
              )}
            </div>
            <div className="package__item">
              <p className="">Status</p>

              {(() => {
                switch (pkg.status) {
                  case "0":
                    return (
                      <span className="badge badge__blue">unpublished</span>
                    );
                  case "1":
                    return (
                      <span className="badge badge__green">published</span>
                    );
                  case "2":
                    return <span className="badge badge__red">deleted</span>;
                }
              })()}
            </div>
          </div>
        </div>
      )}
    </div>
  );

  const packageWaiting = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package waiting periods</h3>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Period</div>
          <div class="accounts__table-cell">Benefits</div>
        </div>

        {!loading &&
          pkg &&
          pkg.waiting_periods &&
          pkg.waiting_periods.length > 0 &&
          pkg.waiting_periods.map((period, i) => (
            <div class="accounts__table-row" key={i}>
              <div class=" accounts__table-cell">{period.period} months</div>
              <div class=" accounts__table-cell">{period.benefits.map((benefit, i) => (<span>{benefit}, </span>))}</div>
            </div>
          ))}
      </div>
        
   
    </div>
  );

  const packageRates = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Package Rates</h3>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Minimum age</div>
          <div class="accounts__table-cell">Maximum age</div>
          <div class="accounts__table-cell">Rate</div>
        </div>

        {!loading &&
          pkg &&
          pkg.individual_rates &&
          pkg.individual_rates.length > 0 &&
          pkg.individual_rates.map((rate, i) => (
            <div class="accounts__table-row" key={i}>
              <div class=" accounts__table-cell">{rate.min_age}</div>
              <div class=" accounts__table-cell">{rate.max_age}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">{rate.rate}</p>
              </div>
            </div>
          ))}
      </div>

      <div className="package__container">
        <div className="package__premiums">
          <div className="package__item">
            <p className="">payment type</p>
            <span>{pkg.payment_type}</span>
          </div>
          <div className="package__item">
            <p className="">currency</p>
            <span>{pkg.currency}</span>
          </div>
          <div className="package__item">
            <p className="">family premium</p>
            <span>{pkg.premium_family}</span>
          </div>
          <div className="package__item">
            <p className="">dependent</p>
            <span>{pkg.premium_dependent}</span>
          </div>
        </div>
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/packages" className="content__link">
            Packages
            <Chevron />
          </Link>

          <span className="content__current">medical package (2342354)</span>
        </div>
        <div className="accounts__">
          {pkg && <h2 className="accounts__heading">{pkg.name}</h2>}
        </div>

        <div className="content__pages">
          {packageDetails()}
          {packageWaiting()}
          {packageRates()}
        </div>
      </div>
      {!loading && pkg && pkg.casket && (
        <ModalEditFunPackage
          onClose={() => {
            setShowEditPackage(false);
          }}
          show={showEditPackage}
          pkg={pkg}
        />
      )}

      {!loading && pkg && (
        <Action
          onClose={() => {
            setShowPublishPackage(false);
          }}
          show={showPublishPackage}
          action={publish}
          title={"Do you want to publish?"}
          paragraph={
            "  Once package has been published, it will now be available to create policies"
          }
        />
      )}
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default PackageMed;

import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useHistory,
  useParams,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";
import { getFunAccount } from "../api/account-funeral";
import { getAccountFunDependents } from "../api/dependent-funeral";
import { getAccountClaims } from "../api/claims-funeral";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateDependent from "../components/ModalCreateDependent";
import ModalCreateClaim from "../components/ModalCreateClaim";
import ModalEditAccountFun from "../components/ModalEditAccountFun";
import ModalCreatePayment from "../components/ModalCreatePayment";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";

const AccountFun = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(1);
  const [showNewDep, setShowNewDep] = useState(false);
  const [showNewClaim, setShowNewClaim] = useState(false);
  const [showNewPayment, setShowNewPayment] = useState(false);
  const [showEditAccount, setShowEditAccount] = useState(false);
  const [account, setAccount] = useState([]);
  const [dependents, setDependents] = useState([]);
  const [claims, setClaims] = useState([]);
  const [payments, setPayments] = useState([]);
  const [loading, setLoading] = useState(true);

  const init = () => {
    setLoading(true);
    getFunAccount(token, id)
      .then((res) => {
        console.log("response", res.data);
        setAccount(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const getDependents = () => {
    getAccountFunDependents(token, id)
      .then((res) => {
        console.log("response", res.data);
        setDependents(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const getClaims = () => {
    getAccountClaims(token, id)
      .then((res) => {
        console.log("response", res.data);
        setClaims(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    init();
  }, []);
  useEffect(() => {
    getDependents();
  }, []);
  useEffect(() => {
    getClaims();
  }, []);

  const AccountDependents = () => (
    <div className="accounts__container">
      <div className="accounts__header">
        <h3 className="accounts__title">
          Dependents <span>({dependents.length})</span>
        </h3>
        <div className="accounts__options">
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewDep(true)}
            >
              Add dependent
            </button>
          </div>
        </div>
      </div>

      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Name</div>
          <div class="accounts__table-cell">ID number</div>
          <div class="accounts__table-cell">Date of birth</div>
          <div class="accounts__table-cell">gender</div>
          <div class="accounts__table-cell">relationship</div>
          <div class="accounts__table-cell">occupation</div>
          <div class="accounts__table-cell">Date Added</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {!loading && dependents.length === 0 && (
          <div className="accounts__no-accounts">
            <p>No dependents</p>
            <span>
              There are no dependents to this account. to add a dependent click
              add dependent
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey "
                onClick={() => setShowNewDep(true)}
              >
                Add dependent
              </button>
            </div>
          </div>
        )}
        {!loading &&
          dependents.length > 0 &&
          dependents.map((dependent, i) => (
            <div class="accounts__table-row">
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                  {dependent.name} {dependent.surname}
                </p>
              </div>
              <div class=" accounts__table-cell">{dependent.national_id}</div>
              <div class=" accounts__table-cell">
                {moment(dependent.date_of_birth).format("LL")}{" "}
              </div>
              <div class=" accounts__table-cell">{dependent.gender}</div>
              <div class=" accounts__table-cell">{dependent.relationship}</div>
              <div class=" accounts__table-cell">{dependent.occupation}</div>
              <div class=" accounts__table-cell">
                {moment(dependent.created_at).format("LLL")}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (dependent.status) {
                    case "0":
                      return <span className="badge badge__green">active</span>;
                    case "1":
                      return <span className="badge badge__green">active</span>;
                    case "2":
                      return <span className="badge badge__orange">deceased</span>;
                    case "3":
                      return <span className="badge badge__red">deleted</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
      {account.package_fun && (
        <ModalCreateDependent
          onClose={() => {
            setShowNewDep(false);
          }}
          show={showNewDep}
          accountFun={account.id}
          packageFun={account.package_fun.id}
        />
      )}
    </div>
  );

  const accountClaims = () => (
    <div className="accounts__container">
      <div className="accounts__header">
        <h3 className="accounts__title">
          Claims <span>({claims.length})</span>
        </h3>
        <div className="accounts__options">
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewClaim(true)}
            >
              New claim
            </button>
          </div>
        </div>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Claim ID</div>
          <div class="accounts__table-cell">Deceased type</div>
          <div class="accounts__table-cell">Claiment Name</div>
          <div class="accounts__table-cell">Sum Assured</div>
          <div class="accounts__table-cell">Date created</div>
          <div class="accounts__table-cell">Status</div>
        </div>
        {!loading && claims.length === 0 && (
          <div className="accounts__no-accounts">
            <p>No claims</p>
            <span>
              There are no claims to this account. to create claim click new
              claim
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey "
                onClick={() => setShowNewClaim(true)}
              >
                New claim
              </button>
            </div>
          </div>
        )}

        {!loading &&
          claims.length > 0 &&
          claims.map((claim, i) => (
            <div class="accounts__table-row" key={claim.id}>
              <div class=" accounts__table-cell">{claim.claim_id}</div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (claim.deceased_type) {
                    case 0:
                      return <span>holder</span>;
                    case 1:
                      return <span>dependent</span>;
                  }
                })()}
              </div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                  {claim.claiment_name} {claim.claiment_surname}
                </p>
              </div>

              <div class=" accounts__table-cell">{claim.sum_assured}</div>
              <div class=" accounts__table-cell">
                {moment(claim.created_at).format("LLL")}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (claim.status) {
                    case "0":
                      return <span className="badge badge__blue">pending</span>;
                    case "1":
                      return (
                        <span className="badge badge__green">approved</span>
                      );
                    case "2":
                      return (
                        <span className="badge badge__orange">rejected</span>
                      );
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
      {account.package_fun && account.account_main &&(
        <ModalCreateClaim
          onClose={() => {
            setShowNewClaim(false);
          }}
          show={showNewClaim}
          accountFun={account.id}
          mainAccount={account.account_main.id}
          dependents={dependents}
          packageFun={account.package_fun.id}
        />
      )}
    </div>
  );

  const accountPayments = () => (
    <div className="accounts__container">
      <div className="accounts__header">
        <h3 className="accounts__title">
          Payments <span>({payments.length})</span>
        </h3>
        <div className="accounts__options">
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewPayment(true)}
            >
              New payments
            </button>
          </div>
        </div>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">name</div>
          <div class="accounts__table-cell">amount</div>
          <div class="accounts__table-cell">method</div>
          <div class="accounts__table-cell">balance</div>
          <div class="accounts__table-cell">date</div>
          <div class="accounts__table-cell">Currency</div>
          <div class="accounts__table-cell">Status</div>
        </div>
        {!loading && payments.length === 0 && (
          <div className="accounts__no-accounts">
            <p>No payments</p>
            <span>There are no payments to this account to display</span>
            <div className=""> </div>
          </div>
        )}
        {!loading &&
          payments.length > 0 &&
          payments.map((payment, i) => (
            <div class="accounts__table-row" key={payment.id}>
              <div class=" accounts__table-cell">{payment.payment_id}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                  {payment.deceased_name} {payment.deceased_surname}
                </p>
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (payment.deceased_type) {
                    case 0:
                      return <span>holder</span>;
                    case 1:
                      return <span>dependent</span>;
                  }
                })()}
              </div>
              <div class=" accounts__table-cell">
                {payment.deceased_relationship}
              </div>
              <div class=" accounts__table-cell">{payment.paymentent_name}</div>
              <div class=" accounts__table-cell">
                {payment.package_fun.currency}
              </div>
              <div class=" accounts__table-cell">
                {moment(payment.created_at).format("LLL")}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (payment.status) {
                    case "0":
                      return <span className="badge badge__blue">pending</span>;
                    case "1":
                      return (
                        <span className="badge badge__green">approved</span>
                      );
                    case "2":
                      return (
                        <span className="badge badge__orange">rejected</span>
                      );
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
      {account && account.account_main && account.package_fun && (
        <ModalCreatePayment
          onClose={() => {
            setShowNewPayment(false);
          }}
          show={showNewPayment}
          start={1}
          premium={account.premium}
          mainAccount={account.account_main.id}
          accountFun={account.id}
          accountPackage={account.package_fun.id}
        />
      )}
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            Accounts
            <Chevron />
          </Link>
          <Link to="/accounts" className="content__link">
            9485RT
            <Chevron />
          </Link>
          <span className="content__current">2342354</span>
        </div>
        <div className="accounts__">
          <h2 className="accounts__heading">funeral</h2>
        </div>

        <div className="account__container">
          <div className="account__header">
            <h3 className="account__title">Policy details</h3>
            <div className="account__options">
              <div className="">
                <button className="btn btn--blue-grey">Archive</button>
              </div>
              <div className="">
                <button className="btn btn--blue-grey">Lapse</button>
              </div>
              <div className="">
                <button className="btn btn--blue-grey">Email</button>
              </div>
              <div className="">
                <button
                  className="btn btn--blue-grey"
                  onClick={() => setShowEditAccount(true)}
                >
                  Edit
                </button>
              </div>
              <div className="">
                <button className="btn btn--blue-grey">Delete</button>
              </div>
            </div>
          </div>
          <div className="account__policy-info">
            {loading && <Loader message={"Fetching policy details"} />}
            {!loading && account && (
              <>
                <div className="account__status">
                  <div className="account__item">
                    <p className="">Status</p>
                    {(() => {
                      switch (account.status) {
                        case "0":
                          return <span className="badge badge__green">active</span>;
                        case "1":
                          return <span className="badge badge__orange">lapsed</span>;
                        case "2":
                          return <span className="badge badge__red">achirved</span>;
                      }
                    })()}
                  </div>
                </div>

                <div class=" account__policy">
                  <div className="account__item">
                    <p className="">Package</p>
                    {account.package_fun && (
                      <span>{account.package_fun.name}</span>
                    )}
                  </div>
                  <div className="account__item">
                    <p className="">Balance</p>
                    <span>{account.balance}</span>
                  </div>
                  <div className="account__item">
                    <p className="">Premium</p>
                    <span>{account.premium}</span>
                  </div>
                  <div className="account__item">
                    <p className="">Created At</p>
                    <span> {moment(account.created_at).format("LL")}</span>
                  </div>
                  <div className="account__item">
                    <p className="">Created By</p>
                    {account && account.created_by && (
                      <span>
                        {account.created_by.name} {account.created_by.surname}
                      </span>
                    )}
                  </div>
                </div>
              </>
            )}
          </div>
          <ModalEditAccountFun
            onClose={() => {
              setShowEditAccount(false);
            }}
            show={showEditAccount}
          />
        </div>

        <div className="account__nav">
          <ul className="account__nav-list">
            <p
              className={
                tab === 1 ? "account__nav-item--active" : "account__nav-item"
              }
              onClick={() => setTab(1)}
            >
              Dependents
            </p>
            <p
              className={
                tab === 2 ? "account__nav-item--active" : "account__nav-item"
              }
              onClick={() => setTab(2)}
            >
              Claims
            </p>
            <p
              className={
                tab === 3 ? "account__nav-item--active" : "account__nav-item"
              }
              onClick={() => setTab(3)}
            >
              Payments
            </p>
          </ul>
        </div>
        <div className="">
          {tab === 1 && AccountDependents()}
          {tab === 2 && accountClaims()}
          {tab === 3 && accountPayments()}
        </div>
      </div>
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default AccountFun;

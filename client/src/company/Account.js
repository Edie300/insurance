import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useParams,
  useHistory,
} from "react-router-dom";
import moment from "moment";
import PrivateRoute from "../auth/PrivateRoute";
import { isAuthenticated } from "../auth";
import { getMainAccount, getSubAccounts, approveMainAccount } from "../api/account-main";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateAccount from "../components/ModalCreateAccount";
import ModalEditAccount from "../components/ModalEditAccount";
import ModalEmail from "../components/ModalEmail";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as PaperClip } from ".././assets/svg/paperclip.svg";

import IMG from ".././assets/images/img.jpg";

const Account = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [tab, setTab] = useState(2);
  const [showNewAccount, setShowNewAccount] = useState(false);
  const [showEditAccount, setShowEditAccount] = useState(false);
  const [showNewEmail, setShowNewEmail] = useState(false);
  const [account, setAccount] = useState([]);
  const [accountId, setAccountId] = useState({ account_main: id });
  const [policies, setPolicies] = useState([]);
  const [loading, setLoading] = useState(true);

  const getAccount = () => {
    setLoading(true);
    getMainAccount(token, id)
      .then((res) => {
        console.log("response", res.data);
        setAccount(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const fetchSubAccounts = () => {
    setLoading(true);

    getSubAccounts(token, accountId)
      .then((res) => {
        console.log("response", res.data);
        setPolicies(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    getAccount();
  }, []);

  useEffect(() => {
    fetchSubAccounts();
  }, []);

  const approveAccount = () => {
    setLoading(true);
    approveMainAccount(token, id)
    .then((res) => {
      console.log("response", res.data);
      setLoading(false);
    })
    .catch((error) => {
      console.log("::Error::", error);
      setLoading(false);
    });
  };

  const viewAccount = (fun_id) => {
    history.push(`funeral/${fun_id}`);
  };

  const accountPolicies = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">
          Policies<span>({policies.length})</span>
        </h3>
        <div className="account__options">
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewAccount(true)}
            >
              Add policy
            </button>
          </div>
        </div>
      </div>

      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Package</div>
          <div class="accounts__table-cell">Type</div>
          <div class="accounts__table-cell">Payment Status</div>
          <div class="accounts__table-cell">Premium</div>
          <div class="accounts__table-cell">balance</div>
          <div class="accounts__table-cell">Created</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching policies"} />}

        {!loading && policies.length === 0 && (
          <div className="accounts__no-accounts">
            <p>No policies</p>
            <span>
              There are no policies to this account. to add policy click add
              policy
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey "
                onClick={() => setShowNewAccount(true)}
              >
                add policy
              </button>
            </div>
          </div>
        )}
        {!loading &&
          policies.length > 0 &&
          policies.map((policy, i) => (
            <div
              class="accounts__table-row"
              key={policy.id}
              onClick={() => viewAccount(policy.id)}
            >
              <div class=" accounts__table-cell">
                {(() => {
                  switch (policy.type) {
                    case 0:
                      return (
                        <span className="">{policy.package_fun.name}</span>
                      );
                    case 1:
                      return (
                        <span className="">{policy.package_med.name}</span>
                      );
                    case 2:
                      return (
                        <span className="">{policy.package_motor.name}</span>
                      );
                  }
                })()}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (policy.type) {
                    case 0:
                      return <span className="">Funeral</span>;
                    case 1:
                      return <span className="">medical</span>;
                    case 2:
                      return <span className="">motor</span>;
                  }
                })()}
              </div>
              <div class=" accounts__table-cell">{policy.payment_status}</div>
              <div class=" accounts__table-cell">{policy.premium}</div>
              <div class=" accounts__table-cell">{policy.balance}</div>
              <div class=" accounts__table-cell">
                {moment(policy.created_at).format("LL")}
              </div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (policy.status) {
                    case "0":
                      return <span className="badge badge__green">active</span>;
                    case "1":
                      return <span className="badge badge__green">active</span>;
                    case "2":
                      return <span className="badge badge__orange">lapased</span>;
                    case "3":
                      return <span className="badge badge__red">archived</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const clientDetails = () => (
    <div className="account__container">
      <div className="account__header">
        <h3 className="account__title">Account details</h3>
        <div className="account__options">
          {account && account.status === 0 && (
            <div className="">
              <button className="btn btn--blue-grey" onClick={approveAccount}>Activate</button>
            </div>
          )}
          {account && account.status === 1 && (
            <div className="">
              <button className="btn btn--blue-grey">Suspend</button>
            </div>
          )}
          <div className="">
            <button className="btn btn--blue-grey">Email</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue-grey"
              onClick={() => setShowEditAccount(true)}
            >
              Edit
            </button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Delete</button>
          </div>
        </div>
      </div>
      {loading && <Loader />}
      {!loading && account && (
        <>
          <div class=" account__client">
            <div className="account__item">
              <p className="">Name</p>
              <span>{account.name}</span>
            </div>
            <div className="account__item">
              <p className="">Surname</p>
              <span>{account.surname}</span>
            </div>
            <div className="account__item">
              <p className="">Gender</p>
              <span>{account.gender}</span>
            </div>
            <div className="account__item">
              <p className="">Date of Birth</p>
              <span>{moment(account.date_of_birth).format("LL")}</span>
            </div>
            <div className="account__item">
              <p className="">Title</p>
              <span>{account.title}</span>
            </div>
            <div className="account__item">
              <p className="">Marital status</p>
              <span>{account.marital_status}</span>
            </div>

            <div className="account__item">
              <p className="">National ID</p>
              <span>{account.national_id}</span>
            </div>
            <div className="account__item">
              <p className="">Passport Number</p>
              <span>{account.passport}</span>
            </div>

            <div className="account__item">
              <p className="">Drivers licence</p>
              <span>{account.drivers_license}</span>
            </div>
            <div className="account__item">
              <p className="">phone</p>
              <span>{account.phone}</span>
            </div>
            <div className="account__item">
              <p className="">email</p>
              <span>{account.email}</span>
            </div>
            <div className="account__item">
              <p className="">Address</p>
              <span>{account.address}</span>
            </div>
            <div className="account__item">
              <p className="">Status</p>
              {(() => {
                switch (account.status) {
                  case 0:
                    return <span className="badge badge__blue">pending</span>;
                  case 1:
                    return <span className="badge badge__green">active</span>;
                  case 2:
                    return (
                      <span className="badge badge__orange">suspended</span>
                    );
                  case 3:
                    return <span className="badge badge__red">deleted</span>;
                }
              })()}
            </div>
          </div>
          <div className="account__section">
            <p className="">Attachments</p>
          </div>
          <div className="account__docs">
            <div className="account__doc">
              <img src={IMG} alt="image" />
            </div>
            <div className="account__doc">
              <img src={IMG} alt="image" />
            </div>
            <div className="account__doc">
              <img src={IMG} alt="image" />
            </div>
          </div>
        </>
      )}
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/accounts" className="content__link">
            Accounts
            <Chevron />
          </Link>
          <span className="content__current">{account.account_id}</span>
        </div>
        <div className="accounts__">
          <h2 className="accounts__heading">{account.account_id}</h2>
        </div>
        <div className="content__pages">
          {clientDetails()}
          {accountPolicies()}
        </div>
      </div>
      <ModalCreateAccount
        onClose={() => {
          setShowNewAccount(false);
        }}
        show={showNewAccount}
        start={1}
        mainAccount={id}
      />
      <ModalEditAccount
        onClose={() => {
          setShowEditAccount(false);
        }}
        show={showEditAccount}
      />
      <ModalEmail
        onClose={() => {
          setShowNewEmail(false);
        }}
        show={showNewEmail}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Account;

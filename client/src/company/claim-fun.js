import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  Redirect,
  useHistory,
  useParams,
} from "react-router-dom";
import moment from "moment";
import { isAuthenticated } from "../auth";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";

import ModalEditFunPackage from "../components/ModalEditFunPackage";

import { publishPackageFun } from "../api/package-funeral";
import { unpublishPackageFun } from "../api/package-funeral";

import { getFunDependent } from "../api/account-funeral";
import { getClaim } from "../api/claims-funeral";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Check } from ".././assets/svg/check.svg";
import { ReactComponent as X } from ".././assets/svg/x.svg";

const ClaimFun = () => {
  const { path } = useRouteMatch();
  let history = useHistory();
  let { id } = useParams();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [claim, setClaim] = useState({});
  const [loading, setLoading] = useState(false);
  const [reload, setReload] = useState(false);
  const [dependent, setDependent] = useState({});
  const [depId, setDepId] = useState({});
  const [showEditPackage, setShowEditPackage] = useState(false);
  const [showPublishPackage, setShowPublishPackage] = useState(false);
  const [showDeletePackage, setShowDeletePackage] = useState(false);

  const init = () => {
    setLoading(true);
    getClaim(token, id)
      .then((res) => {
        console.log("response", res.data);
        setClaim(res.data);
        // setDepId(R)
        getdependent(res.data.dependent);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const getdependent = (depId) => {
    getFunDependent(token, depId)
      .then((res) => {
        console.log("response", res.data);
        setDependent(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    init();
  }, [setReload]);

  const publish = () => {
    publishPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
        setReload(!reload);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const unpublish = () => {
    unpublishPackageFun(token, id)
      .then((res) => {
        console.log("response", res.data);
        setReload(!reload);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <Link to="/claims" className="content__link">
            Claims
            <Chevron />
          </Link>

          <span className="content__current">Funeral claim </span>
        </div>
        <div className="accounts__">
          {claim && <h2 className="accounts__heading">{claim.name}</h2>}
        </div>

        <div className="content__pages">
          <div className="accounts__container">
            <div className="account__header">
              <h3 className="account__title">Claim details</h3>
              <div className="account__options">
                {claim && claim.status === "0" && (
                  <div className="">
                    <button className="btn btn--blue-grey">Approve</button>
                  </div>
                )}
                {claim && claim.status === "1" && (
                  <div className="">
                    <button className="btn btn--blue-grey" type="button">
                      Reject
                    </button>
                  </div>
                )}
                <div className="">
                  <button
                    className="btn btn--blue-grey"
                    type="button"
                    onClick={() => setShowEditPackage(true)}
                  >
                    Edit
                  </button>
                </div>
                <div className="">
                  <button className="btn btn--blue-grey">Delete</button>
                </div>
              </div>
            </div>
            {loading && <Loader message={"Fetching package details"} />}
            {!loading && claim && (
              <div className="package__container">
                <p className="package__section">Claim details</p>
                <div class="package__details">
                  <div className="package__item">
                    <p className="">claim type</p>

                    {(() => {
                      switch (claim.claim_type) {
                        case 0:
                          return <span>principal</span>;
                        case 1:
                          return <span>dependent</span>;
                      }
                    })()}
                  </div>
                  <div className="package__item">
                    <p className="">Date of death</p>
                    <span> {moment(claim.date_of_death).format("LLL")}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Cause of death</p>
                    <span>{claim.cause_of_death}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Date of funeral</p>
                    <span>{claim.date_of_funeral}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Place of burial</p>
                    <span>{claim.place_of_burial}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment name</p>
                    <span>{claim.claiment_name}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment surname</p>
                    <span>{claim.claiment_surname}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment title</p>
                    <span>{claim.claiment_title}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment date of birth</p>
                    <span> {moment(claim.claiment_date_of_birth).format("LLL")}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment email</p>
                    <span>{claim.claiment_email}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment address</p>
                    <span>{claim.claiment_address}</span>
                  </div>
                  <div className="package__item">
                    <p className="">claiment phone</p>
                    <span>{claim.claiment_cell}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment national ID</p>
                    <span>{claim.claiment_national_id}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment passport</p>
                    <span>{claim.claiment_passport}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Claiment relationship</p>
                    <span>{claim.claiment_relationship}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Bank account holder</p>
                    <span>{claim.bank_account_holder}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Bank account number</p>
                    <span>{claim.bank_account_number}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Bank name</p>
                    <span>{claim.bank_name}</span>
                  </div>
                  <div className="package__item">
                    <p className="">Bank branch code</p>
                    <span>{claim.bank_branch_code}</span>
                  </div>

                  <div className="package__item">
                    <p className="">created by</p>
                    {claim && claim.created_by && (
                      <span>
                        {claim.created_by.name} {claim.created_by.surname}
                      </span>
                    )}
                  </div>
                  <div className="package__item">
                    <p className="">Status</p>

                    {(() => {
                      switch (claim.status) {
                        case "0":
                          return (
                            <span className="badge badge__blue">pending</span>
                          );
                        case "1":
                          return (
                            <span className="badge badge__green">approved</span>
                          );
                        case "2":
                          return (
                            <span className="badge badge__orange">
                              rejected
                            </span>
                          );
                      }
                    })()}
                  </div>
                </div>
                {claim && claim.account_main && (
                  <div>
                    <p className="package__section">Account details</p>
                    <div class="package__details">
                      <div className="package__item">
                        <p className="">Account number</p>
                        <span>{claim.account_main.account_id}</span>
                      </div>
                      <div className="package__item">
                        <p className="">Name</p>
                        <span>{claim.account_main.name}</span>
                      </div>
                      <div className="package__item">
                        <p className="">Surname</p>
                        <span>{claim.account_main.surname}</span>
                      </div>
                      <div className="package__item">
                        <p className="">gender</p>
                        <span>{claim.account_main.gender}</span>
                      </div>
                      <div className="package__item">
                        <p className="">Phone</p>
                        <span>{claim.account_main.phone}</span>
                      </div>
                      <div className="package__item">
                        <p className="">Email</p>
                        <span>{claim.account_main.email}</span>
                      </div>
                    </div>
                  </div>
                )}

                {claim && dependent && (
                  <div>
                    <p className="package__section">Deceased details</p>
                    <div class="package__details">
                      <div className="package__item">
                        <p className="">Deceased Name </p>
                        <span>{dependent.name}</span>
                      </div>
                   

                    <div className="package__item">
                      <p className="">Deceased surname</p>
                      <span>{dependent.surname}</span>
                    </div>

                    <div className="package__item">
                      <p className="">Deceased date of birth</p>
                      <span>{moment(dependent.date_of_birth).format("LLL")}</span>
                    </div>

                    <div className="package__item">
                      <p className="">Deceased Address </p>
                      <span>{dependent.address}</span>
                    </div>

                    <div className="package__item">
                      <p className="">Deceased relationship</p>
                      <span>{dependent.relationship}</span>
                    </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default ClaimFun;

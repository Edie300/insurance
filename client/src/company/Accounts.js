import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useHistory,
} from "react-router-dom";

import moment from "moment";
import { isAuthenticated } from "../auth";
import { getMainAccounts } from "../api/account-main";
import { getCompanyBranches } from "../api/companies";

import Navigation from "./navigation";
import Header from "./header";
import Loader from "../components/Loader";
import ModalCreateAccount from "../components/ModalCreateAccount";
import ModalSearch from "../components/ModalSearch";
import { ReactComponent as Chevron } from ".././assets/svg/chevron-right.svg";
import { ReactComponent as Settings } from ".././assets/svg/settings.svg";

const Clients = () => {
  const { path } = useRouteMatch();
  let history = useHistory();

  const {
    user: { _id, name, email, role, company },
  } = isAuthenticated();

  const token = isAuthenticated().token;

  const [showNewAccount, setShowNewAccount] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [branches, setBranches] = useState([]);
  const [accounts, setAccounts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [query, setQuery] = useState({
    account_id: "",
    branch: "",
    status: [],
    gender: [],
    created_at: "",
    sort: "",
  });

  const init = () => {
    setLoading(true);
    getMainAccounts(token)
      .then((res) => {
        console.log("response", res.data);
        setAccounts(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const companyBranches = () => {
    getCompanyBranches(token, company.id)
      .then((res) => {
        console.log("response", res.data);
        setBranches(res.data);
      })
      .catch((error) => {
        console.log("::Error::", error);
      });
  };

  useEffect(() => {
    init();
  }, []);

  useEffect(() => {
    companyBranches();
  }, []);

  const handleQuaryChange = (name) => (event) => {
    setQuery({ ...query, [name]: event.target.value });
  };

  const handleApply = () => {
    setLoading(true);
    getMainAccounts(token, query)
      .then((res) => {
        console.log("response", res.data);
        setAccounts(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("::Error::", error);
        setLoading(false);
      });
  };

  const handleReset = () => {
    setQuery({
      ...query,
      account_id: "",
      branch: "",
      status: [],
      gender: [],
      created_at: "",
      sort: "",
    });
  };

  const viewAccount = (id) => {
    history.push(`${path}/${id}`);
  };

  const accountTable = () => (
    <div className="accounts__container">
      <div className="accounts__header">
        <h3 className="accounts__title">
          Accounts <span>({accounts.length})</span>
        </h3>
        <div className="accounts__options">
          <div className="">
            <button className="btn btn--blue-grey">Export</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Print</button>
          </div>
          <div className="">
            <button className="btn btn--blue-grey">Send email</button>
          </div>
          <div className="">
            <button
              className="btn btn--blue"
              onClick={() => setShowNewAccount(true)}
            >
              Create account
            </button>
          </div>
        </div>
      </div>
      <div className="accounts__filter">
        <div className="accounts__search-box">
          <input
            className="form__input"
            type="text"
            placeholder="account number,  phone, national ID"
            onChange={handleQuaryChange("account_id")}
          />
          <button className="btn">Search</button>
        </div>
        <div
          className="accounts__settings"
          onClick={() => setShowFilters(true)}
        >
          <Settings />
        </div>
        <div className="accounts__pagination">
          <div className="accounts__previous">
            <Chevron />
          </div>
          <div className="accounts__page">
            <p>2</p>
          </div>
          <div className="accounts__next">
            <Chevron />
          </div>
        </div>
      </div>
      <div class=" accounts__table">
        <div class=" accounts__table-header">
          <div class="accounts__table-cell">Account ID</div>
          <div class="accounts__table-cell">Name</div>
          <div class="accounts__table-cell">ID Number</div>
          <div class="accounts__table-cell">gender</div>
          <div class="accounts__table-cell">Phone</div>
          <div class="accounts__table-cell">Status</div>
        </div>

        {loading && <Loader message={"Fetching accounts"} />}

        {!loading && accounts.length < 1 && (
          <div className="accounts__no-accounts">
            <p>No accounts</p>
            <span>
              There are no accounts to this organisation. to start creating
              accounts click create account
            </span>
            <div className="">
              <button
                className="btn btn--blue-grey"
                onClick={() => setShowNewAccount(true)}
              >
                Create account
              </button>
            </div>
          </div>
        )}

        {!loading &&
          accounts.length > 0 &&
          accounts.map((account, i) => (
            <div
              class="accounts__table-row"
              key={account.id}
              onClick={() => viewAccount(account.id)}
            >
              <div class=" accounts__table-cell">{account.account_id}</div>
              <div class=" accounts__table-cell">
                <p className="accounts__table-name">
                  {account.name} {account.surname}
                </p>
              </div>
              <div class=" accounts__table-cell">{account.national_id}</div>
              <div class=" accounts__table-cell">{account.gender}</div>
              <div class=" accounts__table-cell">{account.phone}</div>
              <div class=" accounts__table-cell">
                {(() => {
                  switch (account.status) {
                    case 0:
                      return <span className="badge badge__blue">pending</span>;
                    case 1:
                      return <span className="badge badge__green">active</span>;
                    case 2:
                      return <span className="badge badge__orange">suspended</span>;
                    case 3:
                      return <span className="badge badge__red">deleted</span>;
                  }
                })()}
              </div>
            </div>
          ))}
      </div>
    </div>
  );

  const content = () => (
    <>
      <div className="content">
        <div className="content__breadcrum">
          <span className="content__current">Accounts</span>
        </div>
        <div className="accounts__">
          <h2 className="accounts__heading">Accounts</h2>
        </div>

        <div className="content__pages">{accountTable()}</div>
      </div>
      <ModalCreateAccount
        onClose={() => {
          setShowNewAccount(false);
        }}
        show={showNewAccount}
      />
      <ModalSearch
        onClose={() => {
          setShowFilters(false);
        }}
        show={showFilters}
        query={query}
        branches={branches}
        setQuery={setQuery}
        handleApply={handleApply}
        handleReset={handleReset}
      />
    </>
  );

  return (
    <>
      <Navigation />
      <Header />
      {content()}
    </>
  );
};
export default Clients;

import React from "react";
const Submit = ({ navigation }) => {
  const { go } = navigation;
  return (
    <div>
      <h3>Application has been submitted</h3>
      
      <div className="registration__button">
        <button onClick={() => go("client")} class="btn btn--blue">
        New Application
        </button>
      </div>
    </div>
  );
};

export default Submit;

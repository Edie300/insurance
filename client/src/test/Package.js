import React from "react";

import IMG00 from "../assets/images/casket_00.jpg";
import IMG01 from "../assets/images/casket_01.jpg";
import IMG02 from "../assets/images/casket_02.jpg";

import ItemForm from "./ItemForm";

const Address = ({ setForm, formData, navigation }) => {
  const { address, city, state, zip } = formData;

  const { previous, next } = navigation;

  return (
    <div className="registration__container">
      <h1 className="registration__title">Choose package</h1>
      <form action="#" className="form">
        <table class="table no-wrap">
          <thead className="table__header-text">
            <tr className="">
              <th>Product Id</th>
              <th>Name</th>
              <th>Casket</th>
              <th>Policy Maturity</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p>00000</p>
              </td>
              <td>premium</td>
              <td>
                <div className="table-image">
                  <img src={IMG00} />
                </div>
              </td>
              <td>20 yrs</td>
              <td>
                <p>option</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>00000</p>
              </td>
              <td>premium</td>
              <td>
                <div className="table-image">
                  <img src={IMG01} />
                </div>
              </td>
              <td>20 yrs</td>
              <td>
                <p>option</p>
              </td>
            </tr>
          </tbody>
        </table>

        <div className="registration__button-box">
          <div className="registration__button">
            <button onClick={previous} class="btn btn--blue">
              Prev
            </button>
          </div>
          <div className="registration__button">
            <button onClick={next} class="btn btn--blue">
              Next
            </button>
          </div>
        </div>
      </form>

      {/* <h3>Address</h3>
      <ItemForm
        label="Address"
        name="address"
        value={address}
        onChange={setForm}
      />
      <ItemForm label="City" name="city" value={city} onChange={setForm} />
      <ItemForm label="Zip" name="zip" value={zip} onChange={setForm} /> */}
      {/* <div>
        <button onClick={previous}>Previous</button>
        <button onClick={next}>Next</button>
      </div> */}
    </div>
  );
};

export default Address;

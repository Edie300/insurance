import React, { useState, useEffect } from "react";
import { isAuthenticated } from "../auth";
import { createFuneralAccount } from "../api/account-funeral";
import IconEdit from "../components/iconEdit";

const Review = ({ setForm, formData, navigation }) => {
  const {
    photo,
    name,
    surname,
    national_id,
    dob,
    gender,
    marital_status,
    cell,
    email,
    address,
  } = formData;
  const { go } = navigation;

  // const { user: { _id, role, company },} = isAuthenticated();

  const token = isAuthenticated().token;

  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   let form = new FormData();

  //   form.append('photo', photo);
  //   form.append('name', name);
  //   form.append('surname', surname);
  //   form.append('national_id', national_id);

  //   createFuneralAccount(token, form)
  //     .then((res) => {
  //       console.log("Response::", res.data);
  //       // go("submit")
  //     })
  //     .catch((error) => {
  //       console.log("Error", error);
  //     });
  // };

  return (
    <div className="registration__container">
      <h1 className="registration__title">Review details</h1>

      <div className="registration__review">
        <div className="registration__review-item">
          <div className="registration__review-header">
            <h3 className="registration__review-title"> Client details</h3>
            <IconEdit />
            {/* <button
              className="btn btn--blue-border registration__review-edit"
              onClick={() => go("client")}
            >
              Edit
            </button> */}
          </div>

          <div className="registration__review-details">
            <p className="">Name</p>
            <p className=""> {`${name}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Surname</p>
            <p className=""> {`${surname}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">National ID</p>
            <p className=""> {`${national_id}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Date of Birth</p>
            <p className=""> {`${dob}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Gender</p>
            <p className=""> {`${gender}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Cell</p>
            <p className=""> {`${cell}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Email</p>
            <p className=""> {`${email}`}</p>
          </div>
          <div className="registration__review-details">
            <p className="">Address</p>
            <p className=""> {`${address}`}</p>
          </div>
        </div>

        <div className="registration__review-item">
          <div className="registration__review-header">
            <h3 className="registration__review-title"> Package</h3>
            <IconEdit />
            {/* <button
              className="btn btn--blue-border registration__review-edit"
              onClick={() => go("package")}
            >
              Edit
            </button> */}
          </div>
          <div className="registration__review-details">
            <p className="">Package</p>
            <p className="">Basic plan</p>
          </div>
        </div>

        <div className="registration__review-item">
          <div className="registration__review-header">
            <h3 className="registration__review-title"> Dependents</h3>
            <IconEdit />
            {/* <button
              className="btn btn--blue-border registration__review-edit"
              onClick={() => go("dependents")}
            >
              Edit
            </button> */}
          </div>

          <div className="registration__review-details">
            <p className="">Dependents here</p>
          </div>
        </div>
      </div>

      <div className="registration__button-box">
        <div className="registration__button">
          <button onClick={() => go("submit")} class="btn btn--blue">
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default Review;

import React, { useState, useEffect } from "react";

const ItemForm = ({ label, children, type = "text", ...otherProps }) => {
  useEffect(() => {
    console.log("PROPS",otherProps)
  }, []);
  return (
    <div>
      {type === "text" ? (
        <>
          <label>{label}</label>
          <input type={type} {...otherProps} />
        </>
      ) : (
        <>
          <label />
          <input type={type} {...otherProps} />
          {label}
        </>
      )}
    </div>
  );
};

export default ItemForm;

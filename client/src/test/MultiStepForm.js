import React from "react";
import { useForm, useStep } from "react-hooks-helper";

import Client from "./Client";
import Package from "./Package";
import Dependents from "./Dependents";
import Review from "./Review";
import Submit from "./Submit";



const steps = [
  { id: "client" },
  { id: "package" },
  { id: "dependents" },
  { id: "review" },
  { id: "submit" }
];

// const defaultData = {
//   firstName: "Jane",
//   lastName: "Doe",
//   nickName: "Jan",
//   address: "200 South Main St",
//   city: "Anytown",
//   state: "CA",
//   zip: "90505",
//   email: "email@domain.com",
//   phone: "+61 4252 454 332"
// };

const defaultData = {
  name: "edwin",
  surname: "smith",
  national_id: "12345t08",
  dob: "2000-12-12",
  gender: "male",
  marital_status: "single",
  cell: "0777027620",
  email: "muvandie@gmail.com",
  address: "8506 nkulumane bulawayo zimbabwe",
  dependents: [],
  package: ""
};

const MultiStepForm = () => {
  const [formData, setForm] = useForm(defaultData);
  const { step, navigation } = useStep({ initialStep: 0, steps });
  const { id } = step;

  const props = { formData, setForm, navigation };

  switch (id) {
    case "client":
      return <Client {...props} />;
    case "package":
      return <Package {...props} />;
    case "dependents":
      return <Dependents {...props} />;
    case "review":
      return <Review {...props} />;
    case "submit":
      return <Submit {...props} />;
    default:
      return null;
  }
};

export default MultiStepForm;

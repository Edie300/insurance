import React from "react";

import ItemForm from "./ItemForm";

const Contact = ({ setForm, formData, navigation }) => {
  const { phone, email } = formData;

  const { previous, next } = navigation;

  return (
    <div className="registration__container">
      <h1 className="registration__title">Add Dependents</h1>

      <form action="#" className="form">
        <div className="registration__block">
          <div className="form__group registration__item">
            <label className="form__label">name</label>
            <input
              className="form__input"
              type="text"
              placeholder="name"
              required
            />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">surname</label>
            <input
              className="form__input"
              type="text"
              placeholder="surname"
              required
            />
          </div>
        </div>
        <div className="registration__button-box">
        <div className="registration__button">
            <button onClick={previous} class="btn btn--blue">
              Prev
            </button>
          </div>
          <div className="registration__button">
            <button onClick={next} class="btn btn--blue">
              Next
            </button>
          </div>
        </div>
      </form>

      {/* <h3>Contact </h3>
      <ItemForm label="Phone" name="phone" value={phone} onChange={setForm} />
      <ItemForm label="E-mail" name="email" value={email} onChange={setForm} />
      <div>
        <button onClick={previous}>Previous</button>
        <button onClick={next}>Next</button>
      </div> */}
    </div>
  );
};

export default Contact;

import React, { useState, useEffect } from "react";



const Names = ({ setForm, formData, navigation }) => {
  const {
    photo,
    name,
    surname,
    national_id,
    dob,
    gender,
    marital_status,
    cell,
    email,
    address,
  } = formData;

  const { next } = navigation;
  useEffect(() => {
    console.log("Formdata::",formData)
  }, [photo]);

  return (
    <div className="registration__container">
      <h1 className="registration__title">Add Client</h1>
      <form action="#" className="form">
        <div className="registration__block">
          <div className="registration__item ">
            <div className="registration__photo-box">
              <p className="">Add ID client</p>
            </div>
          </div>

          {/* <div className="form__group registration__item ">
            <label class="file" style={{ marginLeft: "4rem" }}>
              <input
                type="file"
                name="photo"
                accept="image/*"
                value={photo}
                onChange={setForm}
                required
              />
              <span class="file-custom"></span>
            </label>
          </div> */}
          <div className="form__group registration__item">
            <label className="form__label">name</label>
            <input
              className="form__input"
              type="text"
              name="name"
              value={name}
              onChange={setForm}
              required
            />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">surname</label>
            <input
              className="form__input"
              type="text"
              name="surname"
              value={surname}
              onChange={setForm}
              required
            />
          </div>
        </div>
        <div className="registration__block">
          <div className="form__group registration__item">
            <label className="form__label">national id</label>
            <input
              className="form__input"
              type="text"
              name="national_id"
              value={national_id}
              onChange={setForm}
              required
            />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">date of birth</label>
            <input
              className="form__input"
              type="text"
              name="dob"
              value={dob}
              onChange={setForm}
              required
            />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">gender</label>
            <input
              className="form__input"
              type="text"
              name="gender"
              value={gender}
              onChange={setForm}
              required
            />
          </div>
        </div>
        <div className="registration__block">
          <div className="form__group registration__item">
            <label className="form__label">marital status</label>
            <input
              className="form__input"
              type="text"
              name="marital_status"
              value={marital_status}
              onChange={setForm}
              required
            />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">Mr/Mrs/Dr/Prof</label>
            <input className="form__input" type="text" required />
          </div>
          <div className="form__group registration__item">
            <label className="form__label">phone</label>
            <input
              className="form__input"
              type="text"
              name="cell"
              value={cell}
              onChange={setForm}
              required
            />
          </div>
        </div>
        <div className="registration__block">
          <div className="form__group registration__item">
            <label className="form__label">Email</label>
            <input
              className="form__input"
              type="email"
              name="email"
              value={email}
              onChange={setForm}
              required
            />
          </div>
          <div className="form__group registration__item--2">
            <label className="form__label">address</label>
            <input
              className="form__input"
              type="text"
              name="address"
              value={address}
              onChange={setForm}
              required
            />
          </div>
        </div>

        <div className="registration__button-box">
          <div className="registration__button">
            <button onClick={next} class="btn btn--blue">
              Next
            </button>
          </div>
        </div>
      </form>

      {/*  <ItemForm
        label="First Name"
        name="name"
        value={name}
        onChange={setForm}
      />
      <ItemForm
        label="Last Name"
        name="lastName"
        value={lastName}
        onChange={setForm}
      />
      <ItemForm
        label="Nick Name"
        name="nickName"
        value={nickName}
        onChange={setForm}
      /> */}
    </div>
  );
};

export default Names;

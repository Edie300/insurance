import React from "react";
import ReactDOM from "react-dom";



//***************************[ STYLES]************************* */
import "./sass/main.scss";


// linear icons
import "./assets/css/icon-font.css"

//***************************[ ROUTES]************************* */
import Routes from "./Routes";

ReactDOM.render(<Routes />, document.getElementById("root"));

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CompanyRoute from "./auth/PrivateRoute";

import LandingPage from "./core/landingPage";
import SignIn from "./core/SignIn";
import Recover from "./core/Recover";
import Loader from "./components/Loader";

// *******************[ ROUTE GUARDS ]**********************
import PrivateRoute from "./auth/PrivateRoute";
import AdminRoute from "./auth/AdminRoute";

// *******************[ COMPANY ]**********************
import Home from "./company/Home";
import Payments from "./company/Payments";
import Accounts from "./company/Accounts";
import AccountsNew from "./company/AccountsNew";
import AccountFun from "./company/Accounts-fun";
import Account from "./company/Account";
import Claims from "./company/Claims";
import ClaimFun from "./company/claim-fun";
import Packages from "./company/Packages";
import PackageFun from "./company/package-fun";
import PackageMed from "./company/package-med";
import Resources from "./company/Resources";
import Casket from "./company/Casket";
import Analytics from "./company/Analytics";
import Users from "./company/Users";
import Settings from "./company/Settings";

// *******************[ ADMIN ]**********************
import AdminHome from "./Admin/Dashboard";
import AdminCompanies from "./Admin/Companies";
import AdminCompany from "./Admin/Company";
import AdminAccounts from "./Admin/Accounts";
import AdminClaims from "./Admin/Claims";
import AdminPackages from "./Admin/Packages";
import AdminAnalytics from "./Admin/Analytics";
import AdminUsers from "./Admin/Users";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/signin" exact component={SignIn} />
        <Route path="/recovery" component={Recover} />
        <Route path="/loader" component={Loader} />

        {/* *****************[ COMPANY ROUTES ]**************************** */}
        <PrivateRoute path="/home" exact component={Home} />
        <PrivateRoute path="/payments" component={Payments} />
        <PrivateRoute path="/accounts" exact component={Accounts} />
        <PrivateRoute
          path="/accounts/new_registrations"
          exact
          component={AccountsNew}
        />
        <PrivateRoute path="/accounts/:id" exact component={Account} />
        <PrivateRoute
          path="/accounts/funeral/:id"
          exact
          component={AccountFun}
        />
        <PrivateRoute path="/claims" exact component={Claims} />
        <PrivateRoute path="/claims/funeral/:id" exact component={ClaimFun} />
        <PrivateRoute path="/packages" exact component={Packages} />
        <PrivateRoute
          path="/packages/funeral/:id"
          exact
          component={PackageFun}
        />
        <PrivateRoute
          path="/packages/medical/:id"
          exact
          component={PackageMed}
        />
        <PrivateRoute path="/resources" exact component={Resources} />
        <PrivateRoute path="/resources/caskets/:id" exact component={Casket} />
        <PrivateRoute path="/analytics" exact component={Analytics} />
        <PrivateRoute path="/users" exact component={Users} />
        <PrivateRoute path="/settings" component={Settings} />

        {/* *****************[ COMPANY ROUTES ]**************************** */}
        <AdminRoute path="/admin/dashboard" exact component={AdminHome} />
        <AdminRoute path="/admin/companies" exact component={AdminCompanies} />
        <AdminRoute
          path="/admin/companies/:id"
          exact
          component={AdminCompany}
        />
        <AdminRoute path="/admin/accounts" exact component={AdminAccounts} />
        <AdminRoute path="/admin/claims" exact component={AdminClaims} />
        <AdminRoute path="/admin/packages" exact component={AdminPackages} />
        <AdminRoute path="/admin/analytics" exact component={AdminAnalytics} />
        <AdminRoute path="/admin/users" exact component={AdminUsers} />
      </Switch>
    </Router>
  );
};
export default Routes;

import express from "express";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";
import cors from "cors";
import { errorHandler, NotFoundError } from "@totum-insurance/common";

// import { currentUserRouter } from "./routes/current-user";
import { indexUserRouter } from "./routes/index";
import { showCompanyUsersRouter } from "./routes/show-company-users";
import { showUserRouter } from "./routes/show-user";
import { createUserRouter } from "./routes/create-user";
import { signinRouter } from "./routes/signin";
import { signoutRouter } from "./routes/signout";
import { signupRouter } from "./routes/signup";

const app = express();

// Add a list of allowed origins.
const allowedOrigins = ["http://localhost:3000"];
const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors());
app.set("trust proxy", true);
app.use(json());
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    signed: false,
    secure: false,
  })
);

// app.use(currentUserRouter);
app.use(indexUserRouter);
app.use(showCompanyUsersRouter);
app.use(showUserRouter);
app.use(createUserRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(signupRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };

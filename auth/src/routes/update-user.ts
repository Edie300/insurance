import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";

import { User } from "../models/user";

const router = express.Router();

router.put(
  "/api/users/update",

  async (req: Request, res: Response) => {
    res.send({message: "user updated"});
  }
);

export { router as updateUserRouter };

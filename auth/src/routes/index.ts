import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError } from "@totum-insurance/common";

import { User } from "../models/user";

const router = express.Router();

router.get(
  "/api/users",

  async (req: Request, res: Response) => {
    const users = await User.find().populate('company').populate('branch');
    res.send(users);
  }
);

export { router as indexUserRouter };

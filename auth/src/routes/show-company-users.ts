import express, { Request, Response } from "express";
import { body } from "express-validator";
import { validateRequest, BadRequestError, requireAuth } from "@totum-insurance/common";

import { User } from "../models/user";

const router = express.Router();

router.get(
  "/api/users/companies/:companyId",
  requireAuth,

  async (req: Request, res: Response) => {
    const users = await User.find({company: req.params.companyId}).populate('company').populate('branch');

    
    res.send(users);
  }
);

export { router as showCompanyUsersRouter };

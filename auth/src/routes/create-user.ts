import express, { Request, Response } from "express";
import { body } from "express-validator";
import jwt from "jsonwebtoken";
import AWS from "aws-sdk";
import {
  requireAuth,
  validateRequest,
  BadRequestError,
  NotFoundError,
  UserStatus,
} from "@totum-insurance/common";

import { User } from "../models/user";
import { Company } from "../models/company";
import { Branch } from "../models/branch";

import { UserCreatedPublisher } from "../events/publishers/user-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

const awsConfig = {
  accessKeyId: "AKIAU262SNIMGNH3M2OD",
  secretAccessKey: "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",
  region: "us-east-1",
  apiVersion: "2010-12-01",
};

const SES = new AWS.SES(awsConfig);

router.post(
  "/api/users/create",
  requireAuth,
  validateRequest,
  async (req: Request, res: Response) => {
    const {
      email,
      name,
      surname,
      phone,
      address,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      employee_id,
      date_of_birth,
      department,
      role,
      branch,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
    } = req.body;


    // find admin user
    const adminUser = await User.findById(req.currentUser!.id);

    if (!adminUser) {
      throw new NotFoundError();
    }
    // Check branch
    const checkBranch = await Branch.findById(branch);

    if (!checkBranch) {
      console.log(`branch not found with id ${branch}`);
      throw new NotFoundError();
    }

    const password = (Math.random() * 100000).toFixed();
    const company = adminUser.company;



    // const existingUser = await User.findOne({ email });

    // if (existingUser) {
    //   throw new BadRequestError("Email in use");
    // }

    // Check if company exists
    const checkCompany = await Company.findById(company);

    if (!checkCompany) {
      console.log(`company not found with id ${company}`);
      throw new NotFoundError();
    }

    // Check if company exists for company
    // const getBranch = await Branch.findOne({_id: branch, company: company});


    console.log("password", password);

    const user = User.build({
      email,
      password,
      name,
      surname,
      phone,
      address,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      employee_id,
      date_of_birth,
      department,
      role,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
      status: UserStatus.Created,
      company,
      branch,
    });
    await user.save();

    new UserCreatedPublisher(natsWrapper.client).publish({
      id: user.id,
      name: user.name,
      surname: user.surname,
      employee_id: user.employee_id,
      gender: user.gender,
      date_of_birth: user.date_of_birth,
      title: user.title,
      national_id: user.national_id,
      passport: user.passport,
      drivers_licence: user.drivers_licence,
      phone: user.phone,
      address: user.address,
      email: user.email,
      department: user.department,
      role: user.role,
      permissions_accounts: user.permissions_accounts,
      permissions_packages: user.permissions_packages,
      permissions_claims: user.permissions_claims,
      permissions_users: user.permissions_users,
      status: user.status,
      company: user.company,
      branch: user.branch,
      version: user.version,
    });

    // Generate JWT
    const userJwt = jwt.sign(
      {
        id: user.id,
        email: user.email,
      },
      "EDWIN" // process.env.JWT_KEY!
    );

    console.log("token", userJwt);

    // Store it on session object
    req.session = {
      jwt: userJwt,
    };

    // prepare for email
    const params = {
      Source: "totum1000@gmail.com",
      Destination: {
        ToAddresses: [email],
      },
      ReplyToAddresses: ["totum1000@gmail.com"],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `
                <html>
                  <h1>account password</h1>
                  <p>Use this password to log in to your account</p>
                  <h2 style="color:red;">${password}</h2>
                  <i>totum.com</i>
                </html>
              `,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: "Account registration",
        },
      },
    };

    const emailSent = SES.sendEmail(params).promise();
    emailSent
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });

    res.status(201).send(user);
  }
);

export { router as createUserRouter };

import express, { Request, Response } from "express";
import { body } from "express-validator";
import {
  validateRequest,
  BadRequestError,
  requireAuth,
} from "@totum-insurance/common";

import { User } from "../models/user";

const router = express.Router();

router.get(
  "/api/users/:id",
  requireAuth,

  async (req: Request, res: Response) => {
    const user = await User.findById(req.params.id)
      .populate("company")
      .populate("branch");
    res.send(user);
  }
);

export { router as showUserRouter };

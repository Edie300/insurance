import express, { Request, Response } from "express";
import { body } from "express-validator";
import jwt from "jsonwebtoken";
import {
  validateRequest,
  BadRequestError,
  NotFoundError,
  UserStatus
} from "@totum-insurance/common";

import { User } from "../models/user";
import { Company } from "../models/company";
import { Branch } from "../models/branch";

import { UserCreatedPublisher } from "../events/publishers/user-created-publisher";
import { natsWrapper } from "../nats-wrapper";

const router = express.Router();

router.post(
  "/api/users/signup",
  [
    body("email").isEmail().withMessage("Email must be valid"),
    body("password")
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage("Password must be between 4 and 20 characters"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const {
      email,
      password,
      name,
      surname,
      phone,
      address,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      employee_id,
      date_of_birth,
      department,
      role,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
      company,
      branch,
    } = req.body;

    console.log("req.body::", req.body)

    const existingUser = await User.findOne({ email });

    if (existingUser) {
      throw new BadRequestError("Email in use");
    }

    // Check if company exists
    const getCompany = await Company.findById(company);

    if (!getCompany) {
      console.log(`company not found with id ${company}`)
      throw new NotFoundError();
    }

    // Check if company exists for company 
    // const getBranch = await Branch.findOne({_id: branch, company: company});

    const getBranch = await Branch.findById(branch);

    if (!getBranch) {
      console.log(`branch not found with id ${branch}`)
      throw new NotFoundError();
    }

    const user = User.build({
      email,
      password,
      name,
      surname,
      phone,
      address,
      gender,
      title,
      national_id,
      passport,
      drivers_licence,
      employee_id,
      date_of_birth,
      department,
      role,
      permissions_accounts,
      permissions_packages,
      permissions_claims,
      permissions_users,
      status: UserStatus.Created,
      company,
      branch,
    });
    await user.save();

    new UserCreatedPublisher(natsWrapper.client).publish({
      id: user.id,
      name: user.name,
      surname: user.surname,
      employee_id: user.employee_id,
      gender: user.gender,
      date_of_birth: user.date_of_birth,
      title:user.title,
      national_id:user.national_id,
      passport:user.passport,
      drivers_licence:user.drivers_licence,
      phone: user.phone,
      address: user.address,
      email: user.email,
      department: user.department,
      role: user.role,
      permissions_accounts: user.permissions_accounts,
      permissions_packages: user.permissions_packages,
      permissions_claims: user.permissions_claims,
      permissions_users: user.permissions_users,
      status: user.status,
      company: user.company,
      branch: user.branch,
      version: user.version,
    });

    // Generate JWT
    const token = jwt.sign(
      {
        id: user.id,
        email: user.email,
      },
      "EDWIN" // process.env.JWT_KEY!
    );

    // console.log("token", userJwt);

    // // Store it on session object
    // req.session = {
    //   jwt: userJwt,
    // };

  res.status(201).send({user, token});
  }
);

export { router as signupRouter };

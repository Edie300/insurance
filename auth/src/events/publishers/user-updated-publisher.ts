import { Subjects, Publisher, UserUpdatedEvent } from '@totum-insurance/common';

export class UserUpdatedPublisher extends Publisher<UserUpdatedEvent> {
  subject: Subjects.UserUpdated = Subjects.UserUpdated;
}

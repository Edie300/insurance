import { Publisher, UserCreatedEvent, Subjects } from '@totum-insurance/common';

export class UserCreatedPublisher extends Publisher<UserCreatedEvent> {
  subject: Subjects.UserCreated = Subjects.UserCreated;
}

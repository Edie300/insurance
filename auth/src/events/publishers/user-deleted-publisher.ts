import { Publisher, UserDeletedEvent, Subjects } from '@totum-insurance/common';

export class UserDeletedPublisher extends Publisher<UserDeletedEvent> {
  subject: Subjects.UserDeleted = Subjects.UserDeleted;
}

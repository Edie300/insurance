import { Message } from "node-nats-streaming";
import {
  Subjects,
  Listener,
  BranchCreatedEvent,
} from "@totum-insurance/common";
import { Branch } from "../../models/branch";
import { queueGroupName } from "./queue-group-name";

export class BranchCreatedListener extends Listener<BranchCreatedEvent> {
  subject: Subjects.BranchCreated = Subjects.BranchCreated;
  queueGroupName: string = queueGroupName;

  async onMessage(data: BranchCreatedEvent["data"], msg: Message) {
    console.log("branch-created-listener::", data);
    const { id, name, phones, landlines, address, status, company } = data;

    const branch = Branch.build({id, name, phones, landlines, address, status, company });
    await branch.save();

    msg.ack();
  }
}
